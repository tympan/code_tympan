.. _packaging-sec:

=========================
Debian bookwork packaging
=========================

This section explains how to prepare and compile a Debian package of `Code_TYMPAN` for a Debian bookwork
distribution.

The files needed to generate a package are located in **Code_TYMPAN/debian** directory.

Before building, don't forget to add the 3rd parties to the archive (which are not versioned).

Install dependencies
====================

::

   sudo apt update
   sudo apt install -y software-properties-common
   sudo apt install -y cmake swig git libboost1.74-dev libboost-regex1.74-dev
   sudo apt install -y libqt5opengl5 libqt5opengl5-dev libqt5webview5
   sudo apt install -y python3-pyqt5 python3-pyqtgraph
   sudo apt install -y python3-numpy python3-scipy python3-matplotlib 
   sudo apt install -y python3-pandas python3-xlsxwriter python3-shapely
   sudo apt install -y python3 python3-dev libcgal-dev cython3    
   sudo apt install -y qml-module-qtwebview qml-module-qtwebsockets qml-module-qtwebchannel 
   sudo apt install -y qml-module-qtquick*

Build package
=============

This command must be run from *Code_TYMPAN* parent directory.

::

    cd debian
    ./build.sh

Install package
===============

::

    sudo apt-get update
    sudo apt install ./code-tympan_<version>_amd64.deb

The last command installs the missing dependencies listed in the package.

launch Code_tympan
==================

::

   Code_TYMPAN


uninstall debian package
========================

::

   sudo apt purge code-tympan


More information
================

For more information, read `Debian documentation`_.

.. _`Debian documentation`: https://www.debian.org/doc/manuals/maint-guide/
