.. _api-part:

============================
C++ documentation by Doxygen
============================

This section provides the Doxygen_ documentation of `Tympan C++ source files <doxygen/index.html>`_.

.. _Doxygen: https://www.doxygen.nl/index.html
