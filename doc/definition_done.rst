.. definition_done:

Definition of done
==========================

The acceptance criteria for an issue are :

   - Deviations from standard 17534-3 and/or excel 9613 calculator are :
   
      * Ideal threshold, automatic validation : +-0.01 dB
	  
      * Authorised threshold defined in the standard : +-0.05 dB
	  
   - If any change was made in the calculation rules, the developper must run the unit tests
   
   - All tests must pass
   
   - A code review by an other developper must be performed
   
   - If a choice between two solution had to be made, the choice must be documented in the "technical choices" section of the documentation
   
   - The Doxygen or Docstring documentation must include all attributes and methods
	  
   - When modifying code, the comments should be translated to english 
   
   - New code should be commented if necessary
