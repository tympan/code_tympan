.. _code-structure:

Structure of the code base
==========================

First level hierarchy
---------------------

* ``3rdparty``, third party code, to be filed with the dependencies archive's content
* ``cmake``, cmake scripts
* ``debian``, debian packaging
* ``doc``, project's developper documentation
* ``Python``, Python files hierarchy
* ``qml``, QML files used with Qt Quick framework
* ``resources``, GUI assets (images, fonts, etc.)
* ``tests``, test files hierarchy
* ``tools``, various developping utilities
* ``Tympan``, C++ files hierarchy
* ``web``, web resources


C++ code hierarchy (i.e. the `Tympan` directory)
------------------------------------------------

::

  Tympan
   \_ core
    _ geometric_methods
     \_ AcousticRaytracer
      _ ConvexHullFinder
    _ gui
     \_ app (former `TympanApp`)
      _ gl (former `DataManagerGraphics`)
      _ tools 
      _ widgets (former `DataManagerIHM`)
    _ models
     \_ business - application model (former `DataManagerMetier`)
      _ common - common representations (former `CommonTools`)
      _ solver - problem and result models for the solver
    _ solvers
     \_ 9613
       \_ 9613Solver
        _ DefaultSolver
    _ CMakeLists.txt - used to define compiling rules for the folder

* ``core`` may not include from other Tympan directories
* ``models`` may only include from ``core``
* ``gui`` may only include from ``core`` and ``model/site``
* ``solvers`` may only include from ``core``, ``model/solver`` and ``model/results``

  * Under solvers are stored common classes for all solver families
  * Under 9613 are stored common classes for 9613 family solver
  * Under 9613Solver are stored specific classes for 9613Solver
  * Under DefaultSolver are stored specific classes for DefaultSolver  

Python code hierarchy (i.e. the `Python` directory)
---------------------------------------------------

File hierarchy
~~~~~~~~~~~~~~

::

  Python
   \_ bin
    _ include
    _ tests
    _ tympan
     \_ __pycache__
      _ altimetry
      _ models


Python namespaces
~~~~~~~~~~~~~~~~~

* ``tympan.altimetry``
* ``tympan.models``

Web code hierarchy
------------------

File hierarchy
~~~~~~~~~~~~~~

::

  web
   \_ css
    _ js
     \_ tests


Test code hierarchy
-------------------

| Like the C++ part, the Python part has unit tests. There are 74 tests (06/2022) running with C++ tests in the classic pipeline.
| In the test folder of the python part we also find:

* data folder : folder used to store resources for tests
* CMakeLists.txt : used to define compiling rules for the folder
* utils.py : paths configuration for test in Python

For more information, see :doc:`testing`


Executables
=========================

Usage
-------------------

| There are a number of methods for using TYMPAN.
| From a typical user point of view:

* starts the GUI (binary)

* used by the GUI to start computation for a site XML file (python)

* used by the GUI to computate altimetry for a site XML file (python)

* used by the python library to start computation for a solver model (python
  thin wrapper around solvers code)


| From a developer's point of view:
| There are a number of scripts that have been developed to simplify use and debugging by a developer (or an advanced user).
| Available here : `Link to TYMPAN scripts repository <https://gitlab.com/tympan/tympan_tool_box>`_

* 1_xxxxx_install.bat : Used to generate the build and install folders of TYMPAN (debug or release depending on the script).
* 2_xxxxx_Tests_unitaires.bat : Used to launch unit tests (debug or release depending on the script).
* 3_xxxxx_Build_Doc.bat : Used to generate the doc.
* 4_xxxxx_Launch_Tympan_GUI.bat : Used to launches TYMPAN with the graphical interface (debug or release depending on the script).
* 5_Debug_Tympan_Mode_Interactive : Used to launch the python calculation with pdb (very useful for debugging).


Embedded Python and Tympan release generation
---------------------------------------------

| The TYMPAN project embeds a version of python as well as all the python packages necessary for the proper functioning of the software. 
| However, in the case of a package or python version upgrade, this folder must be generated correctly.
| Here is the procedure.


Reminder
~~~~~~~~~~~~~~~~~

| If the installation procedure was correctly followed, there should have a folder containing python and a virtual environment corresponding to the version of python installed.
| Reminder of commands:

.. code-block:: winbatch

    # Create virtual environment
    cd dists\python
    C:\Python310\python.exe -m venv venv310tympan

    # Activate virtual environment
    C:\dists\python\venv310tympan\Scripts\activate.bat


In this virtual environment, the packages listed in the requirements file had to be installed using the following commands:

.. code-block:: winbatch

    pip install -U pip
    pip install -U -r requirements.txt
    pip install -U -r requirements-dev.txt


| However these commands modify only the virtual environment and in no case the python installed locally.
| The python file embedded in TYMPAN corresponds to the python installed in which the necessary packages have been installed. We will therefore see the exact procedure.


Generate Python embedded in TYMPAN
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As said previously, to build a complete TYMPAN version (which can be delivered) you must therefore embed python. However, it is not enough to copy the python folder, you must first have installed the necessary packages in the sources folder. Here's the procedure to follow :

* To be sure that the version is clean,it is recommended first reinstalling the version of python that will be used. `(Python release link) <https://www.python.org/downloads/source/>`_


* Install the packages necessary for TYMPAN in the python folder that we have just installed (different from virtual environement).

.. code-block:: winbatch

    # Go to folder
    cd C:\Python310\Scripts
    
    # install packages
    pip.exe install -U -r C:\projects\code_tympan\requirements.txt

You can check that the installation went correctly by going to inspect the C:\\Python310\\Lib\\site-packages folder which must now contain the packages necessary for TYMPAN.

.. image:: _static/built_resources/Python_package_folder_check.png
  :width: 400
  :align: center

* The python folder is now correctly prepared. All that remains is copy python into the TYMPAN folder (and rename the folder with release name).

.. image:: _static/built_resources/copy_python_tympan.png
  :width: 400
  :align: center
