.. _devel-deploying:

===============================
Windows installer
===============================

Overview
========

The Windows installer is generated with `Inno Setup`_. The generation script can
be found in ``tools`` (``tympan_installer.iss``).

.. _`Inno Setup`: http://www.jrsoftware.org/isinfo.php

There are 5 main steps during the installation:

* Copy of Code_TYMPAN install directory

* Copy of TYKAL sources

* Copy of TYBox sources 

* Copy of a custom Python directory

* Compile with inno setup

It is advised to create a specific directory for the files that needs to be packaged (ex : Livraison_bd_Tympan_Tykal_rec), and to create 
a git branch for the version of Code_TYMPAN that will be released (ex : release/X.Y.Z `git flow`_).

.. _`git flow`: https://www.atlassian.com/fr/git/tutorials/comparing-workflows/gitflow-workflow


Copy of Code_TYMPAN install directory
-------------------------------------

* Set version number in CMakeLists.txt

* Compile from scratch in release mode

* Copy the content of the install folder in the directory that will be packaged


Copy of TYKAL sources
---------------------

* Make sure that the file 'resources\version.ini' contains the correct Code_TYMPAN version number.

* Copy the launcher, resources and tykal folder, as well as the README.md, the run.bat, and the setup.py files in the tykal-app folder of the directory that will be packaged.


Copy of TYBox sources
---------------------

* Make sure that the script 'run_tybox.bat' contains the path to the version of Code_TYMPAN that will be installed by the package

* Copy the resources folder, the source folder, the main.py file, the README.md file, and run_tybox.bat script in a specific tybox-app folder located in the directory to be packaged.


Copy of a custom Python directory
------------------------------------

* Make sure that this version of Python contains the packages specified in Code_TYMPAN and TYBox requirements file.

* `Generate the embedded python`_
.. _`Generate the embedded python`: https://code-tympan.readthedocs.io/en/latest/code_structure.html#generate-python-embedded-in-tympan  


Compile with inno setup
-----------------------

* Open the install file ``tympan_installer.iss`` in the Tools folder with `Inno Setup`

* Make sure that the folders paths, version number, excluded files such as .pyc files are all in the install file

* click on "Build". The installer will be created in an ``Output`` directory in the same directory as the script

