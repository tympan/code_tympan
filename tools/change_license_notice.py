"""
Adapt and run this script to change or insert GNU GPL Licence notice in source files
Processed folders :
- Tympan
- python
- qml
- web

Processed extensions
- C++ : .h, .cpp, .hpp
- Python : .py
- Cython : .pyx, .pxd
- HTML : .html
- JS : .js
- QML : .qml

OLD_LICENSE_NOTICE_XXX holds the text of the old license notice
NEW_LICENSE_NOTICE_XXX holds the text of the new license notice
See other constants to adapt scripts they are self-explanatory
"""

import os
import mmap
from pathlib import Path
from collections import namedtuple

OLD_LICENSE_NOTICE_2012 = """/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */"""

OLD_LICENSE_NOTICE_2012_2014 = """/*
 * Copyright (C) <2012-2014> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */"""

OLD_LICENSE_NOTICE_2014 = """/*
 * Copyright (C) <2014> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */"""

AUTO_GENERATED_FILES_NOTICE = """/* WARNING: I am an auto-generated header file, don't modify me !! */"""

NEW_LICENSE_NOTICE_CPP_STYLE = """/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */"""

NEW_LICENSE_NOTICE_PYTHON_STYLE = """#
# Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
# This file is part of Code_TYMPAN (R).
# Code_TYMPAN (R) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# Code_TYMPAN (R) is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along
# with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
#"""

NEW_LICENSE_NOTICE_HTML_STYLE = """<!--
Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
This file is part of Code_TYMPAN (R).
Code_TYMPAN (R) is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
Code_TYMPAN (R) is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along
with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
-->"""

SOURCE_ROOT = Path("C:/projects/code_tympan")
CPP_DIR = Path("Tympan")
PYTHON_DIR = Path("python")
QML_DIR = Path("qml")
WEB_DIR = Path("web")
CPP_SOURCES_ROOT_PATH = SOURCE_ROOT / CPP_DIR
PYTHON_SOURCES_ROOT_PATH = SOURCE_ROOT / PYTHON_DIR
QML_SOURCES_ROOT_PATH = SOURCE_ROOT / QML_DIR
WEB_SOURCES_ROOT_PATH = SOURCE_ROOT / WEB_DIR

FILES_WITH_OLD_LICENSE = {}
FILES_WITHOUT_LICENSE = {}
SUPPORTED_SUFFIXES = [".html", ".h", ".cpp", ".in", ".hpp", ".py", ".js", ".qml", ".pxd", ".pyx"]
EXCLUDED_FILE_NAMES = ["GpPluginLeaflet-2.4.0-beta4.js", "leaflet-171.js", "qwebchannel.js"]
EXCLUDED_FILE_PATHES = [
    Path("C:/projects/code_tympan/Tympan/geometric_methods/AcousticRaytracer/Geometry/3d.cpp"),
    Path("C:/projects/code_tympan/Tympan/geometric_methods/AcousticRaytracer/Geometry/3d.h"),
    Path("C:/projects/code_tympan/Tympan/geometric_methods/AcousticRaytracer/Geometry/mathlib.h"),
]

FILES_WITH_OLD_LICENSE_NB = 0
FILES_WITHOUT_LICENSE_NB = 0
FILES_EXCLUDED_NB = 0
TOTAL_FILES_NB = 0

Fichier = namedtuple("Fichier", ["fullpath", "extension", "license"])


def build_files_dicts(path):
    """
    Build the dicts FILES_WITH_OLD_LICENSE and FILES_WITHOUT_LICENSE
    """
    global FILES_WITH_OLD_LICENSE_NB
    global FILES_WITHOUT_LICENSE_NB
    global FILES_EXCLUDED_NB
    global TOTAL_FILES_NB
    for directory, dirs, files in os.walk(path):
        for filename in files:
            file_full_path: Path = Path(directory) / Path(filename)
            TOTAL_FILES_NB += 1
            if (
                (
                    not (file_full_path.suffix in SUPPORTED_SUFFIXES)
                    or file_full_path.name in EXCLUDED_FILE_NAMES
                    or file_full_path in EXCLUDED_FILE_PATHES
                )
                or is_auto_generated(file_full_path)
                or is_test(file_full_path)
            ):
                FILES_EXCLUDED_NB += 1
                continue
            license_notice = get_old_gpl_licence_in_file(file_full_path)
            if license_notice is not None:
                add_file_to_dict(FILES_WITH_OLD_LICENSE, file_full_path, license_notice)
                FILES_WITH_OLD_LICENSE_NB += 1
            else:
                add_file_to_dict(FILES_WITHOUT_LICENSE, file_full_path, license_notice)
                FILES_WITHOUT_LICENSE_NB += 1


def get_old_gpl_licence_in_file(filepath):
    """
    Search for one of the old license notices in file and returns the corresponding key
    If no license notice is found, None is returned.
    """
    ret = None
    with open(filepath) as f:
        s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
        if (s.find(OLD_LICENSE_NOTICE_2012.encode(encoding="utf-8"))) != -1:
            ret = "NOTICE_2012"
        if s.find(OLD_LICENSE_NOTICE_2012_2014.encode(encoding="utf-8")) != -1:
            ret = "NOTICE_2012_2014"
        if s.find(OLD_LICENSE_NOTICE_2014.encode(encoding="utf-8")) != -1:
            ret = "NOTICE_2014"
    return ret


def is_auto_generated(filepath):
    """
    If the file is auto generated, return True.
    Auto generated files are excluded from processing license notice
    """
    ret = False
    with open(filepath) as f:
        s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
        if s.find(AUTO_GENERATED_FILES_NOTICE.encode(encoding="utf-8")) != -1:
            ret = True
    return ret


def is_test(filepath):
    """
    If the file is a test file, return True.
    Test files are excluded from processing license notice
    """
    return filepath.name.startswith("test") or filepath.parent.name.startswith("test")


def add_file_to_dict(file_dict, file_full_path, license_notice=None):
    """
    Add file to dict
    The 'Fichier' object contains
    - file_full_path which is the complete path to the file
    - file_dict is the dict to which the file must be added
    - license_notice is the actual license notice of the file
    """
    file_ext = Path(file_full_path).suffixes[0]
    files_list = file_dict.get(file_ext, [])
    fichier = Fichier(file_full_path, file_ext, license_notice)
    if not files_list:
        file_dict[file_ext] = [fichier]
    else:
        files_list.append(fichier)


def replace_old_license_by_new(old_license, new_license, file_full_path):
    """
    Replace the old license with the new one.
    """
    # Opening our text file in read only
    # mode using the open() function
    with open(file_full_path, "r") as file:
        # Reading the content of the file
        # using the read() function and storing
        # them in a new variable
        data = file.read()

        # Searching and replacing the text
        # using the replace() function
        data = data.replace(old_license, new_license)

    # Opening our text file in write only
    # mode to write the replaced content
    with open(file_full_path, "w", newline="\n") as file:
        # Writing the replaced data in our
        # text file
        file.write(data)


def insert_new_license(p_new_license_notice, file_full_path):
    """
    Insert new license notice in the file
    """
    with open(file_full_path, "r+", newline="\n") as file:
        original_content = file.read()
        file.seek(0, 0)  # Move the cursor to top line
        firstline = file.readline()
        file.seek(0, 0)
        file.write(p_new_license_notice)  # Add license
        file.write("\n")  # Add a new blank line
        if firstline != "\n":
            file.write("\n")  # Add a new blank line
        file.write(original_content)


def get_new_license_notice_from_extension(extension):
    """
    Get the new license notice from extension
    """
    ret = None
    if extension in [".h", ".cpp", ".in", ".hpp", ".js", ".qml"]:
        ret = NEW_LICENSE_NOTICE_CPP_STYLE
    elif extension in [".py", ".pxd", ".pyx"]:
        ret = NEW_LICENSE_NOTICE_PYTHON_STYLE
    elif extension in [".html"]:
        ret = NEW_LICENSE_NOTICE_HTML_STYLE
    else:
        raise Exception(
            "The extension {0} is unknown, please revert all changes and update this script".format(extension)
        )
    return ret


def get_old_license_notice_from_license(old_license):
    """
    Get the old license notice from license key
    """
    ret = None
    if old_license == "NOTICE_2012":
        ret = OLD_LICENSE_NOTICE_2012
    elif fichier.license == "NOTICE_2012_2014":
        ret = OLD_LICENSE_NOTICE_2012_2014
    elif fichier.license == "NOTICE_2014":
        ret = OLD_LICENSE_NOTICE_2014
    else:
        raise Exception(
            "The license notice {0} is unknown, please revert all changes and update this script".format(
                old_license
            )
        )
    return ret


# replace_old_license_by_new(
#     OLD_LICENSE_NOTICE_2012,
#     NEW_LICENSE_NOTICE_CPP_STYLE,
#     Path("C:/projects/code_tympan/Tympan/core/chrono.h")
# )
# insert_new_license(
#     NEW_LICENSE_NOTICE_PYTHON_STYLE,
#     Path("C:/projects/code_tympan/python/bin/_util.py")
# )


build_files_dicts(CPP_SOURCES_ROOT_PATH)
build_files_dicts(PYTHON_SOURCES_ROOT_PATH)
build_files_dicts(QML_SOURCES_ROOT_PATH)
build_files_dicts(WEB_SOURCES_ROOT_PATH)

print("\nNB OF FILES WITH OLD LICENSE : {0}".format(FILES_WITH_OLD_LICENSE_NB))
print("NB OF FILES WITHOUT LICENSE  : {0}".format(FILES_WITHOUT_LICENSE_NB))
print("NB OF FILES EXCLUDED         : {0}".format(FILES_EXCLUDED_NB))
print("-----------------------------------")
print("TOTAL NB OF FILES            : {0}".format(TOTAL_FILES_NB))

print("Process Files with old license")
for key, item in FILES_WITH_OLD_LICENSE.items():
    print("Extension : ", key)
    for fichier in item:
        new_license_notice = get_new_license_notice_from_extension(fichier.extension)
        old_license_notice = get_old_license_notice_from_license(fichier.license)
        replace_old_license_by_new(old_license_notice, new_license_notice, fichier.fullpath)

print("Process files without license")
for key, item in FILES_WITHOUT_LICENSE.items():
    print("Extension : ", key)
    for fichier in item:
        new_license_notice = get_new_license_notice_from_extension(fichier.extension)
        insert_new_license(new_license_notice, fichier.fullpath)
