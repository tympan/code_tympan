[Setup]
AppName=Code_TYMPAN
AppVersion=4.6.0
DefaultDirName=C:\Code_TYMPAN
DefaultGroupName=Code_TYMPAN
OutputBaseFilename=setup_TYMPAN_4_6_0_TYKAL_1_3__TYBOX_1_1_20241209
; won't require admin rights to install (per-user install)
PrivilegesRequired=lowest
ChangesEnvironment=true
; signing installer
; DiskSliceSize=max
; DiskSpanning=yes
; SignTool=signtool

[Files]
;; Code_TYMPAN
; qt bearer
Source: "C:\Livraison_bd_Tympan_Tykal\bearer\*"; DestDir: "{app}\bearer";
; py scripts
Source: "C:\Livraison_bd_Tympan_Tykal\bin\*"; DestDir: "{app}\bin"; Excludes: "*.pyc";
; python/cython modules
Source: "C:\Livraison_bd_Tympan_Tykal\cython\*"; DestDir: "{app}\cython"; Excludes: "*.pyc"; Flags: recursesubdirs
; qt iconengines
Source: "C:\Livraison_bd_Tympan_Tykal\iconengines\*"; DestDir: "{app}\iconengines";
; qt imageformats
Source: "C:\Livraison_bd_Tympan_Tykal\imageformats\*"; DestDir: "{app}\imageformats";
; C++ libs
Source: "C:\Livraison_bd_Tympan_Tykal\lib\*"; DestDir: "{app}\lib";
; qt windows platform
Source: "C:\Livraison_bd_Tympan_Tykal\platforms\*"; DestDir: "{app}\platforms"; Flags: recursesubdirs
; c++ acoustic solvers
Source: "C:\Livraison_bd_Tympan_Tykal\plugins\*"; DestDir: "{app}\plugins";
; position
Source: "C:\Livraison_bd_Tympan_Tykal\position\*"; DestDir: "{app}\position";
; printsupport
Source: "C:\Livraison_bd_Tympan_Tykal\printsupport\*"; DestDir: "{app}\printsupport";
; python 64 bits
Source: "C:\Livraison_bd_Tympan_Tykal\python310\*"; DestDir: "{app}\Python310"; Flags: recursesubdirs
; qmltooling
Source: "C:\Livraison_bd_Tympan_Tykal\qmltooling\*"; DestDir: "{app}\qmltooling";
; Qt
Source: "C:\Livraison_bd_Tympan_Tykal\Qt\*"; DestDir: "{app}\Qt"; Flags: recursesubdirs
; QtGraphicalEffects
Source: "C:\Livraison_bd_Tympan_Tykal\QtGraphicalEffects\*"; DestDir: "{app}\QtGraphicalEffects"; Flags: recursesubdirs
; QtQml
Source: "C:\Livraison_bd_Tympan_Tykal\QtQml\*"; DestDir: "{app}\QtQml"; Flags: recursesubdirs
; QtQuick
Source: "C:\Livraison_bd_Tympan_Tykal\QtQuick\*"; DestDir: "{app}\QtQuick"; Flags: recursesubdirs
; QtQuick.2
Source: "C:\Livraison_bd_Tympan_Tykal\QtQuick.2\*"; DestDir: "{app}\QtQuick.2";
; QtWebChannel
Source: "C:\Livraison_bd_Tympan_Tykal\QtWebChannel\*"; DestDir: "{app}\QtWebChannel";
; QtWebEngine
Source: "C:\Livraison_bd_Tympan_Tykal\QtWebEngine\*"; DestDir: "{app}\QtWebEngine"; Flags: recursesubdirs
; QtWebSockets
Source: "C:\Livraison_bd_Tympan_Tykal\QtWebSockets\*"; DestDir: "{app}\QtWebSockets";
; QtWebView
Source: "C:\Livraison_bd_Tympan_Tykal\QtWebView\*"; DestDir: "{app}\QtWebView";
; fonts/images/css/js
Source: "C:\Livraison_bd_Tympan_Tykal\resources\*"; DestDir: "{app}\resources"; Flags: recursesubdirs
; scenegraph
Source: "C:\Livraison_bd_Tympan_Tykal\scenegraph\*"; DestDir: "{app}\scenegraph";
; sqldrivers
Source: "C:\Livraison_bd_Tympan_Tykal\sqldrivers\*"; DestDir: "{app}\sqldrivers";
; qt styles
Source: "C:\Livraison_bd_Tympan_Tykal\styles\*"; DestDir: "{app}\styles"; Flags: recursesubdirs 
; qt translations
Source: "C:\Livraison_bd_Tympan_Tykal\translations\*"; DestDir: "{app}\translations"; Flags: recursesubdirs 
; tykal
Source: "C:\Livraison_bd_Tympan_Tykal\tykal-app\*"; DestDir: "{app}\tykal-app"; Excludes: "*.pyc"; Flags: recursesubdirs
; TYBox
Source: "C:\Livraison_bd_Tympan_Tykal\tybox-app\*"; DestDir: "{app}\tybox-app"; Excludes: "*.pyc"; Flags: recursesubdirs
; web
Source: "C:\Livraison_bd_Tympan_Tykal\web\*"; DestDir: "{app}\web"; Flags: recursesubdirs
; webview
Source: "C:\Livraison_bd_Tympan_Tykal\webview\*"; DestDir: "{app}\webview";
; 3rd party libraries + tympan executable
Source: "C:\Livraison_bd_Tympan_Tykal\*"; DestDir: "{app}"; Excludes: "runTympanRelease.bat, SetEnvTympan.bat"

[UninstallDelete]
Type: filesandordirs; Name: "{app}"
