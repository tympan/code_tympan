[Setup]
AppName=Code_TYMPAN_rec
AppVersion=4.6.0_rec_20240909
DefaultDirName=C:\Code_TYMPAN_rec
DefaultGroupName=Code_TYMPAN_rec
OutputBaseFilename=setup_TYMPAN_4_6_0_rec_TYKAL_1_3__TYBOX_1_1_20240909
; won't require admin rights to install (per-user install)
PrivilegesRequired=lowest
ChangesEnvironment=true
; signing installer
; DiskSliceSize=max
; DiskSpanning=yes
; SignTool=signtool

[Files]
;; Code_TYMPAN
; qt bearer
Source: "C:\Livraison_bd_Tympan_Tykal_rec\bearer\*"; DestDir: "{app}\bearer";
; py scripts
Source: "C:\Livraison_bd_Tympan_Tykal_rec\bin\*"; DestDir: "{app}\bin"; Excludes: "*.pyc";
; python/cython modules
Source: "C:\Livraison_bd_Tympan_Tykal_rec\cython\*"; DestDir: "{app}\cython"; Excludes: "*.pyc"; Flags: recursesubdirs
; qt iconengines
Source: "C:\Livraison_bd_Tympan_Tykal_rec\iconengines\*"; DestDir: "{app}\iconengines";
; qt imageformats
Source: "C:\Livraison_bd_Tympan_Tykal_rec\imageformats\*"; DestDir: "{app}\imageformats";
; C++ libs
Source: "C:\Livraison_bd_Tympan_Tykal_rec\lib\*"; DestDir: "{app}\lib";
; qt windows platform
Source: "C:\Livraison_bd_Tympan_Tykal_rec\platforms\*"; DestDir: "{app}\platforms"; Flags: recursesubdirs
; c++ acoustic solvers
Source: "C:\Livraison_bd_Tympan_Tykal_rec\plugins\*"; DestDir: "{app}\plugins";
; position
Source: "C:\Livraison_bd_Tympan_Tykal_rec\position\*"; DestDir: "{app}\position";
; printsupport
Source: "C:\Livraison_bd_Tympan_Tykal_rec\printsupport\*"; DestDir: "{app}\printsupport";
; python 64 bits
Source: "C:\Livraison_bd_Tympan_Tykal_rec\python310\*"; DestDir: "{app}\Python310"; Flags: recursesubdirs
; qmltooling
Source: "C:\Livraison_bd_Tympan_Tykal_rec\qmltooling\*"; DestDir: "{app}\qmltooling";
; Qt
Source: "C:\Livraison_bd_Tympan_Tykal_rec\Qt\*"; DestDir: "{app}\Qt"; Flags: recursesubdirs
; QtGraphicalEffects
Source: "C:\Livraison_bd_Tympan_Tykal_rec\QtGraphicalEffects\*"; DestDir: "{app}\QtGraphicalEffects"; Flags: recursesubdirs
; QtQml
Source: "C:\Livraison_bd_Tympan_Tykal_rec\QtQml\*"; DestDir: "{app}\QtQml"; Flags: recursesubdirs
; QtQuick
Source: "C:\Livraison_bd_Tympan_Tykal_rec\QtQuick\*"; DestDir: "{app}\QtQuick"; Flags: recursesubdirs
; QtQuick.2
Source: "C:\Livraison_bd_Tympan_Tykal_rec\QtQuick.2\*"; DestDir: "{app}\QtQuick.2";
; QtWebChannel
Source: "C:\Livraison_bd_Tympan_Tykal_rec\QtWebChannel\*"; DestDir: "{app}\QtWebChannel";
; QtWebEngine
Source: "C:\Livraison_bd_Tympan_Tykal_rec\QtWebEngine\*"; DestDir: "{app}\QtWebEngine"; Flags: recursesubdirs
; QtWebSockets
Source: "C:\Livraison_bd_Tympan_Tykal_rec\QtWebSockets\*"; DestDir: "{app}\QtWebSockets";
; QtWebView
Source: "C:\Livraison_bd_Tympan_Tykal_rec\QtWebView\*"; DestDir: "{app}\QtWebView";
; fonts/images/css/js
Source: "C:\Livraison_bd_Tympan_Tykal_rec\resources\*"; DestDir: "{app}\resources"; Flags: recursesubdirs
; scenegraph
Source: "C:\Livraison_bd_Tympan_Tykal_rec\scenegraph\*"; DestDir: "{app}\scenegraph";
; sqldrivers
Source: "C:\Livraison_bd_Tympan_Tykal_rec\sqldrivers\*"; DestDir: "{app}\sqldrivers";
; qt styles
Source: "C:\Livraison_bd_Tympan_Tykal_rec\styles\*"; DestDir: "{app}\styles"; Flags: recursesubdirs 
; qt translations
Source: "C:\Livraison_bd_Tympan_Tykal_rec\translations\*"; DestDir: "{app}\translations"; Flags: recursesubdirs 
; tykal
Source: "C:\Livraison_bd_Tympan_Tykal_rec\tykal-app\*"; DestDir: "{app}\tykal-app"; Excludes: "*.pyc"; Flags: recursesubdirs
; TYBox
Source: "C:\Livraison_bd_Tympan_Tykal_rec\tybox-app\*"; DestDir: "{app}\tybox-app"; Excludes: "*.pyc"; Flags: recursesubdirs
; web
Source: "C:\Livraison_bd_Tympan_Tykal_rec\web\*"; DestDir: "{app}\web"; Flags: recursesubdirs
; webview
Source: "C:\Livraison_bd_Tympan_Tykal_rec\webview\*"; DestDir: "{app}\webview";
; 3rd party libraries + tympan executable
Source: "C:\Livraison_bd_Tympan_Tykal_rec\*"; DestDir: "{app}"; Excludes: "runTympanRelease.bat, SetEnvTympan.bat"

[UninstallDelete]
Type: filesandordirs; Name: "{app}"
