/**
 *
 * @brief test of family class OSpectre
 *
 *  Created on: november 4, 2022
 *  Author: Jean Ripolles <jean.ripolles@edf.fr>
 *
 */
#include "gtest/gtest.h"
#include "Tympan/models/common/spectre.h"
#include "Tympan/core/exceptions.h"

const double MODULE_TO_TEST1[] = {-56.02, -56.02, 31.47, 31.47, 31.47, 31.46,  31.46, 31.46,
                                  31.41,  31.41,  31.41, 31.25, 31.25, 31.25,  30.93, 30.93,
                                  30.93,  30.51,  30.51, 30.51, 29.72, 29.72,  29.72, 27.02,
                                  27.02,  27.02,  16.59, 16.59, 16.59, -56.02, -56.02};

TEST(OSpectre, OSpectre)
{
    // Default constuctor
    OSpectre spectre;
    EXPECT_TRUE(spectre.isValid());
    EXPECT_EQ(spectre.getType(), SPECTRE_TYPE_ATT);
    EXPECT_EQ(spectre.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(spectre.getForm(), SPECTRE_FORM_TIERS);

    // setDefaultValue
    spectre.setDefaultValue(93.0);
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        EXPECT_EQ(spectre.getTabValReel()[i], 93.0);
    }

    // operator ==
    OSpectre spectre2{&MODULE_TO_TEST1[0], TY_SPECTRE_DEFAULT_NB_ELMT, 0};
    spectre2.setType(SPECTRE_TYPE_LP);
    spectre2.setValid(false);
    spectre2.setEtat(SPECTRE_ETAT_LIN); // Just for test purpose
    // Fill spectrum array with expected values
    OSpectre expectedSpectre;
    expectedSpectre.setType(SPECTRE_TYPE_LP);
    expectedSpectre.setValid(false);
    expectedSpectre.setEtat(SPECTRE_ETAT_LIN); // Just for test purpose
    expectedSpectre.setValue(16.0, -56.02);
    expectedSpectre.setValue(20.0, -56.02);
    expectedSpectre.setValue(25.0, 31.47);
    expectedSpectre.setValue(31.5, 31.47);
    expectedSpectre.setValue(40.0, 31.47);
    expectedSpectre.setValue(50.0, 31.46);
    expectedSpectre.setValue(63.0, 31.46);
    expectedSpectre.setValue(80.0, 31.46);
    expectedSpectre.setValue(100.0, 31.41);
    expectedSpectre.setValue(125.0, 31.41);
    expectedSpectre.setValue(160.0, 31.41);
    expectedSpectre.setValue(200.0, 31.25);
    expectedSpectre.setValue(250.0, 31.25);
    expectedSpectre.setValue(315.0, 31.25);
    expectedSpectre.setValue(400.0, 30.93);
    expectedSpectre.setValue(500.0, 30.93);
    expectedSpectre.setValue(630.0, 30.93);
    expectedSpectre.setValue(800.0, 30.51);
    expectedSpectre.setValue(1000.0, 30.51);
    expectedSpectre.setValue(1250.0, 30.51);
    expectedSpectre.setValue(1600.0, 29.72);
    expectedSpectre.setValue(2000.0, 29.72);
    expectedSpectre.setValue(2500.0, 29.72);
    expectedSpectre.setValue(3150.0, 27.02);
    expectedSpectre.setValue(4000.0, 27.02);
    expectedSpectre.setValue(5000.0, 27.02);
    expectedSpectre.setValue(6300.0, 16.59);
    expectedSpectre.setValue(8000.0, 16.59);
    expectedSpectre.setValue(10000.0, 16.59);
    expectedSpectre.setValue(12500.0, -56.02);
    expectedSpectre.setValue(16000.0, -56.02);

    ASSERT_EQ(spectre2, spectre2);
    ASSERT_EQ(spectre2, expectedSpectre);
}

TEST(OSpectre, valGlobDBLin)
{
    const double values[] = {50.0, 60.0, 50.0, 40.0, 20.0, 10.0, 60.0, 80.0, 20.0};
    OSpectre s{values, 9, 0};
    s.setForm(SPECTRE_FORM_OCT);

    EXPECT_NEAR(s.valGlobDBLin(), 80.09, 1e-2);
}

TEST(OSpectre, valGlobDBA)
{
    const double values[] = {50.0, 60.0, 50.0, 40.0, 20.0, 10.0, 60.0, 80.0, 20.0};
    OSpectre s{values, 9, 0};
    s.setForm(SPECTRE_FORM_OCT);

    EXPECT_NEAR(s.valGlobDBA(), 81.05, 1e-2);
}

TEST(OSpectre, fromOSpectreOctave)
{
    // -- Test conversion from OSpectreOctave of type Lp to OSpectre
    // Init OSpectreOctave of type Lp in dB as in 9613Solver
    const double valuesSO[] = {50.0, 60.0, 50.0, 40.0, 20.0, 10.0, 60.0, 80.0, 20.0};
    OSpectreOctave SO{valuesSO, 9, 0};
    SO.setEtat(SPECTRE_ETAT_DB);
    SO.setType(SPECTRE_TYPE_LP);

    // Test affectation to OSpectre
    OSpectre S = SO;

    // Expected results
    const double expectedResults[] = {-56.02, -56.02, 45.23, 45.23, 45.23, 55.23,  55.23, 55.23,
                                      45.23,  45.23,  45.23, 35.23, 35.23, 35.23,  15.23, 15.23,
                                      15.23,  5.23,   5.23,  5.23,  55.23, 55.23,  55.23, 75.23,
                                      75.23,  75.23,  15.23, 15.23, 15.23, -56.02, -56.02};

    // Check assertions
    EXPECT_TRUE(S.isValid());
    EXPECT_EQ(S.getType(), SPECTRE_TYPE_LP);
    EXPECT_EQ(S.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(S.getForm(), SPECTRE_FORM_TIERS);

    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        EXPECT_NEAR(S.getTabValReel()[i], expectedResults[i], 0.01);
    }
}

TEST(OSpectreOctave, OSpectreOctave)
{
    // Default constructor
    OSpectreOctave spectreOct;
    EXPECT_TRUE(spectreOct.isValid());
    EXPECT_EQ(spectreOct.getType(), SPECTRE_TYPE_ATT);
    EXPECT_EQ(spectreOct.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(spectreOct.getForm(), SPECTRE_FORM_OCT);

    // setDefaultValue
    spectreOct.setDefaultValue(93.0);
    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_EQ(spectreOct.getTabValReel()[i], 93.0);
    }

    // Default value constructor
    OSpectreOctave spectreOct2(93.0);
    EXPECT_TRUE(spectreOct2.isValid());
    EXPECT_EQ(spectreOct2.getType(), SPECTRE_TYPE_ATT);
    EXPECT_EQ(spectreOct2.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(spectreOct2.getForm(), SPECTRE_FORM_OCT);
    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_EQ(spectreOct2.getTabValReel()[i], 93.0);
    }

    // Copy constructor
    OSpectreOctave spectreOct3{spectreOct2};
    EXPECT_TRUE(spectreOct3.isValid());
    EXPECT_EQ(spectreOct3.getType(), SPECTRE_TYPE_ATT);
    EXPECT_EQ(spectreOct3.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(spectreOct3.getForm(), SPECTRE_FORM_OCT);
    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_EQ(spectreOct3.getTabValReel()[i], 93.0);
    }

    // Constructor from OSpectre object in SPECTRE_OCT_FORM
    OSpectre spectre = OSpectre::makeOctSpect();
    spectre.setType(SPECTRE_TYPE_LW);
    spectre.setValue(16.0, 93.0);
    spectre.setValue(20.0, 93.0);
    spectre.setValue(25.0, 93.0);
    spectre.setValue(31.5, 93.0);
    spectre.setValue(40.0, 93.0);
    spectre.setValue(50.0, 93.0);
    spectre.setValue(63.0, 93.0);
    spectre.setValue(80.0, 93.0);
    spectre.setValue(100.0, 93.0);

    // Tested function
    // NB : An OSpectreOctave built from an OSpectre is converted to (or stay in) dB for 9613Solver
    // computations
    OSpectreOctave spectreOct4{spectre};
    EXPECT_TRUE(spectreOct4.isValid());
    EXPECT_EQ(spectreOct4.getType(), SPECTRE_TYPE_LW);
    EXPECT_EQ(spectreOct4.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(spectreOct4.getForm(), SPECTRE_FORM_OCT);
    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_EQ(spectreOct4.getTabValReel()[i], 93.0);
    }

    // Constructor from OSpectre object in SPECTRE_TIERS_FORM
    OSpectre spectre2{88.23};
    spectre2.setType(SPECTRE_TYPE_LW);
    OSpectreOctave spectreOct5{spectre2};
    EXPECT_TRUE(spectreOct5.isValid());
    EXPECT_EQ(spectreOct5.getType(), SPECTRE_TYPE_LW);
    EXPECT_EQ(spectreOct5.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(spectreOct5.getForm(), SPECTRE_FORM_OCT);
    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(spectreOct5.getTabValReel()[i], 93.0, 0.01);
    }

    // Conversion from OSpectreOctave to OSpectre
    OSpectreOctave spectreOctLp;
    spectreOctLp.setType(SPECTRE_TYPE_LP);
    spectreOctLp.setValue(31.5, 36.24);
    spectreOctLp.setValue(63.0, 36.23);
    spectreOctLp.setValue(125.0, 36.18);
    spectreOctLp.setValue(250.0, 36.02);
    spectreOctLp.setValue(500.0, 35.70);
    spectreOctLp.setValue(1000.0, 35.28);
    spectreOctLp.setValue(2000.0, 34.49);
    spectreOctLp.setValue(4000.0, 31.79);
    spectreOctLp.setValue(8000.0, 21.36);

    OSpectre spectreLp = spectreOctLp.toTOct();

    OSpectre expectedSpectreLp;
    expectedSpectreLp.setType(SPECTRE_TYPE_LP);
    // Fill spectrum array with expected values
    expectedSpectreLp.setValue(16.0, -56.02);
    expectedSpectreLp.setValue(20.0, -56.02);
    expectedSpectreLp.setValue(25.0, 31.47);
    expectedSpectreLp.setValue(31.5, 31.47);
    expectedSpectreLp.setValue(40.0, 31.47);
    expectedSpectreLp.setValue(50.0, 31.46);
    expectedSpectreLp.setValue(63.0, 31.46);
    expectedSpectreLp.setValue(80.0, 31.46);
    expectedSpectreLp.setValue(100.0, 31.41);
    expectedSpectreLp.setValue(125.0, 31.41);
    expectedSpectreLp.setValue(160.0, 31.41);
    expectedSpectreLp.setValue(200.0, 31.25);
    expectedSpectreLp.setValue(250.0, 31.25);
    expectedSpectreLp.setValue(315.0, 31.25);
    expectedSpectreLp.setValue(400.0, 30.93);
    expectedSpectreLp.setValue(500.0, 30.93);
    expectedSpectreLp.setValue(630.0, 30.93);
    expectedSpectreLp.setValue(800.0, 30.51);
    expectedSpectreLp.setValue(1000.0, 30.51);
    expectedSpectreLp.setValue(1250.0, 30.51);
    expectedSpectreLp.setValue(1600.0, 29.72);
    expectedSpectreLp.setValue(2000.0, 29.72);
    expectedSpectreLp.setValue(2500.0, 29.72);
    expectedSpectreLp.setValue(3150.0, 27.02);
    expectedSpectreLp.setValue(4000.0, 27.02);
    expectedSpectreLp.setValue(5000.0, 27.02);
    expectedSpectreLp.setValue(6300.0, 16.59);
    expectedSpectreLp.setValue(8000.0, 16.59);
    expectedSpectreLp.setValue(10000.0, 16.59);
    expectedSpectreLp.setValue(12500.0, -56.02);
    expectedSpectreLp.setValue(16000.0, -56.02);

    auto spectreLpValues = spectreLp.getTabValReel();
    auto expectedSpectreLpValues = expectedSpectreLp.getTabValReel();
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        EXPECT_NEAR(spectreLpValues[i], expectedSpectreLpValues[i], 0.01);
    }
    EXPECT_TRUE(spectreLp.isValid());
    EXPECT_EQ(spectreLp.getType(), SPECTRE_TYPE_LP);
    EXPECT_EQ(spectreLp.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(spectreLp.getForm(), SPECTRE_FORM_TIERS);
}

TEST(OSpectreOctave, OSpectreOctave_operator_plus)
{
    // -- TEST sum of 2 OSpectreOctave
    // Init operands
    OSpectreOctave A;
    A.setValue(31.5, 20.32);
    A.setValue(63.0, 25.63);
    A.setValue(125.0, 28.44);
    A.setValue(250.0, 35.02);
    A.setValue(500.0, 37.70);
    A.setValue(1000.0, 40.28);
    A.setValue(2000.0, 42.49);
    A.setValue(4000.0, 50.79);
    A.setValue(8000.0, 71.36);

    EXPECT_TRUE(A.isValid());
    EXPECT_EQ(A.getType(), SPECTRE_TYPE_ATT);
    EXPECT_EQ(A.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(A.getForm(), SPECTRE_FORM_OCT);

    OSpectreOctave B;
    B.setValue(31.5, 2.05);
    B.setValue(63.0, 1.41);
    B.setValue(125.0, 7.4);
    B.setValue(250.0, 0);
    B.setValue(500.0, -1.74);
    B.setValue(1000.0, -4.23);
    B.setValue(2000.0, 8.94);
    B.setValue(4000.0, 0.97);
    B.setValue(8000.0, 3.42);

    EXPECT_TRUE(B.isValid());
    EXPECT_EQ(B.getType(), SPECTRE_TYPE_ATT);
    EXPECT_EQ(B.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(B.getForm(), SPECTRE_FORM_OCT);

    // TESTED OPERATION
    OSpectreOctave S = A + B;

    // Expected results
    const double expectedResults[] = {22.37, 27.04, 35.84, 35.02, 35.96, 36.05, 51.43, 51.76, 74.78};

    // Check assertions
    EXPECT_TRUE(S.isValid());
    EXPECT_EQ(S.getType(), SPECTRE_TYPE_ATT);
    EXPECT_EQ(S.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(S.getForm(), SPECTRE_FORM_OCT);

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(S.getTabValReel()[i], expectedResults[i], 0.01);
    }
}

TEST(OSpectreOctave, OSpectreOctave_operator_mult_coef)
{
    // -- TEST sum of 2 OSpectreOctave
    // Init operands
    OSpectreOctave A;
    A.setValue(31.5, 23.47);
    A.setValue(63.0, -14.58);
    A.setValue(125.0, 46.87);
    A.setValue(250.0, 63.45);
    A.setValue(500.0, 54.21);
    A.setValue(1000.0, 59.78);
    A.setValue(2000.0, 49.78);
    A.setValue(4000.0, 53.21);
    A.setValue(8000.0, 37.14);

    EXPECT_TRUE(A.isValid());
    EXPECT_EQ(A.getType(), SPECTRE_TYPE_ATT);
    EXPECT_EQ(A.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(A.getForm(), SPECTRE_FORM_OCT);

    double coef = 1.13;

    // TESTED OPERATION
    OSpectreOctave P = A * coef;

    // Expected results
    const double expectedResults[] = {26.52, -16.48, 52.96, 71.7, 61.26, 67.55, 56.25, 60.13, 41.97};

    // Check assertions
    EXPECT_TRUE(P.isValid());
    EXPECT_EQ(P.getType(), SPECTRE_TYPE_ATT);
    EXPECT_EQ(P.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(P.getForm(), SPECTRE_FORM_OCT);

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(P.getTabValReel()[i], expectedResults[i], 0.01);
    }
}

TEST(OSpectreOctave, sumDB)
{
    // -- Test dB sum of two OSpectreOctave
    // Init operands
    const double values_A[] = {45.8, 32.7, 22.94, -5.9, 33.22, -23.14, 0.25, 75.84, 64.87};
    OSpectreOctave A{values_A, 9, 0};

    const double values_B[] = {23.78, 24.89, -12.9, -8.45, 53.15, -23.14, 12.4, 75.84, 62.12};
    OSpectreOctave B{values_B, 9, 0};

    // TESTED OPERATION
    OSpectreOctave dbSum = A.sumdB(B);

    // Expected results
    const double expectedResults[] = {45.83, 33.37, 22.94, -3.98, 53.19, -20.13, 12.66, 78.85, 66.72};

    // Check assertions
    EXPECT_TRUE(dbSum.isValid());
    EXPECT_EQ(dbSum.getType(), SPECTRE_TYPE_ATT);
    EXPECT_EQ(dbSum.getEtat(), SPECTRE_ETAT_DB);
    EXPECT_EQ(dbSum.getForm(), SPECTRE_FORM_OCT);

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(dbSum.getTabValReel()[i], expectedResults[i], 0.01);
    }
}

TEST(OSpectreOctave, sum_octave_tiers)
{
    // -- TEST sum of OSpectreOctave and OSpectre throws exeption
    // Init operands
    OSpectreOctave A;
    A.setValue(31.5, 23.47);
    A.setValue(63.0, -14.58);
    A.setValue(125.0, 46.87);
    A.setValue(250.0, 63.45);
    A.setValue(500.0, 54.21);
    A.setValue(1000.0, 59.78);
    A.setValue(2000.0, 49.78);
    A.setValue(4000.0, 53.21);
    A.setValue(8000.0, 37.14);

    OSpectre B;
    B.setValue(16.0, 23.47);
    B.setValue(20.0, -14.58);
    B.setValue(25.0, 46.87);
    B.setValue(31.5, 63.45);
    B.setValue(40.0, 54.21);
    B.setValue(50.0, 59.78);
    B.setValue(63.0, 49.78);
    B.setValue(80.0, 53.21);
    B.setValue(100.0, 37.14);
    B.setValue(125.0, 23.47);
    B.setValue(160.0, -14.58);
    B.setValue(315.0, 46.87);
    B.setValue(400.0, 63.45);
    B.setValue(500.0, 54.21);
    B.setValue(630.0, 59.78);
    B.setValue(800.0, 49.78);
    B.setValue(1000.0, 53.21);
    B.setValue(1250.0, 37.14);
    B.setValue(1600.0, 49.78);
    B.setValue(2000.0, 53.21);
    B.setValue(2500.0, 37.14);
    B.setValue(3150.0, 23.47);
    B.setValue(4000.0, -14.58);
    B.setValue(5000.0, 46.87);
    B.setValue(6300.0, 63.45);
    B.setValue(8000.0, 54.21);
    B.setValue(10000.0, 59.78);
    B.setValue(12500.0, 49.78);
    B.setValue(16000.0, 53.21);

    ASSERT_THROW(OSpectre C = A + B, tympan::invalid_data);
    ASSERT_THROW(OSpectreOctave D = A + B, tympan::invalid_data);
    ASSERT_THROW(OSpectre E = B + A, tympan::invalid_data);
    ASSERT_THROW(OSpectreOctave F = B + A, tympan::invalid_data);
}
