/**
 *
 * @brief Functional tests of the TYTrajet class
 *
 *  Created on: january 18, 2018
 *  Author: Philippe CHAPUIS <philippe.chapuis@c-s.fr>
 *
 */

#define _USE_MATH_DEFINES

#include "gtest/gtest.h"
#include "Tympan/solvers/9613/DefaultSolver/TYTrajetDefaultSolver.h"
#include "Tympan/solvers/9613/DefaultSolver/TYCheminDefaultSolver.h"
#include "Tympan/models/solver/entities.hpp"
#include "Tympan/solvers/9613/DefaultSolver/TYSolverDefaultSolver.h"
#include "Tympan/models/solver/acoustic_problem_model.hpp"
#include "Tympan/models/solver/data_model_common.hpp"
#include "Tympan/solvers/9613/DefaultSolver/TYAcousticModelDefaultSolver.h"
#include <math.h>
#include "testutils.h"

#define M_PI 3.14159265358979323846 /* pi */

/**
 * @brief Fixture which provides an instance of TYTrajet
 */
class TYTrajetTest : public testing::Test
{
private:
public:
};

// Testing the TYTrajet::getPEnergetique method
TEST_F(TYTrajetTest, getPEnergetique)
{
    // initialisation des objets communs
    TYInitUserCase initCase;
    initCase.initGlobal(100.0);

    // variable condition atmospherique
    AtmosphericConditions _atmos_test(101325., 20., 50.);
    OSpectre _expectedSpectre;
    OSpectre _returnSpectreDB;

    // On pose les bases du trajet --> où se trouvent la Source et le Recepteur
    TYTrajetDefaultSolver mTrajet(initCase.m_acousticSourceList.at(0), initCase.m_acousticReceptionList.at(0));

    // CAS 1 CHEMIN DIRECT
    // appel de la méthode cas chemin direct
    _expectedSpectre = initCase.initDirectPathWay();
    _expectedSpectre = _expectedSpectre.mult(_expectedSpectre);

    // vérification du nombre d'étapes dans le tableau
    EXPECT_EQ(1, initCase.m_tabEtape01.size());

    initCase.m_chemin_direct.calcAttenuation(initCase.m_tabEtape01, _atmos_test);

    mTrajet.addChemin(initCase.m_chemin_direct);
    mTrajet.addCheminDirect(initCase.m_chemin_direct);

    _returnSpectreDB = mTrajet.getPEnergetique(_atmos_test);

    // TODO Mettre à jour les spectres attendus avec la calculette 03_détails_calcul_acoustique.xlsx
    EXPECT_FALSE(_expectedSpectre == _returnSpectreDB);

    // CAS CHEMIN_ECRAN
    // On pose les bases du trajet --> où se trouvent la Source et le Recepteur
    TYTrajetDefaultSolver mTrajetEcran(initCase.m_acousticSourceList.at(0), initCase.m_acousticReceptionList.at(0));

    // appel de la méthode cas chemin direct
    _expectedSpectre = initCase.initBlockPathWay();
    _expectedSpectre = _expectedSpectre.mult(_expectedSpectre);

    // vérification du nombre d'étapes dans le tableau
    EXPECT_EQ(4, initCase.m_tabEtape01.size());

    initCase.m_chemin_ecran.calcAttenuation(initCase.m_tabEtape01, _atmos_test);

    mTrajetEcran.addChemin(initCase.m_chemin_ecran);
    mTrajetEcran.addCheminDirect(initCase.m_chemin_direct);

    initCase.m_chemin_ecran.calcAttenuation(initCase.m_tabEtape01, _atmos_test);

    _returnSpectreDB = mTrajetEcran.getPEnergetique(_atmos_test);

    // TODO Mettre à jour les spectres attendus avec la calculette 03_détails_calcul_acoustique.xlsx
    EXPECT_FALSE(_expectedSpectre == _returnSpectreDB);
}

// Testing the TYTrajet::getPInterference method
TEST_F(TYTrajetTest, getPInterference)
{
    // initialisation des objets communs
    TYInitUserCase initCase;
    initCase.initGlobal(100.0);

    // variable condition atmospherique
    AtmosphericConditions _atmos_test(101325., 20., 50.);
    OSpectre _expectedSpectre;
    OSpectre _returnSpectreDB;

    // On pose les bases du trajet --> où se trouvent la Source et le Recepteur
    TYTrajetDefaultSolver mTrajet(initCase.m_acousticSourceList.at(0), initCase.m_acousticReceptionList.at(0));

    // Cas CHEMIN DIRECT
    // appel de la méthode cas chemin direct
    _expectedSpectre = initCase.initDirectPathWay();
    _expectedSpectre = _expectedSpectre.mult(_expectedSpectre);

    // vérification du nombre d'étapes dans le tableau
    EXPECT_EQ(1, initCase.m_tabEtape01.size());

    initCase.m_chemin_direct.calcAttenuation(initCase.m_tabEtape01, _atmos_test);

    mTrajet.addChemin(initCase.m_chemin_direct);
    mTrajet.addCheminDirect(initCase.m_chemin_direct);

    _returnSpectreDB = mTrajet.getPInterference(_atmos_test);

    // TODO Mettre à jour les spectres attendus avec la calculette 03_détails_calcul_acoustique.xlsx
    EXPECT_FALSE(_expectedSpectre == _returnSpectreDB);

    // CAS CHEMIN_ECRAN
    // On pose les bases du trajet --> où se trouvent la Source et le Recepteur
    TYTrajetDefaultSolver mTrajetEcran(initCase.m_acousticSourceList.at(0), initCase.m_acousticReceptionList.at(0));

    // appel de la méthode cas chemin écran
    _expectedSpectre = initCase.initBlockPathWay();
    _expectedSpectre = _expectedSpectre.mult(_expectedSpectre);

    // vérification du nombre d'étapes dnas le tableau
    EXPECT_EQ(4, initCase.m_tabEtape01.size());

    initCase.m_chemin_ecran.calcAttenuation(initCase.m_tabEtape01, _atmos_test);

    mTrajetEcran.addChemin(initCase.m_chemin_ecran);
    mTrajetEcran.addCheminDirect(initCase.m_chemin_direct);

    initCase.m_chemin_ecran.calcAttenuation(initCase.m_tabEtape01, _atmos_test);

    _returnSpectreDB = mTrajetEcran.getPInterference(_atmos_test);

    // TODO Mettre à jour les spectres attendus avec la calculette 03_détails_calcul_acoustique.xlsx
    EXPECT_FALSE(_expectedSpectre == _returnSpectreDB);

    // Cas CHEMIN REFLEX
    // On pose les bases du trajet --> où se trouvent la Source et le Recepteur
    TYTrajetDefaultSolver mTrajetREFLEX(initCase.m_acousticSourceList.at(0), initCase.m_acousticReceptionList.at(0));

    // appel de la méthode cas chemin réflexion
    _expectedSpectre = initCase.initReflexionPathWay();
    _expectedSpectre = _expectedSpectre.mult(_expectedSpectre);

    // vérification du nombre d'étapes dans le tableau
    EXPECT_EQ(2, initCase.m_tabEtape01.size());

    initCase.m_chemin_reflex.calcAttenuation(initCase.m_tabEtape01, _atmos_test);

    mTrajetREFLEX.addChemin(initCase.m_chemin_reflex);
    mTrajetREFLEX.addCheminDirect(initCase.m_chemin_direct);

    initCase.m_chemin_reflex.calcAttenuation(initCase.m_tabEtape01, _atmos_test);

    _returnSpectreDB = mTrajetREFLEX.getPInterference(_atmos_test);

    // TODO Mettre à jour les spectres attendus avec la calculette 03_détails_calcul_acoustique.xlsx
    EXPECT_FALSE(_expectedSpectre == _returnSpectreDB);
}
