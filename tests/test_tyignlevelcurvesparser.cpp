﻿/**
 *
 * @brief Test methods of TYIGNLevelCurvesParser class
 *
 *  Created on: june 13, 2024
 *  Author: Jean Ripolles <jean.ripolles@edf.fr>
 *
 */
#include "gtest/gtest.h"
#include "Tympan/gis/TYIGNLevelCurvesParser.h"
#include "Tympan/models/common/mathlib.h"
#include "TympanTestsConfig.h"
#include <iostream>

/**
 * @brief Fixture which provides an xml file containing a set of cevel curves
 */
class LevelCurves01Test : public ::testing::Test
{
protected:
    void SetUp() override
    {
        /// Data setup:
        QString filePath{tympan::path_to_test_data("gis/level_curves_from_ign_01.xml").c_str()};
        m_parser = std::make_shared<TYIGNLevelCurvesParser>();
        m_parser->loadXmlFile(filePath);
    }

    // void TearDown() override {}

    // Inersection array
    std::shared_ptr<TYIGNLevelCurvesParser> m_parser;
};

// Testing TYIGNLevelCurvesParser::buildPolylineFromCourbe method for one level curve
TEST_F(LevelCurves01Test, buildPolylineFromCourbe)
{
    // Pre-conditions
    EXPECT_TRUE(m_parser->hasRetrievedLevelCurves()) << "Level curves have not been retrieved.";
    EXPECT_TRUE(m_parser->isXmlLoaded()) << "XML document is not loaded.";

    // Defini SIG Coords
    OCoord3D SIGCoords{0, 0, 0};

    // Get first level curve
    QDomElement root = m_parser->getXmlDocument().documentElement();
    QDomNodeList courbeList = root.elementsByTagName("ELEVATION.CONTOUR.LINE:courbe");
    QDomElement courbe = courbeList.at(0).toElement();

    // Define expected results

    // Call tested method
    bool ret = m_parser->buildPolylineFromCourbe(courbe, SIGCoords);

    // Check method results
    EXPECT_TRUE(ret) << "Method buildPolylineFromCourbe returned false.";

    EXPECT_FALSE(m_parser->getCT().constraints().empty())
        << "No constraint to altimetry info map has been built.";

    // Get first level curve id
    auto constraint_it = m_parser->getCT().constraints_begin();
    Constraint_id constraint_id = *constraint_it;

    // Check number of points in the first level curve
    auto point_it = m_parser->getCT().points_in_constraint(constraint_id);
    auto nb_points = point_it.size();

    EXPECT_EQ(31, nb_points) << "Number of points in the first level curve is not as expected.";

    // Check altimetry info for the first level curve
    // Wrap the Constraint_id
    ConstraintIdWrapper wrapped_id(constraint_id);

    auto it = m_parser->getConstraintToAltimetryInfoMap().find(wrapped_id);
    ASSERT_NE(it, m_parser->getConstraintToAltimetryInfoMap().end()) << "wrapped_id not found in the map.";

    const TYIGNLevelCurvesParser::AltimetryInfo& info = it->second;
    EXPECT_EQ("courbe.612409", info.id) << "Level curve id is not as expected.";
    EXPECT_EQ(185.0, info.altitude) << "Level curve altitude is not as expected.";
}

// Testing TYIGNLevelCurvesParser::simplifyPolylines method for one level curve
TEST_F(LevelCurves01Test, simplifyPolylines)
{
    // Pre-conditions
    EXPECT_TRUE(m_parser->hasRetrievedLevelCurves()) << "Level curves have not been retrieved.";
    EXPECT_TRUE(m_parser->isXmlLoaded()) << "XML document is not loaded.";

    // Define SIG Coords
    OCoord3D SIGCoords{0, 0, 0};

    // Get first level curve
    QDomElement root = m_parser->getXmlDocument().documentElement();
    QDomNodeList courbeList = root.elementsByTagName("ELEVATION.CONTOUR.LINE:courbe");
    QDomElement courbe = courbeList.at(0).toElement();

    // Build polyline corresponding to first level curve
    bool ret = m_parser->buildPolylineFromCourbe(courbe, SIGCoords);
    // Get first level curve id
    auto constraint_it = m_parser->getCT().constraints_begin();
    Constraint_id constraint_id = *constraint_it;

    // Get initial number of points
    auto point_it = m_parser->getCT().points_in_constraint(constraint_id);
    auto initial_nb_points = point_it.size();

    // Call tested method
    size_t nb_removed_vertices = m_parser->simplifyPolylines();
    std::cout << "Number of removed vertices is " << nb_removed_vertices << std::endl;

    // Check method results
    EXPECT_EQ(nb_removed_vertices, 23) << "Number of removed vertices is not as expected.";

    // Get final number of points
    point_it = m_parser->getCT().points_in_constraint(constraint_id);
    auto final_nb_points = point_it.size();

    EXPECT_FALSE(m_parser->getCT().constraints().empty())
        << "No constraint to altimetry info map has been built.";

    EXPECT_EQ(final_nb_points, initial_nb_points - nb_removed_vertices)
        << "Final number of points in the first level curve is not as expected.";

    // Check altimetry info for the first level curve
    // Wrap the Constraint_id
    ConstraintIdWrapper wrapped_id(constraint_id);
    auto it = m_parser->getConstraintToAltimetryInfoMap().find(wrapped_id);
    ASSERT_NE(it, m_parser->getConstraintToAltimetryInfoMap().end()) << "wrapped_id not found in the map.";

    const TYIGNLevelCurvesParser::AltimetryInfo& info = it->second;
    EXPECT_EQ("courbe.612409", info.id) << "Level curve id is not as expected.";
    EXPECT_EQ(185.0, info.altitude) << "Level curve altitude is not as expected.";
}
