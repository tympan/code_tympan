/**
 * \file test_environment.cpp
 * \test Some debuging of the testing environment
 *
 *  Created on: 6 nov. 2012
 *      Author: Anthony Truchet <anthony.truchet@logilab.fr>
 */

#include <cstdlib>
#include <string>

#include "gtest/gtest.h"

#include <QDir>

#include "TympanTestsConfig.h"

using std::cout;
using std::cerr;
using std::endl;

TEST(TestEnvironment, Path)
{
#ifdef _WIN32
    char* path = nullptr;
    size_t sz = 0;
    ASSERT_TRUE(_dupenv_s(&path, &sz, "PATH") == 0);
#else
    char* path = getenv("PATH");
    ASSERT_TRUE(path != nullptr);
    cout << "PATH = " << path << endl;
#endif
}

TEST(TestEnvironment, LdPath)
{
#ifdef _WIN32
    char* path = nullptr;
    size_t sz = 0;
    _dupenv_s(&path, &sz, "LD_LIBRARY_PATH");
    if(path == NULL)
        path = "";
#else
    char* path = getenv("LD_LIBRARY_PATH");
    if(path == nullptr)
        path = (char*)"";
    cout << "LD_LIBRARY_PATH = " << path << endl;
#endif
}

TEST(TestEnvironment, TestDataPath)
{
    std::string expected_data_path = tympan::path_to_test_data("");
    EXPECT_EQ(expected_data_path, std::string(TYMPAN_TESTS_DATA_DIR));
}
