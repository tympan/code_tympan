/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.15
import QtQuick.Window 2.15
import QtWebView 1.1
import QtWebSockets 1.1
import QtWebChannel 1.0
import QtQuick.Controls 2.15
import QtQuick.LocalStorage 2.0
import codetympan.org 1.0

Window {
    id: modalWindow
    width: 1600
    height: 950
    visible: true
    modality: Qt.ApplicationModal
    title: qsTr("Sélection de la zone")
    
    property bool debugMode: false  // Add the debugMode property

    GeographicData {
        id: geographicData
        objectName: "geographicData"
        WebChannel.id: "core"
        function receiveCoordinates(text){
            console.log("receiveCoordinates: ", text)
            geographicData.landtake_coordinates = text
        }
        signal sendCoordinates(string text)
        signal cancelRequested()
        signal oKRequested()
        function receiveCrs(crs){
            console.log("receiveCrs: ", crs)
            geographicData.crs = crs
        }
        function receiveImg(img){
            console.log("receiveImg")
            geographicData.background_img = img
        }
        function receiveLevelCurves(level_curves){
            console.log("receiveLevelCurves")
            geographicData.level_curves = level_curves
        }
        function receiveScaleFactor(scale_factor){
            console.log("receiveScaleFactor: ", scale_factor)
            geographicData.scale_factor = scale_factor
        }
        function receiveImageWidth(image_width){
            console.log("receiveImageWidth: ", image_width)
            geographicData.image_width = image_width
        }

        function cancel() {
            disconnectChannel("cancel");
            geographicData.cancelRequested();
            modalWindow.close();
        }
        function view_send_ok() {
            disconnectChannel("view_send_ok");
            geographicData.oKRequested();
            modalWindow.close();
        }

    }

    Text {
        id: landtakeInput
        width: parent.width - 20 // Adjust width to fit the window width
        text: geographicData.landtake_coordinates + " " + geographicData.crs
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 10 // Add margin to the top and sides
    }

    WebView {
        id: webView
        url: "qrc:/web/TYZoneSelectPage.html"
        width: parent.width - 20 // Adjust width to fit the window width
        height: parent.height - landtakeInput.height - 40 // Adjust height to fit WebView below TextField
        anchors.top: landtakeInput.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.margins: 10 // Add margin to the top and sides
    }

    WebSocketTransport{
        id: transport
    }

    WebSocketServer {
        id: webSocket
        listen: true
        port: 49327
        onClientConnected: {
            // Example usage
            debugLog("Client connected");
            if(webSocket.status === WebSocket.Open){
                channel.connectTo(transport)
                debugLog("Connected WebChannel to transport.")
                webSocket.onTextMessageReceived.connect(transport.textMessageReceive)
                transport.onMessageChanged.connect(webSocket.sendTextMessage)
            }
        }
    }

    WebChannel {
        id: channel
        registeredObjects: [geographicData]
    }

    function disconnectChannel(reason) {
        channel.disconnectFrom(transport);
        debugLog("Channel disconnected from transport. Reason : " + reason);
    }

    // Helper function to log only in debug mode
    function debugLog(message) {
        if (debugMode) {
            console.log(message);
        }
    }

    Component.onCompleted: {
        // Open the local storage database
        var db = LocalStorage.openDatabaseSync("Code_TYMPAN", "1.0", "Local Storage", 1000000);

        // Clear the contents of local storage
        db.transaction(function(tx) {
            tx.executeSql('DROP TABLE IF EXISTS ItemTable');
            tx.executeSql('CREATE TABLE IF NOT EXISTS ItemTable (key TEXT UNIQUE, value TEXT)');
        });

        // Load the contents of customConfig.json from qrc and store it in local storage
        var fileContent = Qt.resolvedUrl("qrc:/web/customConfig.json").toString();
        if (fileContent !== "") {
            db.transaction(function(tx) {
                tx.executeSql('INSERT INTO ItemTable VALUES (?, ?)', ["customConfig", fileContent]);
            });
            debugLog("customConfig.json data stored in local storage.");
        } else {
            console.error("Failed to read customConfig.json.");
        }
    }
    Component.onDestruction: {
        // Disconnect the channel from the transport when the component is destroyed
        disconnectChannel("Component.onDestruction");
    }
}
