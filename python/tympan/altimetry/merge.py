#
# Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
# This file is part of Code_TYMPAN (R).
# Code_TYMPAN (R) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# Code_TYMPAN (R) is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along
# with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
#

"""Merging sub-sites for the computation of the altimetry.


The main entry point is the function recursively_merge_all_subsites.
It uses the class SiteNodeGeometryCleaner to perform a recursive merge
of all the site note of a compound site to build a single site.
"""
from copy import copy
from itertools import combinations, chain

from shapely import geometry, ops
from .datamodel import SiteNode, InconsistentGeometricModel, SiteLandtake, WaterBody, SiteArea


def recursively_merge_all_subsites(rootsite, allow_outside=True):
    """Merges all subsites and their subsites and so on into this merger."""
    cleaned = SiteNodeGeometryCleaner(rootsite, rootsite)
    if not allow_outside:
        for feature in rootsite.level_curves:
            # Perhaps consider also other types of features?
            if isinstance(feature, SiteLandtake):
                # Do not consider site landtake equivalent level curve which
                # is by definition coincident with site geometry.
                continue
            if not cleaned.root_site_shape.contains(feature.shape):
                raise InconsistentGeometricModel(
                    "L'élément : {name} n'est pas strictement contenu dans le site principal : "
                    "{site} (attention un élément doit être inclus dans le site principal)".format(
                        name=feature.name, site=rootsite.name
                    )
                )
    cleaned.process_all_features()
    subsites_to_be_processed = list(rootsite.subsites)
    while subsites_to_be_processed:
        current_site = subsites_to_be_processed.pop()
        cleaned.merge_subsite(current_site, rootsite)
        subsites_to_be_processed.extend(current_site.subsites)
    return cleaned


def build_site_shape_with_hole(site):
    site.ensure_ok()
    exterior = site.shape.exterior
    if site.shape.interiors:
        raise ValueError("The site is not expected to already have holes")
    for hole in site.subsites:
        if not site.shape.contains(hole.shape):
            raise InconsistentGeometricModel(
                "{name} n'est pas strictement contenu dans {parent}".format(name=hole.name, parent=site.name)
            )
    holes = [hole.shape.exterior for hole in site.subsites]
    return geometry.Polygon(exterior, holes)


def build_root_site_shape(root_site):
    root_site.ensure_ok()
    return geometry.Polygon(root_site.shape.exterior)


class SiteNodeGeometryCleaner(object):
    """Clean a sitenode geometry, subsites excluded and build reports.

    More precisely this class is build from a root SiteNode. Then the
    ``process_*`` method walk through the geometrical elements (level
    curves, material areas, landtakes, ...) and associate them with a
    geometry we are sure to be within the site landtake and outside the
    landtake of any subsite.

    The level curves are cut and the polygonal features are filtered
    according to the following policy:

    * the IDs of feature overlapping the boundary of the site node are
      reported in the ``erroneous_overlap`` list attributes

    * the IDs of feature entirely out of the site node are reported in
      the ``ignored_feature`` list attributes

    The new geometry computed for each feature is available through
    the ``geom`` dictionary attribute, which is indexed by the
    feature IDs.

    An equivalent site is maintained with the original features.  The
    class also implement a __getitem__ method so that both the new
    geometry and the original properties can be accessed with ``geom,
    info = cleaner[feature_id]``.
    """

    def __init__(self, sitenode, root_site):
        if not isinstance(sitenode, SiteNode):
            raise TypeError("A altimetry.datamodel.SiteNode was expected")
        self.sitenode = sitenode
        self.root_site_shape = build_root_site_shape(root_site)
        build_site_shape_with_hole(self.sitenode)
        self.geom = {}
        self.ignored_features = []
        self.erroneous_overlap = []
        self._sorted_material_areas = []
        self.equivalent_site = SiteNode(sitenode.build_coordinates()[0], id=None)
        self.equivalent_site._cleaner = self  # For tests.

    def _add_feature_with_new_shape(self, feature, shape):
        assert not isinstance(feature, SiteNode)
        id_ = feature.id
        existing_shape = self.geom.pop(id_, None)
        if existing_shape and not existing_shape.equals(shape):
            raise ValueError(
                "ID %s is already associated to a different shape %s" % (id_, existing_shape.wkt)
            )
        self.equivalent_site.add_child(feature)
        self.geom[id_] = shape

    def feature_from_id(self, id):
        return self.equivalent_site.features_by_id[id]

    def name_from_id(self, id):
        return self.feature_from_id(id).name

    def merged_site(self):
        """Return the merged site"""
        merged_site = SiteNode(self.sitenode.build_coordinates()[0], id=self.sitenode.id)
        for feature_id, shape in self.geom.items():
            feature = copy(self.feature_from_id(feature_id))
            feature.set_shape(shape)
            merged_site.add_child(feature)
        return merged_site

    def trace(self, merged_site):
        """
        This method is kept for Debug and must not be called in Release version
        """
        print("Cleaner : self._sorted_material_areas")
        print(self._sorted_material_areas)
        print("Merged_site : %s " % merged_site.__dict__)
        print("merged_site children : %s" % merged_site.children)
        for feature_type, feature_list in merged_site.children.items():
            print("Feature type : %s " % feature_type)
            for feature in feature_list:
                print("Feature id : %s, name : %s, shape : %s " % (feature.id, feature.name, feature.shape))

    def __getitem__(self, feature_id):
        return self.geom[feature_id], self.feature_from_id(feature_id).build_properties()

    def _process_lineic_features(self):
        for polyline in chain(self.sitenode.level_curves, self.sitenode.roads):
            if isinstance(polyline, WaterBody):
                continue
            else:
                shape = polyline.shape.intersection(self.root_site_shape)
                if self.root_site_shape.crosses(polyline.shape):
                    raise InconsistentGeometricModel(
                        "L'élément : {polyline} n'est pas strictement inclus dans le site principal ("
                        "attention un élément doit être inclus dans le site principal)".format(
                            polyline=polyline.name
                        )
                    )
                if self.root_site_shape.disjoint(polyline.shape):
                    raise InconsistentGeometricModel(
                        "L'élément : {polyline} est entièrement en dehors du site principal (attention un "
                        "élément doit être inclus dans le site principal)".format(polyline=polyline.name)
                    )
                self._add_feature_with_new_shape(polyline, shape)

    def _add_or_reject_polygonal_feature(self, feature):
        """
        Implements the no overlap policy for material area and land-takes
        """
        # NB : given the possibilities of shapely, allowing to cut the
        # polygonal features so that the associated geometries lie within
        # the site land-take would be a simple matter of changing this method.

        if self.root_site_shape.overlaps(feature.shape):
            self.erroneous_overlap.append(feature.id)
            raise InconsistentGeometricModel(
                "L'élément : {name} n'est pas strictement inclus dans le site principal (attention un "
                "élément doit être inclus dans le site principal)".format(name=feature.name)
            )
        if self.root_site_shape.disjoint(feature.shape):
            self.ignored_features.append(feature.id)
            raise InconsistentGeometricModel(
                "L'élément : {name} est entièrement en dehors du site principal (attention un "
                "élément doit être inclus dans le site principal)".format(name=feature.name)
            )
        self._add_feature_with_new_shape(feature, feature.shape)
        return True

    def process_material_areas(self):
        for area in self.sitenode.material_areas:
            if self._add_or_reject_polygonal_feature(area):
                pos = self.insert_position_for_sorted_material_area(area)
                self._sorted_material_areas.insert(pos, area.id)

    def process_infrastructure_landtakes(self):
        for landtake in self.sitenode.landtakes:
            self._add_or_reject_polygonal_feature(landtake)

    def process_subsites_landtakes(self):
        for subsite in self.sitenode.subsites:
            if self.sitenode.shape.crosses(subsite.shape):
                raise InconsistentGeometricModel(
                    "{site} n'est pas strictement contenu dans {parent}".format(
                        site=subsite.name, parent=self.sitenode.name
                    )
                )
            if self.sitenode.shape.disjoint(subsite.shape):
                raise InconsistentGeometricModel(
                    "{site} est entièrement en dehors de {parent}".format(
                        site=subsite.name, parent=self.sitenode.name
                    )
                )

    def process_all_features(self):
        self.process_subsites_landtakes()
        self._process_lineic_features()
        self.process_material_areas()
        self.process_infrastructure_landtakes()

    def import_cleaned_geometries_from(self, othercleaner):
        """Create new geometry and info into the site of this cleaner
        representing the cleaned geometry for each feature of the other
        cleaner.

        Info are shared between the self and the other cleaner.
        """
        for feature_id, shape in othercleaner.geom.items():
            self._add_feature_with_new_shape(othercleaner.feature_from_id(feature_id), shape)

    def merge_subsite(self, subsite, rootsite):
        """Merge the cleaned geometries for subsite into the self cleaner"""
        subcleaner = SiteNodeGeometryCleaner(subsite, rootsite)
        subcleaner.process_all_features()
        if subcleaner.erroneous_overlap:
            msg = (
                "Can not merge subsite {subsite} because of features {ids} " "overlapping its boundaries."
            ).format(subsite=subsite.name, ids=subcleaner.erroneous_overlap)
            raise InconsistentGeometricModel(msg, subsite=subsite.id, ids=subcleaner.erroneous_overlap)
        self.import_cleaned_geometries_from(subcleaner)
        self._merge_subsite_materials(subcleaner)

    def insert_position_for_sorted_material_area(self, inserted_area):
        """Insert the ID of inserted_area into _sorted_material_areas so that
        area appear most inner first.

        Return its position in _sorted_material_areas.

        The invariant is that for each pair of areas in the list at
        position i and j with i<j, either A(i) and A(J) are disjoint
        or A(i) is included in A(j).

        """
        for i, area_id in enumerate(self._sorted_material_areas):
            area_geom, area_info = self[area_id]
            area_name = self.name_from_id(area_id)
            if inserted_area.shape.overlaps(area_geom):
                if isinstance(inserted_area, SiteArea):
                    continue
                elif isinstance(inserted_area, SiteNode):
                    return i
                elif isinstance(inserted_area, WaterBody) and area_info["type"] == "WaterBody":
                    continue
                else:
                    inserted_area_name = inserted_area.name
                    positions = [
                        (round(p[0]), round(p[1]))
                        for p in inserted_area.shape.intersection(area_geom).exterior.coords
                    ]
                    msg = (
                        "Superposition de materiaux : "
                        "\nSite : {subsite} \nMateriaux : {names} \nPositions : {positions}"
                    ).format(
                        subsite=self.sitenode.name, names=[area_name, inserted_area_name], positions=positions
                    )

                    raise InconsistentGeometricModel(
                        msg, ids=[area_id, inserted_area.id], names=[area_name, inserted_area_name]
                    )
            if inserted_area.shape.within(area_geom):
                return i
        # The inserted_area is disjoint with all others, insert it at the
        # end
        return len(self._sorted_material_areas)

    def _merge_subsite_materials(self, subcleaner):
        """Expect subcleaner is a cleaner for a subsite of self.sitenode and
        import into self the ordered list of the subsite material
        areas
        """
        # A site node is not a material area but has a geometry which
        # is enough to call insert_position_for_sorted_material_area to get the
        # place where to insert its own material
        pos = self.insert_position_for_sorted_material_area(subcleaner.sitenode)
        self._sorted_material_areas[pos:pos] = subcleaner.material_areas_inner_first()

    def check_issues_with_material_area_order(self):
        """Diagnostic helper: returns violation of the ordering for material area"""
        problems = set([])
        for i, area_i_id in enumerate(self._sorted_material_areas):
            for j in range(i + 1, len(self._sorted_material_areas)):
                area_j_id = self._sorted_material_areas[j]
                area_i_geom = self.geom[area_i_id]
                area_j_geom = self.geom[area_j_id]
                if not (area_i_geom.within(area_j_geom) or area_i_geom.disjoint(area_j_geom)):
                    problems.add((area_i_id, area_j_id))
        return list(problems)

    def material_areas_inner_first(self):
        return list(self._sorted_material_areas)

    def postprocess_waterbodies_shapes(self):
        """
        This method is called after the recursive merge of all subsites.
        Its purpose is to postprocess waterbodies shapes to allow them encroaching on subsites
        The intersection area between a SiteArea and a WaterBody is a WaterBody.
        """
        for water_body in self.equivalent_site.water_bodies:
            # Retrieve water body shape from its id and insure its validity (.buffer(0))
            # cf https://stackoverflow.com/questions/20833344/fix-invalid-polygon-in-shapely
            water_body_shape = self.geom[water_body.id].buffer(0)
            modified_dict = {}
            for feature_id, shape in self.geom.items():
                if feature_id != water_body.id and shape.crosses(water_body_shape):
                    new_shape = shape.difference(water_body_shape)
                    feature = self.equivalent_site.features_by_id[feature_id]
                    feature.set_shape(new_shape)
                    modified_dict[feature_id] = new_shape
                    # modify water body to add intersection points with the original shape
                    if not isinstance(shape, geometry.LineString):
                        feature_boundary = geometry.LineString(shape.boundary)
                    else:
                        # the LineString.boundary attribute contains its two extremities
                        # in our case, we consider LineStrings to be their own boundary
                        feature_boundary = shape

                    # split the water body along the feature's boundary
                    splitted_water_body = ops.split(water_body_shape, feature_boundary)

                    # merge it back to obtain its original shape
                    # with the added intersection points on its boundary
                    water_body_shape = ops.cascaded_union([geom for geom in splitted_water_body])
                    modified_dict[water_body.id] = water_body_shape
            for feature_id, modified_shape in modified_dict.items():
                self.geom[feature_id] = modified_shape

    def postprocess_siteareas(self):
        """
        This method is called after the recursive merge of all sites.
        Subsites can cross each other, as long as their default ground material has the same resistivity.
        If two subsites cross each other with default ground material of different resistivity, then an
        InconsistentGeometricModel exception must be raised.
        A subsite is always allowed to be strictly included in other subsite. In that case, the ground
        material of the inner subsite must be taken into account.
        """
        for site_area_1, site_area_2 in combinations(self.equivalent_site.site_areas, 2):
            if (
                site_area_1.shape.overlaps(site_area_2.shape)
                and site_area_1.material.resistivity != site_area_2.material.resistivity
            ):
                positions = [
                    (round(p[0]), round(p[1]))
                    for p in site_area_1.shape.intersection(site_area_2.shape).exterior.coords
                ]
                msg = (
                    "2 sous-sites se superposent avec des terrains par défaut de résistivité différente."
                    "\nRésistivité 1 : {resistivity_1}"
                    "\nRésistivité 2 : {resistivity_2}"
                    "\nAire de superposition : {positions}"
                ).format(
                    resistivity_1=site_area_1.material.resistivity,
                    resistivity_2=site_area_2.material.resistivity,
                    positions=positions,
                )
                raise InconsistentGeometricModel(
                    msg, ids=[site_area_1.id, site_area_2.id], names=[site_area_1.name, site_area_2.name]
                )
