#
# Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
# This file is part of Code_TYMPAN (R).
# Code_TYMPAN (R) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# Code_TYMPAN (R) is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along
# with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
#

"""core objects for Code_TYMPAN, headers
"""

from libcpp.string cimport string
from libcpp cimport bool
from tympan.models._solver cimport AcousticProblemModel, AcousticResultModel, SolverConfiguration

# STL ##########################################################################

cdef extern from "boost/shared_ptr.hpp" namespace "boost":

    cdef cppclass shared_ptr[T]:
        shared_ptr(T*)
        shared_ptr()
        T *get()

cdef extern from "memory" namespace "std":

    cdef cppclass unique_ptr[T]:
        T *get()


# Qt ###########################################################################

cdef extern from "qstring.h":
    cdef cppclass QString:
        string toStdString() const
        QString(const char*)

# Tympan #######################################################################

cdef extern from "Tympan/core/smartptr.h":
    cdef cppclass SmartPtr[T]:
        SmartPtr()
        SmartPtr(T*)
        T* getRealPointer()
        T* _pObj

cdef extern from "Tympan/core/idgen.h":
    cdef cppclass OGenID:
        OGenID(const OGenID& other)
        OGenID()
        OGenID(const QString& string_id)
        const QString toString() const

cdef extern from "Tympan/core/interfaces.h":
    cdef cppclass SolverInterface:
        SolverInterface()
        void purge()
        bool solve(const AcousticProblemModel& aproblem,
                   const AcousticResultModel& aresult,
                   shared_ptr[SolverConfiguration])
