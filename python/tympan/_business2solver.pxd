#
# Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
# This file is part of Code_TYMPAN (R).
# Code_TYMPAN (R) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# Code_TYMPAN (R) is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along
# with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
#

#cython: language_level=3

from libcpp.map cimport map
from libcpp.vector cimport vector

from tympan._core cimport SmartPtr, OGenID, SolverInterface
from tympan.models cimport _business as tybusiness
from tympan.models cimport _common as tycommon
from tympan.models cimport _solver as tysolver

cdef class Business2MicroSource:
    cdef map[tybusiness.TYElem_ptr, vector[SmartPtr[tybusiness.TYGeometryNode]]] map_sources

cdef business2microsource(map[tybusiness.TYElem_ptr, vector[SmartPtr[tybusiness.TYGeometryNode]]] map_elt_srcs)

cdef extern from "Tympan/models/business/TYPluginManager.h" namespace "tympan":
    void load_solvers(const char *path)
    SolverInterface* select_solver(OGenID uuid) except +
