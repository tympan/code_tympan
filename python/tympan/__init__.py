#
# Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
# This file is part of Code_TYMPAN (R).
# Code_TYMPAN (R) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# Code_TYMPAN (R) is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along
# with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
#

"""Code_TYMPAN: acoustic simulations based on C++ libraries

Allows to:

- Build a project from a XML file describing an industrial site
- Compute and export the altimetry from the topography and infrastructures of the site
- Create an abstract model from the "business" description, describing the
  topography as a mesh, and with elementary sources and receptors
- Run an acoustic simulation on an abstract model
- Update the business model with the results of the altimetry computation or
  with the result of the acoustic computation

Here goes an example of the typical use that can be done of this API:

.. code-block:: python

    # Build a project from a site XML description (update site infrastructure altimetry)
    project = Project.from_xml("my_project.xml")
    # Build a simplified model from the project (the new infrastructure
    # altitudes will be taken into account)
    model = Model.from_project(project)
    # Load and configure the acoustic solver
    solver = Solver.from_project(project)
    # Run the simulation
    result = solver.solve(model)
    # Update the project with the results of the computation
    project.import_result(model, result)

This API can be used to do other types of computations, here goes an example of how to do a computation
with user-defined sources:

.. code-block:: python

    # Build a project from a XML model
    project = Project.from_xml("my_project.xml")
    # build solver model, but don't use the project sources
    model = Model.from_project(project, set_sources=False)
    # Manually define the sources (position and spectrum) depending on your needs
    model.add_source(Source((0, 0, 0), np.array([100.0] * 31, dtype=float)))
    model.add_source(Source((100, 50, 0), np.array([150.0] * 31, dtype=float)))
    solver = Solver.from_project(project)
    result = solver.solve(model)
    # retrieve combined spectra per receptor
    combined_spectra = result.combined_spectra()
"""
import os

TYMPAN_INSTALL_PATH = os.environ.get("TYMPAN_INSTALL_PATH")
if os.name == "posix":  # linux
    os.environ["PATH"] = TYMPAN_INSTALL_PATH + os.pathsep + os.environ["PATH"]
else:  # windows
    os.add_dll_directory(TYMPAN_INSTALL_PATH)
