import unittest

import numpy as np
from numpy.testing import assert_allclose

from tympan.models import Spectrum


class SpectrumTC(unittest.TestCase):
    def test_constant(self):
        s = Spectrum.constant(1.2)
        assert_allclose(s.values, np.ones(31) * 1.2)

    def test_multiply(self):
        s1 = Spectrum.constant(1.2)
        s2 = s1 * 2
        assert_allclose(s2.values, np.ones(31) * 2.4)


if __name__ == "__main__":
    unittest.main()
