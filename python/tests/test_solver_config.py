import io
import os
import tempfile

from utils import TympanTC, TEST_DATA_DIR  # TEST_SOLVERS_DIR
from tympan.models.project import Project
from tympan.models.solver import _set_solver_config
from tympan.models import _solver as cysolver


def _set_solver_config_from_computation(computation):
    """Wrapper around _set_solver_config for testing purpose."""
    parameters_fp = io.StringIO(computation.solver_parameters.decode())
    return _set_solver_config(parameters_fp)


class TestSolverConfig(TympanTC):
    def test_parameters_serialization(self):
        config = """
[tutu]
titi=15549.15
toto=True
tata=0
"""
        # Open a basic project that doesn't have solver parameters
        project = self.load_project("", "empty_site.xml")
        # Set a custom solver configuration to it
        project.current_computation.solver_parameters = config
        # Export to XML and reimport it
        with tempfile.NamedTemporaryFile(suffix=".xml", delete=False) as f:
            project.to_xml(f.name)
            configured_project = Project.from_xml(f.name)
            # Check configuration didn't disappear
            self.assertEqual(configured_project.current_computation.solver_parameters.decode(), config)
        f.close()
        os.unlink(f.name)

    def test_set_solver_config(self):
        # Open a basic project with a custom solver configuration
        project = self.load_project("", "test_solver_params.xml")
        _set_solver_config_from_computation(project.current_computation)
        solver_config = cysolver.Configuration.get()
        # 10. by default, 20. in the XML file
        self.assertEqual(solver_config.H1parameter, 20.0)

    def test_parameters_parsing(self):
        # Open a basic project with a custom solver configuration
        project = self.load_project("", "test_solver_params.xml")
        _set_solver_config_from_computation(project.current_computation)
        solver_config = cysolver.Configuration.get()
        # bool
        self.assertEqual(solver_config.UseReflection, True)
        self.assertEqual(solver_config.PropaConditions, False)
        # float
        self.assertEqual(solver_config.H1parameter, 20.0)
        # double
        self.assertEqual(solver_config.AnalyticDMax, 3000.0)
        # int
        self.assertEqual(solver_config.PropaConditions, 0)


class TestSolverSelection(TympanTC):
    def test_select_solver(self):
        project = Project.from_xml(os.path.join(TEST_DATA_DIR, "empty_site.xml"))
        self.assertEqual(project.current_computation.solver_id, "{a98b320c-44c4-47a9-b689-1dd352daa8b2}")


class TestSolverConfigConversion(TympanTC):
    def test_convert_T42_T43(self):
        self.load_project("", "T42_model_SR_Keep_Rays_True.xml")
        solver_config = cysolver.Configuration.get()
        self.assertEqual(solver_config.KeepRays, True)
        with self.assertRaises(AttributeError) as cm:
            param = solver_config.Anime3DForceC  # noqa
        self.assertEqual(
            str(cm.exception), "'tympan.models._solver.Configuration' object has no attribute 'Anime3DForceC'"
        )


if __name__ == "__main__":
    import unittest

    unittest.main()
