import unittest

from utils import TympanTC, _test_solve_with_file


class TestSolveSOURCE_REFLEXION_BATIMENT(TympanTC):
    def test_source_reflexion_batiment(self):
        _test_solve_with_file("TEST_SOURCE_REFLEXION_BATIMENT_NO_RESU.xml", self)


if __name__ == "__main__":
    unittest.main()
