#
# Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
# This file is part of Code_TYMPAN (R).
# Code_TYMPAN (R) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# Code_TYMPAN (R) is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along
# with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
#

# -*- coding: utf-8 -*-
"""
Created on Wed Mar 18 17:48:34 2015

@author: I17841

Example script illustrating plotting of PLY data using Mayavi.  Mayavi
is not a dependency of plyfile, but you will need to install it in order
to run this script.  Failing to do so will immediately result in
ImportError.
"""

from argparse import ArgumentParser

import numpy
from mayavi import mlab

from plyfile import PlyData


def main():
    parser = ArgumentParser()
    parser.add_argument("ply_filename")

    args = parser.parse_args()

    plot(PlyData.read(args.ply_filename))
    mlab.show()


def plot(ply):
    """
    Plot vertices and triangles from a PlyData instance. Assumptions:
        `ply' has a 'vertex' element with 'x', 'y', and 'z'
            properties;
        `ply' has a 'face' element with an integral list property
            'vertex_indices', all of whose elements have length 3.
    """
    vertex = ply["vertex"].data

    (x, y, z) = (vertex[t] for t in ("x", "y", "z"))

    mlab.points3d(x, y, z, color=(1, 1, 1), mode="point")

    if "face" in ply:
        tri_idx = ply["face"].data["vertex_indices"]
        idx_dtype = tri_idx[0].dtype

        triangles = numpy.fromiter(tri_idx, [("data", idx_dtype, (3,))], count=len(tri_idx))["data"]

        mlab.triangular_mesh(x, y, z, triangles, color=(0, 1, 0), opacity=0.2)


main()
