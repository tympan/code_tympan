#
# Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
# This file is part of Code_TYMPAN (R).
# Code_TYMPAN (R) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# Code_TYMPAN (R) is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along
# with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
#

"""
Script for launching a Tympan calculation, taking as input parameters:
- an XML Tympan model
- a CSV road file or a directory that contains one or several CSV road files
- the target directory
"""

import argparse
import numpy as np
import os
from pathlib import Path

from tympan.altimetry import (
    builder,
    datamodel,
    export_to_ply,
)
from tympan.models.project import Project
from tympan.models.solver import (
    Model,
    Solver,
)


def parse_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("project", help="xml project file")
    parser.add_argument("road", help="csv road file or directory" " that contains csv road files")
    parser.add_argument("-t", "--target-dir", help="target repository", default=os.getcwd())
    args = parser.parse_args()
    # check arguments
    project_path = Path(args.project)
    if not project_path.exists():
        raise IOError("Project {} doesn't exists".format(project_path))
    road_path = Path(args.road)
    if not road_path.exists():
        raise IOError("Road(s) {} doesn't exists".format(road_path))
    if road_path.is_dir():
        road_files = road_path.glob("*.csv")
    else:
        road_files = [road_path]
    target_dir = Path(args.target_dir)
    if not target_dir.exists():
        raise IOError("Directory {} doesn't exists".format(target_dir))
    return project_path, road_files, target_dir


if __name__ == "__main__":
    solverdir = os.environ.get("TYMPAN_SOLVER_DIR")

    # parse arguments
    project_file, road_files, target_dir = parse_args()
    # load project
    project = Project.from_xml(str(project_file), verbose=True, update_altimetry=False)
    # build site
    asite = builder.build_sitenode(project.site)
    # create and add road(s)
    for road_file in road_files:
        road = datamodel.Road(
            datamodel.load_csv_road(str(road_file)),
            id=road_file.name,
            parent_site=asite,
        )

    # build altimetry
    merged_site, mesh, feature_by_face = builder.build_altimetry(asite)

    # update project site
    material_by_face = builder.material_by_face(feature_by_face)
    project.site.update_altimetry(mesh, material_by_face)

    # save mesh (PLY)
    export_to_ply(mesh, material_by_face, str(target_dir.joinpath("mesh.ply")))

    # call solver
    solver = Solver.from_project(project, solverdir, verbose=True)
    model = Model.from_project(project)
    result = solver.solve(model)

    # save results
    np.savetxt(target_dir.joinpath("results.txt"), result.spectrum(0, 0).values)
    # save project
    project.import_result(model, result)
    project.to_xml(str(target_dir.joinpath("results.xml")))
