#! /bin/sh

export TYMPAN_SOLVERDIR=/var/lib/code-tympan/code-tympan/plugins
export LD_LIBRARY_PATH=/var/lib/code-tympan:/var/lib/code-tympan/code-tympan/cython/CGAL:$LD_LIBRARY_PATH
export PYTHONPATH=/var/lib/code-tympan/code-tympan/cython:$PYTHONPATH
export CGAL_BINDINGS_PATH=/var/lib/code-tympan/code-tympan/cython/CGAL
export TYMPAN_INSTALL_DIR=/var/lib/code-tympan/code-tympan
export TYMPAN_INSTALL_PATH=/var/lib/code-tympan/code-tympan
export PYTHONTYMPAN=/usr/bin/python3
export TYMPAN_PYTHON_INTERP=/usr/bin/python3

cd /var/lib/code-tympan/code-tympan
exec /var/lib/code-tympan/code-tympan/Tympan

