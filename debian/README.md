# Debian package

## Build dependencies

```bash
sudo apt update
sudo apt install -y software-properties-common
sudo apt install -y cmake swig git libboost1.74-dev libboost-regex1.74-dev
sudo apt install -y libqt5opengl5 libqt5opengl5-dev
sudo apt install -y python3 python3-dev libcgal-dev
```

## build debian package

```bash
./build.sh
```

## install debian package

```bash
sudo apt install ./code-tympan_4.5.0_amd64.deb
```

## launch Code_tympan

```bash
Code_TYMPAN
```

## uninstall debian package

```bash
sudo apt purge code-tympan
```
