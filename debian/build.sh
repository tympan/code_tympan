#! /bin/bash

set -eo pipefail

# Set dirs
ROOT=$(pwd)
TMPDIR=${ROOT}/.tmp
BUILDDIR=${ROOT}/build
SRCDIR=${BUILDDIR}/src
PKGDIR=${BUILDDIR}/pkg


PYTHON="python3"
PYTHON_INCLUDE_DIR=`ls -d /usr/include/python3*`

TYMPAN_INSTALL_PATH=${SRCDIR}/dist
TYMPAN_PYTHON_INTERP=${TMPDIR}/venv/bin/python

mkdir -p ${TMPDIR} ${SRCDIR} ${PKGDIR}

function build() {
    echo "==> Building code-tympan…"
	echo "-> Preparing Python venv…"
	${PYTHON} -m venv ${TMPDIR}/venv && source ${TMPDIR}/venv/bin/activate
	pip install -r ../requirements.txt
	pip install -r ../requirements-dev.txt
	cd ${SRCDIR}
	echo "-> Building…"
	cmake ${ROOT}/.. -DCMAKE_INSTALL_PREFIX=${TYMPAN_INSTALL_PATH} \
		         -DPYTHON_INCLUDE_DIR=${PYTHON_INCLUDE_DIR} \
				 -DCMAKE_BUILD_TYPE:STRING=Release

	make -j$(grep -c '^processor' /proc/cpuinfo)
	make install
	cd ${ROOT}
}

function prepare () {
    echo "==> Preparing Debian package…"
	# lib
	mkdir -p ${PKGDIR}/var/lib
	cp -r ${SRCDIR}/dist/lib ${PKGDIR}/var/lib/code-tympan
	mkdir -p ${PKGDIR}/var/lib/code-tympan/code-tympan
	install ${SRCDIR}/dist/Tympan ${PKGDIR}/var/lib/code-tympan/code-tympan/Tympan
	cp -r ${SRCDIR}/dist/bin ${PKGDIR}/var/lib/code-tympan/code-tympan/bin
	cp -r ${SRCDIR}/dist/cython ${PKGDIR}/var/lib/code-tympan/code-tympan/cython
	cp -r ${SRCDIR}/dist/plugins ${PKGDIR}/var/lib/code-tympan/code-tympan/plugins
	cp -r ${SRCDIR}/dist/resources ${PKGDIR}/var/lib/code-tympan/code-tympan/resources
	# bin
	mkdir -p ${PKGDIR}/usr/bin
	install ${ROOT}/tympan.sh ${PKGDIR}/usr/bin/Code_TYMPAN
	# Debian
	mkdir -p ${PKGDIR}/DEBIAN
	cp ${ROOT}/control ${PKGDIR}/DEBIAN/control
	# Desktop entry
	mkdir -p ${PKGDIR}/usr/share/applications ${PKGDIR}/usr/share/pixmaps
	cp ${ROOT}/tympan.desktop ${PKGDIR}/usr/share/applications
	cp ${ROOT}/code-tympan.png ${PKGDIR}/usr/share/pixmaps
}

function package() {
    echo "==> Building Debian package…"
	dpkg-deb --build ${PKGDIR} ${ROOT}

}

function main() {
	test -f ${SRCDIR}/dist/Tympan || build
	test -f ${PKGDIR}/DEBIAN/control || prepare
	test -f ${ROOT}/*.deb || package
}


if [[ ${1} == "clean" ]];then
	echo "Cleaning…"
	rm -rf ${BUILDDIR} ${TMPDIR} ${ROOT}/*.deb
	echo "done"
else
	main
fi
