/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

import { handleResponseError } from "./handleResponseError.js";

// Function to convert image URL to base64
export async function imageURLToBase64(imageUrl) {
    // Fetch the image data
    const response = await fetch(imageUrl);
    await handleResponseError(response, "Erreur lors de la récupération de l'image de fond: ");

    // Get the content type from the response headers
    const contentType = response.headers.get('Content-Type');

    // Check if the content type is valid for an image
    if (!isValidImageContentType(contentType)) {
        throw new Error("Erreur: L'image renvoyée n'a pas une extension autorisée");
    }

    // Convert the image data to base64
    const blob = await response.blob();
    const base64Image = await convertBlobToBase64(blob);
    return base64Image;
}

// Function to check if the content type is valid for an image
export function isValidImageContentType(contentType) {
    console.log("isValidImageContentType");
    return /^image\/(jpeg|png|gif|bmp)$/.test(contentType);
}

// Function to convert blob data to base64
export function convertBlobToBase64(blob) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onloadend = () => {
            if (reader.result) {
                // Remove the data URL prefix
                const base64Image = reader.result.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
                resolve(base64Image);
            } else {
                reject(new Error("Echec lors de la lecture des données de l'image"));
            }
        };
        reader.readAsDataURL(blob);
    });
}
