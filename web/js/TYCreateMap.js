/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

var createMap = function () {
    try {
        hideLoadingIndicator();
        // Création de la map
        var layerWMTS = L.geoportalLayer.WMTS({
            layer: "GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2",
            apiKey: "cartes"
        });

        var layerWMS = L.geoportalLayer.WMS({
            layer: "SCAN1000_PYR-JPEG_WLD_WM",
            apiKey: "cartes"
        });

        map = L.map("map", {
            zoom: 10,
            center: L.latLng(45.2, 5.7)
        });

        layerWMS.addTo(map);
        layerWMTS.addTo(map);

        var drawnItems = new L.FeatureGroup();
        map.addLayer(drawnItems);

        var drawControl = new L.Control.Draw({
            edit: false,
            draw: {
                polygon: false,
                marker: false,
                polyline: false,
                circle: false,
                circlemarker: false
            }
        });
        map.addControl(drawControl);

        map.on(L.Draw.Event.CREATED, function (event) {
            if (Object.keys(event).length === 0)
                return;

            try {
                var layer = event.layer;
                drawnItems.clearLayers();
                drawnItems.addLayer(layer);

                if (layer.getLatLngs().length === 0)
                    return;

                var latLngs = layer.getLatLngs()[0];

                // Convert each coordinate to EPSG:3857
                var transformed_coordinates = latLngs.map(function (coord) {
                    if (coord.hasOwnProperty('lat') && coord.hasOwnProperty('lng')) {
                        var latlng = L.latLng(coord.lat, coord.lng);
                        var projected = L.CRS.EPSG3857.project(latlng);
                        return [projected.x, projected.y];
                    }
                    else
                        return;
                });

                // Create the bounding box in EPSG:3857 format
                bbox.epsg_3857 = transformed_coordinates[0] + "," + transformed_coordinates[2];

                // Compute diagonal distance of the selected map
                var delta_x = Math.abs(transformed_coordinates[2][0] - transformed_coordinates[0][0]);
                var delta_y = Math.abs(transformed_coordinates[2][1] - transformed_coordinates[0][1]);
                var projected_distance = Math.sqrt(delta_x * delta_x + delta_y * delta_y);

                document.getElementById("coordinates").value = JSON.stringify(bbox.epsg_3857);

                // Convert each coordinate to EPSG:4326
                transformed_coordinates = latLngs.map(function (coord) {
                    if (coord.hasOwnProperty('lat') && coord.hasOwnProperty('lng')) {
                        var latlng = L.latLng(coord.lat, coord.lng);
                        var projected = L.CRS.EPSG4326.project(latlng);
                        return [projected.y, projected.x];
                    }
                    else
                        return;
                });

                // Create the bounding box in EPSG:4326 format
                bbox.epsg_4326 = transformed_coordinates[0] + "," + transformed_coordinates[2];

                // Scale is ratio of real distance by EPSG:3857 distance
                var real_distance = map.distance(latLngs[0], latLngs[2])
                img_prop.scale_factor = real_distance / projected_distance;

                toggleOkButton();
            }
            catch (error) {
                alert("Erreur:", error.message);
            }
        });

        var crs_value = map.options.crs.code;
        document.getElementById("crs").value = crs_value;
    }
    catch (error) {
        alert("Erreur:", error.message);
    }
};

// Retrieve the custom config JSON from local storage
var customConfigJson = localStorage.getItem("customConfig");

Gp.Services.getConfig({
    customConfigFile: customConfigJson,
    timeOut: 20000,
    onSuccess: createMap,
    onFailure: function (e) {
        alert("Erreur lors de l'accès à la base de donnée:", e.message);
    }
});
