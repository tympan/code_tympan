/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

// handleResponseError.js

export async function handleResponseError(response, errorMessagePrefix) {
    // Check if the response is ok
    if (!response.ok) {
        // Clone the response to allow multiple reads of the response body
        let clone = response.clone();

        // Check if the status code indicates an error
        if (response.status >= 400) {
            // Read the response body as text
            const xmlText = await clone.text();

            // Parse the XML response
            const parser = new DOMParser();
            const xmlDoc = parser.parseFromString(xmlText, "text/xml");
            
            let errorMessage = "";

            // Check if the "body" element exists
            const serviceExceptionElement = xmlDoc.querySelector("ServiceException");
            if (serviceExceptionElement) {
                // Extract errorMessage from body if it exists
                errorMessage = serviceExceptionElement.textContent.trim();
            } else {
                // Check if "ows:ExceptionText" exists
                const exceptionTextElements = xmlDoc.getElementsByTagNameNS("http://www.opengis.net/ows/1.1", "ExceptionText");
                if (exceptionTextElements.length > 0) {
                // Extract errorMessage from "ows:ExceptionText" if it exists
                    errorMessage = exceptionTextElements[0].textContent.trim();
                } else {
                    // If neither "body" nor "ows:ExceptionText" exists, set errorMessage to "Erreur inconnue"
                    errorMessage = "Erreur inconnue";
                }
            }
            
            // Construct the error message with the specified prefix
            const prefixedErrorMessage = errorMessagePrefix + errorMessage;

            // Throw an error with the error message
            throw new Error(prefixedErrorMessage);
        }
    }
}
