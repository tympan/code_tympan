/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

// Function to enable/disable the OK button based on input value
function toggleOkButton() {
    // Get the input element
    var coordinatesInput = document.getElementById("coordinates");
    // Get the OK button
    var okButton = document.getElementById("okButton");

    if (coordinatesInput.value.trim() !== "") {
        okButton.disabled = false;
    } else {
        okButton.disabled = true;
    }
}

// Function to check the availability of the web service resource
function checkWebServiceAvailability() {
    if (navigator.onLine) {
        // Make an HTTP request to the resource
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "https://data.geopf.fr/wfs/ows?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetFeature&RESULTTYPE=hits&typeName=ELEVATION.CONTOUR.LINE:courbe&cql_filter=BBOX(geom,45.18568174474312,5.3673791885375985, 45.189160069755594,5.373816490173341)&crs=EPSG:4326", true);
        xhr.onload = function() {
            if (xhr.status >= 200 && xhr.status < 300) {
                // If the resource is available, ensure the checkbox is checked and no tooltip is needed
                var checkbox = document.getElementById("levelCurvesCheckbox");
                checkbox.checked = true;
                checkbox.disabled = false;
                var checkboxSection = document.getElementById("checkboxSection");
                checkboxSection.classList.remove("unavailable");
                checkboxSection.classList.add("available");
            } else {
                // If the resource is not available, uncheck and disable the checkbox and add a tooltip
                setCheckboxUnavailable();
            }
        };
        xhr.onerror = function() {
            // Handle network errors by disabling the checkbox and setting a tooltip
            setCheckboxUnavailable();
        };
        xhr.send();
    }
    else {
        // If the browser is offline, disable the checkbox
        setCheckboxUnavailable();
    }
}

// Function to disable the checkbox and show a tooltip
function setCheckboxUnavailable() {
    var checkbox = document.getElementById("levelCurvesCheckbox");
    checkbox.checked = false;
    checkbox.disabled = true;
    var checkboxSection = document.getElementById("checkboxSection");
    checkboxSection.classList.add("unavailable");
    checkboxSection.classList.remove("available");
}

// Function to show loading indicator
function showLoadingIndicator() {
  document.body.style.cursor = "wait";
  document.getElementById("overlay").style.display = "block";
}

// Function to hide loading indicator
function hideLoadingIndicator() {
  document.body.style.cursor = "default";
  document.getElementById("overlay").style.display = "none";
}
