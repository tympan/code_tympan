/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

//BEGIN SETUP
import { imageURLToBase64 } from "./imageManagement.js";
import { handleResponseError } from "./handleResponseError.js";

function output(message) {
    var output = document.getElementById("output");
    output.innerHTML = output.innerHTML + message + "\n";
}
// define image resolution
function computeHeight(x1, x2, y1, y2, IMGWidth) {
    var width = Math.abs(x2 - x1);
    var height = Math.abs(y2 - y1);
    if (width === 0 || height === 0)
        return NaN;
    var IMGHeight = IMGWidth * height / width;
    return IMGHeight;
}

window.onload = function () {
    // Call the function to check the availability of the web service resource
    checkWebServiceAvailability();

    var baseUrl = "ws://localhost:49327";

    output("Connexion au serveur WebSocket à l'adresse " + baseUrl + ".");
    var socket = new WebSocket(baseUrl);

    socket.onclose = function () {
        console.error("Web channel fermée");
    };
    socket.onerror = function (error) {
        console.error("Erreur web channel: " + error);
    };
    socket.onopen = function () {
        output("WebSocket connecté, mise en place de QWebChannel.");
        new QWebChannel(socket, function (channel) {
            // make core object accessible globally
            window.core = channel.objects.core;
            coordinates.innerHTML = core.coordinates;
            document.getElementById("okButton").onclick = handleOkButtonClick;
            document.getElementById("cancelButton").onclick = handleCancelButtonClick;
            core.sendCoordinates.connect(function (message) {
                output("Coordonnées reçues-" + core.coordinates + " : " + message);
            });
            output("Connecté au WebChannel, prêt à envoyer/revevoir des messages!");
        });
    }
}

async function handleOkButtonClick() {
    try {
        showLoadingIndicator();
        // Send coordinates to backend
        var input = document.getElementById("coordinates");
        var text = input.value;
        if (!text) {
            return;
        }
        output("Coordonnées envoyées: " + text);
        core.receiveCoordinates(text);

        // Send crs to backend
        input = document.getElementById("crs");
        text = input.value;
        if (!text) {
            return;
        }
        output("SCR envoyé: " + text);
        core.receiveCrs(text);

        // Send background image to backend
        var values = bbox.epsg_3857.split(",");
        var x1 = parseFloat(values[0]);
        var y1 = parseFloat(values[1]);
        var x2 = parseFloat(values[2]);
        var y2 = parseFloat(values[3]);
        img_prop.width = 2560;

        var IMGHeight = computeHeight(x1, x2, y1, y2, img_prop.width);
        const imageUrl = "https://data.geopf.fr/wms-r?LAYERS=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2&FORMAT=image/png&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&STYLES=&CRS=EPSG:3857&BBOX=" + bbox.epsg_3857 + "&WIDTH=" + img_prop.width + "&HEIGHT=" + IMGHeight;
        // const imageUrl = 'https://data.geopf.fr/wms-r?LAYERS=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2&FORMAT=image/png&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&STYLES=&CRS=EPSG:3857&BBOX=' + bbox.epsg_3857 + '&HEIGHT=' + IMGHeight;
        const base64Image = await imageURLToBase64(imageUrl);
        output("Sent background image");
        core.receiveImg(base64Image);

        // Get the checkbox element
        var checkbox = document.getElementById("levelCurvesCheckbox");
        // Check if the checkbox is checked
        if (checkbox.checked) {
            // Send level curves to backend
            // The following request returns too many level curves. BBOX filter problem on WFS request submited to IGN support the 2024.02.21
            // const levelCurvesUrl = 'https://data.geopf.fr/wfs/ows?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&typeName=ELEVATION.CONTOUR.LINE:courbe&BBOX='+bbox.epsg_4326+'&crs=EPSG:4326';
            // Build polygon in crs EPSG:4326
            values = bbox.epsg_4326.split(",");
            if (values.length != 4)
                return;
            x1 = values[0];
            y1 = values[1];
            x2 = values[2];
            y2 = values[3];
            var polygon_4326 = "(" + x1 + " " + y1 + ", " + x1 + " " + y2 + ", " + x2 + " " + y2 + ", " + x2 + " " + y1 + ", " + x1 + " " + y1 + ")";

            const nbHitsUrl = "https://data.geopf.fr/wfs/ows?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetFeature&RESULTTYPE=hits&typeName=ELEVATION.CONTOUR.LINE:courbe&cql_filter=INTERSECTS(geom, POLYGON(" + polygon_4326 + "))&crs=EPSG:4326";
            const hitResponse = await fetch(nbHitsUrl);
            // Check if the response is ok
            if (hitResponse.ok) {
                // Read the response body as text
                const xmlText = await hitResponse.text();

                // Parse the XML response
                const parser = new DOMParser();
                const xmlDoc = parser.parseFromString(xmlText, "text/xml");
                const message = xmlDoc.getElementsByTagName("wfs:FeatureCollection")[0].attributes[8].value;

                if (message > 3000) {    // 3000 is hit limit in ArcGIS
                    // Throw an error with the error message
                    throw new Error("Nombre de courbes de niveaux trop élevé, nombre de courbes: " + message);
                }
            }

            const levelCurvesUrl = "https://data.geopf.fr/wfs/ows?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&typeName=ELEVATION.CONTOUR.LINE:courbe&cql_filter=INTERSECTS(geom, POLYGON(" + polygon_4326 + "))&crs=EPSG:4326";
            // const levelCurvesUrl = 'https://data.geopf.fr/wfs/ows?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&typeName=ELEVATION.CONTOUR.LINE:courb&cql_filter=INTERSECTS(geom, POLYGON(' + polygon_4326 + '))&crs=EPSG:4326';

            const response = await fetch(levelCurvesUrl);
            await handleResponseError(response, "Erreur lors de la récupération des courbes de niveau: ");

            const xmlString = await response.text();
            output("Courbes de niveau envoyées");
            core.receiveLevelCurves(xmlString);
        }

        output("Facteur d'échelle envoyé: " + img_prop.scale_factor);
        core.receiveScaleFactor(img_prop.scale_factor);

        output("Largeur d'image envoyée: " + img_prop.width);
        core.receiveImageWidth(img_prop.width);

        // Emit signal indicating that all resources have been loaded successfully
        core.view_send_ok();

    } catch (error) {
        hideLoadingIndicator();
        alert(error.message);
        // Perform actions to revert to a stable state in the HTML form
        return null; // Return null or another value indicating failure    }
    }
}

function handleCancelButtonClick() {
    core.cancel();
}

//END SETUP
