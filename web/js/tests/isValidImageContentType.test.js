import { isValidImageContentType } from '../imageManagement.js';

describe('isValidImageContentType', () => {
    test('should return true for valid image content types', () => {
        // Valid image content types
        const validTypes = ['image/jpeg', 'image/png', 'image/gif', 'image/bmp'];

        // Test each valid content type
        validTypes.forEach(contentType => {
            expect(isValidImageContentType(contentType)).toBe(true);
        });
    });

    test('should return false for invalid image content types', () => {
        // Invalid image content types
        const invalidTypes = ['text/plain', 'application/json', 'image/svg+xml', ''];

        // Test each invalid content type
        invalidTypes.forEach(contentType => {
            expect(isValidImageContentType(contentType)).toBe(false);
        });
    });
});
