# Add the Google Test project as an external project.
ExternalProject_Add(GTest
  URL ${TYMPAN_3RDPARTY_GTEST}
  URL_MD5 ${TYMPAN_3RDPARTY_GTEST_MD5}
  # Configuration step. GTest doc said "Use shared (DLL) run-time lib even when
  # Google Test is built as static lib."
  CMAKE_ARGS -Dgtest_force_shared_crt=ON
  INSTALL_COMMAND ""
  BUILD_IN_SOURCE 0
)

ExternalProject_Get_Property(GTest SOURCE_DIR BINARY_DIR)
set(GTEST_SOURCE_DIR ${SOURCE_DIR})
set(GTEST_BINARY_DIR ${BINARY_DIR})
set(GTEST_LIB_DIR ${BINARY_DIR}/lib)
set(GTEST_INCLUDE_DIR ${SOURCE_DIR}/googletest/include)
set(GMOCK_INCLUDE_DIR ${SOURCE_DIR}/googlemock/include)
# Force to be an include directory to avoid warnings on a project you have no control over
include_directories(SYSTEM ${GTEST_INCLUDE_DIR} ${GMOCK_INCLUDE_DIR} ${GTEST_LIB_DIR})

message(STATUS ${CMAKE_BUILD_TYPE})

# Function to copy GTest libraries
function(copy_gtest_libraries target)
    if(WIN32 AND CMAKE_BUILD_TYPE MATCHES "Debug")
        add_custom_command(
            TARGET ${target} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BINARY_DIR}/lib/Debug/gtestd.lib ${GTEST_LIB_DIR}/Debug/gtest.lib)
        add_custom_command(
            TARGET ${target} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BINARY_DIR}/lib/Debug/gtest_maind.lib ${GTEST_LIB_DIR}/Debug/gtest_main.lib)
        add_custom_command(
            TARGET ${target} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BINARY_DIR}/lib/Debug/gmockd.lib ${GTEST_LIB_DIR}/Debug/gmock.lib)
        add_custom_command(
            TARGET ${target} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BINARY_DIR}/lib/Debug/gmock_maind.lib ${GTEST_LIB_DIR}/Debug/gmock_main.lib)
    endif()
endfunction()

# Call the function for GTest
copy_gtest_libraries(GTest)

if(TYMPAN_DEBUG_CMAKE)
  message(STATUS "INFO GTEST_SOURCE_DIR: " "${GTEST_SOURCE_DIR}" )
  message(STATUS "INFO GTEST_BINARY_DIR: " "${GTEST_BINARY_DIR}" )
  message(STATUS "INFO GTEST_LIB_DIR: " "${GTEST_LIB_DIR}" )
  message(STATUS "INFO GTEST_INCLUDE_DIR: " "${GTEST_INCLUDE_DIR}" )
  message(STATUS "INFO GMOCK_INCLUDE_DIR: " "${GMOCK_INCLUDE_DIR}" )
endif()
