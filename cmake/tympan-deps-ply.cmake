# Configure and install the C dependency for reading and writting PLY files
# PLY files are simple file for storing polygonal meshes.
#
# The PLY file format is documented here :
#     http://www.cs.virginia.edu/~gfx/Courses/2001/Advanced.spring.01/plylib/Ply.txt
# The following C library is used from the C++ side:
#     http://w3.impa.br/~diego/software/rply/
# The following Python module is used from the Python side:
#     https://github.com/dranjan/python-plyfile
# Since 4.3.0, python-plyfile, only used by tests, is installed via pip in the dev environment

set(RPLY_DIR "${CMAKE_SOURCE_DIR}/3rdparty/rply-1.1.4"
  CACHE PATH "The root directory for the RPLY source tree")
set(RPLY_SOURCES "${RPLY_DIR}/rply.c")
set(RPLY_INCLUDE_DIRECTORY "${RPLY_DIR}")
if(SYS_NATIVE_WIN)
    add_library(rply STATIC ${RPLY_SOURCES})
else(SYS_NATIVE_WIN)
    add_library(rply SHARED ${RPLY_SOURCES})
endif(SYS_NATIVE_WIN)
set(RPLY_LIB rply)

install(TARGETS ${RPLY_LIB} ARCHIVE DESTINATION lib
                           LIBRARY DESTINATION lib
                           RUNTIME DESTINATION . )
