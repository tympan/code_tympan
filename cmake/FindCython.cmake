# Find the Cython compiler.
#
# This code sets the following variables:
#
#  CYTHON_EXECUTABLE
#
# See also UseCython.cmake

#=============================================================================
# Copyright 2011 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================

# Use the Cython executable that lives next to the Python executable
# if it is a local installation.
if(WIN32)
  # Windows-specific configuration
  find_package(Python 3.10 EXACT REQUIRED COMPONENTS Interpreter Development)
else()
  # Configuration for non-Windows platforms (e.g., Linux, macOS)
  find_package(Python 3.10 REQUIRED COMPONENTS Interpreter Development)
endif()

if( Python_Interpreter_FOUND )
  get_filename_component( _python_path ${Python_EXECUTABLE} DIRECTORY )
  find_program( CYTHON_EXECUTABLE
    NAMES cython cython.bat
    HINTS ${_python_path}
    )
else()
  find_program( CYTHON_EXECUTABLE
    NAMES cython cython.bat cython3
    )
endif()


include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( Cython REQUIRED_VARS CYTHON_EXECUTABLE )

mark_as_advanced( CYTHON_EXECUTABLE )

