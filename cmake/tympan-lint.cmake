find_program(FLAKE8_BIN NAMES flake8)
find_program(BLACK_BIN NAMES black)
find_program(CYTHON_LINT_BIN NAMES cython-lint)

file(GLOB_RECURSE CYTHON_TYMPAN_SOURCES RELATIVE ${CMAKE_SOURCE_DIR} "python/tympan/*.pyx")

add_custom_target(lint
  WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
  COMMAND echo "Flake8"
  COMMAND ${FLAKE8_BIN} python tests tools
  COMMAND echo "Black"
  COMMAND ${BLACK_BIN} -l 110 --check python tests tools
  COMMAND echo "cython-lint"
  COMMAND ${CYTHON_LINT_BIN} --max-line-length 120 ${CYTHON_TYMPAN_SOURCES}
  COMMAND echo "clang-format"
  COMMAND python tools/check_format.py
)
