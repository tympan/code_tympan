/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TY_IGN_GEO_PROVIDER_H
#define TY_IGN_GEO_PROVIDER_H

#include <QObject>
#include <QQuickWindow>
#include <QQmlApplicationEngine>
#include "TYIGNLevelCurvesParser.h"
#include "TYIGNGeoPfMocker.h"

/**
 * @brief This class holds a singleton instance which is an entry point for accessing IGN Geoplatform
 * in order to create a georeferenced project.
 */
class TYIGNGeoProvider : public QObject
{
    Q_OBJECT

private:
    TYIGNGeoProvider();

    static TYIGNGeoProvider* singleton_;

    std::shared_ptr<QQmlApplicationEngine> _engine;

    /**
     * @brief True when web engine is intialized.
     *
     */
    bool _isInitialised;

    /**
     * @brief Set to false when displaying zone selection page.
     * When selection is validated, it is set to true in order to avoid multiple signal emissions.
     */
    bool _isSelectionValidated;

    QObject* _geographicData;

public:
    TYIGNGeoProvider(TYIGNGeoProvider& other) = delete;
    void operator=(const TYIGNGeoProvider&) = delete;

    static TYIGNGeoProvider* getInstance();
    const QString getLandtakeCoord();
    void setLandtakeCoord(const QString& text);
    const QString getScaleFactor();
    void setScaleFactor(const QString& text);
    const QString getImageWidth();
    void setImageWidth(const QString& text);
    bool saveImageToFile(const QString& filePath);

    void initEngine();

    bool isInitialised()
    {
        return _isInitialised;
    }

    void openModalWindow();
    void closeModalWindow();
    void buildLevelCurves(const OCoord3D& SIGCoords, const OBox& selectedZone, double scaleFactor);
    std::vector<double> coordinatesToDouble(const QString& coord);
    /**
     * @brief Mocks IGN Geoplatform web services when they are down
     *
     * @return \c true if the mock succeded else returns \c false
     */
    bool mockGeoPf();

    void resetGeographicData();

    std::shared_ptr<TYIGNLevelCurvesParser> _parser;
    std::shared_ptr<TYIGNGeoPfMocker> _mocker;

signals:
    void geoProjectCreationRequested();

private slots:
    void handleCancelRequest();
    void handleOKRequest();
    void resetEngine();
};

#endif // TY_IGN_GEO_PROVIDER_H
