/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

#include "TYIGNGeoPfMocker.h"
#include "TYIGNGeoProvider.h"

bool TYIGNGeoPfMocker::mockGeoPf()
{
    bool ret = true;
    ret &= setLandtakeCoordinates();
    ret &= setImageProperties();
    ret &= loadLevelCurves();
    return ret;
}
bool TYIGNGeoPfMocker::setLandtakeCoordinates()
{
    std::ifstream coordinatesFile("C:/projects/tympan/issues/530 - Mock IGN/landtake_coordinates.txt");
    if (!coordinatesFile.is_open())
    {
        std::cerr << "Failed to open the file landtake_coordinates.txt. Put it in "
                     "C:/projects/tympan/issues/530 - Mock IGN/"
                  << std::endl;
        return false;
    }

    std::string line;
    if (std::getline(coordinatesFile, line))
    {
        TYIGNGeoProvider::getInstance()->setLandtakeCoord(QString::fromStdString(line));
    }
    else
    {
        std::cerr << "Failed to read from the file." << std::endl;
        return false;
    }

    return true;
}
bool TYIGNGeoPfMocker::setImageProperties()
{
    std::ifstream imgPropsFile("C:/projects/tympan/issues/530 - Mock IGN/img_props.txt");
    if (!imgPropsFile.is_open())
    {
        std::cerr
            << "Failed to open the file img_props.txt. Put it in C:/projects/tympan/issues/530 - Mock IGN/"
            << std::endl;
        return false;
    }

    int lineNumber = 0;
    std::string line;
    while (std::getline(imgPropsFile, line) && lineNumber < 2)
    {
        if (lineNumber == 0)
        {
            TYIGNGeoProvider::getInstance()->setImageWidth(QString::fromStdString(line));
        }
        else if (lineNumber == 1)
        {
            TYIGNGeoProvider::getInstance()->setScaleFactor(QString::fromStdString(line));
        }
        ++lineNumber;
    }

    imgPropsFile.close();
    return true;
}
bool TYIGNGeoPfMocker::loadLevelCurves()
{
    TYIGNGeoProvider::getInstance()->_parser->loadXmlFile(
        "C:/projects/tympan/issues/530 - Mock IGN/level_curves.xml");
    return true;
}
