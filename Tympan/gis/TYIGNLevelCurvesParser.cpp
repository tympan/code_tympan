/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

// TYIGNLevelCurvesParser.cpp
#include "TYIGNLevelCurvesParser.h"
#include "Tympan/core/logging.h"

TYIGNLevelCurvesParser::TYIGNLevelCurvesParser(QObject* parent)
    : QObject(parent), m_isXmlLoaded(false), m_hasRetrievedLevelCurves(false)
{
}

void TYIGNLevelCurvesParser::reset()
{
    m_hasRetrievedLevelCurves = false;
    m_isXmlLoaded = false;
    m_xmlDocument.clear();
    m_ct.clear();
}

bool TYIGNLevelCurvesParser::loadXmlFile(const QString& filePath)
{
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Failed to open XML file for reading:" << file.errorString();
        return false;
    }

    QDomDocument newDocument;
    if (!newDocument.setContent(&file))
    {
        qDebug() << "Failed to set XML content from file";
        file.close();
        return false;
    }

    file.close();

    // Copy the content of the newly loaded document to the member variable
    m_xmlDocument = newDocument;
    m_isXmlLoaded = true;
    m_hasRetrievedLevelCurves = true;
    return true;
}

void TYIGNLevelCurvesParser::parseXmlData(const OCoord3D& SIGCoords, const OBox& selectedZone,
                                          double scaleFactor)
{
    if (!m_hasRetrievedLevelCurves)
    {
        qDebug() << "Level curves have not been retrieved.";
        return;
    }

    if (!m_isXmlLoaded)
    {
        qDebug() << "XML document is not loaded.";
        return;
    }

    buildCTFromXMLFile(SIGCoords);
    simplifyPolylines();
    createLevelCurvesFromCT(SIGCoords, selectedZone, scaleFactor);
}

bool TYIGNLevelCurvesParser::buildXmlDocument(const QString& text)
{
    bool ret = false;
    if (text.isEmpty())
    {
        m_hasRetrievedLevelCurves = false;
    }
    else
    {
        m_hasRetrievedLevelCurves = true;
        QDomDocument newDocument;
        if (!newDocument.setContent(text))
        {
            qDebug() << "Failed to set XML content from text";
        }
        else
        {
            // Copy the content of the newly constructed document to the member variable
            m_xmlDocument = newDocument;
            m_isXmlLoaded = true;
            ret = true;
        }
    }
    return ret;
}

bool TYIGNLevelCurvesParser::buildCTFromXMLFile(const OCoord3D& SIGCoords)
{
    bool ret = true;
    QDomElement root = m_xmlDocument.documentElement();
    QDomNodeList courbeList = root.elementsByTagName("ELEVATION.CONTOUR.LINE:courbe");

    OMessageManager::get()->debug(QString("Number of courbe elements found: %1").arg(courbeList.size()));

    for (int i = 0; i < courbeList.size(); ++i)
    {
        QDomElement courbe = courbeList.at(i).toElement();
        buildPolylineFromCourbe(courbe, SIGCoords);
    }

    return true;
}

bool TYIGNLevelCurvesParser::buildPolylineFromCourbe(const QDomElement& courbe, const OCoord3D& SIGCoords)
{
    QString id = courbe.attribute("gml:id");
    OMessageManager::get()->debug(QString("Courbe ID: %1").arg(id));

    double altitude = courbe.firstChildElement("ELEVATION.CONTOUR.LINE:altitude").text().toDouble();
    OMessageManager::get()->debug(QString("Altitude: %1").arg(altitude));

    AltimetryInfo altiInfo(id, altitude);

    QDomElement geom =
        courbe.firstChildElement("ELEVATION.CONTOUR.LINE:geom").firstChildElement("gml:LineString");
    QString srsName = geom.attribute("srsName");
    OMessageManager::get()->debug(QString("SRS Name: %1").arg(srsName));

    QString posList = geom.firstChildElement("gml:posList").text();
    QStringList points = posList.split(" ");
    OMessageManager::get()->debug(QString("Number of points: %1").arg(points.size() / 2));

    Point currentPoint;
    std::vector<Point> CGALpoints;
    int currentIndex = 0;

    while (currentIndex < points.size())
    {
        currentPoint = getCGALPoint(points, currentIndex, SIGCoords, altitude);
        CGALpoints.push_back(currentPoint);
        currentIndex += 2;
    }

    auto constraint_id = m_ct.insert_constraint(CGALpoints);

    // Associate AltimetryInfo with the constraint
    // Wrap the Constraint_id
    ConstraintIdWrapper wrapped_id(constraint_id);
    m_constraintToAltimetryInfoMap[wrapped_id] = altiInfo;

    return true;
}

size_t TYIGNLevelCurvesParser::simplifyPolylines()
{
    size_t nb_removed_vertices = 0;
    double dStopParam = 0.25;
    nb_removed_vertices = PS::simplify(m_ct, Cost(), Stop(dStopParam));
    return nb_removed_vertices;
}

bool TYIGNLevelCurvesParser::createLevelCurvesFromCT(const OCoord3D& SIGCoords, const OBox& selectedZone,
                                                     double scaleFactor)
{
    bool ret = true;
    OBox selectedZoneTympanCoords{selectedZone};
    selectedZoneTympanCoords.Translate(OVector3D() - OVector3D(SIGCoords));

    for (Constraint_iterator constraint_it = m_ct.constraints_begin();
         constraint_it != m_ct.constraints_end(); ++constraint_it)
    {
        Constraint_id constraint_id = *constraint_it;
        // Wrap the Constraint_id
        ConstraintIdWrapper wrapped_id(constraint_id);

        auto it = getConstraintToAltimetryInfoMap().find(wrapped_id);
        if (it == getConstraintToAltimetryInfoMap().end())
        {
            return false;
        }

        const TYIGNLevelCurvesParser::AltimetryInfo& info = it->second;
        QString id = info.id;
        double altitude = info.altitude;

        TYPoint currentPoint;
        auto vit = m_ct.points_in_constraint_begin(constraint_id);

        while (vit != m_ct.points_in_constraint_end(constraint_id))
        {
            // Search for first point of level curve inside selected zone
            bool firstPointFound = false;
            while (!firstPointFound && vit != m_ct.points_in_constraint_end(constraint_id))
            {
                currentPoint = TYPoint{vit->x(), vit->y(), altitude};
                // getPoint(points, currentIndex, SIGCoords, altitude);
                firstPointFound = selectedZoneTympanCoords.isInside2D(currentPoint);
                vit++;
            }
            // If a first point is found
            if (firstPointFound)
            {
                TYTabPoint listPoints;
                // Add first point to level curve
                TYPoint fistPointTympanCoords{currentPoint._x * scaleFactor, currentPoint._y * scaleFactor,
                                              currentPoint._z};
                listPoints.push_back(fistPointTympanCoords);
                // Then add all following points inside zone to the level curve
                bool isPointInsideZone = true;
                while (isPointInsideZone && vit != m_ct.points_in_constraint_end(constraint_id))
                {
                    currentPoint = TYPoint{vit->x(), vit->y(), altitude};
                    isPointInsideZone = selectedZoneTympanCoords.isInside2D(currentPoint);
                    if (isPointInsideZone)
                    {
                        TYPoint newPointTympanCoords{currentPoint._x * scaleFactor,
                                                     currentPoint._y * scaleFactor, currentPoint._z};
                        listPoints.push_back(newPointTympanCoords);
                    }
                    vit++;
                }
                // If there are at least two points then create the level curve
                if (listPoints.size() >= 2)
                {
                    LPTYCourbeNiveau crb = new TYCourbeNiveau();
                    crb->setName(id);
                    crb->setAltitude(altitude);
                    crb->setListPoints(listPoints);
                    // Emit signal with the created TYCourbeNiveau object
                    emit courbeNiveauCreated(crb);
                }
            }
        }
    }
    return ret;
}

TYPoint TYIGNLevelCurvesParser::convertLatLonToWebMercator(double latitude, double longitude)
{
    TYPoint result;

    const double earthRadius = 6378137.0; // Earth radius in meters
    const double originShift = 2.0 * M_PI * earthRadius / 2.0;

    result._x = longitude * originShift / 180.0;
    result._y = log(tan((90.0 + latitude) * M_PI / 360.0)) / (M_PI / 180.0);
    result._y = result._y * originShift / 180.0;

    return result;
}

TYPoint TYIGNLevelCurvesParser::getPoint(const QStringList& points, int index, const OCoord3D& SIGCoords,
                                         double altitude)
{
    double latitude = points[index].toDouble();
    double longitude = points[index + 1].toDouble(); // Longitude comes after latitude in the XML

    // Project each point to EPSG:3857
    TYPoint projectedPoint = convertLatLonToWebMercator(latitude, longitude);
    projectedPoint._x -= SIGCoords._x;
    projectedPoint._y -= SIGCoords._y;
    projectedPoint._z = altitude;

    return projectedPoint;
}

Point TYIGNLevelCurvesParser::getCGALPoint(const QStringList& points, int index, const OCoord3D& SIGCoords,
                                           double altitude)
{
    TYPoint tyPoint = getPoint(points, index, SIGCoords, altitude);
    Point ret{tyPoint._x, tyPoint._y};
    return ret;
}
