/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "TYGeographicData.h"

TYGeographicData::TYGeographicData(QObject* parent) : QObject(parent) {}

void TYGeographicData::setLandtakeCoordinates(const QString& text)
{
    if (text != m_landtake_coordinates)
    {
        m_landtake_coordinates = text;
        emit landtakeCoordinatesChanged();
    }
}

QString TYGeographicData::crs() const
{
    return m_crs;
}

void TYGeographicData::setCrs(const QString& text)
{
    if (text != m_crs)
    {
        m_crs = text;
        emit crsChanged();
    }
}

QString TYGeographicData::landtakeCoordinates() const
{
    return m_landtake_coordinates;
}

void TYGeographicData::setBackgroundImg(const QString& base64Image)
{
    if (base64Image != m_base64Image)
    {
        m_base64Image = base64Image;
        m_image = QImage();
        emit backgroundImgChanged();
    }
}

QString TYGeographicData::backgroundImg() const
{
    return m_base64Image;
}

QString TYGeographicData::levelCurves() const
{
    return m_level_curves;
}

void TYGeographicData::setLevelCurves(const QString& text)
{
    if (text != m_level_curves)
    {
        m_level_curves = text;
        emit levelCurvesChanged();
    }
}

QString TYGeographicData::scaleFactor() const
{
    return m_scale_factor;
}

void TYGeographicData::setScaleFactor(const QString& text)
{
    if (text != m_scale_factor)
    {
        m_scale_factor = text;
        emit scaleFactorChanged();
    }
}

QString TYGeographicData::imageWidth() const
{
    return m_image_width;
}

void TYGeographicData::setImageWidth(const QString& text)
{
    if (text != m_image_width)
    {
        m_image_width = text;
        emit imageWidthChanged();
    }
}

QImage TYGeographicData::getImage() const
{
    if (m_image.isNull() && !m_base64Image.isEmpty())
    {
        m_image = buildImageFromBase64(m_base64Image);
    }
    return m_image;
}

bool TYGeographicData::saveImageToFile(const QString& filePath)
{
    QImage image = getImage();
    if (image.isNull())
    {
        qWarning() << "Image is null, cannot save.";
        return false;
    }

    // Save the image to a PNG file
    return image.save(filePath, "PNG");
}

QImage TYGeographicData::buildImageFromBase64(const QString& base64Image) const
{
    // Decode base64 image string to byte array
    QByteArray imageData = QByteArray::fromBase64(base64Image.toUtf8());

    // Load byte array into a QImage
    QImage image;
    image.loadFromData(imageData);

    return image;
}
