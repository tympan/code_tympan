/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TYIGNGEOPFMOCKER_H
#define TYIGNGEOPFMOCKER_H

/**
 * @brief Holds the responsability of the MOCK_IGN mode.
 * This mode si reserved for developer when the IGN web services are down or not reliable.
 * The developer has to :
 *  - set option TYMPAN_MOCK_IGN to on in tympan-build-options.cmake
 *  - put the following files in the folder "c:/projects/tympan/issues/530 - Mock IGN"
 *      - image.png, the background image
 *      - level_curves.xml, the file sent by IGN web service containing level curves in XML format
 *      - landtake_coordinates.txt, the coordinates of the selected zone in EPSG:3857
 *      - img_props.txt, this text file holds image width on the first line and scale factor on the second
 *
 * The resources must be consistent and have to be built when  IGN Geoplatform is on.
 *
 */
class TYIGNGeoPfMocker
{
public:
    bool mockGeoPf();
    bool setLandtakeCoordinates();
    bool setImageProperties();
    bool loadLevelCurves();
};
#endif // TYIGNGEOPFMOCKER_H
