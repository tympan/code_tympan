/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

// TYIGNLevelCurvesParser.h
#ifndef TYIGNLEVELCURVESPARSER_H
#define TYIGNLEVELCURVESPARSER_H

#include <QObject>
#include <QtXml>
#include <iostream>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Constrained_triangulation_plus_2.h>
#include <CGAL/Polyline_simplification_2/simplify.h>
#include <CGAL/Polyline_simplification_2/Squared_distance_cost.h>
#include <CGAL/Unique_hash_map.h>

namespace PS = CGAL::Polyline_simplification_2;
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef PS::Vertex_base_2<K> Vb;
typedef CGAL::Constrained_triangulation_face_base_2<K> Fb;
typedef CGAL::Triangulation_data_structure_2<Vb, Fb> TDS;
typedef CGAL::Constrained_Delaunay_triangulation_2<K, TDS, CGAL::Exact_predicates_tag> CDT;
typedef CGAL::Constrained_triangulation_plus_2<CDT> CT;
typedef CT::Point Point;
typedef CT::Constraint_id Constraint_id;

class ConstraintIdWrapper
{
public:
    std::size_t id;

    ConstraintIdWrapper(const Constraint_id& constraint_id)
    {
        static const size_t shift = (size_t)log2(1 + sizeof(Constraint_id));
        id = (std::size_t)(constraint_id.second) >> shift;
    }

    bool operator==(const ConstraintIdWrapper& other) const
    {
        return id == other.id;
    }
};

// Custom hash function for ConstraintIdWrapper
namespace std
{
template <> struct hash<ConstraintIdWrapper>
{
    std::size_t operator()(const ConstraintIdWrapper& wrapper) const
    {
        return std::hash<std::size_t>()(wrapper.id);
    }
};
} // namespace std

typedef CT::Constraint_iterator Constraint_iterator;
typedef CT::Vertices_in_constraint_iterator Vertices_in_constraint_iterator;
typedef CT::Points_in_constraint_iterator Points_in_constraint_iterator;
typedef PS::Stop_below_count_ratio_threshold Stop;
typedef PS::Scaled_squared_distance_cost Cost;
#include "Tympan/models/business/topography/TYCourbeNiveau.h"

/**
 * @brief Holds the business logic of parsing the response of level curve
 * IGN web service, in order to send signals to create level curves in Tympan site
 *
 */
class TYIGNLevelCurvesParser : public QObject
{
    Q_OBJECT
public:
    struct AltimetryInfo
    {
        QString id;
        double altitude;

        // Default constructor
        AltimetryInfo() : id(""), altitude(0.0) {}

        // Constructor
        AltimetryInfo(const QString& idVal, double altitudeVal) : id(idVal), altitude(altitudeVal) {}
    };

    explicit TYIGNLevelCurvesParser(QObject* parent = nullptr);

    /**
     * \fn bool isXmlLoaded() const
     * \brief Getter for the isXmlLoaded flag.
     * \return The isXmlLoaded flag.
     */
    bool isXmlLoaded() const
    {
        return m_isXmlLoaded;
    }

    /**
     * \fn bool hasRetrievedLevelCurves() const
     * \brief Getter for the hasRetrievedLevelCurves flag.
     * \return The hasRetrievedLevelCurves flag.
     */
    bool hasRetrievedLevelCurves() const
    {
        return m_hasRetrievedLevelCurves;
    }

    /**
     * \fn QDomDocument getXmlDocument() const
     * \brief Getter for the xmlDocument.
     * \return The xmlDocument.
     */
    QDomDocument getXmlDocument() const
    {
        return m_xmlDocument;
    }

    /**
     * \fn const CT& getCT() const
     * \brief Getter for the CT member.
     * \return A constant reference to the CT member.
     */
    const CT& getCT() const
    {
        return m_ct;
    }

    /**
     * \fn std::unordered_map<CT::Constraint_id, AltimetryInfo>& getConstraintToAltimetryInfoMap()
     * For test purpose only
     * \brief Getter for the constraint to AltimetryInfo map.
     * \return A constant reference to the map associating constraints with AltimetryInfo objects.
     */
    std::unordered_map<ConstraintIdWrapper, AltimetryInfo>& getConstraintToAltimetryInfoMap()
    {
        return m_constraintToAltimetryInfoMap;
    }

    /*
     * @brief Reset parser
     */
    void reset();

    bool loadXmlFile(const QString& filePath);
    void parseXmlData(const OCoord3D& SIGCoords, const OBox& selectedZone, double scaleFactor);

    /*
     * @brief Build an XML document from text
     *
     * @param text The text used to instantiate XML document
     *
     * @return bool <code>true</code> if succeeds;
     *              <code>false</code> if not.
     */
    bool buildXmlDocument(const QString& text); // New method to build XML document from text

    /**
     * @brief Build constrained triangulation from IGN XML file containing level curves
     * @param SIGCoords The coordinates of the origin of the zone in Tympan coordinate system
     * @return bool <code>true</code> if succeeds;
     *              <code>false</code> if not.
     */
    bool buildCTFromXMLFile(const OCoord3D& SIGCoords);

    /*
     * @brief Build a polyline from an IGN level curve
     * The polyligne is added as a new constraint to the constrained triangulation class member m_ct
     *
     * @param courbe The IGN courbe element in xml file
     *
     * @param SIGCoords The coordinates of the origin of the zone in Tympan coordinate system
     *
     * @return bool <code>true</code> if succeeds;
     *              <code>false</code> if not.
     */
    bool buildPolylineFromCourbe(const QDomElement& courbe, const OCoord3D& SIGCoords);

    /*
     * @brief Simplify polyines defined as constraints in the constrained triangulation class member m_ct
     *
     * @return size_t The number of removed vertices
     */
    size_t simplifyPolylines();

    /**
     * @brief Create level curves from CGAL constrained triangulation
     *
     * @param SIGCoords The coordinates of the origin of the zone in Tympan coordinate system
     * @param selectedZone Zone selected by user in IGN coordinate system
     * @param scaleFactor Scale factor depending from latitude which preserves geometry at the best when
     * projecting in Tympan 2D coordinate system
     *
     * @return bool <code>true</code> if succeeds;
     *              <code>false</code> if not.
     */
    bool createLevelCurvesFromCT(const OCoord3D& SIGCoords, const OBox& selectedZone, double scaleFactor);

    static TYPoint convertLatLonToWebMercator(double latitude, double longitude);

    /*
     * @brief Get the point of given index in the points list in Tympan coordinate system
     *
     * @param points The list of the coordinates of the points returned by IGN WFS service for level curves.
     * These coordinates are in EPSG::4326 with latitude longitude for each point. All coordinates are
     * separated by space
     *
     * @param index The index of the point times 2, as there are 2 coordinates by point
     *
     * @param SIGCoords The coordinates of the origin of the zone in Tympan coordinate system
     *
     * @param altitude The altitude of the level curve
     *
     * @return bool <code>true</code> if succeeds;
     *              <code>false</code> if not.
     */
    static TYPoint getPoint(const QStringList& points, int index, const OCoord3D& SIGCoords, double altitude);

    static Point getCGALPoint(const QStringList& points, int index, const OCoord3D& SIGCoords,
                              double altitude);

signals:
    void courbeNiveauCreated(LPTYCourbeNiveau courbeNiveau);

public slots:

private:
    QDomDocument m_xmlDocument;
    bool m_isXmlLoaded;
    bool m_hasRetrievedLevelCurves;

    CT m_ct;
    // Map to keep association between constraints and AltimetryInfo objects
    std::unordered_map<ConstraintIdWrapper, AltimetryInfo> m_constraintToAltimetryInfoMap;
};

#endif // TYIGNLEVELCURVESPARSER_H
