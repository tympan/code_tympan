/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "TYIGNGeoProvider.h"
#include "TYWebSocketTransport.h"
#include "TYGeographicData.h"

#include <QQmlComponent>
#include <QQuickItem>
#include <QQmlContext>
#include <QQuickWindow>

TYIGNGeoProvider* TYIGNGeoProvider::singleton_ = nullptr;

TYIGNGeoProvider::TYIGNGeoProvider() : _geographicData(nullptr)
{
    _engine = std::make_shared<QQmlApplicationEngine>();
    _parser = std::make_shared<TYIGNLevelCurvesParser>();
    _mocker = std::make_shared<TYIGNGeoPfMocker>();
    _isInitialised = false;
    _isSelectionValidated = false;
}

TYIGNGeoProvider* TYIGNGeoProvider::getInstance(/*QObject* parent*/)
{
    if (singleton_ == nullptr)
    {
        singleton_ = new TYIGNGeoProvider();
    }
    return singleton_;
}

void TYIGNGeoProvider::resetEngine()
{
    // If the Window is not hidden, then it has been maximised or minimised and the engine must not be reset
    QQuickWindow* window = qobject_cast<QQuickWindow*>(sender());
    if (window && window->visibility() != QWindow::Hidden)
    {
        return;
    }
    // Disconnect any existing connections
    // Clean up any resources related to the previous QML content
    _engine->clearComponentCache();
    _engine->trimComponentCache(); // Also trim the component cache
    _engine->collectGarbage();     // Collect any garbage

    // Remove all objects from the rootObjects list
    QList<QObject*> rootObjects = _engine->rootObjects();
    for (QObject* obj : rootObjects)
    {
        // Destroy the object
        obj->deleteLater();
    }

    // Clear the rootObjects list
    rootObjects.clear();
    // Additionally, you may want to reset other variables or states
    _isInitialised = false;
    _isSelectionValidated = false;
    _geographicData = nullptr; // Reset geographic data object
}

void TYIGNGeoProvider::initEngine()
{
    Q_INIT_RESOURCE(qml);
    // Register types to use them in QML
    qmlRegisterType<TYWebSocketTransport>("codetympan.org", 1, 0, "WebSocketTransport");
    qmlRegisterType<TYGeographicData>("codetympan.org", 1, 0, "GeographicData");
    _engine->load(QUrl(QStringLiteral("qrc:/qml/TYZoneSelectPage.qml")));
    QList<QObject*> rootObjects = _engine->rootObjects();
    if (!rootObjects.isEmpty())
    {
        QObject* mainComponent = rootObjects.at(0);
#ifdef QT_DEBUG
        mainComponent->setProperty("debugMode", true);
#else
        mainComponent->setProperty("debugMode", false);
#endif
        QObject* geographicData = mainComponent->findChild<QObject*>("geographicData");
        if (geographicData)
        {
            _geographicData = geographicData;
            resetGeographicData();
            QObject::connect(geographicData, SIGNAL(cancelRequested()), this, SLOT(handleCancelRequest()));
            QObject::connect(geographicData, SIGNAL(oKRequested()), this, SLOT(handleOKRequest()));
        }
    }

    _isInitialised = true;
    _isSelectionValidated = false;
}

void TYIGNGeoProvider::openModalWindow()
{
    // Get the root QML object
    _isSelectionValidated = false;
    if (!_isInitialised)
    {
        initEngine();
    }
    QList<QObject*> rootObjects = _engine->rootObjects();
    if (!rootObjects.isEmpty())
    {
        QObject* mainComponent = rootObjects.at(0);
        QQuickWindow* window = qobject_cast<QQuickWindow*>(mainComponent);
        connect(window, &QQuickWindow::visibilityChanged, TYIGNGeoProvider::getInstance(),
                &TYIGNGeoProvider::resetEngine);
        window->show();
    }
    _parser->reset();
}

void TYIGNGeoProvider::closeModalWindow()
{
    QList<QObject*> rootObjects = _engine->rootObjects();
    if (!rootObjects.isEmpty())
    {
        QObject* mainComponent = rootObjects.at(0);
        QQuickWindow* window = qobject_cast<QQuickWindow*>(mainComponent);
        window->close();

        qDebug() << "Closing Window object.";
    }
}

const QString TYIGNGeoProvider::getLandtakeCoord()
{
    QString ret{};
    if (_geographicData)
    {
        ret = _geographicData->property("landtake_coordinates").toString();
    }
    return ret;
}

void TYIGNGeoProvider::setLandtakeCoord(const QString& text)
{
    if (_geographicData)
    {
        _geographicData->setProperty("landtake_coordinates", text);
    }
}

const QString TYIGNGeoProvider::getScaleFactor()
{
    QString ret{};
    if (_geographicData)
    {
        ret = _geographicData->property("scale_factor").toString();
    }
    return ret;
}

void TYIGNGeoProvider::setScaleFactor(const QString& text)
{
    if (_geographicData)
    {
        _geographicData->setProperty("scale_factor", text);
    }
}

const QString TYIGNGeoProvider::getImageWidth()
{
    QString ret{};
    if (_geographicData)
    {
        ret = _geographicData->property("image_width").toString();
    }
    return ret;
}

void TYIGNGeoProvider::setImageWidth(const QString& text)
{
    if (_geographicData)
    {
        _geographicData->setProperty("image_width", text);
    }
}

bool TYIGNGeoProvider::saveImageToFile(const QString& filePath)
{
    bool ret = false;
    if (_geographicData)
    {
        TYGeographicData* geoData = dynamic_cast<TYGeographicData*>(_geographicData);
        if (geoData != nullptr)
        {
            ret = geoData->saveImageToFile(filePath);
        }
    }
    return ret;
}

void TYIGNGeoProvider::buildLevelCurves(const OCoord3D& SIGCoords, const OBox& selectedZone,
                                        double scaleFactor)
{
    _parser->parseXmlData(SIGCoords, selectedZone, scaleFactor);
}

std::vector<double> TYIGNGeoProvider::coordinatesToDouble(const QString& coord)
{
    QStringList listCoords = coord.split(",");
    const int len = listCoords.size();
    std::vector<double> coordsDouble = std::vector<double>(len);
    for (int i = 0; i < len; i++)
    {
        listCoords[i].remove("\"");
        coordsDouble[i] = listCoords[i].toDouble();
    }
    return coordsDouble;
}

bool TYIGNGeoProvider::mockGeoPf()
{
    bool ret;
    ret = _mocker->mockGeoPf();
    if (ret)
    {
        emit geoProjectCreationRequested();
    }
    return ret;
}

void TYIGNGeoProvider::resetGeographicData()
{
    _geographicData->setProperty("landtake_coordinates", QString(""));
    _geographicData->setProperty("background_img", QString(""));
    _geographicData->setProperty("crs", QString(""));
    _geographicData->setProperty("level_curves", QString(""));
    _geographicData->setProperty("scale_factor", QString(""));
    _geographicData->setProperty("image_width", QString(""));
}

void TYIGNGeoProvider::handleCancelRequest()
{
    // Handle the cancel request here
    // For example, emit a signal to indicate that the cancel action was triggered
    qDebug() << "Geo project creation cancelled";
    _engine->disconnect();
}

void TYIGNGeoProvider::handleOKRequest()
{
    if (!_isSelectionValidated)
    {
        _isSelectionValidated = true;
        qDebug() << "Geo project creation validated";
        _parser->buildXmlDocument(_geographicData->property("level_curves").toString());
        _engine->disconnect();
        emit geoProjectCreationRequested();
    }
}
