/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TYGEOGRAPHICDATA_H
#define TYGEOGRAPHICDATA_H

#include <QImage>
#include <QObject>
#include <qqml.h>
#include <QXmlStreamWriter>
#include <QFile>
#include <QTextStream>

class TYGeographicData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString landtake_coordinates READ landtakeCoordinates WRITE setLandtakeCoordinates NOTIFY
                   landtakeCoordinatesChanged)
    Q_PROPERTY(QString background_img READ backgroundImg WRITE setBackgroundImg NOTIFY backgroundImgChanged)
    Q_PROPERTY(QString crs READ crs WRITE setCrs NOTIFY crsChanged)
    Q_PROPERTY(QString level_curves READ levelCurves WRITE setLevelCurves NOTIFY levelCurvesChanged)
    Q_PROPERTY(QString scale_factor READ scaleFactor WRITE setScaleFactor NOTIFY scaleFactorChanged)
    Q_PROPERTY(QString image_width READ imageWidth WRITE setImageWidth NOTIFY imageWidthChanged)
    QML_ELEMENT
public:
    explicit TYGeographicData(QObject* parent = nullptr);

    void setLandtakeCoordinates(const QString& text);
    QString crs() const;
    void setCrs(const QString& text);
    QString landtakeCoordinates() const;
    void setBackgroundImg(const QString& base64Image);
    QString backgroundImg() const;
    QString levelCurves() const;
    void setLevelCurves(const QString& text);
    QImage getImage() const;
    QString scaleFactor() const;
    void setScaleFactor(const QString& text);
    QString imageWidth() const;
    void setImageWidth(const QString& text);
    Q_INVOKABLE bool saveImageToFile(const QString& filePath);

signals:
    void landtakeCoordinatesChanged();
    void crsChanged();
    void backgroundImgChanged();
    void levelCurvesChanged();
    void scaleFactorChanged();
    void imageWidthChanged();

private:
    QString m_landtake_coordinates;
    QString m_crs;
    QString m_base64Image;
    QString m_level_curves;
    mutable QImage m_image;
    QString m_scale_factor;
    QString m_image_width;

    QImage buildImageFromBase64(const QString& base64Image) const;
};

#endif // TYGEOGRAPHICDATA_H
