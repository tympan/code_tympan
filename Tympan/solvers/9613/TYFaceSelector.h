/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TYFACESELECTOR__
#define __TYFACESELECTOR__

#include "Tympan/models/solver/TYFaceSelectorInterface.h"
#include "Tympan/solvers/TYSolverDefines.h"

class TYTrajet;

/**
 * \brief Building class of the faces list
 */
class TYFaceSelector : public TYFaceSelectorInterface
{
public:
    TYFaceSelector();
    virtual ~TYFaceSelector();

    /**
     * \brief Build the array of intersections
     * \param tabIntersect Array of intersections
     * \param tabPolygon Array of triangles of the acoustic problem
     * \param rayon Ray path
     * \param sourceVolumeId volume_id of source when first ray point is a source
     */
    virtual void selectFaces(std::deque<TYSIntersection>& tabIntersect,
                             const std::vector<TYStructSurfIntersect>& tabPolygon, const OSegment3D& rayon,
                             const string& sourceVolumeId);

protected:
private:
    bool buildPlans(TYSPlan* plan, const OSegment3D& rayon);
    bool CalculSegmentCoupe(const TYStructSurfIntersect& FaceCourante, TYSIntersection& Intersect,
                            OPoint3D& pt1, OPoint3D& pt2, OPoint3D& pt3, const int& indice) const;
    void reorder_intersect(std::deque<TYSIntersection>& tabIntersect); //!< put infrastructure faces on top
};

#endif // __TYFACESELECTOR_9613__
