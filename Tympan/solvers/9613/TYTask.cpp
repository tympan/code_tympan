/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "TYAcousticModel.h"
#include "Tympan/solvers/9613/TYAcousticPathFinder.h"
#include "TYTask.h"

TYTask::TYTask(const tympan::nodes_pool_t& nodes, const tympan::triangle_pool_t& triangles,
               const tympan::material_pool_t& materials, int nNbTrajets)
    : _nNbTrajets(nNbTrajets), _nodes(nodes), _triangles(triangles), _materials(materials)
{
}

TYTask::~TYTask() {}

void TYTask::main()
{
    TabPoint3D ptsTop;
    TabPoint3D ptsLeft;
    TabPoint3D ptsRight;

    // Construction du rayon SR
    OSegment3D rayon;
    getRayonFromTrajet(rayon);
    // _trajet.getPtSetPtRfromOSeg3D(rayon);

    // On selectionne les faces de la scene concernes par le calcul acoustique pour la paire concernee
    selectFaces(rayon);

    // On calcul les trajets acoustiques horizontaux et verticaux reliant la paire source/recepteur
    computePath(ptsTop, ptsLeft, ptsRight);

    // On effectue les calculs acoustiques en utilisant les formules du modele acoustique
    launchAcousticComputation(ptsTop, ptsLeft, ptsRight);

    ptsTop.clear();
    ptsLeft.clear();
    ptsRight.clear();
    _tabIntersect.clear();
}
