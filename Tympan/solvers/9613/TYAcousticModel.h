/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TYACOUSTICMODEL__
#define __TYACOUSTICMODEL__

#include "Tympan/core/interfaces.h"
#include "Tympan/solvers/TYSolverDefines.h"
#include "Tympan/models/common/plan.h"
#include <gtest/gtest_prod.h>

class TYTrajet;
class TYSolver;

/**
 * \class TYAcousticModel
 * \brief Acoustic model for the considered solver
 */
class TYAcousticModel : public AcousticModelInterface
{
public:
    TYAcousticModel();          //<! Constructor
    virtual ~TYAcousticModel(); //<! Destructor

    /// Initialize the acoustic model
    void init();
    /**
     * @brief Compute the wave length for the considered solver
     *
     */
    virtual void computeWaveLength() = 0;

    /*
     * @brief Compute mean slope plan - Kept for ISO 9613-2 ground effect alternative method.
     *
     * @param penteMoyenne Mean slope used to compute the plan corresponding to this slope.
     *
     * @return OPlan The computed mean slope plan.
     */
    OPlan buildMeanSlopePlan(const OSegment3D& penteMoyenne) const;

    /*!
     * \brief Create a segment corresponding to the projection of "director" segment on the ground
     */
    void meanSlope(const OSegment3D& director, OSegment3D& slope) const;

private:
    virtual TYSolver& getSolver() const = 0;

protected:
    bool _useSol;
    bool _useReflex;
    int _propaCond;
    bool _useAtmo;
    bool _interference;
    double _paramH;

    std::unique_ptr<AtmosphericConditions> _pSolverAtmos;
};

#endif // __TYACOUSTICMODEL__
