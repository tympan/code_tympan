/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 */

#include "Tympan/core/defines.h"
#include "Tympan/models/common/plan.h"
#include "Tympan/models/solver/entities.hpp"
#include "TYFaceSelector.h"

TYFaceSelector::TYFaceSelector() {}

TYFaceSelector::~TYFaceSelector() {}

void TYFaceSelector::selectFaces(std::deque<TYSIntersection>& tabIntersect,
                                 const std::vector<TYStructSurfIntersect>& tabPolygon,
                                 const OSegment3D& rayon, const string& sourceVolumeId)
{
    // Construction of cutting planes
    TYSPlan plan[2];
    buildPlans(plan, rayon);

    size_t nbFaces = tabPolygon.size();

    // Test faces which cut vertical plane
    for (unsigned i = 0; i < nbFaces; i++)
    {
        const TYStructSurfIntersect& SI = tabPolygon[i];

        // FIX issue #18 (obstacles are not detected correctly)
        if ((SI.volume_id.size() != 0) && (sourceVolumeId.size() != 0))
        {
            if ((SI.volume_id == sourceVolumeId) || (SI.tabPoint.size() == 0))
            {
                continue;
            }
        }

        // Vertical plane = 0 / Horizontal plane = 1
        TYSIntersection intersection;
        intersection.noIntersect = false;
        intersection.isInfra = SI.is_infra();
        intersection.isEcran = intersection.isInfra;

        bool bVertical = CalculSegmentCoupe(SI, intersection, plan[0].pt1, plan[0].pt2, plan[0].pt3, 0);
        bool bHorizontal = CalculSegmentCoupe(SI, intersection, plan[1].pt1, plan[1].pt2, plan[1].pt3, 1);

        if (bVertical || bHorizontal)
        {
            tabIntersect.push_back(intersection);
        }
    }

    reorder_intersect(tabIntersect); // Put infrastructure elements on top
}

void TYFaceSelector::reorder_intersect(std::deque<TYSIntersection>& tabIntersect)
{
    std::deque<unsigned int> indices;
    std::deque<TYSIntersection> temp;

    // 1 st pass : put infrastructure on top
    for (size_t i = 0; i < tabIntersect.size(); i++)
    {
        if (tabIntersect[i].isInfra)
        {
            temp.push_back(tabIntersect[i]);
        }
        else
        {
            indices.push_back(static_cast<unsigned int>(i));
        }
    }

    // 2nd pass : put remainings after
    for (size_t i = 0; i < indices.size(); i++)
    {
        temp.push_back(tabIntersect[indices[i]]);
    }

    // last : replace tabIntersect
    tabIntersect = temp;
}

bool TYFaceSelector::buildPlans(TYSPlan* plan, const OSegment3D& rayon)
{
    double planOffset = 10.0;
    TYSPlan tmpPlan;
    tmpPlan.pt1 = rayon._ptA;
    tmpPlan.pt2 = rayon._ptB;

    // Construction of vertical plane
    // 3rd point is obtained by copying 1st one and changing z coordinate
    tmpPlan.pt3 = tmpPlan.pt1;
    tmpPlan.pt3._z += planOffset;

    // If A and B coincide (in 2D) on the plane we want to create
    if ((tmpPlan.pt1._x == tmpPlan.pt2._x) && (tmpPlan.pt1._y == tmpPlan.pt2._y))
    {
        // Then move point 2 in order to build an arbitrary valid plane
        tmpPlan.pt2._x += planOffset;
        tmpPlan.pt2._y += planOffset;
    }

    plan[0] = tmpPlan;

    // Construction of horizontal plane
    tmpPlan.pt1 = rayon._ptA;
    tmpPlan.pt2 = rayon._ptB;

    if (tmpPlan.pt2._x != tmpPlan.pt1._x)
    {
        tmpPlan.pt3._z = tmpPlan.pt1._z;
        tmpPlan.pt3._y = tmpPlan.pt1._y + planOffset;
        tmpPlan.pt3._x =
            ((tmpPlan.pt1._y - tmpPlan.pt2._y) * planOffset / (tmpPlan.pt2._x - tmpPlan.pt1._x)) +
            (tmpPlan.pt1._x);
    }
    else if (tmpPlan.pt1._y != tmpPlan.pt2._y)
    {
        tmpPlan.pt3._z = tmpPlan.pt1._z;
        tmpPlan.pt3._x = tmpPlan.pt1._x + planOffset;
        tmpPlan.pt3._y =
            ((tmpPlan.pt2._x - tmpPlan.pt1._x) * planOffset / (tmpPlan.pt1._y - tmpPlan.pt2._y)) +
            (tmpPlan.pt1._y);
    }
    else if (tmpPlan.pt1._z != tmpPlan.pt2._z)
    {
        tmpPlan.pt3._y = tmpPlan.pt1._y;
        tmpPlan.pt3._x = tmpPlan.pt1._x + planOffset;
        tmpPlan.pt3._z =
            ((tmpPlan.pt2._x - tmpPlan.pt1._x) * planOffset / (tmpPlan.pt1._z - tmpPlan.pt2._z)) +
            (tmpPlan.pt1._z);
    }
    else
    {
        return false;
    }

    plan[1] = tmpPlan;

    return true;
}

bool TYFaceSelector::CalculSegmentCoupe(const TYStructSurfIntersect& FaceCourante, TYSIntersection& Intersect,
                                        OPoint3D& pt1, OPoint3D& pt2, OPoint3D& pt3, const int& indice) const
{
    bool bRes = false;
    Intersect.bIntersect[indice] = false;

    OSegment3D segInter;
    OPlan planRayon(pt1, pt2, pt3);
    if (planRayon.intersectsSurface(FaceCourante.tabPoint, segInter))
    {
        Intersect.bIntersect[indice] = true;
        Intersect.segInter[indice] = segInter;
        Intersect.material = FaceCourante.material;
        bRes = true;
    }

    return bRes;
}
