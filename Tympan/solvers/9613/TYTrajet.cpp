/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "Tympan/solvers/9613/TYTrajet.h"
#include "Tympan/models/solver/config.h"

TYTrajet::TYTrajet(tympan::AcousticSource& asrc_, tympan::AcousticReceptor& arcpt_)
    : asrc(asrc_), arcpt(arcpt_), _distance(0.0)
{
    _ptS = asrc.position;
    _ptR = arcpt.position;
    _distance = _ptS.distFrom(_ptR);
}

TYTrajet::TYTrajet(const TYTrajet& other) : asrc(other.asrc), arcpt(other.arcpt)
{
    *this = other;
    if (tympan::SolverConfiguration::get()->KeepRays == false)
    {
        for (unsigned int i = 0; i < _tabRays.size(); i++)
        {
            delete _tabRays.at(i);
            _tabRays.at(i) = nullptr;
        }
        _tabRays.clear();
    }
}

TYTrajet::~TYTrajet() {}

TYTrajet& TYTrajet::operator=(const TYTrajet& other)
{
    if (this != &other)
    {
        _ptS = other._ptS;
        _ptR = other._ptR;
        _distance = other._distance;
        asrc = other.asrc;
        arcpt = other.arcpt;
        asrc_idx = other.asrc_idx;
        arcpt_idx = other.arcpt_idx;
    }
    return *this;
}

bool TYTrajet::operator==(const TYTrajet& other) const
{
    if (this != &other)
    {
        if (_ptS != other._ptS)
        {
            return false;
        }
        if (_ptR != other._ptR)
        {
            return false;
        }
        if (_distance != other._distance)
        {
            return false;
        }
        //  if (asrc != other.asrc) { return false; };
        //  if (arcpt != other.arcpt) ;
        if (asrc_idx != other.asrc_idx)
        {
            return false;
        };
        if (arcpt_idx != other.arcpt_idx)
        {
            return false;
        };
    }

    return true;
}

bool TYTrajet::operator!=(const TYTrajet& other) const
{
    return !operator==(other);
}

std::vector<acoustic_path*>& TYTrajet::get_tab_rays()
{
    return _tabRays;
}
