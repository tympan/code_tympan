/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "TYCheminDefaultSolver.h"
#include "Tympan/models/solver/config.h"

TYCheminDefaultSolver::TYCheminDefaultSolver() : TYChemin()
{
    _attenuation = OSpectreComplex::getEmptyLinSpectre();
}

TYCheminDefaultSolver::TYCheminDefaultSolver(const TYCheminDefaultSolver& other)
{
    *this = other;
}

TYCheminDefaultSolver& TYCheminDefaultSolver::operator=(const TYCheminDefaultSolver& other)
{
    if (this != &other)
    {
        TYChemin::operator=(other);
        _attenuation = other._attenuation;
    }

    return *this;
}

bool TYCheminDefaultSolver::operator==(const TYCheminDefaultSolver& other) const
{
    if (this != &other)
    {
        if (TYChemin::operator!=(other))
        {
            return false;
        }
        if (_attenuation != other._attenuation)
        {
            return false;
        }
    }

    return true;
}

bool TYCheminDefaultSolver::operator!=(const TYCheminDefaultSolver& other) const
{
    return !operator==(other);
}

void TYCheminDefaultSolver::calcAttenuation(const TYTabEtapeDefaultSolver& tabEtapes,
                                            const AtmosphericConditions& atmos)
{
    unsigned int i = 0;

    OSpectre phase = OSpectre::getEmptyLinSpectre(); // Reference de phase

    switch (_typeChemin)
    {
        case TYTypeChemin::CHEMIN_DIRECT: // S*A*e^(i.k.Rd) avec A =attenuation atmosphere et S=directivite de
                                          // la source
            _attenuation = tabEtapes[0]._Absorption; // directivite de la source (S)
            //            _attenuation = _attenuation.mult(atmos.getAtt(_longueur)); // S*A (A = attenuation
            //            atmospherique)
            _attenuation = _attenuation.mult(
                atmos.compute_length_absorption(_longueur)); // S*A (A = attenuation atmospherique)

            //            phase = atmos.getKAcoust().mult(_longueur); // = kRd
            phase = atmos.get_k().mult(_longueur); // = kRd
            _attenuation.setPhase(phase);          // =e^(i.kRd) //*/

            break;

        case TYTypeChemin::CHEMIN_SOL:               // S*A*Q*e^(i.k.Rr)/Rr  //avec Q absorption du sol
            _attenuation = tabEtapes[0]._Absorption; // directivite de la source (S)
            _attenuation = _attenuation.mult(
                atmos.compute_length_absorption(_longueur)); // S*A (A = attenuation atmospherique)

            _attenuation = _attenuation.mult(tabEtapes[1]._Absorption); // S*A*Q
            _attenuation = _attenuation.mult(_distance / _longueur);    // S*A*Q*Rd / Rr

            phase = atmos.get_k().mult(_longueur); // = kRr
            phase = phase.sum(
                tabEtapes[1]
                    ._Absorption.getPhase()); // kRr + epsilon (epsilon = phase du coeff de reflexion du sol

            _attenuation.setPhase(phase);

            break;

        case TYTypeChemin::CHEMIN_ECRAN: //= S*A*Q/D*e^(i.k.Rd + eps) avec Q=module du coefficient de
                                         // reflexion du sol et
            // D=attenuation diffraction
            _attenuation = tabEtapes[0]._Absorption; // S = Directivite de la source
            _attenuation = _attenuation.mult(
                atmos.compute_length_absorption(_longueur)); // S*A (A = attenuation atmospherique)

            phase = atmos.get_k().mult(_longueur); // = kRr

            // On fait le produit des absorptions des etapes a partir de la seconde jusqu'a l'avant derniere
            // la derniere portant l'effet de diffraction
            for (i = 1; i < tabEtapes.size() - 1; i++)
            {
                _attenuation = _attenuation.mult(tabEtapes[i]._Absorption); // S.A.Q
                phase = phase.sum(tabEtapes[i]._Absorption.getPhase());     // kRr + Somme des epsilon i
            }

            _attenuation = _attenuation.div(tabEtapes[tabEtapes.size() - 1]._Attenuation); // S.A.Q/D

            _attenuation.setPhase(phase);
            break;

        case TYTypeChemin::CHEMIN_REFLEX: // S*A*Q*e^(i.k.Rr + eps)/Rr avec Q coefficient de reflexion de la
                                          // paroi, eps = 0
            _attenuation = tabEtapes[0]._Absorption; // S = Directivite de la source
            _attenuation = _attenuation.mult(
                atmos.compute_length_absorption(_longueur)); // S*A (A = attenuation atmospherique)

            phase = atmos.get_k().mult(_longueur); // = kRr

            // On fait le produit des absorptions des etapes ) partir de la deuxieme
            for (i = 1; i < tabEtapes.size(); i++)
            {
                _attenuation = _attenuation.mult(tabEtapes[i]._Absorption); // Produit des modules
                phase = phase.sum(tabEtapes[i]._Absorption.getPhase());     // Somme des phases
            }

            _attenuation = _attenuation.mult(
                _distance /
                _longueur); // <== MODIFIE ON DIVISE PAR Rd² au niveau du solveur (a cause des ecrans)

            _attenuation.setPhase(phase);

            break;

        default:
            break;
    }

    build_eq_path(tabEtapes);
}
