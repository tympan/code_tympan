/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TY_TRAJET_DEFAULTSOLVER__
#define __TY_TRAJET_DEFAULTSOLVER__

#include "Tympan/solvers/9613/TYTrajet.h"
#include "Tympan/solvers/9613/DefaultSolver/TYCheminDefaultSolver.h"
#include "Tympan/models/common/3d.h"
#include "Tympan/models/common/atmospheric_conditions.h"
#include "Tympan/models/solver/entities.hpp"
#include "Tympan/models/common/acoustic_path.h"
#include <gtest/gtest_prod.h>

/**
 * \file TYTrajetDefaultSolver.h
 * \class TYTrajetDefaultSolver
 * \brief  This class TYTrajet (journey) links a couple Source-Receptor and a collection of paths, in addition to the direct path.
 * Specific derivation of TYTrajet for DefaultSolver
 * \version v 1.0
 * \date 2014/01/17
 * \author Projet_Tympan
 */
class TYTrajetDefaultSolver : public TYTrajet
{

    // Methods
public:
    /**
     * \fn TYTrajetDefaultSolver(tympan::AcousticSource& asrc_, tympan::AcousticReceptor& arcpt_)
     * \brief Constructor
     * \param asrc_ Source
     * \param arcpt_ Receptor
     */
    TYTrajetDefaultSolver(tympan::AcousticSource& asrc_, tympan::AcousticReceptor& arcpt_);

    /**
     * \fn virtual ~TYTrajetDefaultSolver()
     * \brief Destructor
     */
    virtual ~TYTrajetDefaultSolver();

    /// Operator =.
    TYTrajetDefaultSolver& operator=(const TYTrajetDefaultSolver& other);
    /// Operator ==.
    bool operator==(const TYTrajetDefaultSolver& other) const;
    /// Operator !=.
    bool operator!=(const TYTrajetDefaultSolver& other) const;

    /**
     * \fn void reset()
     * \brief Reset method.
     */
    void reset() override;

    /**
     * \fn size_t getNbChemins()
     * \brief Return the number of path in *this (in addition to the direct path).
     * \return _chemins.size()
     */
    size_t getNbChemins()
    {
        return _chemins.size();
    }

    /**
     * \fn TYTabCheminDefaultSolver& getChemins()
     * \brief Return the collection of paths of *this
     * \return _chemins
     */
    TYTabCheminDefaultSolver& getChemins()
    {
        return _chemins;
    }

    /**
     * \fn TYTabCheminDefaultSolver& getCheminsDirect()
     * \brief Return an array of the direct paths
     * \return _cheminsDirect
     */
    TYTabCheminDefaultSolver& getCheminsDirect()
    {
        return _cheminsDirect;
    }

    /**
     * \fn void addChemin(const TYCheminDefaultSolver & chemin)
     * \brief Add a new path
     */
    void addChemin(const TYCheminDefaultSolver& chemin);

    /**
     * \fn void addCheminDirect(const TYCheminDefaultSolver & chemin)
     * \brief Add a new path to the array of direct paths
     */
    void addCheminDirect(const TYCheminDefaultSolver& chemin);

    /**
     * \fn TYCheminDefaultSolver getChemin(int index)
     * \brief Return a path thanks to its index.
     * \return _chemins.at(index)
     */
    TYCheminDefaultSolver getChemin(int index)
    {
        return _chemins.at(index);
    }

    /**
     * \fn OSpectre& getSpectre()
     *     const OSpectre getSpectre()
     *     void setSpectre(const OSpectre& spectre)
     * \brief Get/Set the spectrum at the receptor point
     * \return _sLP
     */
    OSpectre& getSpectre()
    {
        return _sLP;
    }
    const OSpectre getSpectre() const
    {
        return _sLP;
    }
    void setSpectre(const OSpectre& spectre)
    {
        _sLP = spectre;
    }

    /**
     * \fn OSpectre getPEnergetique(const AtmosphericConditions& atmos)
     * \brief Compute the acoustic pressure (phase modulation) on the journey
     */
    OSpectre getPEnergetique(const AtmosphericConditions& atmos);

    /**
     * \fn OSpectre getPInterference(const AtmosphericConditions& atmos)
     * \brief Compute the quadratic pressure on the journey
     */
    OSpectre getPInterference(const AtmosphericConditions& atmos);

private:
    OSpectre correctTiers(const OSpectreComplex& si, const OSpectreComplex& sj,
                          const AtmosphericConditions& atmos, const double& ri, const double& rj) const;
    void build_tab_rays();
    FRIEND_TEST(test_TYTrajet, getPInterference);
    FRIEND_TEST(test_TYTrajet, getPEnergetique);

public:
    // Members
protected:
    /// Paths collection
    TYTabCheminDefaultSolver _chemins;

    /// Direct paths collection (without obstacles)
    TYTabCheminDefaultSolver _cheminsDirect;

    OSpectre _sLP;
};
#endif // __TY_TRAJET_DEFAULTSOLVER__
