/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TY_CHEMIN_DEFAULTSOLVER__
#define __TY_CHEMIN_DEFAULTSOLVER__

#include <deque>
#include "Tympan/solvers/9613/TYChemin.h"
#include "Tympan/solvers/9613/DefaultSolver/TYEtapeDefaultSolver.h"
#include "Tympan/models/common/atmospheric_conditions.h"
#include "Tympan/models/common/acoustic_path.h"

/**
 * \file TYCheminDefaultSolver.h
 * \brief Representation of one of the most optimal path between source and receptor: S--->R
 * Specific derivation of TYChemin for DefaultSolver
 * \author Projet_Tympan
 * \version v1.0
 */

/**
 * \class TYCheminDefaultSolver
 *  \brief Representation of one of the most optimal path between source and receptor: S--->R.
 *  The class TYChemin represents a path between a Source and a receptor (Recepteur class). It's constituted
 * of a collection of steps (TYEtape class).
 * Specific derivation of TYChemin for DefaultSolver
 */
class TYCheminDefaultSolver : public TYChemin
{
    // Methods
public:
    /**
     * \fn TYCheminDefaultSolver()
     *\brief Constructor
     */
    TYCheminDefaultSolver();
    /**
     * \fn TYCheminDefaultSolver(const TYCheminDefaultSolver& other)
     *\brief Copy contructor
     */
    TYCheminDefaultSolver(const TYCheminDefaultSolver& other);

    /// Operator =.
    TYCheminDefaultSolver& operator=(const TYCheminDefaultSolver& other);
    /// Operator ==.
    bool operator==(const TYCheminDefaultSolver& other) const;
    /// Operator !=.
    bool operator!=(const TYCheminDefaultSolver& other) const;

    /**
     * \fn void calcAttenuation(const TYTabEtapeDefaultSolver& tabEtapes, const AtmosphericConditions& atmos)
     * \brief Compute the global attenuation on the path
     */
    void calcAttenuation(const TYTabEtapeDefaultSolver& tabEtapes, const AtmosphericConditions& atmos);

    /**
     * \fn OSpectreComplex& getAttenuation()
     *     const OSpectreComplex& getAttenuation()
     * \brief Return the path attenuation
     * \return _attenuation
     */
    OSpectreComplex& getAttenuation()
    {
        return _attenuation;
    }
    const OSpectreComplex& getAttenuation() const
    {
        return _attenuation;
    }

    /**
     * \fn void setAttenuation (const OSpectreComplex& att)
     * \brief Set the attenuation
     */
    void setAttenuation(const OSpectreComplex& att)
    {
        _attenuation = att;
    }

protected:
    /// Attenuation spectrum of the path
    OSpectreComplex _attenuation;
};

/// TYChemin collection
typedef std::deque<TYCheminDefaultSolver> TYTabCheminDefaultSolver;

#endif // __TY_CHEMIN_DEFAULTSOLVER__
