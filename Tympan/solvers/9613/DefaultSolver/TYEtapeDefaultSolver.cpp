/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "TYEtapeDefaultSolver.h"

// TYEtape::TYEtape() {}

// TYEtape::TYEtape(const TYEtape& other)
//{
//     *this = other;
// }

// TYEtape::~TYEtape() {}

TYEtapeDefaultSolver& TYEtapeDefaultSolver::operator=(const TYEtapeDefaultSolver& other)
{
    if (this != &other)
    {
        TYEtape::operator=(other);
        _Absorption = other._Absorption;
        _Attenuation = other._Attenuation;
    }
    return *this;
}

bool TYEtapeDefaultSolver::operator==(const TYEtapeDefaultSolver& other) const
{
    if (this != &other)
    {
        if (TYEtape::operator!=(other))
        {
            return false;
        }
        if (_Absorption != other._Absorption)
        {
            return false;
        }
        if (_Attenuation != other._Attenuation)
        {
            return false;
        }
    }
    return true;
}

bool TYEtapeDefaultSolver::operator!=(const TYEtapeDefaultSolver& other) const
{
    return !operator==(other);
}

// acoustic_event* TYEtape::asEvent() const
//{
//     acoustic_event* returned_event = new acoustic_event();
//     returned_event->pos = _pt;
//     returned_event->type = _type;
//     return returned_event;
// }
