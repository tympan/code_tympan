/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 *
 */

#ifndef __TY_ETAPE_DEFAULTSOLVER__
#define __TY_ETAPE_DEFAULTSOLVER__

#include "Tympan/models/common/3d.h"
#include "Tympan/models/common/spectre.h"
#include "Tympan/models/common/acoustic_path.h"
#include "Tympan/solvers/9613/TYEtape.h"

#include <vector>
#include <deque>

/**
 * \file TYEtape.h
 * \class TYEtape
 * \brief The TYEtape class is used to describe a part (a step) of a path (TYChemin) for the computation of trajectories (TYTrajet) between Source and receptor (Recepteur)
 * \date 2008/01/21
 * \version v 1.1
 * \author Projet_Tympan
 * To a first point of the step is associated an absorption (this
 * one is significant for the first point of the path, which is associated
 * to a source, a directivity and a ground reflection point).
 * A attenuation is also associated, depending of the step length. It is related
 * to the encountered medium (atmosphere, forest, ...).
 */
class TYEtapeDefaultSolver : public TYEtape
{
    // Methods
public:
    /// Operator =.
    TYEtapeDefaultSolver& operator=(const TYEtapeDefaultSolver& other);
    /// Operator ==.
    bool operator==(const TYEtapeDefaultSolver& other) const;
    /// Operator !=.
    bool operator!=(const TYEtapeDefaultSolver& other) const;

    /**
     * \fn  OSpectreComplex getAbsorption()
     *      const OSpectreComplex getAbsorption()
     *      void setAbsorption(const OSpectreComplex & Abso)
     * \brief Get/Set the absorption spectrum associated to the first point of the step.
     * \return _Absorption
     */
    OSpectreComplex getAbsorption()
    {
        return _Absorption;
    }
    const OSpectreComplex getAbsorption() const
    {
        return _Absorption;
    }
    /**
     * Set/Get du spectre d'absorption associe au point de depart.
     */
    void setAbsorption(const OSpectreComplex& Abso)
    {
        _Absorption = Abso;
    }

    /**
     * \fn OSpectre getAttenuation()
     *     const OSpectre getAttenuation()
     *     void setAttenuation(const OSpectre & Att)
     * \brief Get/Set the attenuation spectrum associated to this step.
     * \return _Attenuation
     */
    OSpectre getAttenuation()
    {
        return _Attenuation;
    }
    const OSpectre getAttenuation() const
    {
        return _Attenuation;
    }
    /**
     * Set/Get du spectre d'attenuation associe a cette etape.
     */
    void setAttenuation(const OSpectre& Att)
    {
        _Attenuation = Att;
    }
    // acoustic_event* asEvent() const;

    // Membres
public:
    OSpectreComplex _Absorption; //!< absorption Spectrum
    OSpectre _Attenuation;       //!< attenuation Spectrum
};

/// TYEtape collection.
typedef std::deque<TYEtapeDefaultSolver> TYTabEtapeDefaultSolver;

#endif // __TY_ETAPE_DEFAULTSOLVER__
