/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TY_SOLVER_DEFAULTSOLVER__
#define __TY_SOLVER_DEFAULTSOLVER__

#include <vector>
#include <memory>
#include "Tympan/core/interfaces.h"
#include "Tympan/solvers/9613/TYSolver.h"
#include "Tympan/solvers/9613/DefaultSolver/TYTrajetDefaultSolver.h"
#include "Tympan/models/solver/acoustic_problem_model.hpp"
#include "Tympan/models/solver/acoustic_result_model.hpp"
#include "Tympan/solvers/TYSolverDefines.h"
#include "Tympan/models/solver/config.h"

class TYAcousticModelDefaultSolver;

/**
 * \brief Default solver
 */
class TYSolverDefaultSolver : public TYSolver
{
public:
    TYSolverDefaultSolver();          //!< Constructor
    virtual ~TYSolverDefaultSolver(); //!< Destructor

    TYAcousticModelDefaultSolver* getAcousticModel()
    {
        return _acousticModel.get();
    } //!< Get acoustic model

protected:
    std::unique_ptr<TYAcousticModelDefaultSolver> make_acoustic_model(); //!< TYAcousticModel builder
    std::unique_ptr<TYAcousticModelDefaultSolver> _acousticModel;        //!< Pointer to the TYAcousticModel

private:
    std::vector<TYTrajetDefaultSolver*> _tabTrajets; //!< Vector of TYTrajetDefaultSolver

private:
    /*!
     * \fn void clearTabTrajets()
     * \brief Clear the array of TYTrajetDefaultSolver
     * \return
     */
    void clearTabTrajets() override;

    /*! @copydoc TYSolver::addNewTaskForOneTrajetSrcRcp()
     *
     */
    void addNewTaskForOneTrajetSrcRcp(const tympan::AcousticProblemModel& aproblem, unsigned int src_index,
                                      unsigned int rcp_index, int nbTrajetsForThisSource,
                                      int nNbTrajets) override;
    /*! @copydoc TYSolver::buildAcousticModel()
     *
     */
    void buildAcousticModel() override;
    /*! @copydoc TYSolver::initAcousticModel()
     *
     */
    void initAcousticModel() override;
    /*! @copydoc TYSolver::displayRaysInGUI()
     *
     */
    void displayRaysInGUI(tab_acoustic_path&) override;
    /*! @copydoc TYSolver::buildResultsMatrix()
     *
     */
    void buildResultsMatrix(int nSourceTrajetsNumber, tympan::SpectrumMatrix& matrix) override;
    /*! @copydoc TYSolver::deleteTrajets()
     *
     */
    void deleteTrajets() override;
};

#endif // __TY_SOLVER_DEFAULTSOLVER__
