/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \brief Le plugin associe au solver par defaut
 */

#include "Tympan/core/plugin.h"
#include "Tympan/solvers/9613/DefaultSolver/TYSolverDefaultSolver.h"

Plugin* plugin;

extern "C" PLUGIN_DECL void startPlugin()
{
    plugin = new Plugin();
    // Information sur le plugin
    plugin->setName("DefaultSolver");
    plugin->setAuthor("Projet_Tympan");
    plugin->setVersion("1.0");
    plugin->setDescription("Solveur acoustique par defaut integre a l'application Tympan");
    plugin->setUUID(OGenID("{A98B320C-44C4-47a9-B689-1DD352DAA8B2}"));
    // Creation du solver
    plugin->setSolver(new TYSolverDefaultSolver());
}

extern "C" PLUGIN_DECL Plugin* getPlugin()
{
    return plugin;
}

extern "C" PLUGIN_DECL void stopPlugin()
{
    delete plugin;
}
