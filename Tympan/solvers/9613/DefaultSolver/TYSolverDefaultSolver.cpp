/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "Tympan/core/logging.h"
#include "Tympan/solvers/threading.h"
#include "Tympan/models/common/spectrum_matrix.h"
#include "TYSolverDefaultSolver.h"
#include "TYAcousticModelDefaultSolver.h"
#include "Tympan/solvers/9613/DefaultSolver/TYTaskDefaultSolver.h"
#include "Tympan/solvers/9613/DefaultSolver/TYTrajetDefaultSolver.h"

TYSolverDefaultSolver::TYSolverDefaultSolver() : TYSolver()
{
    // Creation du acoustic model
    _acousticModel = make_acoustic_model();
}

TYSolverDefaultSolver::~TYSolverDefaultSolver()
{
    clearTabTrajets();
}

std::unique_ptr<TYAcousticModelDefaultSolver> TYSolverDefaultSolver::make_acoustic_model()
{
    return std::unique_ptr<TYAcousticModelDefaultSolver>(new TYAcousticModelDefaultSolver(*this));
}

void TYSolverDefaultSolver::clearTabTrajets()
{
    if (_tabTrajets.size() > 0)
    {
        _tabTrajets.clear();
    }
}

void TYSolverDefaultSolver::addNewTaskForOneTrajetSrcRcp(const tympan::AcousticProblemModel& aproblem,
                                                         unsigned int src_index, unsigned int rcp_index,
                                                         int nbTrajectsForOneSource, int nNbTrajets)
{
    TYTrajetDefaultSolver* trajet =
        new TYTrajetDefaultSolver(const_cast<tympan::AcousticProblemModel&>(aproblem).source(src_index),
                                  const_cast<tympan::AcousticProblemModel&>(aproblem).receptor(rcp_index));
    trajet->asrc_idx = src_index;
    trajet->arcpt_idx = rcp_index;
    _tabTrajets.push_back(trajet);

    _pool->push(new TYTaskDefaultSolver(*this, aproblem.nodes(), aproblem.triangles(), aproblem.materials(),
                                        *_tabTrajets.at(nbTrajectsForOneSource), nNbTrajets + 1));
}

void TYSolverDefaultSolver::buildAcousticModel()
{
    if (!_acousticModel)
    {
        _acousticModel = make_acoustic_model();
    }
}

void TYSolverDefaultSolver::initAcousticModel()
{
    _acousticModel->init();
}

void TYSolverDefaultSolver::displayRaysInGUI(tab_acoustic_path& tabRays)
{
    for (unsigned int i = 0; i < _tabTrajets.size(); i++)
    {
        for (size_t j = 0; j < _tabTrajets.at(i)->get_tab_rays().size(); j++)
        {
            tabRays.push_back(_tabTrajets.at(i)->get_tab_rays()[j]);
        }
    }
}

void TYSolverDefaultSolver::buildResultsMatrix(int nSourceTrajectsNumber, tympan::SpectrumMatrix& matrix)
{
    for (int i = 0; i < nSourceTrajectsNumber; i++)
    {
        tympan::source_idx sidx = _tabTrajets.at(i)->asrc_idx;
        tympan::receptor_idx ridx = _tabTrajets.at(i)->arcpt_idx;

        matrix(ridx, sidx) = _tabTrajets.at(i)->getSpectre();
    }
}

void TYSolverDefaultSolver::deleteTrajets()
{
    for (unsigned int cnt = 0; cnt < _tabTrajets.size(); cnt++)
    {
        delete _tabTrajets.at(cnt);
    }
}
