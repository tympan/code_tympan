/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TY_TRAJET__
#define __TY_TRAJET__

#include "Tympan/models/common/3d.h"
#include "Tympan/models/solver/entities.hpp"
#include "Tympan/models/common/acoustic_path.h"
#include <gtest/gtest_prod.h>

/**
 * \file TYTrajet.h
 * \class TYTrajet
 * \brief  This class TYTrajet (journey) links a couple Source-Receptor and a collection of paths, in addition to the direct path.
 * \version v 2.0
 * \date 2024/01/17
 * \author Projet_Tympan
 */
class TYTrajet
{

    // Methods
public:
    /**
     * \fn TYTrajet(tympan::AcousticSource& asrc_, tympan::AcousticReceptor& arcpt_)
     * \brief Constructor
     * \param asrc_ Source
     * \param arcpt_ Receptor
     */
    TYTrajet(tympan::AcousticSource& asrc_, tympan::AcousticReceptor& arcpt_);

    /**
     * \fn TYTrajet(const TYTrajet& other)
     * \brief Copy constructor
     */
    TYTrajet(const TYTrajet& other);
    /**
     * \fn virtual ~TYTrajet()
     * \brief Destructor
     */
    virtual ~TYTrajet();

    /// Operator =.
    TYTrajet& operator=(const TYTrajet& other);
    /// Operator ==.
    bool operator==(const TYTrajet& other) const;
    /// Operator !=.
    bool operator!=(const TYTrajet& other) const;

    /**
     * \fn void reset()
     * \brief Reset method.
     */
    virtual void reset() = 0;

    /**
     * \fn double getDistance()
     *     const double getDistance()
     * \brief Get/Set the distance between source and receptor
     * \return _distance
     */
    double getDistance()
    {
        return _distance;
    }
    const double getDistance() const
    {
        return _distance;
    }

    void setDistance(const double& distance)
    {
        _distance = distance;
    }

    /**
     * \fn void setPtSetPtR(const OPoint3D& pt1, const OPoint3D& pt2)
     *     void getPtSetPtR(OPoint3D& pt1, OPoint3D& pt2)
     * \brief Get/Set points for source and receptor
     */
    void setPtSetPtR(const OPoint3D& pt1, const OPoint3D& pt2)
    {
        _ptS = pt1;
        _ptR = pt2;
    }
    void getPtSetPtR(OPoint3D& pt1, OPoint3D& pt2)
    {
        pt1 = _ptS;
        pt2 = _ptR;
    }

    /**
     * \fn void setPtSetPtRfromOSeg3D(const OSegment3D& seg)
     *     void getPtSetPtRfromOSeg3D(OSegment3D& seg)
     * \brief Get/Set points for source and receptor with an OSegment3D
     */
    void setPtSetPtRfromOSeg3D(const OSegment3D& seg)
    {
        _ptS = seg._ptA;
        _ptR = seg._ptB;
    }
    void getPtSetPtRfromOSeg3D(OSegment3D& seg) const
    {
        seg._ptA = _ptS, seg._ptB = _ptR;
    }

    // Get the tab of rays
    std::vector<acoustic_path*>& get_tab_rays();

private:
public:
    /// Business source
    tympan::AcousticSource& asrc;
    tympan::source_idx asrc_idx;

    /// Business receptor
    tympan::AcousticReceptor& arcpt;
    tympan::receptor_idx arcpt_idx;

    // Members
protected:
    /// Source point definition in the site frame
    OPoint3D _ptS;

    /// Receptor point definition in the site frame
    OPoint3D _ptR;

    /// Distance between source and receptor
    double _distance;

    /// Vector of rays equivalent to chemin
    std::vector<acoustic_path*> _tabRays;
};
#endif // __TY_TRAJET__
