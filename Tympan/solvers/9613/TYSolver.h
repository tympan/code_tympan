/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TY_SOLVER__
#define __TY_SOLVER__

#include <vector>
#include "Tympan/core/interfaces.h"
#include "Tympan/models/solver/acoustic_problem_model.hpp"
#include "Tympan/models/solver/acoustic_result_model.hpp"
#include "Tympan/solvers/TYSolverDefines.h"
#include "Tympan/models/solver/config.h"

class OThreadPool;
class TYAcousticModel;
class TYAcousticPathFinder;
class TYFaceSelector;
class Scene;

/**
 * \brief Class which represents the Solver for 9613 family solvers
 */
class TYSolver : public SolverInterface
{
public:
    TYSolver();          //!< Constructor
    virtual ~TYSolver(); //!< Destructor

    /**
     * \brief Launch the resolution and get the results
     * \param aproblem Acoustic problem
     * \param aresult Results
     * \param configuration Solver configuration
     * \return Always true
     */
    virtual bool solve(const tympan::AcousticProblemModel& aproblem, tympan::AcousticResultModel& aresult,
                       tympan::LPSolverConfiguration configuration);

    const std::vector<TYStructSurfIntersect>& getTabPolygon() const
    {
        return _tabPolygon;
    } //!< Get the array of polygons

    TYFaceSelector* getFaceSelector()
    {
        return _faceSelector.get();
    } //!< Get the face selector
    TYAcousticPathFinder* getAcousticPathFinder()
    {
        return _acousticPathFinder.get();
    } //!< Get the acoustic path finder

    const Scene* getScene() const
    {
        return _scene.get();
    } //!< Get the Scene

    /**
     * \brief Delegate to _faceSelector the build of the array of intersections
     * \param tabIntersect Array of intersections
     * \param rayon Ray path
     * \param sourceVolumeId volume_id of source when first ray point is a source
     */
    void selectFaces(std::deque<TYSIntersection>& tabIntersect, const OSegment3D& rayon,
                     const string& sourceVolumeId);

protected:
    std::unique_ptr<TYFaceSelector> make_face_selector();     //!< TYFaceSelector builder
    std::unique_ptr<TYAcousticPathFinder> make_path_finder(); //!< TYAcousticPathFinder builder

    std::unique_ptr<TYFaceSelector> _faceSelector;             //!< Pointer to the TYFaceSelector
    std::unique_ptr<TYAcousticPathFinder> _acousticPathFinder; //!< Pointer to the TYAcousticPathFinder

private:
    bool buildCalcStruct(const tympan::AcousticProblemModel& aproblem);

    /*!
     * \fn bool appendTriangleToScene()
     * \brief Convertion des triangles Tympan en primitives utilisables par ray tracer.
     * \return Renvoie vrai si l'ensemble des primitives a bien pu etre importe dans la simulation.
     */
    bool appendTriangleToScene();

    /*!
     * \fn void clearTabTrajets() = 0;
     * \brief Clear the specific array of TYTrajet depending on the solver
     */
    virtual void clearTabTrajets() = 0;

    /**
     * @brief Instanciate a new task to compute a Trajet
     *
     * @param aproblem The acoustic problem
     * @param src_index Index of the source in the acoustic problem
     * @param rcp_index Index of the receptor in the acoustic problem
     * @param nbTrajetsForThisSource Number of routes already created for the considered source
     * @param nNbTrajets Total number of routes already created
     */
    virtual void addNewTaskForOneTrajetSrcRcp(const tympan::AcousticProblemModel& aproblem,
                                              unsigned int src_index, unsigned int rcp_index,
                                              int nbTrajetsForThisSource, int nNbTrajets) = 0;
    /**
     * @brief Build the acoustic model
     *
     */
    virtual void buildAcousticModel() = 0;

    /**
     * @brief Initialize the acoustic model
     */
    virtual void initAcousticModel() = 0;
    /**
     * @brief Keep rays in tab in order to display them in GUI
     * @param tabRays The tab of rays
     */
    virtual void displayRaysInGUI(tab_acoustic_path& tabRays) = 0;
    /**
     * @brief Build the matrix of the results for a given source and all the receptors
     * These results are the resulting spectrum of acoustic pressure in Pa at each receptor due to this source
     * @param nSourceTrajetsNumber The number of routes for a given source to all the receptors
     * @param matrix The global contribution matrix
     */
    virtual void buildResultsMatrix(int nSourceTrajetsNumber, tympan::SpectrumMatrix& matrix) = 0;

    /**
     * @brief Delete all the Trajets
     *
     */
    virtual void deleteTrajets() = 0;

    std::vector<TYStructSurfIntersect> _tabPolygon; //!< Vector of TYStructSurfIntersect
protected:
    OThreadPool* _pool;

private:
    std::unique_ptr<Scene> _scene; //!< Pointer to the Scene
};

#endif // __TY_SOLVER__
