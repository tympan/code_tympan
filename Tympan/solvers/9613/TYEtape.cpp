/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "TYEtape.h"

TYEtape::TYEtape() : _type(ACOUSTIC_EVENT_TYPES::TY_NO_TYPE) {}

TYEtape::TYEtape(const TYEtape& other)
{
    *this = other;
}

TYEtape::~TYEtape() {}

TYEtape& TYEtape::operator=(const TYEtape& other)
{
    if (this != &other)
    {
        _type = other._type;
        _pt = other._pt;
        // _Absorption = other._Absorption;
        // _Attenuation = other._Attenuation;
    }
    return *this;
}

bool TYEtape::operator==(const TYEtape& other) const
{
    if (this != &other)
    {
        if (_type != other._type)
        {
            return false;
        }
        if (_pt != other._pt)
        {
            return false;
        }
        // if (_Absorption != other._Absorption)
        //{
        //     return false;
        // }
        // if (_Attenuation != other._Attenuation)
        //{
        //     return false;
        // }
    }
    return true;
}

bool TYEtape::operator!=(const TYEtape& other) const
{
    return !operator==(other);
}

acoustic_event* TYEtape::asEvent() const
{
    acoustic_event* returned_event = new acoustic_event();
    returned_event->pos = _pt;
    returned_event->type = _type;
    return returned_event;
}