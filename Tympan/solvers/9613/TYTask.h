/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TY_TASK__
#define __TY_TASK__

#include "Tympan/solvers/threading.h"
#include "Tympan/solvers/TYSolverDefines.h"

class nodes_pool_t;
class triangle_pool_t;
class material_pool_t;

/**
 * \brief Task of a thread collection for Tympan
 */
class TYTask : public OTask
{
public:
    /**
     * \brief Constructor
     * \param nodes  Nodes
     * \param triangles Triangles
     * \param materials Materials
     * \param nNbTrajets Path number
     */
    TYTask(const tympan::nodes_pool_t& nodes, const tympan::triangle_pool_t& triangles,
           const tympan::material_pool_t& materials, int nNbTrajets);

    ~TYTask(); //!< Destructor

    void main(); //!< Main procedure to run the task

private:
    /**
     * @brief Build a ray from the source->receptor Trajet
     * @param [out] rayon The ray built
     */
    virtual void getRayonFromTrajet(OSegment3D& rayon) = 0;

    /**
     * @brief Delegate to solver the build of the intersecting faces between the scene and the ray
     * @param rayon The considered ray
     */
    virtual void selectFaces(OSegment3D& rayon) = 0;

    /**
     * @brief Delegate to solver the computation of the intersecting top, left and right points between the
     * scene and the ray
     * @param [out] ptsTop Array of points composing the intersection between plane EV and the scene
     * false)
     * @param [out] ptsLeft Array of points composing the intersection betwwen plane EL, on the left, and the
     * scene
     * @param [out] ptsRight Array of points composing the intersection betwwen plane EL, on the right, and
     * the scene
     */
    virtual void computePath(TabPoint3D& ptsTop, TabPoint3D& ptsLeft, TabPoint3D& ptsRight) = 0;

    /**
     * @brief Delegate to solver the acoustic computation for this task
     *
     * @param ptsTop [in] Top points
     * @param ptsLeft [in] Left points
     * @param ptsRight [in] Right points
     */
    virtual void launchAcousticComputation(TabPoint3D& ptsTop, TabPoint3D& ptsLeft, TabPoint3D& ptsRight) = 0;

protected:
    std::deque<TYSIntersection> _tabIntersect; //!< Array of intersections

private:
    unsigned int _nNbTrajets; //!< Path number

    const tympan::nodes_pool_t& _nodes;
    const tympan::triangle_pool_t& _triangles;
    const tympan::material_pool_t& _materials;
};

typedef SmartPtr<TYTask> LPTYTask; //!< Smart Pointer on TYTask.

#endif // __TY_TASK__
