/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TY_CHEMIN_9613SOLVER__
#define __TY_CHEMIN_9613SOLVER__

#include <deque>
#include "Tympan/solvers/9613/9613Solver/TYEtape9613Solver.h"
#include "Tympan/solvers/9613/TYChemin.h"
#include "Tympan/models/common/atmospheric_conditions.h"
#include "Tympan/models/common/acoustic_path.h"

/**
 * \file TYChemin9613Solver.h
 * \brief Representation of one of the most optimal path between source and receptor: S--->R
 * Specific derivation of TYChemin for 9613Solver
 * \author Projet_Tympan
 * \version v1.0
 */

/**
 * \class TYChemin9613Solver
 *  \brief Representation of one of the most optimal path between source and receptor: S--->R.
 *  The class TYChemin represents a path between a Source and a receptor (Recepteur class). It's constituted
 * of a collection of steps (TYEtape class).
 * Specific derivation of TYChemin for 9613Solver
 */
class TYChemin9613Solver : public TYChemin
{
    // Methods
public:
    /**
     * \fn TYChemin9613Solver()
     *\brief Constructor
     */
    TYChemin9613Solver();
    /**
     * \fn TYChemin9613Solver(const TYChemin9613Solver& other)
     *\brief Copy contructor
     */
    TYChemin9613Solver(const TYChemin9613Solver& other);
    /**
     * \fn virtual ~TYChemin9613Solver()
     * \brief Destructor
     */
    ~TYChemin9613Solver() override;

    /// Operator =.
    TYChemin9613Solver& operator=(const TYChemin9613Solver& other);
    /// Operator ==.
    bool operator==(const TYChemin9613Solver& other) const;
    /// Operator !=.
    bool operator!=(const TYChemin9613Solver& other) const;

    /**
     * \fn void calcAttenuation(const TYTabEtape9613Solver& tabEtapes, const AtmosphericConditions & atmos,
                                double dp = 0.0, double hs = 0.0,
                                double hr = 0.0, double Gs = 0.5, double Gm = 0.5, double Gr = 0.5)
     * \brief Compute the global attenuation on the path
     * @param [in] tabEtapes Array of steps.
     * @param [in] atmos Atmospheric ocnditions.
     * @param [in] dp Distance 2D between source and receptor for considered path.
     * @param [in] hs Height of source.
     * @param [in] hr Height of receptor
     * @param [in] Gs Ground factor of the source zone.
     * @param [in] Gm Ground factor of the middle zone.
     * @param [in] Gr Ground factor of the receptor zone.
     *
     */
    void calcAttenuation(const TYTabEtape9613Solver& tabEtapes, const AtmosphericConditions& atmos,
                         double dp = 0.0, double hs = 0.0, double hr = 0.0, double Gs = 0.5, double Gm = 0.5,
                         double Gr = 0.5);

    /**
     * \fn void computeBarAttenuation(const OSpectreOctave& Dz, const bool vertical, const bool left)
     * \ brief compute barrier attenuation on the path
     */
    void computeBarAttenuation(const OSpectreOctave& Dz, const bool vertical, const bool left);

    /**
     * \fn OSpectreOctave& getAttenuation(const TYTypeAttenuation type)
     * \brief Return attenuation of the path of the type
     * \return _attenuations[type]
     */
    OSpectreOctave& getAttenuation(const TYTypeAttenuation type);

    /**
     * \fn void setAttenuation (const TYTypeAttenuation& type, const OSpectreOctave& att)
     * \brief Set the atmospheric attenuation
     */
    void setAttenuation(const TYTypeAttenuation& type, const OSpectreOctave& att)
    {
        _attenuations[type] = att;
    }

    /**
     * \fn void setAttenuationBarWhenNoPath(bool vertical, bool left)
     * \brief Set attenuation bar to max to traduce the lack of diffracted ray on this path
     */
    void setAttenuationBarWhenNoPath(bool vertical, bool left);

protected:
    /// Attenuations spectra of the path
    std::map<TYTypeAttenuation, OSpectreOctave> _attenuations;

private:
    void calcGroundAttenuations(double distance, double hs, double hr, double Gs, double Gm, double Gr);
    OSpectreOctave calcGroundAttenuationSR(double dp, double h, double G);
    OSpectreOctave calcGroundAttenuationM(double q, double Gm);
};

/// TYChemin collection
typedef std::deque<TYChemin9613Solver> TYTabChemin9613Solver;

#endif // __TY_CHEMIN_9613SOLVER__
