/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "Tympan/core/logging.h"
#include "Tympan/solvers/threading.h"
#include "Tympan/models/common/spectrum_matrix.h"
#include "TYSolver9613Solver.h"
#include "TYAcousticModel9613Solver.h"
#include "Tympan/solvers/9613/9613Solver/TYTask9613Solver.h"
#include "Tympan/solvers/9613/9613Solver/TYTrajet9613Solver.h"

TYSolver9613Solver::TYSolver9613Solver() : TYSolver()
{
    // Creation du acoustic model
    _acousticModel = make_acoustic_model();
}

TYSolver9613Solver::~TYSolver9613Solver()
{
    clearTabTrajets();
}

std::unique_ptr<TYAcousticModel9613Solver> TYSolver9613Solver::make_acoustic_model()
{
    return std::unique_ptr<TYAcousticModel9613Solver>(new TYAcousticModel9613Solver(*this));
}

void TYSolver9613Solver::clearTabTrajets()
{
    if (_tabTrajets.size() > 0)
    {
        _tabTrajets.clear();
    }
}

void TYSolver9613Solver::addNewTaskForOneTrajetSrcRcp(const tympan::AcousticProblemModel& aproblem,
                                                      unsigned int src_index, unsigned int rcp_index,
                                                      int nbTrajectsForOneSource, int nNbTrajets)
{
    TYTrajet9613Solver* trajet =
        new TYTrajet9613Solver(const_cast<tympan::AcousticProblemModel&>(aproblem).source(src_index),
                               const_cast<tympan::AcousticProblemModel&>(aproblem).receptor(rcp_index));
    trajet->asrc_idx = src_index;
    trajet->arcpt_idx = rcp_index;
    _tabTrajets.push_back(trajet);

    _pool->push(new TYTask9613Solver(*this, aproblem.nodes(), aproblem.triangles(), aproblem.materials(),
                                     *_tabTrajets.at(nbTrajectsForOneSource), nNbTrajets + 1));
}

void TYSolver9613Solver::buildAcousticModel()
{
    if (!_acousticModel)
    {
        _acousticModel = make_acoustic_model();
    }
}

void TYSolver9613Solver::initAcousticModel()
{
    _acousticModel->init();
}

void TYSolver9613Solver::displayRaysInGUI(tab_acoustic_path& tabRays)
{
    for (unsigned int i = 0; i < _tabTrajets.size(); i++)
    {
        for (size_t j = 0; j < _tabTrajets.at(i)->get_tab_rays().size(); j++)
        {
            tabRays.push_back(_tabTrajets.at(i)->get_tab_rays()[j]);
        }
    }
}

void TYSolver9613Solver::buildResultsMatrix(int nSourceTrajectsNumber, tympan::SpectrumMatrix& matrix)
{
    for (int i = 0; i < nSourceTrajectsNumber; i++)
    {
        tympan::source_idx sidx = _tabTrajets.at(i)->asrc_idx;
        tympan::receptor_idx ridx = _tabTrajets.at(i)->arcpt_idx;

        matrix(ridx, sidx) = _tabTrajets.at(i)->getSpectre();
    }
}

void TYSolver9613Solver::deleteTrajets()
{
    for (unsigned int cnt = 0; cnt < _tabTrajets.size(); cnt++)
    {
        delete _tabTrajets.at(cnt);
    }
}
