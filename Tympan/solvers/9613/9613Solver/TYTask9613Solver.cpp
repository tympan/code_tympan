/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "TYAcousticModel9613Solver.h"
#include "Tympan/solvers/9613/TYAcousticPathFinder.h"
#include "TYSolver9613Solver.h"
#include "TYTask9613Solver.h"

TYTask9613Solver::TYTask9613Solver(TYSolver9613Solver& solver, const tympan::nodes_pool_t& nodes,
                                   const tympan::triangle_pool_t& triangles,
                                   const tympan::material_pool_t& materials, TYTrajet9613Solver& trajet,
                                   int nNbTrajets)
    : TYTask(nodes, triangles, materials, nNbTrajets), _solver(solver), _trajet(trajet)
{
}

void TYTask9613Solver::getRayonFromTrajet(OSegment3D& rayon)
{
    _trajet.getPtSetPtRfromOSeg3D(rayon);
}

void TYTask9613Solver::selectFaces(OSegment3D& rayon)
{
    _solver.selectFaces(_tabIntersect, rayon, _trajet.asrc.volume_id);
}

void TYTask9613Solver::computePath(TabPoint3D& ptsTop, TabPoint3D& ptsLeft, TabPoint3D& ptsRight)
{
    _solver.getAcousticPathFinder()->computePath(_tabIntersect, _trajet, ptsTop, ptsLeft, ptsRight);
}

void TYTask9613Solver::launchAcousticComputation(TabPoint3D& ptsTop, TabPoint3D& ptsLeft,
                                                 TabPoint3D& ptsRight)
{
    _solver.getAcousticModel()->compute(_tabIntersect, _trajet, ptsTop, ptsLeft, ptsRight);
}
