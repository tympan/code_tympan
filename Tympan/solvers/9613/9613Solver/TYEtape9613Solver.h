/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 *
 */

#ifndef __TY_ETAPE_9613SOLVER__
#define __TY_ETAPE_9613SOLVER__

#include "Tympan/models/common/3d.h"
#include "Tympan/models/common/spectre.h"
#include "Tympan/models/common/acoustic_path.h"
#include "Tympan/solvers/9613/TYEtape.h"

#include <vector>
#include <deque>

/**
 * \file TYEtape.h
 * \class TYEtape
 * \brief The TYEtape class is used to describe a part (a step) of a path (TYChemin) for the computation of trajectories (TYTrajet) between Source and receptor (Recepteur)
 * \date 2008/01/21
 * \version v 1.1
 * \author Projet_Tympan
 * To a first point of the step is associated an absorption (this
 * one is significant for the first point of the path, which is associated
 * to a source, a directivity and a ground reflection point).
 * A attenuation is also associated, depending of the step length. It is related
 * to the encountered medium (atmosphere, forest, ...).
 */
class TYEtape9613Solver : public TYEtape
{
    // Methods
public:
    /// Operator =.
    TYEtape9613Solver& operator=(const TYEtape9613Solver& other);
    /// Operator ==.
    bool operator==(const TYEtape9613Solver& other) const;
    /// Operator !=.
    bool operator!=(const TYEtape9613Solver& other) const;

    /**
     * \fn  OSpectreOctave getAbsorption()
     *      const OSpectreOctave getAbsorption()
     *      void setAbsorption(const OSpectreOctave & Abso)
     * \brief Get/Set the absorption spectrum associated to the first point of the step.
     * \return _Absorption
     */

    OSpectreOctave getAbsorption()
    {
        return _Absorption;
    }
    const OSpectreOctave getAbsorption() const
    {
        return _Absorption;
    }
    /**
     * Set/Get du spectre d'absorption associe au point de depart.
     */
    void setAbsorption(const OSpectreOctave& Abso)
    {
        _Absorption = Abso;
    }

    /**
     * \fn OSpectreOctave getAttenuation()
     *     const OSpectreOctave getAttenuation()
     *     void setAttenuation(const OSpectreOctave & Att)
     * \brief Get/Set the attenuation spectrum associated to this step.
     * \return _Attenuation
     */
    OSpectreOctave getAttenuation()
    {
        return _Attenuation;
    }
    const OSpectreOctave getAttenuation() const
    {
        return _Attenuation;
    }
    /**
     * Set/Get du spectre d'attenuation associe a cette etape.
     */
    void setAttenuation(const OSpectreOctave& Att)
    {
        _Attenuation = Att;
    }
    /*acoustic_event* asEvent() const;*/

    // Membres
public:
    OSpectreOctave _Absorption;  //!< absorption Spectrum
    OSpectreOctave _Attenuation; //!< attenuation Spectrum
};

/// TYEtape collection.
typedef std::deque<TYEtape9613Solver> TYTabEtape9613Solver;

#endif // __TY_ETAPE_9613SOLVER__
