/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TY_TRAJET_9613SOLVER__
#define __TY_TRAJET_9613SOLVER__

#include "Tympan/solvers/9613/TYTrajet.h"
#include "Tympan/solvers/9613/9613Solver/TYChemin9613Sover.h"
#include "Tympan/models/common/3d.h"
#include "Tympan/models/common/atmospheric_conditions.h"
#include "Tympan/models/solver/entities.hpp"
#include "Tympan/models/common/acoustic_path.h"
#include <gtest/gtest_prod.h>

/**
 * \file TYTrajet9613Solver.h
 * \class TYTrajet9613Solver
 * \brief  This class TYTrajet (journey) links a couple Source-Receptor and a collection of paths, in addition to the direct path.
 * Specific derivation of TYTrajet for 9613Solver
 * \version v 1.0
 * \date 2024/01/17
 * \author Projet_Tympan
 */
class TYTrajet9613Solver : public TYTrajet
{

    // Methods
public:
    /**
     * \fn TYTrajet9613Solver(tympan::AcousticSource& asrc_, tympan::AcousticReceptor& arcpt_)
     * \brief Constructor
     * \param asrc_ Source
     * \param arcpt_ Receptor
     */
    TYTrajet9613Solver(tympan::AcousticSource& asrc_, tympan::AcousticReceptor& arcpt_);

    /**
     * \fn virtual ~TYTrajet9613Solver()
     * \brief Destructor
     */
    virtual ~TYTrajet9613Solver();

    /// Operator =.
    TYTrajet9613Solver& operator=(const TYTrajet9613Solver& other);
    /// Operator ==.
    bool operator==(const TYTrajet9613Solver& other) const;
    /// Operator !=.
    bool operator!=(const TYTrajet9613Solver& other) const;

    /**
     * \fn void reset()
     * \brief Reset method.
     */
    void reset() override;

    /**
     * \fn size_t getNbChemins()
     * \brief Return the number of path in *this (in addition to the direct path).
     * \return _chemins.size()
     */
    size_t getNbChemins()
    {
        return _chemins.size();
    }

    /**
     * \fn TYTabChemin9613Solver& getChemins()
     * \brief Return the collection of paths of *this
     * \return _chemins
     */
    TYTabChemin9613Solver& getChemins()
    {
        return _chemins;
    }

    /**
     * \fn TYTabChemin9613Solver& getCheminsDirect()
     * \brief Return an array of the direct paths
     * \return _cheminsDirect
     */
    TYTabChemin9613Solver& getCheminsDirect()
    {
        return _cheminsDirect;
    }

    /**
     * \fn void addChemin(const TYChemin9613Solver & chemin)
     * \brief Add a new path
     */
    void addChemin(const TYChemin9613Solver& chemin);

    /**
     * \fn void addCheminDirect(const TYChemin9613Solver & chemin)
     * \brief Add a new path to the array of direct paths
     */
    void addCheminDirect(const TYChemin9613Solver& chemin);

    /**
     * \fn TYChemin9613Solver getChemin(int index)
     * \brief Return a path thanks to its index.
     * \return _chemins.at(index)
     */
    TYChemin9613Solver getChemin(int index)
    {
        return _chemins.at(index);
    }

    /**
     * \fn OSpectreOctave& getSpectreOct()
     *
     * \brief Get the spectrum in octave band at the receptor point
     * Used to compute the pressure spectrum in octave band
     *
     * \return OSpectreOctave&
     */

    OSpectreOctave& getSpectreOct()
    {
        return _sLP;
    }

    /**
     * \fn OSpectre& getSpectre()
     *     const OSpectre getSpectre() const
     * \brief Get the spectrum in third-octave band at the receptor point
     * Used to build the result matrix by TYSolver.cpp
     *
     * \return OSpectre
     */

    OSpectre getSpectre()
    {
        return _sLP.toTOct().toGPhy();
    }
    const OSpectre getSpectre() const
    {
        return _sLP.toTOct().toGPhy();
    }

    void build_tab_rays();

public:
    // Members
protected:
    /// Paths collection
    TYTabChemin9613Solver _chemins;

    /// Direct paths collection (without obstacles)
    TYTabChemin9613Solver _cheminsDirect;

    /// Spectrum in octave band at the receptor point of the journey which integrates geometrical divergence
    /// and the source power
    OSpectreOctave _sLP;
};
#endif // __TY_TRAJET_9613SOLVER__
