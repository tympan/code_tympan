/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TY_TASK_9613SOLVER__
#define __TY_TASK_9613SOLVER__

#include <deque>
#include "Tympan/solvers/threading.h"
#include "Tympan/solvers/9613/TYTask.h"

class TYSolver9613Solver;
class TYTrajet9613Solver;

/**
 * \brief Task of a thread collection for Tympan
 */
class TYTask9613Solver : public TYTask
{
public:
    /**
     * \brief Constructor
     * \param solver 9613Solver solver object
     * \param nodes  Nodes
     * \param triangles Triangles
     * \param materials Materials
     * \param trajet Path
     * \param nNbTrajets Path number
     */
    TYTask9613Solver(TYSolver9613Solver& solver, const tympan::nodes_pool_t& nodes,
                     const tympan::triangle_pool_t& triangles, const tympan::material_pool_t& materials,
                     TYTrajet9613Solver& trajet, int nNbTrajets);

private:
    /*! @copydoc TYTask::getRayonFromTrajet()
     *
     */
    virtual void getRayonFromTrajet(OSegment3D& rayon) override;

    /*! @copydoc TYTask::selectFaces()
     *
     */
    virtual void selectFaces(OSegment3D& rayon) override;

    /*! @copydoc TYTask::computePath()
     *
     */
    virtual void computePath(TabPoint3D& ptsTop, TabPoint3D& ptsLeft, TabPoint3D& ptsRight) override;

    /*! @copydoc TYTask::launchAcousticComputation()
     *
     */
    void launchAcousticComputation(TabPoint3D& ptsTop, TabPoint3D& ptsLeft, TabPoint3D& ptsRight) override;

private:
    TYSolver9613Solver& _solver; //!< Reference to the solver

    TYTrajet9613Solver& _trajet; //!< Reference to the path
};

#endif // __TY_TASK_9613SOLVER__
