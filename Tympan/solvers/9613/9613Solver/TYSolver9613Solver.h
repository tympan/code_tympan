/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TY_SOLVER_9613SOLVER__
#define __TY_SOLVER_9613SOLVER__

#include <vector>
#include <memory>
#include "Tympan/core/interfaces.h"
#include "Tympan/solvers/9613/TYSolver.h"
#include "Tympan/solvers/9613/9613Solver/TYTrajet9613Solver.h"
#include "Tympan/models/solver/acoustic_problem_model.hpp"
#include "Tympan/models/solver/acoustic_result_model.hpp"
#include "Tympan/solvers/TYSolverDefines.h"
#include "Tympan/models/solver/config.h"

class TYAcousticModel9613Solver;

/**
 * \brief 9613 Solver
 */
class TYSolver9613Solver : public TYSolver
{
public:
    TYSolver9613Solver();          //!< Constructor
    virtual ~TYSolver9613Solver(); //!< Destructor

    TYAcousticModel9613Solver* getAcousticModel()
    {
        return _acousticModel.get();
    } //!< Get acoustic model

protected:
    std::unique_ptr<TYAcousticModel9613Solver> make_acoustic_model(); //!< TYAcousticModel builder
    std::unique_ptr<TYAcousticModel9613Solver> _acousticModel;        //!< Pointer to the TYAcousticModel

private:
    std::vector<TYTrajet9613Solver*> _tabTrajets; //!< Vector of TYTrajet9613Solver

private:
    /**
     * \fn void clearTabTrajets()
     * \brief Clear the array of TYTrajet9613Solver
     * \return
     */
    void clearTabTrajets() override;

    /*! @copydoc TYSolver::addNewTaskForOneTrajetSrcRcp()
     *
     */
    void addNewTaskForOneTrajetSrcRcp(const tympan::AcousticProblemModel& aproblem, unsigned int src_index,
                                      unsigned int rcp_index, int nbTrajectsForThisSource,
                                      int nNbTrajets) override;
    /*! @copydoc TYSolver::buildAcousticModel()
     *
     */
    void buildAcousticModel() override;
    /*! @copydoc TYSolver::initAcousticModel()
     *
     */
    void initAcousticModel() override;
    /*! @copydoc TYSolver::displayRaysInGUI()
     *
     */
    void displayRaysInGUI(tab_acoustic_path&) override;
    /*! @copydoc TYSolver::buildResultsMatrix()
     *
     */
    void buildResultsMatrix(int nSourceTrajetsNumber, tympan::SpectrumMatrix& matrix) override;
    /*! @copydoc TYSolver::deleteTrajets()
     *
     */
    void deleteTrajets() override;
};

#endif // __TY_SOLVER_9613SOLVER__
