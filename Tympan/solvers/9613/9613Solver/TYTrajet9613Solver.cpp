/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "TYTrajet9613Solver.h"
#include "Tympan/models/solver/config.h"

TYTrajet9613Solver::TYTrajet9613Solver(tympan::AcousticSource& asrc_, tympan::AcousticReceptor& arcpt_)
    : TYTrajet(asrc_, arcpt_)
{
}

TYTrajet9613Solver::~TYTrajet9613Solver()
{
    reset();
}

void TYTrajet9613Solver::reset()
{
    _chemins.clear();
    _cheminsDirect.clear();
}

TYTrajet9613Solver& TYTrajet9613Solver::operator=(const TYTrajet9613Solver& other)
{
    if (this != &other)
    {
        TYTrajet::operator=(other);
        _chemins = other._chemins;
        _sLP = other._sLP;
    }
    return *this;
}

bool TYTrajet9613Solver::operator==(const TYTrajet9613Solver& other) const
{
    if (this != &other)
    {
        if (TYTrajet::operator!=(other))
        {
            return false;
        }
        if (_chemins != other._chemins)
        {
            return false;
        }
        if (_sLP != other._sLP)
        {
            return false;
        };
    }

    return true;
}

bool TYTrajet9613Solver::operator!=(const TYTrajet9613Solver& other) const
{
    return !operator==(other);
}

void TYTrajet9613Solver::addChemin(const TYChemin9613Solver& chemin)
{
    _chemins.push_back(chemin);
}

void TYTrajet9613Solver::addCheminDirect(const TYChemin9613Solver& chemin)
{
    _cheminsDirect.push_back(chemin);
}

void TYTrajet9613Solver::build_tab_rays()
{
    _tabRays.clear();
    for (size_t i = 0; i < _chemins.size(); i++)
    {
        _tabRays.push_back(_chemins[i].get_ray(_ptR));
    }
}
