/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "TYChemin.h"
#include "Tympan/models/solver/config.h"

TYChemin::TYChemin() : _typeChemin(TYTypeChemin::CHEMIN_DIRECT), _longueur(0.0), _distance(0.0)
{
    _eq_path = new acoustic_path();
}

TYChemin::TYChemin(const TYChemin& other)
{
    *this = other;
}

TYChemin::~TYChemin()
{
    if (tympan::SolverConfiguration::get()->KeepRays == false && _eq_path != nullptr)
    {
        _eq_path->cleanEventsTab();
    }
}

TYChemin& TYChemin::operator=(const TYChemin& other)
{
    if (this != &other)
    {
        _typeChemin = other._typeChemin;
        _distance = other._distance;
        _longueur = other._longueur;
        _eq_path = other._eq_path;
    }

    return *this;
}

bool TYChemin::operator==(const TYChemin& other) const
{
    if (this != &other)
    {
        if (_typeChemin != other._typeChemin)
        {
            return false;
        }
        if (_distance != other._distance)
        {
            return false;
        }
        if (_longueur != other._longueur)
        {
            return false;
        }
        if (_eq_path != other._eq_path)
        {
            return false;
        };
    }

    return true;
}

bool TYChemin::operator!=(const TYChemin& other) const
{
    return !operator==(other);
}

acoustic_path* TYChemin::get_ray(OPoint3D ptR)
{
    // Les chemins écran contiennent déjà le récepteur
    if (_typeChemin != TYTypeChemin::CHEMIN_ECRAN)
    {
        acoustic_event* receptor_event = new acoustic_event();
        receptor_event->pos = ptR;
        receptor_event->type = TYRECEPTEUR;
        _eq_path->addEvent(receptor_event);
    }
    return _eq_path;
}

::std::ostream& operator<<(::std::ostream& out, const TYTypeChemin& value)
{
    static std::map<TYTypeChemin, std::string> strings;
    if (strings.size() == 0)
    {
        strings[TYTypeChemin::CHEMIN_DIRECT] = "DIRECT";
        strings[TYTypeChemin::CHEMIN_SOL] = "SOL";
        strings[TYTypeChemin::CHEMIN_ECRAN] = "ECRAN";
        strings[TYTypeChemin::CHEMIN_REFLEX] = "REFLEX";
    }

    return out << strings[value];
}
