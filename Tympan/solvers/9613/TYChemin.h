/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TY_CHEMIN__
#define __TY_CHEMIN__

#include "Tympan/solvers/9613/TYEtape.h"
#include "Tympan/models/common/atmospheric_conditions.h"
#include "Tympan/models/common/acoustic_path.h"

/**
 * \file TYChemin.h
 * \brief Representation of one of the most optimal path between source and receptor: S--->R
 * \author Projet_Tympan
 * \version v2.0
 */

enum class TYTypeChemin
{
    CHEMIN_DIRECT,
    CHEMIN_SOL,
    CHEMIN_ECRAN,
    CHEMIN_REFLEX
};

::std::ostream& operator<<(::std::ostream& out, const TYTypeChemin& value);

enum class TYTypeAttenuation
{
    DIRECTIVITY_INDEX,
    ATTENUATION_ATM,
    ATTENUATION_GND_S,
    ATTENUATION_GND_R,
    ATTENUATION_GND_M,
    DZ_TOP,
    DZ_LEFT,
    DZ_RIGHT,
    ATTENUATION_BAR_TOP,
    ATTENUATION_BAR_LEFT,
    ATTENUATION_BAR_RIGHT,
    ATTENUATION_BAR,
    ATTENUATION_REFLEX
};

/**
 * \class TYChemin
 *  \brief Representation of one of the most optimal path between source and receptor: S--->R.
 *  The class TYChemin represents a path between a Source and a receptor (Recepteur class). It's constituted
 * of a collection of steps (TYEtape class).
 */
class TYChemin
{
    // Methods
public:
    /**
     * \fn TYChemin()
     *\brief Constructor
     */
    TYChemin();
    /**
     * \fn TYChemin(const TYChemin& other)
     *\brief Copy contructor
     */
    TYChemin(const TYChemin& other);
    /**
     * \fn virtual ~TYChemin()
     * \brief Destructor
     */
    virtual ~TYChemin();

    /// Operator =.
    TYChemin& operator=(const TYChemin& other);
    /// Operator ==.
    bool operator==(const TYChemin& other) const;
    /// Operator !=.
    bool operator!=(const TYChemin& other) const;

    /**
     * \fn double getLongueur()
     *     const double getLongueur()
     *     void setLongueur(const double & longueur)
     *\brief Get/Set the path length
     *\return _longueur
     */
    double getLongueur()
    {
        return _longueur;
    }
    const double getLongueur() const
    {
        return _longueur;
    }

    void setLongueur(const double& longueur)
    {
        _longueur = longueur;
    }

    /**
     * \fn   double getDistance()
     *       const double getDistance()
     *       void setDistance(const double & distance)
     * \brief Get/Set the distance between source and receptor
     *\return _distance
     *
     */
    double getDistance()
    {
        return _distance;
    }
    const double getDistance() const
    {
        return _distance;
    }

    void setDistance(const double& distance)
    {
        _distance = distance;
    }

    /**
     *\fn void setType(const TYTypeChemin& type)
     *\brief Change the path type
     */
    void setType(const TYTypeChemin& type)
    {
        _typeChemin = type;
    }

    /**
     * \fn const TYTypeChemin getType() const
     * \brief Return the path type
     * \return _typeChemin
     */
    const TYTypeChemin getType() const
    {
        return _typeChemin;
    }
    // Members
    /*!
     * \fn template <typename T> void build_eq_path(const T& tabEtapes);
     * \brief build an acoustic_path from the tab of etapes
     */
    template <typename T> void build_eq_path(const T& tabEtapes)
    {
        for (size_t i = 0; i < tabEtapes.size(); i++)
        {
            _eq_path->addEvent(tabEtapes[i].asEvent());
        }
    }

    acoustic_path* get_ray(OPoint3D ptR);

protected:
    /// Path type (has an influence on the algorithm)
    TYTypeChemin _typeChemin;

    /// Total path length
    double _longueur;

    /// Direct distance between source and receptor
    double _distance;

    /// Equivalent acoustic_path
    acoustic_path* _eq_path;

private:
};

/// TYChemin collection
typedef std::deque<TYChemin> TYTabChemin;

#endif // __TY_CHEMIN__
