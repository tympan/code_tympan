/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TY_MODELS_COMMON_MATRIX
#define TY_MODELS_COMMON_MATRIX

#include <vector>

#include "Tympan/models/common/spectre.h"

namespace tympan
{
typedef OSpectre Spectrum;

/**
 * \brief Spectrum matrix N*M used to store results.
 * N is the number of receptors.
 * M is the number of sources.
 */
class SpectrumMatrix
{
public:
    typedef std::vector<std::vector<Spectrum>> impl_matrix_t;

    /// Default constructor
    SpectrumMatrix();
    /// Constructor with several receptors and sources
    SpectrumMatrix(size_t nb_receptors, size_t nb_sources);
    /// Copy constructor
    SpectrumMatrix(const SpectrumMatrix& matrix);
    /// Destructor
    virtual ~SpectrumMatrix(){};

    /// Number of columns (sources) of the matrix
    size_t nb_sources() const
    {
        return _nb_sources;
    };
    /// Number of rows (receptors) of the matrix
    size_t nb_receptors() const
    {
        return data.size();
    };

    /// operator()
    const Spectrum& operator()(size_t receptor_idx, size_t sources_idx) const;
    Spectrum& operator()(size_t receptor_idx, size_t sources_idx);
    /// Set a Spectrum into the matrix
    void setSpectre(size_t receptor_idx, size_t sources_idx, Spectrum spectrum);

    /// Return a vector of Spectrum for a receptor
    const std::vector<Spectrum>& by_receptor(size_t receptor_idx) const;

    /// Clear the matrix for the a given receptor
    void clearReceptor(size_t receptor_idx);

    /// Clear the matrix
    void clear()
    {
        data.clear();
    };

    /// Resize the matrix (data is cleared)
    void resize(size_t nb_receptors, size_t nb_sources);

protected:
    impl_matrix_t data; //!< Matrix

private:
    size_t _nb_sources;

}; // class SpectrumMatrix

} // namespace tympan

#endif
