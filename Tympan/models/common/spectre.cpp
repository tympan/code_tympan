/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#undef min // Something defines a min macro on windows, which breaks std::min

#include <algorithm>

#include "Tympan/models/common/mathlib.h"
#include "spectre.h"

#include <math.h>
#include "Tympan/core/exceptions.h"

#include <iostream>
// using namespace std;

// Minimal working frequency
double OSpectre::_fMin = 16;

// Maximal working frequency
double OSpectre::_fMax = 16000;

double OSpectreAbstract::_defaultValue = TY_SPECTRE_DEFAULT_VALUE;

// Standardized central third-octave frequencies table in Hz.
const double OSpectre::_freqNorm[] = {
    /*  0 */ 16.0,   20.0,    25.0,
    /*  3 */ 31.5,   40.0,    50.0,
    /*  6 */ 63.0,   80.0,    100.0,
    /*  9 */ 125.0,  160.0,   200.0,
    /* 12 */ 250.0,  315.0,   400.0,
    /* 15 */ 500.0,  630.0,   800.0,
    /* 18 */ 1000.0, 1250.0,  1600.0,
    /* 21 */ 2000.0, 2500.0,  3150.0,
    /* 24 */ 4000.0, 5000.0,  6300.0,
    /* 27 */ 8000.0, 10000.0, 12500.0,
    /* 30 */ 16000.0};

std::map<double, int> OSpectre::_mapFreqIndice = setMapFreqIndice();

OSpectreAbstract::OSpectreAbstract()
    : _valid(true), _type(SPECTRE_TYPE_ATT), _etat(SPECTRE_ETAT_DB), _form(SPECTRE_FORM_UNSHAPED)
{
}

OSpectreAbstract::OSpectreAbstract(TYSpectreForm form)
    : _valid(true), _type(SPECTRE_TYPE_ATT), _etat(SPECTRE_ETAT_DB), _form(form)
{
}

OSpectreAbstract::OSpectreAbstract(bool isValid, TYSpectreType type, TYSpectreEtat etat, TYSpectreForm form)
    : _valid(isValid), _type(type), _etat(etat), _form(form)
{
}

OSpectreAbstract& OSpectreAbstract::operator=(const OSpectreAbstract& other)
{
    if (this != &other)
    {
        _valid = other._valid;
        _type = other._type;
        _etat = other._etat;
        _form = other._form;
    }
    return *this;
}

bool OSpectreAbstract::operator==(const OSpectreAbstract& other) const
{
    if (this != &other)
    {
        if (_valid != other._valid)
        {
            return false;
        }
        if (_type != other._type)
        {
            return false;
        }
        if (_etat != other._etat)
        {
            return false;
        }
        if (_form != other._form)
        {
            return false;
        }
    }
    return true;
}

bool OSpectreAbstract::operator!=(const OSpectreAbstract& other) const
{
    return !operator==(other);
}

OSpectreAbstract& OSpectreAbstract::operator+(const double& valeur) const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;
    s._etat = _etat;
    s._type = _type; // Copy spectrum fingerprint
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = this->getTabValReel()[i] + valeur;
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::operator+(const OSpectreAbstract& spectre) const
{
    CHECK_FORM_AND_GET_CONCRETE_INSTANCE;
    s._etat = _etat;
    s._type = _type; // Copy spectrum fingerprint
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = this->getTabValReel()[i] + spectre.getTabValReel()[i];
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::operator-(const OSpectreAbstract& spectre) const
{
    CHECK_FORM_AND_GET_CONCRETE_INSTANCE;
    s._etat = _etat;
    s._type = _type; // Copy spectrum fingerprint
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = this->getTabValReel()[i] - spectre.getTabValReel()[i];
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::operator*(const OSpectreAbstract& spectre) const
{
    CHECK_FORM_AND_GET_CONCRETE_INSTANCE;
    s._etat = _etat;
    s._type = _type; // Copy spectrum fingerprint
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = this->getTabValReel()[i] * spectre.getTabValReel()[i];
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::operator*(const double& coefficient) const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;
    s._etat = _etat;
    s._type = _type; // Copy spectrum fingerprint
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = this->getTabValReel()[i] * coefficient;
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::sumdB(const OSpectreAbstract& spectre) const
{
    OSpectreAbstract& tempoS1(this->toGPhy());
    OSpectreAbstract& tempoS2(spectre.toGPhy());
    OSpectreAbstract& s = tempoS1 + tempoS2;
    // Copy spectrum fingerprint
    s._type = _type;
    // Spectrum is converted again in dB
    return s.toDB();
}

unsigned int OSpectreAbstract::getNbValues() const
{
    unsigned int nbFreq = TY_SPECTRE_DEFAULT_NB_ELMT;
    switch (_form)
    {
        case SPECTRE_FORM_OCT:
            nbFreq = TY_SPECTRE_OCT_NB_ELMT;
            break;
        default:
            nbFreq = TY_SPECTRE_DEFAULT_NB_ELMT;
            break;
    }
    return nbFreq;
}

void OSpectreAbstract::setDefaultValue(const double& valeur)
{
    setDefaultValue(getTabValReel(), getNbValues(), valeur);
}

void OSpectreAbstract::setDefaultValue(double module[], const unsigned int spectreNbElmt,
                                       const double& valeur)
{
    for (unsigned int i = 0; i < spectreNbElmt; i++)
    {
        module[i] = valeur;
    }
}

void OSpectreAbstract::getRangeValueReal(double* valeurs, const short& nbVal, const short& decalage)
{
    for (short i = 0; i < nbVal; i++)
    {
        valeurs[i] = getTabValReel()[i + decalage];
    }
}

OSpectreAbstract& OSpectreAbstract::sum(const OSpectreAbstract& spectre) const
{
    CHECK_FORM_AND_GET_CONCRETE_INSTANCE;

    s._etat = _etat;
    s._type = _type; // Copy type
    s._form = _form; // Copy form

    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = this->getTabValReel()[i] + spectre.getTabValReel()[i];
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::sum(const double& value) const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;
    s.setDefaultValue(value);

    return sum(s);
}

OSpectreAbstract& OSpectreAbstract::mult(const OSpectreAbstract& spectre) const
{
    CHECK_FORM_AND_GET_CONCRETE_INSTANCE;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = this->getTabValReel()[i] * spectre.getTabValReel()[i];
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::mult(const double& coefficient) const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    s._form = _form;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = this->getTabValReel()[i] * coefficient;
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::div(const OSpectreAbstract& spectre) const
{
    CHECK_FORM_AND_GET_CONCRETE_INSTANCE;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        if (spectre.getTabValReel()[i] == 0.0)
        {
            s.getTabValReel()[i] = 1E20;
            s._valid = false;
            break;
        }

        s.getTabValReel()[i] = getTabValReel()[i] / spectre.getTabValReel()[i];
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::div(const double& coefficient) const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    if (coefficient == 0.0) // Division by zero
    {
        s._valid = false;
    }
    else
    {
        for (unsigned int i = 0; i < getNbValues(); i++)
        {
            s.getTabValReel()[i] = this->getTabValReel()[i] / coefficient;
        }
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::subst(const OSpectreAbstract& spectre) const
{
    CHECK_FORM_AND_GET_CONCRETE_INSTANCE;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = this->getTabValReel()[i] - spectre.getTabValReel()[i];
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::subst(const double& valeur) const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = this->getTabValReel()[i] - valeur;
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::invMult(const double& coefficient) const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        if (getTabValReel()[i] == 0.0)
        {
            s.getTabValReel()[i] = 1E20;
            s._valid = false;
            break;
        }

        s.getTabValReel()[i] = coefficient / getTabValReel()[i];
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::inv() const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        if (getTabValReel()[i] == 0.0)
        {
            s.getTabValReel()[i] = 1E20;
            s._valid = false;
            break;
        }

        s.getTabValReel()[i] = 1.0 / getTabValReel()[i];
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::power(const double& puissance) const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = pow(this->getTabValReel()[i], puissance);
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::log(const double& base) const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    double logBase = ::log(base);
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        if (getTabValReel()[i] <= 0.0)
        {
            s.getTabValReel()[i] = 1E20;
            s._valid = false;
            break;
        }

        s.getTabValReel()[i] = ::log(getTabValReel()[i]) / logBase;
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::racine() const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        if (getTabValReel()[i] < 0.0)
        {
            s.getTabValReel()[i] = 1E20;
            s._valid = false;
            break;
        }

        s.getTabValReel()[i] = ::sqrt(getTabValReel()[i]);
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::exp(const double coef)
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = ::exp(coef * getTabValReel()[i]);
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::sin() const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;
    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = ::sin(this->getTabValReel()[i]);
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::cos() const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = ::cos(this->getTabValReel()[i]);
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::abs() const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;
    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = fabs(getTabValReel()[i]);
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::sqrt() const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = ::sqrt(this->getTabValReel()[i]);
    }
    return s;
}

double OSpectreAbstract::valMax()
{
    double result = 0.0;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        result = std::max(this->getTabValReel()[i], result);
    }
    return result;
}

OSpectreAbstract& OSpectreAbstract::seuillage(const double& min, const double max)
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    // If min<=max, we return original spectrum
    if (min >= max)
    {
        return *this;
    }
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = getTabValReel()[i] <= min ? min : getTabValReel()[i];
        s.getTabValReel()[i] = s.getTabValReel()[i] >= max ? max : s.getTabValReel()[i];
    }
    return s;
}

double OSpectreAbstract::sigma()
{
    double somme = 0;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        somme += getTabValReel()[i];
    }
    return somme;
}

const double OSpectreAbstract::sigma() const
{
    double somme = 0;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        somme += getTabValReel()[i];
    }
    return somme;
}

OSpectreAbstract& OSpectreAbstract::round()
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    s._valid = _valid;
    s._form = _form;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        s.getTabValReel()[i] = std::round(getTabValReel()[i] * 100) / 100;
    }
    return s;
}

OSpectreAbstract& OSpectreAbstract::toDB() const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    // If spectrum is already in dB state, we just copy values
    if (_etat == SPECTRE_ETAT_DB)
    {
        s = *this;
        return s;
    }
    unsigned int i = 0;
    s._type = _type; // Copy type
    double coef = 1.0;
    switch (_type)
    {
        case SPECTRE_TYPE_LP:
            coef = 4.0e-10;
            break;
        case SPECTRE_TYPE_LW:
            coef = 1.0e-12;
            break;
        case SPECTRE_TYPE_ATT:
        default:
            coef = 1.0;
            break;
    }
    double dBVal = 0.0;
    for (i = 0; i < getNbValues(); i++)
    {
        // avoid -infinite result
        double val = getTabValReel()[i];
        if (val < EPSILON_15)
        {
            val = EPSILON_15;
        }

        dBVal = 10.0 * log10((val) / coef);
        s.getTabValReel()[i] = dBVal;
    }
    s._etat = SPECTRE_ETAT_DB; // Indicate explicitly dB state
    return s;
}

OSpectreAbstract& OSpectreAbstract::toGPhy() const
{
    OSpectreAbstract* sp = getConcreteInstance();
    OSpectreAbstract& s = *sp;

    if (_etat == SPECTRE_ETAT_LIN) // If spectrum is already in LIN state, we just copy values
    {
        s = *this;
        return s;
    }
    unsigned int i = 0;
    double coef = 1.0;
    s._type = _type; // Copy type
    s._form = _form; // Copy form
    switch (_type)
    {
        case SPECTRE_TYPE_LP:
            coef = 4.0e-10;
            break;
        case SPECTRE_TYPE_LW:
            coef = 1.0e-12;
            break;
        case SPECTRE_TYPE_ATT:
        default:
            coef = 1.0;
            break;
    }
    double powVal = 0.0;
    for (i = 0; i < getNbValues(); i++)
    {
        powVal = pow(10.0, this->getTabValReel()[i] / 10.0) * coef;
        s.getTabValReel()[i] = powVal;
    }
    s._etat = SPECTRE_ETAT_LIN; // Indicate explicitly 'Physical Measure' state
    return s;
}

double OSpectreAbstract::valGlobDBLin() const
{
    OSpectreAbstract& s = this->toDB();
    double valeurGlob = 0.0;
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        valeurGlob = valeurGlob + pow(10, (s.getTabValReel()[i] / 10));
    }
    valeurGlob = 10 * log10(valeurGlob);
    return valeurGlob;
}

double OSpectreAbstract::valGlobDBA() const
{

    double ret = -1.0;
    if (getNbValues() == TY_SPECTRE_DEFAULT_NB_ELMT && this->getForm() == SPECTRE_FORM_TIERS)
    {
        OSpectreAbstract& s(this->toDB() + OSpectre::pondA());
        ret = s.valGlobDBLin();
    }
    else
    {
        OSpectreAbstract& s1 = this->toDB();
        OSpectreAbstract& s(s1 + OSpectreOctave::pondA());
        ret = s.valGlobDBLin();
    }
    return ret;
}

OSpectreAbstract& OSpectreAbstract::toTOct() const
{
    // The return spectrum must be of type OSpectre
    OSpectre* sp = new OSpectre(OSpectre::getEmptyLinSpectre());
    OSpectreAbstract& s = *sp;

    if (_form == SPECTRE_FORM_TIERS) // If object is already in third-octave
    {
        s = *this;
        return s;
    }
    else if (_form != SPECTRE_FORM_OCT) // If spectrum is not in octave
    {
        s._valid = false;
        return s;
    }
    s._type = _type;
    if (_type == SPECTRE_TYPE_ABSO || _type == SPECTRE_TYPE_AUTRE)
    {
        const OSpectreAbstract& travail = *this;
        short indice = 2;
        short nbOctValue = 9;
        double valeur = 0.0;
        for (short i = 0; i < nbOctValue; i++, indice += 3)
        {
            valeur = travail.getTabValReel()[i];
            s.getTabValReel()[indice] = valeur;
            s.getTabValReel()[indice + 1] = valeur;
            s.getTabValReel()[indice + 2] = valeur;
        }
        s.getTabValReel()[0] = s.getTabValReel()[2];
        s.getTabValReel()[1] = s.getTabValReel()[2];
        s.getTabValReel()[29] = s.getTabValReel()[28];
        s.getTabValReel()[30] = s.getTabValReel()[28];
        s._etat = SPECTRE_ETAT_DB;
    }
    else
    {
        OSpectreAbstract& travail = this->toGPhy();
        short indice = 2;
        short nbOctValue = 9;
        double valeur = 0.0;
        double coef = 3.0;
        if (_type == SPECTRE_TYPE_ATT)
        {
            coef = 1.0;
        }
        for (short i = 0; i < nbOctValue; i++, indice += 3)
        {
            valeur = travail.getTabValReel()[i] / coef;
            s.getTabValReel()[indice] = valeur;
            s.getTabValReel()[indice + 1] = valeur;
            s.getTabValReel()[indice + 2] = valeur;
        }
        if (_type == SPECTRE_TYPE_ATT)
        {
            s.getTabValReel()[0] = 1;
            s.getTabValReel()[1] = 1;
            s.getTabValReel()[29] = 1;
            s.getTabValReel()[30] = 1;
        }
        s = s.toDB();
    }
    s._form = SPECTRE_FORM_TIERS; // Indicate explicitly thrid-octave form
    return s;
}

OSpectreAbstract& OSpectreAbstract::toOct() const
{
    OSpectreAbstract& s = OSpectreAbstract::makeOctSpect();
    if (_form == SPECTRE_FORM_OCT) // If spectrum already is in octave
    {
        s = *this;
        return s;
    }
    else if (_form != SPECTRE_FORM_TIERS) // If spectrum is not in thrid-octave
    {
        s._valid = false;
        return s;
    }
    s._etat = SPECTRE_ETAT_LIN; // s is in lin state too at this moment
    s._type = _type;
    if (_type == SPECTRE_TYPE_ABSO || _type == SPECTRE_TYPE_AUTRE)
    {
        const OSpectreAbstract& travail = *this;
        unsigned int indiceDepart = 2;
        unsigned int indice = 0;
        double valeur = 0.0;
        for (unsigned int i = indiceDepart; i < TY_SPECTRE_DEFAULT_NB_ELMT - 2; i += 3, indice++)
        {
            valeur = (travail.getTabValReel()[i] + travail.getTabValReel()[i + 1] +
                      travail.getTabValReel()[i + 2]) /
                     3; // Absorptions mean
            s.getTabValReel()[indice] = valeur;
        }
        s._etat = SPECTRE_ETAT_DB;
    }
    else
    {
        OSpectreAbstract& travail = this->toGPhy();
        unsigned int indiceDepart = 2;
        unsigned int indice = 0;
        double valeur = 0.0;
        double coef = 1.0;
        if (_type == SPECTRE_TYPE_ATT)
        {
            coef = 3.0;
        }
        for (unsigned int i = indiceDepart; i < TY_SPECTRE_DEFAULT_NB_ELMT - 2; i += 3, indice++)
        {
            valeur = (travail.getTabValReel()[i] + travail.getTabValReel()[i + 1] +
                      travail.getTabValReel()[i + 2]) /
                     coef;
            s.getTabValReel()[indice] = valeur;
        }
        s = s.toDB();
    }
    s._form = SPECTRE_FORM_OCT; //  Indicate explicitly octave form
    return s;
}

void OSpectreAbstract::printme() const
{
    std::cout << "Spectrum values: ";
    for (unsigned int i = 0; i < getNbValues(); i++)
    {
        std::cout << getTabValReel()[i] << " ";
    }
    std::cout << std::endl;
}

OSpectreAbstract& OSpectreAbstract::makeOctSpect()
{
    OSpectre* s = new OSpectre(OSpectre::makeOctSpect());
    return *s;
}

OSpectre::OSpectre() : OSpectreAbstract(SPECTRE_FORM_TIERS)
{
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        _module[i] = _defaultValue;
    }
}

OSpectre::OSpectre(double defaultValue) : OSpectreAbstract(SPECTRE_FORM_TIERS)
{
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        _module[i] = defaultValue;
    }
}

OSpectre::OSpectre(const double* valeurs, unsigned nbVal, unsigned decalage)
    : OSpectreAbstract(SPECTRE_FORM_TIERS)
{
    unsigned int i = 0;
    // First init values
    for (i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        _module[i] = _defaultValue;
    }
    unsigned int maxInd = std::min(nbVal + decalage, TY_SPECTRE_DEFAULT_NB_ELMT);
    for (i = decalage; i < maxInd; i++)
    {
        _module[i] = valeurs[i - decalage];
    }
}

OSpectre::OSpectre(const OSpectre& other) : OSpectre()
{
    *this = other;
}

OSpectre::OSpectre(const OSpectreAbstract& other)
{
    *this = other;
}

OSpectre::~OSpectre() {}

OSpectre& OSpectre::operator=(const OSpectre& other)
{
    if (this != &other)
    {
        OSpectreAbstract::operator=(other);
        for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
        {
            _module[i] = other._module[i];
        }
    }
    return *this;
}

OSpectreAbstract& OSpectre::operator=(const OSpectreAbstract& other)
{
    if (this != &other)
    {
        const OSpectre* sp;
        // If other of type OSpectre just cast it
        sp = dynamic_cast<const OSpectre*>(&other);
        // If not, try to cast to an OSpectreOctave and convert to third-octave band
        if (sp == nullptr)
        {
            const OSpectreOctave* spo = dynamic_cast<const OSpectreOctave*>(&other);
            if (spo != nullptr)
            {
                OSpectre s = spo->toTOct();
                sp = &s;
            }
            // If neither conversions are possible raise error
            else
            {
                std::ostringstream oss;
                oss << "Impossible to convert from " << other.getForm() << " to OSpectre";
                assert(false && oss.str().c_str());
            }
        }
        *this = *sp;
    }
    return *this;
}

bool OSpectre::operator==(const OSpectre& other) const
{
    if (this != &other)
    {
        if ((OSpectreAbstract::operator!=(other)))
        {
            return false;
        }
        for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
        {
            if (_module[i] != other._module[i])
            {
                return false;
            }
        }
    }
    return true;
}

bool OSpectre::operator!=(const OSpectre& other) const
{
    return !operator==(other);
}

OSpectreAbstract* OSpectre::getConcreteInstance() const
{
    return new OSpectre();
}

void OSpectre::setValue(const double& freq, const double& reel /*=0.0*/)
{
    int indice = _mapFreqIndice[freq];
    _module[indice] = reel;
}

double OSpectre::getValueReal(double freq)
{
    int indice = _mapFreqIndice[freq];
    return _module[indice];
}

// Static functions

std::map<double, int> OSpectre::setMapFreqIndice()
{
    std::map<double, int> mapFI;
    double value = 0.0;
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        value = _freqNorm[i];
        mapFI[value] = i;
    }
    return mapFI;
}

OTabFreq OSpectre::getTabFreqExact()
{
    OTabFreq tabFreqExact;
    for (int i = 0; i < (int)TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        tabFreqExact.push_back(1000.0 * pow(pow(10.0, 0.1), i - 18));
    }
    return tabFreqExact;
}

OSpectre OSpectre::getOSpectreFreqExact()
{
    OSpectre tabFreqExact;
    for (int i = 0; i < (int)TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        tabFreqExact._module[i] = 1000.0 * pow(pow(10.0, 0.1), i - 18);
    }
    return tabFreqExact;
}

OSpectre OSpectre::pondA()
{
    OSpectre s;
    OTabFreq tabFreqExact = getTabFreqExact();
    long v1 = 12200 * 12200;
    double v2 = 20.6 * 20.6;
    double v3 = 107.7 * 107.7;
    double v4 = 737.9 * 737.9;
    double f2 = 1000.0 * 1000.0; // squared frequency
    double v1000 =
        (v1 * f2 * f2) / ((f2 + v1) * (f2 + v2) * ::sqrt((f2 + v3) * (f2 + v4))); // reference at 1000 Hz
    double valeur = 0.0;
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        f2 = tabFreqExact[i] * tabFreqExact[i];
        valeur = (v1 * f2 * f2) / ((f2 + v1) * (f2 + v2) * ::sqrt((f2 + v3) * (f2 + v4)));
        valeur = 20 * log10(valeur / v1000);
        s._module[i] = valeur;
    }
    return s;
}

OSpectre OSpectre::pondB()
{
    OSpectre s;
    OTabFreq tabFreqExact = getTabFreqExact();
    long v1 = 12200 * 12200;
    double v2 = 20.6 * 20.6;
    double v3 = 158.5 * 158.5;
    double f = 1000.0;
    double f2 = f * f;                                                        // squared frequency
    double v1000 = (v1 * f2 * f) / ((f2 + v1) * (f2 + v2) * ::sqrt(f2 + v3)); // reference at 1000 Hz
    double valeur = 0.0;
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        f = tabFreqExact[i];
        f2 = f * f;
        valeur = (v1 * f2 * f) / ((f2 + v1) * (f2 + v2) * ::sqrt(f2 + v3));
        valeur = 20 * log10(valeur / v1000);
        s._module[i] = valeur;
    }
    return s;
}

OSpectre OSpectre::pondC()
{
    OSpectre s;
    OTabFreq tabFreqExact = getTabFreqExact();
    long v1 = 12200 * 12200;
    double v2 = 20.6 * 20.6;
    double f2 = 1000.0 * 1000.0;                        // squared frequency
    double v1000 = (v1 * f2) / ((f2 + v1) * (f2 + v2)); // reference at 1000 Hz
    double valeur = 0.0;
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        f2 = tabFreqExact[i] * tabFreqExact[i];
        valeur = (v1 * f2) / ((f2 + v1) * (f2 + v2));
        valeur = 20 * log10(valeur / v1000);
        s._module[i] = valeur;
    }
    return s;
}

OSpectre OSpectre::getLambda(const double& c)
{
    OSpectre s;
    // Copy spectrum fingerprint
    s._etat = SPECTRE_ETAT_LIN;
    s._type = SPECTRE_TYPE_AUTRE;
    OTabFreq tabFreqExact = getTabFreqExact();
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        s._module[i] = c / tabFreqExact[i];
    }
    return s;
}

bool OSpectre::isTonalite() const
{
    if ((_etat == SPECTRE_ETAT_LIN) || (_form == SPECTRE_FORM_OCT))
    {
        return false;
    }
    const double seuil = 315;
    double a = NAN, b = NAN, c = NAN, d = NAN, e = NAN, ab = NAN, de = NAN, diffG = NAN, diffD = NAN,
           freq = NAN, tolerence = NAN;
    a = _module[0];
    b = _module[1];
    c = _module[2];
    d = _module[3];
    e = _module[4];
    tolerence = 10.0;
    for (unsigned int i = 5; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        ab = 10 * ::log10(((::pow(10, a / 10.0) + ::pow(10, b / 10.0)) / 2.0));
        de = 10 * ::log10(((::pow(10, d / 10.0) + ::pow(10, e / 10.0)) / 2.0));
        diffG = c - ab;
        diffD = c - de;
        freq = OSpectre::_freqNorm[i - 3];
        if (freq > seuil)
        {
            tolerence = 5.0;
        }
        if ((diffG > tolerence) && (diffD > tolerence))
        {
            return true;
        }
        a = b;
        b = c;
        c = d;
        d = e;
        e = _module[i];
    }
    return false;
}

OSpectre OSpectre::getEmptyLinSpectre(const double& valInit)
{
    OSpectre s(valInit);
    s._etat = SPECTRE_ETAT_LIN;
    return s;
}

OSpectre OSpectre::makeOctSpect()
{
    OSpectre s;
    s._form = SPECTRE_FORM_OCT;
    return s;
}

::std::ostream& operator<<(::std::ostream& os, const OSpectre& s)
{
    os << "Spectrum["
       << "type=" << s.getType() << ", state=" << s.getEtat() << "]" << std::endl;
    os << "        (";
    for (unsigned i = 0; i < s.getNbValues(); ++i)
    {
        os << s.getTabValReel()[i] << ", ";
    }
    os << ")" << std::endl;
    return os;
}

/* OSpectreComplex ************************************************************/

OSpectreComplex::OSpectreComplex() : OSpectre()
{
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        _phase[i] = 0.0;
    }
}

OSpectreComplex::OSpectreComplex(const TYComplex& defaultValue) : OSpectre(defaultValue.real())
{
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        _phase[i] = defaultValue.imag();
    }
}

OSpectreComplex::OSpectreComplex(const OSpectreComplex& other)
{
    *this = other;
}

OSpectreComplex::OSpectreComplex(const OSpectre& other)
{
    OSpectre::operator=(other);
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        _phase[i] = 0.0;
    }
}

OSpectreComplex::OSpectreComplex(const OSpectre& spectre1, const OSpectre& spectre2)
{
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        _module[i] = (spectre1.getTabValReel())[i];
        _phase[i] = (spectre2.getTabValReel())[i];
    }
}

OSpectreComplex::~OSpectreComplex() {}

OSpectreComplex& OSpectreComplex::operator=(const OSpectreComplex& other)
{
    if (this != &other)
    {
        OSpectre::operator=(other);

        for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
        {
            _phase[i] = other._phase[i];
        }
    }
    return *this;
}

OSpectreAbstract& OSpectreComplex::operator=(const OSpectreAbstract& other)
{
    if (this != &other)
    {
        const OSpectreComplex* sc;
        sc = dynamic_cast<const OSpectreComplex*>(&other);

        // If other of type OSpectreComplex just cast it
        if (sc != nullptr)
        {
            *this = *sc;
        }
        // If not, convert from an OSpectre and init phase values to zero
        else
        {
            OSpectre::operator=(other);
            for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
            {
                _phase[i] = 0;
            }
        }
    }
    return *this;
}

bool OSpectreComplex::operator==(const OSpectreComplex& other) const
{
    if (this != &other)
    {
        if ((OSpectre::operator!=(other)))
        {
            return false;
        }

        for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
            if (_phase[i] != other._phase[i])
            {
                return false;
            }
    }
    return true;
}

bool OSpectreComplex::operator!=(const OSpectreComplex& other) const
{
    return !operator==(other);
}

OSpectreComplex OSpectreComplex::operator+(const OSpectreComplex& spectre) const
{
    OSpectreComplex s;
    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    // A complex number is defined as z = a + ib
    std::complex<double> z1; // This is the original one
    std::complex<double> z2; // This is the one we add
    std::complex<double> z3; // This is the returned complex number i.e. the result
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        z1 = std::polar(_module[i], _phase[i]);
        z2 = std::polar(spectre._module[i], spectre._phase[i]);
        z3 = z1 + z2;

        s._module[i] = std::abs(z3);
        s._phase[i] = std::arg(z3);
    }
    return s;
}

OSpectreComplex OSpectreComplex::operator*(const OSpectreComplex& spectre) const
{
    // Product of two complex in modulus/phase
    //  = modulus product and phases sum
    OSpectreComplex s;
    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        s._module[i] = _module[i] * spectre._module[i];
        s._phase[i] = _phase[i] + spectre._phase[i];
    }
    return s;
}

OSpectreComplex OSpectreComplex::operator*(const OSpectre& spectre) const
{
    // Product of two complex in modulus/phase
    //  = modulus product and phases sum
    OSpectreComplex s;
    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        s._module[i] = _module[i] * (spectre.getTabValReel())[i];
        s._phase[i] = _phase[i];
    }
    return s;
}

OSpectreComplex OSpectreComplex::operator*(const double& coefficient) const
{
    OSpectreComplex s;
    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        s._module[i] = _module[i] * coefficient;
        s._phase[i] = _phase[i];
    }
    return s;
}

OSpectreComplex OSpectreComplex::operator/(const OSpectreComplex& spectre) const
{
    // Quotient of two complex in modulus/phase
    //  = modulus quotient and phases difference
    OSpectreComplex s;
    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        s._module[i] = _module[i] / spectre._module[i];
        s._phase[i] = _phase[i] - spectre._phase[i];
    }
    return s;
}

void OSpectreComplex::setValue(const double& freq, const double& reel, const double& imag)
{
    int indice = _mapFreqIndice[freq];
    _module[indice] = reel;
    _phase[indice] = imag;
}

void OSpectreComplex::setValue(const double& freq, const TYComplex& cplx)
{
    setValue(freq, cplx.real(), cplx.imag());
}

double OSpectreComplex::getValueImag(double freq, bool* pValid)
{
    int indice = _mapFreqIndice[freq];
    return _phase[indice];
}

void OSpectreComplex::setPhase(const OSpectre& spectre)
{
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        _phase[i] = spectre.getTabValReel()[i];
    }
}

void OSpectreComplex::setPhase(const double& valeur)
{
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        _phase[i] = valeur;
    }
}

OSpectre OSpectreComplex::getPhase() const
{
    OSpectre s = OSpectre::getEmptyLinSpectre();
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        s.getTabValReel()[i] = _phase[i];
    }
    return s;
}

OSpectre OSpectreComplex::getModule() const
{
    OSpectre s = OSpectre::getEmptyLinSpectre();
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        s.getTabValReel()[i] = _module[i];
    }
    return s;
}

OSpectreComplex OSpectreComplex::toModulePhase() const
{
    OSpectreComplex s;
    double module = NAN, phase = NAN, reel = NAN, imag = NAN;
    // Copy spectrum fingerprint
    s._etat = _etat;
    s._type = _type;
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        reel = this->_module[i];
        imag = this->_phase[i];
        module = ::sqrt(reel * reel + imag * imag);
        phase = atan2(imag, reel);
        s._module[i] = module;
        s._phase[i] = phase;
    }
    return s;
}

OSpectreComplex OSpectreComplex::getCplxSpectre(const double& valInit)
{
    OSpectreComplex s(OSpectre::getEmptyLinSpectre(valInit));
    for (unsigned int i = 0; i < TY_SPECTRE_DEFAULT_NB_ELMT; i++)
    {
        s._phase[i] = 0.0;
    }
    return s;
}

/* OSpectreOctave ************************************************************/
// Standardized central octave frequencies table in Hz.
const double OSpectreOctave::_freqNorm[] = {31.5, 63.0, 125.0, 250.0, 500.0, 1000.0, 2000.0, 4000.0, 8000.0};

const double OSpectreOctave::_AWeighting[] = {-39.5, -26.2, -16.1, -8.6, -3.2, 0.0, 1.2, 1.0, -1.1};

std::map<double, int> OSpectreOctave::_mapFreqIndice = setMapFreqIndice();

OSpectreOctave::OSpectreOctave() : OSpectreAbstract(SPECTRE_FORM_OCT)
{
    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        _module[i] = _defaultValue;
    }
}

OSpectreOctave::OSpectreOctave(bool isValid, TYSpectreType type, TYSpectreEtat etat)
    : OSpectreAbstract(isValid, type, etat, SPECTRE_FORM_OCT)
{
    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        _module[i] = _defaultValue;
    }
}

OSpectreOctave::OSpectreOctave(double defaultValue) : OSpectreAbstract(SPECTRE_FORM_OCT)
{
    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        _module[i] = defaultValue;
    }
}

OSpectreOctave::OSpectreOctave(const double* valeurs, unsigned nbVal, unsigned decalage)
    : OSpectreAbstract(SPECTRE_FORM_OCT)
{
    unsigned int i;
    // D'abord on initialise les valeurs
    for (i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        _module[i] = _defaultValue;
    }
    unsigned int maxInd = std::min(nbVal + decalage, TY_SPECTRE_OCT_NB_ELMT);
    for (i = decalage; i < maxInd; i++)
    {
        _module[i] = valeurs[i - decalage];
    }
}

OSpectreOctave::OSpectreOctave(const OSpectreOctave& other) : OSpectreAbstract(SPECTRE_FORM_OCT)
{
    *this = other;
}

OSpectreOctave::OSpectreOctave(const OSpectreAbstract& other) : OSpectreAbstract(SPECTRE_FORM_OCT)
{
    *this = other;
}

OSpectreOctave::OSpectreOctave(const OSpectre& other) : OSpectreAbstract(SPECTRE_FORM_OCT)
{
    OSpectreAbstract& s = other.toOct();
    _valid = s.isValid();
    _type = s.getType();
    _etat = s.getEtat();
    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        _module[i] = s.getTabValReel()[i];
    }
}

OSpectreOctave::~OSpectreOctave() {}

OSpectreOctave& OSpectreOctave::operator=(const OSpectreOctave& other)
{
    if (this != &other)
    {
        OSpectreAbstract::operator=(other);
        for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
        {
            _module[i] = other._module[i];
        }
    }
    return *this;
}

OSpectreAbstract& OSpectreOctave::operator=(const OSpectreAbstract& other)
{
    if (this != &other)
    {

        const OSpectreOctave* so;
        // If other if of octave form then build OSpectreOctave from values
        if (other.getForm() == SPECTRE_FORM_OCT)
        {
            so = dynamic_cast<const OSpectreOctave*>(&other);
        }
        else if (other.getForm() == SPECTRE_FORM_TIERS)
        // If other is third-octave form then convert other to octave before building from values
        {
            OSpectreAbstract& otherOctave = other.toOct();
            // In this case toOct() returns an OSPectre object so we need to convert it
            const OSpectre* s = dynamic_cast<const OSpectre*>(&otherOctave);
            const OSpectreOctave so1{*s};
            so = &so1;
        }
        else
        {
            std::ostringstream oss;
            oss << "Impossible to convert from " << other.getForm() << " to OSpectreOctave";
            assert(false && oss.str().c_str());
        }
        if (so != nullptr)
        {
            *this = *so;
        }
        else
        {
            assert(false && "Invalid OSpectreOctave object during conversion");
        }
    }
    return *this;
}

bool OSpectreOctave::operator==(const OSpectreOctave& other) const
{
    if (this != &other)
    {
        if ((OSpectreAbstract::operator!=(other)))
        {
            return false;
        }
        for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
        {
            if (_module[i] != other._module[i])
            {
                return false;
            }
        }
    }
    return true;
}

bool OSpectreOctave::operator!=(const OSpectreOctave& other) const
{
    return !operator==(other);
}

OSpectreAbstract* OSpectreOctave::getConcreteInstance() const
{
    return new OSpectreOctave();
}

void OSpectreOctave::setValue(const double& freq, const double& reel /*=0.0*/)
{
    int indice = _mapFreqIndice[freq];
    _module[indice] = reel;
}

// Static functions

OTabFreq OSpectreOctave::getTabFreqExact()
{
    OTabFreq tabFreqExact;
    short indice = 2;
    for (int i = 0; i < (int)TY_SPECTRE_OCT_NB_ELMT; i++, indice += 3)
    {
        tabFreqExact.push_back(1000.0 * pow(pow(10.0, 0.1), indice + 1 - 18));
    }
    return tabFreqExact;
}

OSpectreOctave OSpectreOctave::pondA()
{
    return OSpectreOctave{_AWeighting, 9, 0};
}

OSpectreOctave OSpectreOctave::getLambda(const double& c)
{
    OSpectreOctave s;
    // Copy spectrum fingerprint
    s._etat = SPECTRE_ETAT_LIN;
    s._type = SPECTRE_TYPE_AUTRE;
    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        s._module[i] = c / _freqNorm[i];
    }
    return s;
}

std::map<double, int> OSpectreOctave::setMapFreqIndice()
{
    std::map<double, int> mapFI;
    double value = 0.0;
    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        value = _freqNorm[i];
        mapFI[value] = i;
    }
    return mapFI;
}

OSpectreOctave OSpectreOctave::getEmptyLinSpectre(const double& valInit)
{
    OSpectreOctave s(valInit);
    s._etat = SPECTRE_ETAT_LIN;
    return s;
}
