/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file data_model_common.hpp
 * \brief Common utilities and includes for all data models
 *
 *  Created on: 6 nov. 2012
 *      Author: Anthony Truchet <anthony.truchet@logilab.fr>
 */

#ifndef TYMPAN__DATA_MODEL_COMMON_HPP__INCLUDED
#define TYMPAN__DATA_MODEL_COMMON_HPP__INCLUDED

#include "std_boost_utils.hpp"

#include "Tympan/models/common/3d.h"
#include "Tympan/models/common/spectre.h"

// This section import some types from CommonTools
// under a more generic name into the tympan namespace. Indeed,
// there must be no dependency to TY-classes and no direct
// ones to O-classes from within the solver data model.
namespace tympan
{
typedef OPoint3D Point;
typedef OVector3D Vector;
typedef OSpectre Spectrum;
typedef OSpectreComplex ComplexSpectrum;

typedef std::deque<Point> nodes_pool_t;
typedef size_t node_idx;
} // namespace tympan

namespace tympan
{

/**
 * @brief The base of all entity classes.
 *
 * This class is \em required to be \em virtually inherited by all
 * entity classes. It provides some convenient dynamic access and cast method.
 */
class BaseEntity
{
protected:
    BaseEntity(){}; //!< Constructor

public:
    virtual ~BaseEntity(){}; //!< Destructor

    //    virtual tympan::shared_ptr<BaseEntity> ptr_any() = 0;
    //    virtual tympan::shared_ptr<BaseEntity const> ptr_any() const = 0;
    //
    //    template <class T>
    //    tympan::shared_ptr<T> ptr()
    //    { return boost::dynamic_pointer_cast<T>(ptr_any()); }
    //
    //    template <class T>
    //    tympan::shared_ptr<T const> ptr() const
    //    { return boost::dynamic_pointer_cast<T const>(ptr_any()); }
};

} /* namespace tympan */

#endif /* TYMPAN__DATA_MODEL_COMMON_HPP__INCLUDED */
