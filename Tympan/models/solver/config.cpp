/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "config.h"

namespace tympan
{

// Initialise a NULL.
LPSolverConfiguration SolverConfiguration::_pInstance = 0;

SolverConfiguration::SolverConfiguration()
{
    AtmosPressure = 101325.;
    AtmosTemperature = 20.;
    AtmosHygrometry = 50.;
    // AnalyticC0 = 340.;
    AnalyticGradC = 0.;
    AnalyticGradV = 0.;
    WindDirection = 0.;

    RayTracingOrder = 0;
    Discretization = 2;
    NbRaysPerSource = 10000;
    MaxLength = 5000;
    SizeReceiver = 10.;
    Accelerator = 1;
    MaxTreeDepth = 12;
    AngleDiffMin = 5.;
    CylindreThick = 0.5f;
    MaxProfondeur = 10;
    UseSol = 1;
    MaxReflexion = 5;
    MaxDiffraction = 2;
    DiffractionUseRandomSampler = false;
    NbRayWithDiffraction = 0;
    DiffractionDropDownNbRays = true;
    DiffractionFilterRayAtCreation = true;
    UsePathDifValidation = true;
    MaxPathDifference = 25.;
    DiffractionUseDistanceAsFilter = true;
    KeepDebugRay = false;
    UsePostFilters = true;
    MeshElementSizeMax = 0.0; //!< Not used
    RefineMesh = true;
    UseVolumesLandtake = false;

    // EnableFullTargets = false;
    // TargetsDensity = 0.1f;

    InitialAngleTheta = 0.;
    InitialAnglePhi = 0.;
    AnalyticNbRay = 20;
    AnalyticTMax = 10.;
    AnalyticH = 0.1;
    AnalyticDMax = 3000;

    AnalyticTypeTransfo = 1;

    MinSRDistance = 0.3;
    NbThreads = 4;
    UseRealGround = true;
    UseLateralDiffraction = true;
    UseReflection = true;
    KeepRays = false;

    PropaConditions = 0;
    DSWindDirection = 0;
    AngleFavorable = 45;
    AngleDefavorable = 45;

    H1parameter = 10.;
    ModSummation = false;

    DebugUseCloseEventSelector = true;
    DebugUseDiffractionAngleSelector = true;
    DebugUseDiffractionPathSelector = true;
    DebugUseFermatSelector = true;
    DebugUseFaceSelector = true;
}

LPSolverConfiguration SolverConfiguration::get()
{
    if (!_pInstance)
    {
        _pInstance = boost::shared_ptr<SolverConfiguration>(new SolverConfiguration());
    }
    return _pInstance;
}

void SolverConfiguration::set(LPSolverConfiguration config)
{
    _pInstance = config;
}

SolverConfiguration::~SolverConfiguration() {}

} // namespace tympan
