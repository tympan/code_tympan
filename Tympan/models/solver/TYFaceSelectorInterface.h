/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 *
 */

#ifndef __TYFACESELECTORINTERFACE__
#define __TYFACESELECTORINTERFACE__

/**
 * \brief Interface of TYFaceSelector classes
 */
class TYFaceSelectorInterface
{
public:
    TYFaceSelectorInterface() {}          //!< Constructor
    virtual ~TYFaceSelectorInterface() {} //!< Destructor
};

#endif // __TYFACESELECTORINTERFACE__
