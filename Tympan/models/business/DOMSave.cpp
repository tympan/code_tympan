/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>

#include <qfile.h>

#include "DOMSave.h"
#include <boost/foreach.hpp>
#include "Tympan/core/logging.h"

// using namespace std;

//////////////////////////////////////////////////////////////////////////////
// DOMSave
// ---------------------------------------------------------------------------
//  Local const data
//
//  Note: This is the 'safe' way to do these strings. If you compiler supports
//        L"" style strings, and portability is not a concern, you can use
//        those types constants directly.
// ---------------------------------------------------------------------------

int DOMSave::saveNodeToFile(DOM_Node& nodeToSave, const char* fileName)
{
    QFile file(fileName);
    QByteArray byte_array;
    QXmlStreamWriter xmlStream(&byte_array);
    QDomDocument docToSave;
    QDomElement elementToSave;
    QDomElement rootElement;
    bool ret = false;

    // If the node to save is already a document node print
    // it. Otherwise, create new document, import the node to save
    // into it and print it.
    if (nodeToSave.isDocument())
    {
        docToSave = nodeToSave.toDocument();
    }
    else
    {
        docToSave.importNode(nodeToSave, true);
    }

    xmlStream.setAutoFormatting(true);
    xmlStream.setAutoFormattingIndent(1);

    xmlStream.writeStartDocument("1.0", false);
    xmlStream.writeDTD("<!DOCTYPE Tympan SYSTEM 'Tympan.dtd'>");

    // Get root node of the document
    rootElement = docToSave.documentElement();

    ret = writeElementToStream(rootElement, xmlStream);

    xmlStream.writeEndDocument();

    // If writing to QXmlStreamWriter succeeds then open file to replace its content
    // Otherwise do not open file in order to preserve it
    if (ret)
    {
        if (!file.open(QIODevice::WriteOnly))
        {
            return 3;
        }
        file.write(byte_array);
        file.flush();
        file.close();
        return 0;
    }
    else
    {
        return -1;
    }
}

bool DOMSave::writeElementToStream(const QDomElement& pElement, QXmlStreamWriter& pStream)
{
    bool ret = true;

    // Writes pElement to pStream
    QString tagName = pElement.tagName();
    pStream.writeStartElement(tagName);

    QVector<QString> attribute_names;
    const QDomNamedNodeMap attributes = pElement.attributes();
    for (int i = 0; i < attributes.length(); ++i)
    {
        attribute_names << attributes.item(i).toAttr().name();
    }

    std::stable_sort(attribute_names.begin(), attribute_names.end());

    BOOST_FOREACH (QString attr_name, attribute_names)
    {
        const QDomAttr attr = attributes.namedItem(attr_name).toAttr();
        QString attrName = attr.name();
        QString attrValue = attr.value();
        pStream.writeAttribute(attrName, attrValue);
    }

    // Build node list from pElement in order to write recursively all QDomElement
    const QDomNodeList nodeList = pElement.childNodes();
    int nodecount = nodeList.length();

    for (int i = 0; i < nodecount; i++)
    {
        QDomNode nodeTosave = nodeList.item(i);
        if (nodeTosave.isElement())
        {
            QDomElement elementToSave = nodeTosave.toElement();
            ret = ret && writeElementToStream(elementToSave, pStream);
        }
        else if (nodeTosave.nodeType() == QDomNode::TextNode)
        {
            QDomText textToWrite = nodeTosave.toText();
            pStream.writeCharacters(textToWrite.data());
        }
        else
        {
            OMessageManager::get()->info("Echec de la sauvegarde du noeud QDomNode %s de type %d",
                                         nodeTosave.nodeName(), nodeTosave.nodeType());
            ret = false;
        }
    }
    pStream.writeEndElement();
    return ret;
}

QString DOMSave::saveNodeToString(DOM_Node& nodeToSave)
{
    QString outputString;
    QTextStream textSream(&outputString);

    // XXX Encoding of project files is hardcoded here to be Latin-1
    // TODO The handling of the encodings within XML, for file names and for the GUI is to be cleaned.
    textSream << "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"no\"?>\n";

    // Is the node to save is already a document node print
    // it. Otherwise, create new document, import the node to save
    // into it and print it.
    if (nodeToSave.isDocument())
    {
        textSream << nodeToSave.toDocument().toString();
    }
    else
    {
        QDomDocument doc;
        doc.importNode(nodeToSave, true);
        textSream << doc.toString();
    }
    return outputString;
}
