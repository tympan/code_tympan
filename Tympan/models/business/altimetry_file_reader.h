/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file altimetry_file_reader.h
 *
 * @brief Provides for reading back an altimetry written in a file
 *
 * @author Anthony Truchet <anthony.truchet@logilab.fr>
 *
 */

#ifndef TYMPAN__ALTIMETRYREADER_H__INCLUDED
#define TYMPAN__ALTIMETRYREADER_H__INCLUDED

#include <memory>
#include <vector>
#include <string>
#include <limits>

#include "Tympan/core/exceptions.h"
#include "Tympan/models/common/3d.h"
#include "Tympan/models/common/triangle.h"

namespace tympan
{

struct mesh_io_error : /*virtual*/ std::runtime_error, virtual tympan::exception
{
    mesh_io_error(const std::string& desc) : std::runtime_error(desc){};
};

class IMeshReader
{
public:
    /// @brief Type of the points array returned
    typedef std::deque<OPoint3D> points_array_t;
    /// @brief Type of the faces array returned
    typedef std::deque<OTriangle> faces_array_t;
    /// @brief Type of the material array returned
    typedef std::deque<std::string> materials_array_t;

    /**
     * @brief Getter for the points read
     */
    virtual const points_array_t& points() const = 0;

    /**
     * @brief Getter for the faces read
     */
    virtual const faces_array_t& faces() const = 0;

    /**
     * @brief Getter for the faces read
     */
    virtual const materials_array_t& materials() const = 0;

    /**
     * @brief read the file whose name was given at reader's construction time
     */
    virtual void read() = 0;
};

/// @ Makes a reader for the file name given as argument
std::unique_ptr<IMeshReader> make_altimetry_ply_reader(const std::string filename);

}; // namespace tympan

#endif // TYMPAN__ALTIMETRYREADER_H__INCLUDED
