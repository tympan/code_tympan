/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#if TY_USE_IHM

    #include <iostream>

    #include <qapplication.h>
    #include <qcursor.h>

    #include "OLocalizator.h"
    #include "TYProgressManager.h"

    #define TR(id) OLocalizator::getString("TYProgressManager", (id))

// Initialisation.
TYProgressDialog* TYProgressManager::_pProgressDialog = NULL;
int TYProgressManager::_stepSize = 1;

/*static*/ void TYProgressManager::create(QWidget* pParent, const char* name /*=0*/)
{
    QString strBtnCancel = TR("id_cancel_btn");
    if (strBtnCancel.length() == 0)
    {
        strBtnCancel = "Cancel";
    }
    _pProgressDialog = new TYProgressDialog(name, strBtnCancel, 100, pParent, name, true);
    _pProgressDialog->setAutoClose(true);
    _pProgressDialog->setAutoReset(true);
    _pProgressDialog->setMinimumDuration(0);
    // "Decoration"
    _pProgressDialog->setCursor(Qt::ArrowCursor);
    QString strCaption = TR("id_caption");
    if (strCaption.length() == 0)
    {
        strCaption = name;
    }
    _pProgressDialog->setWindowTitle(strCaption);
    _pProgressDialog->setValue(_pProgressDialog->maximum());
}

/*static*/ void TYProgressManager::set(int totalSteps, int stepSize /*=1*/)
{
    if (!_pProgressDialog)
    {
        return;
    }

    _pProgressDialog->reset();

    if (totalSteps <= 0)
    {
        return;
    }
    if (stepSize <= 0)
    {
        return;
    }

    _stepSize = stepSize;
    _pProgressDialog->setMaximum(totalSteps);
    _pProgressDialog->show();
}

/*static*/ void TYProgressManager::step()
{
    bool dummy = false;
    step(dummy);
}

/*static*/ void TYProgressManager::step(bool& wasCancelled)
{
    if (!_pProgressDialog)
    {
        return;
    }

    // Si le bouton "Cancel" a ete enfonce
    wasCancelled = _pProgressDialog->wasCanceled();

    // On progresse de la taille du pas
    _pProgressDialog->setValue(_pProgressDialog->value() + _stepSize);

    // On donne la main a l'application si necessaire (redraw...)
    qApp->processEvents();
}
/*static*/ int TYProgressManager::getProgress()
{
    if (!_pProgressDialog)
    {
        return 0;
    }

    // on retourne la valeur de progression
    return _pProgressDialog->value();
}

/*static*/ void TYProgressManager::setProgress(int progress)
{
    if (!_pProgressDialog)
    {
        return;
    }

    // on positionne la progression a la valeur donnee
    _pProgressDialog->setValue(progress);

    // On donne la main a l'application si necessaire (redraw...)
    qApp->processEvents();
}

/*static*/ void TYProgressManager::stepToEnd()
{
    if (!_pProgressDialog)
    {
        return;
    }

    // On progresse jusqu'a la fin
    _pProgressDialog->setValue(_pProgressDialog->maximum());

    // On donne la main a l'application si necessaire (redraw...)
    qApp->processEvents();
}

/* static */ void TYProgressManager::setWindowTitle(const char* caption)
{
    if (!_pProgressDialog)
    {
        return;
    }

    // On progresse jusqu'a la fin
    _pProgressDialog->setWindowTitle(caption);

    // On donne la main a l'application si necessaire (redraw...)
    qApp->processEvents();
}
/* static */ void TYProgressManager::setMessage(const char* message)
{
    if (!_pProgressDialog)
    {
        return;
    }

    // On progresse jusqu'a la fin
    _pProgressDialog->setLabelText(message);

    // On donne la main a l'application si necessaire (redraw...)
    qApp->processEvents();
}

#endif // TY_USE_IHM
