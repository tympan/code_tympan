/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 */

#ifndef __TY_USER_SRC_REGIME__
#define __TY_USER_SRC_REGIME__

#include "TYRegime.h"
#include "TYDirectivity.h"

/**
 * Classe pour definir les differents regimes d'une source ponctuelle.
 */
class TYUserSrcRegime : public TYRegime
{
    OPROTODECL(TYUserSrcRegime)

    // Methodes
public:
    /**
     * Constructeur.
     */
    TYUserSrcRegime();
    /**
     * Constructeur par copie.
     */
    TYUserSrcRegime(const TYUserSrcRegime& other);
    /**
     * Destructeur.
     */
    virtual ~TYUserSrcRegime();

    /// Operateur =.
    TYUserSrcRegime& operator=(const TYUserSrcRegime& other);
    /// Operateur ==.
    bool operator==(const TYUserSrcRegime& other) const;
    /// Operateur !=.
    bool operator!=(const TYUserSrcRegime& other) const;

    /**
     * Fonction de copie recursive (virtuelle)
     *
     * @param pOther destination de la copy
     * @param copyId vrai par defaut
     * @param pUseCopyTag faux par defaut
     *
     */
    virtual bool deepCopy(const TYElement* pOther, bool copyId = true, bool pUseCopyTag = false);

    virtual std::string toString() const;

    virtual DOM_Element toXML(DOM_Element& domElement);
    virtual int fromXML(DOM_Element domElement);

    // Membres
public:
    LPTYDirectivity _pDirectivite;

protected:
};

/// Tableau de regimes.
typedef std::vector<TYUserSrcRegime> TYTabUserSrcRegimes;

#endif // __TY_USER_SRC_REGIME__
