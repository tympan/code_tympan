/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 */

#ifndef __TY_SOURCE__
#define __TY_SOURCE__

#include "Tympan/models/business/TYElement.h"

/// Tableau associatif Regime/Spectre.
typedef std::map<int, LPTYSpectre> TYMapRegimeSpectre;

/**
 * Classe de base des sources.
 */
class TYSource : public TYElement
{
    OPROTODECL(TYSource)
    TY_EXTENSION_DECL_ONLY(TYSource)

    // Methodes
public:
    /**
     * Constructeur.
     */
    TYSource();
    /**
     * Constructeur par copie.
     */
    TYSource(const TYSource& other);
    /**
     * Destructeur.
     */
    virtual ~TYSource();

    /// Operateur =.
    TYSource& operator=(const TYSource& other);
    /// Operateur ==.
    bool operator==(const TYSource& other) const;
    /// Operateur !=.
    bool operator!=(const TYSource& other) const;

    /**
     * Fonction de copie recursive (virtuelle)
     *
     * @param pOther destination de la copy
     * @param copyId vrai par defaut
     * @param pUseCopyTag faux par defaut
     *
     */
    virtual bool deepCopy(const TYElement* pOther, bool copyId = true, bool pUseCopyTag = false);

    virtual std::string toString() const;

    virtual DOM_Element toXML(DOM_Element& domElement);
    virtual int fromXML(DOM_Element domElement);

    /**
     * Set/Get du spectre.
     */
    virtual TYSpectre* getSpectre(int regime = -1) const
    {
        return _pSpectre._pObj;
    }

    /**
     * Get/Set du Spectre (independemment d'un eventuel attenuateur) - utilise par le widget -
     */
    virtual TYSpectre* getCurrentSpectre() const
    {
        return _pSpectre._pObj;
    }

    virtual void setSpectre(LPTYSpectre pSpectre)
    {
        _pSpectre = pSpectre;
    }

    // Membres
protected:
    LPTYSpectre _pSpectre;
};

#endif // __TY_SOURCE__
