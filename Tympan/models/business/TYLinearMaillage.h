/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 *
 */

#ifndef __TY_SEGMENT_MAILLAGE__
#define __TY_SEGMENT_MAILLAGE__

#include "Tympan/models/business/geometry/TYSegment.h"
#include "TYMaillage.h"

/**
 * \file TYLinearMaillage.h
 * \class TYLinearMaillage
 * \brief Classe de definition d'un maillage lineaire.
 * \version v 1.1
 * \author Projet_Tympan
 * \date 2008/01/21
 *
 */
class TYLinearMaillage : public TYMaillage
{
    OPROTODECL(TYLinearMaillage)
    TY_EXT_GRAPHIC_DECL_ONLY(TYLinearMaillage)
    TY_EXTENSION_DECL_ONLY(TYLinearMaillage)

    // Methodes
public:
    /**
     * \fn TYLinearMaillage()
     * \brief Constructeur.
     * Constructeur de la classe TYLinearMaillage
     */
    TYLinearMaillage();

    /**
     * \fn TYLinearMaillage(const TYLinearMaillage& other)
     * \brief Constructeur par copie.
     * Constructeur par copie de la classe TYLinearMaillage
     */
    TYLinearMaillage(const TYLinearMaillage& other);

    /**
     * \fn virtual ~TYLinearMaillage()
     * \brief Destructeur.
     *  Destructeur de la classe TYLinearMaillage
     */
    virtual ~TYLinearMaillage();

    /// Operateur =.
    TYLinearMaillage& operator=(const TYLinearMaillage& other);

    /// Operateur ==.
    bool operator==(const TYLinearMaillage& other) const;

    /// Operateur !=.
    bool operator!=(const TYLinearMaillage& other) const;

    /**
     * Fonction de copie recursive (virtuelle)
     *
     * @param pOther destination de la copy
     * @param copyId vrai par defaut
     * @param pUseCopyTag faux par defaut
     *
     */
    virtual bool deepCopy(const TYElement* pOther, bool copyId = true, bool pUseCopyTag = false);

    virtual std::string toString() const;

    virtual DOM_Element toXML(DOM_Element& domElement);
    virtual int fromXML(DOM_Element domElement);

    bool toXML(const std::string& sFilePath);
    bool fromXML(const std::string& sFilePath);

    std::string toXMLString();
    bool fromXMLString(const std::string& sXMLString);

    /**
     * Suppression du resultat de calcul
     */
    virtual void clearResult();

    /**
     * \fn virtual void make(LPTYSegment pSeg, double densite = TY_MAILLAGE_DEFAULT_DENSITE)
     * \brief Rempli la structure de points de calcul a partir d'un segment et d'une densite de points.
     * \param pSeg: Le segment delimitant le maillage.
     * \param densite: La densite de points de calcul.
     */
    virtual void make(LPTYSegment pSeg, double densite = TY_MAILLAGE_DEFAULT_DENSITE);

    /**
     * \fn LPTYSegment getSegment()
     * \brief Retourne le segment associe a ce maillage.
     * \return _pSeg Le segment associe a ce maillage.
     */
    LPTYSegment getSegment()
    {
        return _pSeg;
    }

    /**
     * \fn double getDensite()
     * \brief Retourne la densite de points de calcul.
     * \return _densite: La densite de points de calcu
     */
    double getDensite()
    {
        return _densite;
    }

    /**
     * \fn void setDensite(double densite)
     * \brief Fixe la densite de points de calcul.
     */
    void setDensite(double densite)
    {
        _densite = densite;
        if (densite != 0.0)
        {
            _distancePointsCalcul = std::round(100.0f / _densite) / 100.0f;
        }
    }

    /**
     * \fn double getDistance()
     * \brief Retourne la distance entre 2 points de calcul.
     * \return _distancePointsCalcul
     */
    double getDistance() const
    {
        return _distancePointsCalcul;
    }

    // Membres
protected:
    /// Le segment associe a ce maillage.
    LPTYSegment _pSeg;

    /// La densite de points de calcul.
    double _densite;

    /// La distance entre 2 points de calcul consécutifs
    double _distancePointsCalcul;
};

/// Smart Pointer sur TYLinearMaillage.
typedef SmartPtr<TYLinearMaillage> LPTYLinearMaillage;

/// Noeud geometrique de type TYLinearMaillage.
typedef TYGeometryNode TYLinearMaillageGeoNode;

/// Smart Pointer sur TYLinearMaillageGeoNode.
typedef SmartPtr<TYLinearMaillageGeoNode> LPTYLinearMaillageGeoNode;

/// Collection de noeuds geometriques de type TYLinearMaillage.
typedef std::vector<LPTYLinearMaillageGeoNode> TYTabLinearMaillageGeoNode;

#endif // __TY_SEGMENT_MAILLAGE__
