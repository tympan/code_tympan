/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 */

#ifndef __TY_ACOUSTIC_POLYGON__
#define __TY_ACOUSTIC_POLYGON__

#include "Tympan/models/business/geometry/TYPolygon.h"
#include "TYAcousticSurface.h"

/**
 * Classe de definition d'un polygon acoustique.
 * La partie geometrie est deleguee a un TYPolygon membre.
 */
class TYAcousticPolygon : public TYAcousticSurface
{
    OPROTODECL(TYAcousticPolygon)
    TY_EXTENSION_DECL_ONLY(TYAcousticPolygon)
    TY_EXT_GRAPHIC_DECL_ONLY(TYAcousticPolygon)

    // Methodes
public:
    /**
     * Constructeur par defaut.
     */
    TYAcousticPolygon();
    /**
     * Constructeur par copie.
     */
    TYAcousticPolygon(const TYAcousticPolygon& other);
    /**
     * Destructeur.
     */
    virtual ~TYAcousticPolygon();

    /// Operateur =.
    TYAcousticPolygon& operator=(const TYAcousticPolygon& other);
    /// Operateur ==.
    bool operator==(const TYAcousticPolygon& other) const;
    /// Operateur !=.
    bool operator!=(const TYAcousticPolygon& other) const;

    /**
     * Effectue une copie en allouant de la memoire (deep copy) et
     * non pas en copiant seulement les pointeurs (shallow copy).
     * Cette methode s'utilise comme l'operateur egal.
     * L'ID peut ne pas etre copie, le parent n'est jamais copie.
     *
     * @param pOther L'element a copier.
     * @param copyId Copie l'identifiant unique ou pas.
     * @param pUseCopyTag utilisation du tag de copie
     *
     * @return <code>true</code> si la copie est possible;
     *         <code>false</code> sinon.
     */
    virtual bool deepCopy(const TYElement* pOther, bool copyId = true, bool pUseCopyTag = false);

    virtual std::string toString() const;

    virtual DOM_Element toXML(DOM_Element& domElement);
    virtual int fromXML(DOM_Element domElement);

    virtual TYTabSourcePonctuelleGeoNode getSrcs() const;
    virtual TYSourcePonctuelle srcPonctEquiv() const;
    virtual void distriSrcs();
    virtual bool setSrcsLw();

    virtual double surface() const;
    virtual OVector3D normal() const;
    virtual OPlan plan() const;
    virtual TYTabPoint getContour(int n = -1) const;
    virtual TYTabPoint3D getOContour(int n = -1) const;
    virtual int intersects(const TYSurfaceInterface* pSurf, OSegment3D& seg) const;
    virtual int intersects(const OSegment3D& seg, OPoint3D& pt) const;
    virtual int intersects(const OPoint3D& pt) const;

    /**
     * Set/Get du polygon.
     */
    virtual LPTYPolygon getPolygon()
    {
        return _pPolygon;
    }
    /**
     * Set/Get du polygon.
     */
    const LPTYPolygon getPolygon() const
    {
        return _pPolygon;
    }

    virtual void inverseNormale()
    {
        _pPolygon->inverseNormale();
    }

    /**
     * @brief Export the surface as a triangular mesh
     *
     * NB : This is only a delegate to the underlying TYRectangle method.
     *
     * @param points output argument filled with the vertices of the triangulation
     * @param triangles output argument filled with the faces of the triangulation
     * @param geonode associated node
     */
    virtual void exportMesh(std::deque<OPoint3D>& points, std::deque<OTriangle>& triangles,
                            const TYGeometryNode& geonode) const
    {
        return _pPolygon->exportMesh(points, triangles, geonode);
    }

    // Membres
protected:
    /// Le polygone definissant la geometrie de cet acoustic polygon.
    LPTYPolygon _pPolygon;
};

typedef SmartPtr<TYAcousticPolygon> LPTYAcousticPolygon;

#endif // __TY_ACOUSTIC_POLYGON__
