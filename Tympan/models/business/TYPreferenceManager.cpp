/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 */

#if TY_USE_IHM

    #include <qwidget.h>
    #include <qrect.h>
    #include <qstring.h>
    #include <qfile.h>

    #include "Tympan/models/business/acoustic/TYSpectre.h"
    #include "Tympan/gui/app/TYApplication.h"

    #include "TYPreferenceManager.h"
    #include "OLocalizator.h"

OPreferenceManager* TYPreferenceManager::_prefMngr = nullptr;
QString* TYPreferenceManager::_fileName = nullptr;

OPreferenceManager& TYPreferenceManager::getInstance()
{
    if (_prefMngr == nullptr)
    {
        _prefMngr = new OPreferenceManager("Tympan");
    }
    return *_prefMngr;
}

void TYPreferenceManager::completePreferences()
{
    QString DefaultSettingsFilePath = OLocalizator::getResourcePath() + "DefaultSettings.xml";

    _prefMngr->completePreferences(DefaultSettingsFilePath);
}

QString& TYPreferenceManager::getFileName()
{
    if (_fileName == nullptr)
    {
        _fileName = new QString("");
    }
    return *_fileName;
}

bool TYPreferenceManager::init(const QString& regularSettingsFileName, bool& isCopiedFromDefaultSettings,
                               const std::unique_ptr<QFile> settingsFile)
{
    bool ret = true;
    QString effectiveFileName;
    // If we haven't found a settings file or it does not exist
    if (settingsFile == nullptr || !settingsFile->exists())
    {
        // Copy the default settings in the regular setting file
        QString DefaultSettingsFilePath = OLocalizator::getResourcePath() + "DefaultSettings.xml";
        ret = copyDefaultSettings(DefaultSettingsFilePath, regularSettingsFileName);
        isCopiedFromDefaultSettings = true;
        effectiveFileName = regularSettingsFileName;
    }
    else
    // Else the settings file name is the file name of the file found
    {
        effectiveFileName = settingsFile->fileName();
    }
    setFileName(effectiveFileName);
    return ret;
}

void TYPreferenceManager::reset()
{
    getInstance().reset();
}

void TYPreferenceManager::setFileName(const QString& fileName)
{
    getFileName() = fileName;
}

bool TYPreferenceManager::read()
{
    if (QFile::exists(getFileName()))
    {
        return getInstance().readXML(getFileName());
    }
    else
    {
        return false;
    }
}

bool TYPreferenceManager::write()
{
    return getInstance().writeXML(getFileName());
}

bool TYPreferenceManager::exists(const QString& pref)
{
    return getInstance().exists(pref);
}

void TYPreferenceManager::saveGeometryToPreferences(const QString& pref, const QWidget* pWidget)
{
    Q_ASSERT(pWidget);

    getInstance().setBool(pref + "Maximized", pWidget->isMaximized());

    QPoint pos = pWidget->pos();
    QSize size = pWidget->size();

    if (pos.x() < 0)
    {
        pos.setX(0);
    }
    if (pos.y() < 0)
    {
        pos.setY(0);
    }
    if (size.width() <= 0)
    {
        size.setWidth(100);
    }
    if (size.height() <= 0)
    {
        size.setHeight(100);
    }

    getInstance().setFrame(pref, pos.x(), pos.y(), size.width(), size.height());
}

void TYPreferenceManager::loadGeometryFromPreferences(const QString& pref, QWidget* pWidget)
{
    Q_ASSERT(pWidget);

    if (getInstance().getBool(pref + "Maximized"))
    {
        pWidget->showMaximized();
    }
    else
    {
        int posX = 0, posY = 0, sizeX = 0, sizeY = 0;

        getInstance().getFrame(pref, posX, posY, sizeX, sizeY);

        if (posX < 0)
        {
            posX = 0;
        }
        if (posY < 0)
        {
            posY = 0;
        }
        // pWidget->move(posX, posY);
        if (sizeX <= 0)
        {
            sizeX = 100;
        }
        if (sizeY <= 0)
        {
            sizeY = 100;
        }
        pWidget->resize(sizeX, sizeY);
    }
}

void TYPreferenceManager::setSpectre(const QString& pref, const TYSpectre* pSpectre)
{
    setSpectre(getInstance().getCurrentDirectory(), pref, pSpectre);
}

void TYPreferenceManager::setSpectre(const QString& dir, const QString& pref, const TYSpectre* pSpectre)
{
    for (unsigned int i = 0; i < pSpectre->getNbValues(); i++)
    {
        setFloat(pref + OPreferenceManager::intToString(i), pSpectre->getTabValReel()[i]);
    }
}

TYSpectre* TYPreferenceManager::getSpectre(const QString& pref)
{
    return getSpectre(getInstance().getCurrentDirectory(), pref);
}

TYSpectre* TYPreferenceManager::getSpectre(const QString& dir, const QString& pref)
{
    int nbFreq = 0;

    // Search array size
    while (exists(dir, pref + OPreferenceManager::intToString(nbFreq)))
    {
        ++nbFreq;
    }

    if (nbFreq <= 0)
    {
        return nullptr;
    }

    TYSpectre* pSpectre = new TYSpectre();
    for (int i = 0; i < nbFreq; i++)
    {
        double value = getFloat(dir, pref + OPreferenceManager::intToString(i));
        pSpectre->getTabValReel()[i] = value;
    }

    return pSpectre;
}

bool TYPreferenceManager::copyDefaultSettings(const QString& sourcePath, const QString& destinationPath)
{
    QFile sourceFile(sourcePath);
    QFile destinationFile(destinationPath);

    // Check if the source file exists
    if (!sourceFile.exists())
    {
        qDebug() << "Source file does not exist:" << sourcePath;
        return false;
    }

    // Attempt to open the source file for reading
    if (!sourceFile.open(QIODevice::ReadOnly))
    {
        qDebug() << "Error opening source file for reading:" << sourcePath;
        return false;
    }

    // Attempt to open the destination file for writing
    if (!destinationFile.open(QIODevice::WriteOnly))
    {
        qDebug() << "Error opening destination file for writing:" << destinationPath;
        sourceFile.close();
        return false;
    }

    // Copy the file content
    QByteArray fileContent = sourceFile.readAll();
    if (destinationFile.write(fileContent) == -1)
    {
        qDebug() << "Error writing to destination file:" << destinationPath;
        sourceFile.close();
        destinationFile.close();
        return false;
    }

    qDebug() << "File copied successfully from" << sourcePath << "to" << destinationPath;

    // Close the file handles
    sourceFile.close();
    destinationFile.close();

    return true;
}

#endif // TY_USE_IHM
