/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#if TY_USE_IHM
    #include "Tympan/gui/gl/TYPanelGraphic.h"
#endif
#include "TYPanel.h"

TY_EXT_GRAPHIC_INST(TYPanel);

TYPanel::TYPanel()
{
    _name = TYNameManager::get()->generateName(getClassName());

    _firstPos = "";
    _secondPos = "";
    _value = "";
}

TYPanel::TYPanel(const TYPanel& other)
{
    *this = other;
}

TYPanel::~TYPanel() {}

TYPanel& TYPanel::operator=(const TYPanel& other)
{
    if (this != &other)
    {
        TYElement::operator=(other);
        _firstPos = other._firstPos;
        _secondPos = other._secondPos;
        _value = other._value;
    }
    return *this;
}

bool TYPanel::operator==(const TYPanel& other) const
{
    if (this != &other)
    {
        if (TYElement::operator!=(other))
        {
            return false;
        }
        if (_firstPos != other._firstPos)
        {
            return false;
        }
        if (_secondPos != other._secondPos)
        {
            return false;
        }
        if (_value != other._value)
        {
            return false;
        }
    }
    return true;
}

bool TYPanel::operator!=(const TYPanel& other) const
{
    return !operator==(other);
}

bool TYPanel::deepCopy(const TYElement* pOther, bool copyId /*=true*/, bool pUseCopyTag /*=false*/)
{
    if (!TYElement::deepCopy(pOther, copyId))
    {
        return false;
    }

    TYPanel* pOtherPanel = (TYPanel*)pOther;

    _firstPos = pOtherPanel->_firstPos;
    _secondPos = pOtherPanel->_secondPos;
    _value = pOtherPanel->_value;

    return true;
}

std::string TYPanel::toString() const
{
    return "TYPanel";
}
