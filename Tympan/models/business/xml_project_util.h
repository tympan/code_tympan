/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file xml_project_util.h
 *
 * @brief Utilities to load a project and a solver.
 *
 */

#ifndef TYMPAN__XML_PROJECT_UTIL_HPP__INCLUDED
#define TYMPAN__XML_PROJECT_UTIL_HPP__INCLUDED

#include "Tympan/models/business/TYProjet.h"

namespace tympan
{
/**
 * @brief load an XML project file
 * @param filename the file containing the project to load
 * project Output argument : this pointer (expected to be NULL) will be
 *                made to refer to the loaded \c TYProjet.
 * @return true if the project could be loader, false otherwise
 */
LPTYProjet load_project(const char* filename);

/**
 * @brief save a project into an XML file
 * @param filename the file where to save the project
 * @param project : pointer to the project to save
 */
void save_project(const char* filename, const LPTYProjet& project);

/**
 * @brief load one or several objects from an xml file
 * @param filename the file where to save the project
 * @return std::vector<SmartPtr<TYElement>> an array of element that shoud be converted to business elements
 */
std::vector<LPTYElement> load_elements(const char* filename);

} /* namespace tympan */

#endif /* TYMPAN__XML_PROJECT_UTIL_HPP__INCLUDED */
