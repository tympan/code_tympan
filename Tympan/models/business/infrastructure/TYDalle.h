/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 *
 */

#ifndef __TY_DALLE__
#define __TY_DALLE__

#include "Tympan/models/business/geoacoustic/TYAcousticPolygon.h"
#include "TYParoi.h"

/**
 * Classe de definition d'une dalle.
 * Pour la representation des sols et plafonds.
 */
class TYDalle : public TYAcousticPolygon
{
    OPROTODECL(TYDalle)
    TY_EXTENSION_DECL_ONLY(TYDalle)
    TY_EXT_GRAPHIC_DECL_ONLY(TYDalle)

    // Methodes
public:
    /**
     * Constructeur par defaut.
     */
    TYDalle();
    /**
     * Constructeur par copie.
     */
    TYDalle(const TYDalle& other);
    /**
     * Destructeur.
     */
    virtual ~TYDalle();

    /// Operateur =.
    TYDalle& operator=(const TYDalle& other);
    /// Operateur ==.
    bool operator==(const TYDalle& other) const;
    /// Operateur !=.
    bool operator!=(const TYDalle& other) const;

    /**
     * Effectue une copie en allouant de la memoire (deep copy) et
     * non pas en copiant seulement les pointeurs (shallow copy).
     * Cette methode s'utilise comme l'operateur egal.
     * L'ID peut ne pas etre copie, le parent n'est jamais copie.
     *
     * @param pOther L'element a copier.
     * @param copyId Copie l'identifiant unique ou pas.
     * @param pUseCopyTag utilisation du tag de copie
     *
     * @return <code>true</code> si la copie est possible;
     *         <code>false</code> sinon.
     */
    virtual bool deepCopy(const TYElement* pOther, bool copyId = true, bool pUseCopyTag = false);

    virtual std::string toString() const;

    virtual DOM_Element toXML(DOM_Element& domElement);
    virtual int fromXML(DOM_Element domElement);

    /**
     * Set/Get de la paroi.
     */
    LPTYParoi getParoi()
    {
        return _pParoi;
    }
    /**
     * Set/Get de la paroi.
     */
    const LPTYParoi getParoi() const
    {
        return _pParoi;
    }
    /**
     * Set/Get de la paroi.
     */
    void setParoi(const LPTYParoi pParoi);

    /**
     * Get/Set de l'etat bloque du type de paroi
     */
    void setParoiLocked(const bool& bVal);
    bool isParoiLocked()
    {
        return _bParoiLocked;
    }
    const bool isParoiLocked() const
    {
        return _bParoiLocked;
    }

    /**
     * \fn  LPTYMateriauConstruction getMateriau()
     * \brief Surcharge de la methode getMateriau pour la dalle
     * \return le materiau de construction de la dalle
     */
    TYMateriauConstruction* getMateriau();

    // Membres
protected:
    /// Une paroi associee a cette dalle.
    LPTYParoi _pParoi;

    /**
     * Indicateur de "blocage" du type de paroi
     * (evite une mise a jour depuis l'etage)
     */
    bool _bParoiLocked;
};

#endif // __TY_DALLE__
