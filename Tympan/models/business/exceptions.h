/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file exceptions.h
 *
 * \brief Define some exception related utilities relying on DataManagerMetier classes
 */

#ifndef __TYMPAN__DMM_EXCEPTIONS_HPP__INCLUDED__
#define __TYMPAN__DMM_EXCEPTIONS_HPP__INCLUDED__

#include <deque>
#include "Tympan/core/exceptions.h"

namespace tympan
{
typedef boost::error_info<struct tag_elements_implied, std::deque<LPTYElement>> elements_implied_errinfo;
typedef boost::error_info<struct tag_position, OPoint3D> position_errinfo;
} // namespace tympan

#endif // __TYMPAN__DMM_EXCEPTIONS_HPP__INCLUDED__
