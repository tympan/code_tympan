/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file cgal_bridge.h
 *
 * \brief Bridges TY* types with CGAL functionalities exposed by @file cgal_tools.hpp
 *
 * \author Anthony Truchet <anthony.truchet@logilab.fr>
 */

#ifndef TYMPAN__CGAL_BRIDGE_HPP__INCLUDED
#define TYMPAN__CGAL_BRIDGE_HPP__INCLUDED

#include <deque>
#include <memory>

#include "Tympan/core/exceptions.h"
#include "Tympan/models/common/triangle.h"
#include "Tympan/models/common/3d.h"

// Forward declarations
#include "Tympan/core/smartptr.h"
class TYPolygon;
class TYTopographie;
class TYSol;
class TYTerrain;
class TYCourbeNiveau;
/// Smart pointer sur TYSol.
typedef SmartPtr<TYSol> LPTYSol;

#ifndef BOOST_NOEXCEPT
    #define BOOST_NOEXCEPT
#endif

namespace tympan
{

class ITYPolygonTriangulator
{
public:
    virtual ~ITYPolygonTriangulator(){};

    /**
     * @brief Export the surface as a triangular mesh
     *
     * NB : This function expects empty deques and will clear the deque passed.
     * TODO : It would be better to use output iterators
     *
     * @param points output argument filled with the vertices of the triangulation
     * @param triangles output argument filled with the faces of the triangulation
     */
    virtual void exportMesh(std::deque<OPoint3D>& points, std::deque<OTriangle>& triangles) const = 0;

}; // class ITYPolygonTriangulator

std::unique_ptr<ITYPolygonTriangulator> make_polygon_triangulator(const TYPolygon& poly);

} // namespace tympan

#endif // TYMPAN__CGAL_BRIDGE_HPP__INCLUDED
