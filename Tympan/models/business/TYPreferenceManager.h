/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 *
 */

#ifndef __TY_PREFERENCE_MANAGER__
#define __TY_PREFERENCE_MANAGER__

#include "Tympan/core/defines.h"
#include <qfile.h>
#include <memory>

#if TY_USE_IHM

    #include "Tympan/models/business/OPreferenceManager.h"

class QWidget;
class QString;
class TYSpectre;

/**
 * Static class for managing preferences in Tympan.
 * This static class instantiates an object of type OPreferenceManager
 * and provides a static interface for easy use.
 */
class TYPreferenceManager
{
public:
    /**
     * Returns the unique instance that manages preferences.
     */
    static OPreferenceManager& getInstance();

    /**
     * \brief Complete the missing user preferences with the default values
     */
    static void completePreferences();

    /**
     * Initializes the preference manager.
     * The preference/value associative array is loaded from existing settings or with default values.
     *
     * @param regularSettingsFileName The regular settings file name, typically "settings.xml"
     *
     * @param isCopiedFromDefaultSettings <code>true</code> if the settings has been copied from default;
     *                                    <code>false</code> otherwise.
     *
     * @param settingsFile QFile being the regular settings file or the most recent found
     *
     * @return <code>true</code> if the initialization succeeds;
     *         <code>false</code> otherwise.
     *
     */
    static bool init(const QString& regularSettingsFileName, bool& isCopiedFromDefaultSettings,
                     const std::unique_ptr<QFile> settingsFile);

    /**
     * Clears the preference array.
     */
    static void reset();

    /**
     * Sets the path of the preference file.
     *
     * @param fileName The path of the preference file to use.
     */
    static void setFileName(const QString& fileName);

    /**
     * Opens the XML preference file and parses it to update the preference array.
     *
     * @return <code>false</code> if the file could not be opened.
     *
     * @see setFileName()
     */
    static bool read();

    /**
     * Saves the preference file in XML format.
     *
     * @return <code>false</code> if the file could not be saved.
     *
     * @see setFileName()
     */
    static bool write();

    /**
     * Tests if a preference is present in the preference array.
     *
     * @param pref The name of the preference to test.
     *
     * @return <code>true</code> if the preference exists;
     *         <code>false</code> otherwise.
     */
    static bool exists(const QString& pref);

    /**
     * Sets the current category for reading and writing parameters.
     *
     * @param dir The name of the category; if it does not exist, it will be created.
     */
    static void setCurrentDirectory(QString dir)
    {
        getInstance().setCurrentDirectory(dir);
    }

    /**
     * Returns the current category for reading and writing parameters.
     *
     * @return The name of the current category.
     */
    static QString getCurrentDirectory()
    {
        return getInstance().getCurrentDirectory();
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setString(const QString& pref, const QString& value)
    {
        getInstance().setString(pref, value);
    }

    /**
     * Searches for a resource and returns its associated value.
     *
     * @param pref The name of the preference to search for.
     *
     * @return The value associated with the preference.
     */
    static QString getString(const QString& pref)
    {
        return getInstance().getString(pref);
    }

    /**
     * Updates an array of values associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param valueArray The array of values to associate with the preference.
     * @param sizeArray The number of values in the passed array.
     */
    static void setStringArray(const QString& pref, const QString* valueArray, const int& sizeArray)
    {
        getInstance().setStringArray(pref, valueArray, sizeArray);
    }

    /**
     * Searches for a resource and returns its array of associated values.
     *
     * @param pref The name of the preference to search for.
     * @param sizeArray This integer will contain the size of the returned array.
     *
     * @return An array containing the values associated with the preference.
     *         The caller of this method is responsible for deleting this array.
     */
    static QString* getStringArray(const QString& pref, int& sizeArray)
    {
        return getInstance().getStringArray(pref, sizeArray);
    }

    #if TY_ARCH_TYPE == TY_ARCHITECTURE_64
    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setUInt(const QString& pref, const unsigned int& value)
    {
        getInstance().setUInt(pref, value);
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setUInt(const QString& pref, const size_t& value)
    {
        getInstance().setUInt(pref, value);
    }
        #if TY_COMPILER == TY_COMPILER_MSVC
    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setUInt(const QString& pref, const unsigned long& value)
    {
        getInstance().setUInt(pref, value);
    }
        #endif
    #else
    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setUInt(const QString& pref, const size_t& value)
    {
        getInstance().setUInt(pref, value);
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setUInt(const QString& pref, const unsigned long& value)
    {
        getInstance().setUInt(pref, value);
    }
    #endif

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setInt(const QString& pref, const int& value)
    {
        getInstance().setInt(pref, value);
    }

    /**
     * Searches for a resource and returns its associated value.
     *
     * @param pref The name of the preference to search for.
     *
     * @return The value associated with the preference.
     */
    static int getInt(const QString& pref)
    {
        return getInstance().getInt(pref);
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setBool(const QString& pref, const bool& value)
    {
        getInstance().setBool(pref, value);
    }

    /**
     * Searches for a resource and returns its associated value.
     *
     * @param pref The name of the preference to search for.
     *
     * @return The value associated with the preference.
     */
    static bool getBool(const QString& pref)
    {
        return getInstance().getBool(pref);
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setFloat(const QString& pref, const float& value)
    {
        getInstance().setFloat(pref, value);
    }

    /**
     * Searches for a resource and returns its associated value.
     *
     * @param pref The name of the preference to search for.
     *
     * @return The value associated with the preference.
     */
    static float getFloat(const QString& pref)
    {
        return getInstance().getFloat(pref);
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setDouble(const QString& pref, const double& value)
    {
        getInstance().setDouble(pref, value);
    }

    /**
     * Searches for a resource and returns its associated value.
     *
     * @param pref The name of the preference to search for.
     *
     * @return The value associated with the preference.
     */
    static double getDouble(const QString& pref)
    {
        return getInstance().getDouble(pref);
    }

    /**
     * Save the geometry (position and size) of a widget.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param pWidget The widget whose geometry needs to be saved.
     */
    static void saveGeometryToPreferences(const QString& pref, const QWidget* pWidget);

    /**
     * Load and apply the position and size of a window.
     *
     * @param pref The name of the preference to search for.
     * @param pWidget The widget whose geometry needs to be retrieved.
     */
    static void loadGeometryFromPreferences(const QString& pref, QWidget* pWidget);

    /**
     * Save the pixel coordinates of a point.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param posX The X-coordinate.
     * @param posY The Y-coordinate.
     */
    static void setPoint(const QString& pref, const int& x, const int& y)
    {
        getInstance().setPoint(pref, x, y);
    }

    /**
     * Load the pixel coordinates of a point.
     *
     * @param pref The name of the preference to search for.
     * @param posX The X-coordinate.
     * @param posY The Y-coordinate.
     */
    static void getPoint(const QString& pref, int& x, int& y)
    {
        getInstance().getPoint(pref, x, y);
    }

    /**
     * Save the RGB components of a color, in float.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param red The red component.
     * @param green The green component.
     * @param blue The blue component.
     */
    static void setColor(const QString& pref, const float& r, const float& g, const float& b)
    {
        getInstance().setColor(pref, r, g, b);
    }

    /**
     * Load the RGB components of a color, in float.
     *
     * @param pref The name of the preference to search for.
     * @param red The red component.
     * @param green The green component.
     * @param blue The blue component.
     */
    static void getColor(const QString& pref, float& r, float& g, float& b)
    {
        getInstance().getColor(pref, r, g, b);
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     *
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setSpectre(const QString& pref, const TYSpectre*);

    /**
     * Searches for a resource and returns its associated value.
     *
     * @param pref The name of the preference to search for.
     *
     * @return The value associated with the preference.
     */
    static TYSpectre* getSpectre(const QString& pref);

    /**
     * Tests if a preference is present in the preference array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference to test.
     *
     * @return <code>true</code> if the preference exists;
     *         <code>false</code> otherwise.
     */
    static bool exists(const QString& dir, const QString& pref)
    {
        return getInstance().exists(dir, pref);
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setString(const QString& dir, const QString& pref, const QString& value)
    {
        getInstance().setString(dir, pref, value);
    }

    /**
     * Searches for a resource and returns its associated value.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference to search for.
     *
     * @return The value associated with the preference.
     */
    static QString getString(const QString& dir, const QString& pref)
    {
        return getInstance().getString(dir, pref);
    }

    /**
     * Updates an array of values associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param valueArray The array of values to associate with the preference.
     * @param sizeArray The number of values contained in the passed array.
     */
    static void setStringArray(const QString& dir, const QString& pref, const QString* valueArray,
                               const int& sizeArray)
    {
        getInstance().setStringArray(dir, pref, valueArray, sizeArray);
    }

    /**
     * Searches for a resource and returns its array of associated values.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference to search for.
     * @param sizeArray This integer will contain the size of the returned array.
     *
     * @return An array containing the values associated with the preference.
     *         The caller of this method is responsible for deleting this array.
     */
    static QString* getStringArray(const QString& dir, const QString& pref, int& sizeArray)
    {
        return getInstance().getStringArray(dir, pref, sizeArray);
    }

    #if TY_ARCH_TYPE == TY_ARCHITECTURE_64
    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setUInt(const QString& dir, const QString& pref, const unsigned int& value)
    {
        getInstance().setUInt(dir, pref, value);
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setUInt(const QString& dir, const QString& pref, const size_t& value)
    {
        getInstance().setUInt(dir, pref, value);
    }
        #if TY_COMPILER == TY_COMPILER_MSVC
    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setUInt(const QString& dir, const QString& pref, const unsigned long& value)
    {
        getInstance().setUInt(dir, pref, value);
    }
        #endif
    #else
    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setUInt(const QString& dir, const QString& pref, const size_t& value)
    {
        getInstance().setUInt(dir, pref, value);
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setUInt(const QString& dir, const QString& pref, const unsigned long& value)
    {
        getInstance().setUInt(dir, pref, value);
    }
    #endif

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setInt(const QString& dir, const QString& pref, const int& value)
    {
        getInstance().setInt(dir, pref, value);
    }

    /**
     * Searches for a resource and returns its associated value.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference to search for.
     *
     * @return The value associated with the preference.
     */
    static int getInt(const QString& dir, const QString& pref)
    {
        return getInstance().getInt(dir, pref);
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preference array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setBool(const QString& dir, const QString& pref, const bool& value)
    {
        getInstance().setBool(dir, pref, value);
    }

    /**
     * Searches for a resource and returns its associated value.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference to search for.
     *
     * @return The value associated with the preference.
     */
    static bool getBool(const QString& dir, const QString& pref)
    {
        return getInstance().getBool(dir, pref);
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preferences array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setFloat(const QString& dir, const QString& pref, const float& value)
    {
        getInstance().setFloat(dir, pref, value);
    }

    /**
     * Searches for a resource and returns its associated value.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference to search for.
     *
     * @return The value associated with the preference.
     */
    static float getFloat(const QString& dir, const QString& pref)
    {
        return getInstance().getFloat(dir, pref);
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preferences array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setDouble(const QString& dir, const QString& pref, const double& value)
    {
        getInstance().setDouble(dir, pref, value);
    }

    /**
     * Searches for a resource and returns its associated value.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference to search for.
     *
     * @return The value associated with the preference.
     */
    static double getDouble(const QString& dir, const QString& pref)
    {
        return getInstance().getDouble(dir, pref);
    }

    /**
     * Saves the pixel coordinates of a point.
     * If the preference does not exist, it is added to the preferences array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param posX The X coordinate.
     * @param posY The Y coordinate.
     */
    static void setPoint(const QString& dir, const QString& pref, const int& x, const int& y)
    {
        getInstance().setPoint(dir, pref, x, y);
    }

    /**
     * Loads the pixel coordinates of a point.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference to search for.
     * @param posX The X coordinate.
     * @param posY The Y coordinate.
     */
    static void getPoint(const QString& dir, const QString& pref, int& x, int& y)
    {
        getInstance().getPoint(dir, pref, x, y);
    }

    /**
     * Saves the RGB components of a color, as floats.
     * If the preference does not exist, it is added to the preferences array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param red The red component.
     * @param green The green component.
     * @param blue The blue component.
     */
    static void setColor(const QString& dir, const QString& pref, const float& r, const float& g,
                         const float& b)
    {
        getInstance().setColor(dir, pref, r, g, b);
    }

    /**
     * Loads the RGB components of a color, as floats.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference to search for.
     * @param red The red component.
     * @param green The green component.
     * @param blue The blue component.
     */
    static void getColor(const QString& dir, const QString& pref, float& r, float& g, float& b)
    {
        getInstance().getColor(dir, pref, r, g, b);
    }

    /**
     * Updates the value associated with a preference.
     * If the preference does not exist, it is added to the preferences array.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference.
     * @param value The value to associate with the preference.
     */
    static void setSpectre(const QString& dir, const QString& pref, const TYSpectre*);

    /**
     * Searches for a resource and returns its associated value.
     * Uses the specified category.
     *
     * @param dir The name of the category.
     * @param pref The name of the preference to search for.
     *
     * @return The value associated with the preference.
     */
    static TYSpectre* getSpectre(const QString& dir, const QString& pref);

    /**
     * Returns a version number to know if values have been modified.
     */
    static int getLastModificationCode()
    {
        return getInstance().getLastModificationCode();
    }

    /**
     * Copy DefaultSettings.xml from the resources folder to the Tympan user folder.
     *
     * @param sourcePath File path to DefaultSettings.xml.
     * @param destinationPath Path to Settings.xml to which copy DefaultSettings.
     *
     * @return True if the copy succeeds, else False.
     */
    static bool copyDefaultSettings(const QString& sourcePath, const QString& destinationPath);

private:
    /// Associative array of parameter/value.
    static OPreferenceManager* _prefMngr;

    /// Preference file.
    static QString& getFileName();
    static QString* _fileName;
};

#endif // TY_USE_IHM

#endif // __O_PREFERENCE_MANAGER__
