/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 *
 */

#ifndef TYPOINT_PARCOURS_H
#define TYPOINT_PARCOURS_H

#define SEUIL_DISTANCE_POINTS_CONFONDUS                                                                      \
    0.02 //!< Below a distance of 2 centimeters, the points are considered on the same location
#define SEUIL_DETERMNANT_COLINEAIRE                                                                          \
    (1E-10) //!< Below this absolute value, the determinant is not a valid criteria to assess the collinearity
            //!< of two vectors

/**
 * \struct TYPointParcours
 * \brief Point of a path
 */
struct TYPointParcours
{
    double x;                                 //!< x coordinate of the point
    double y;                                 //!< y coordinate of the point
    double z;                                 //!< z coordinate of the point
    int Identifiant;                          //!< Point id
    bool isInfra;                             //!< Flag set to indicate if the point is an infrastructure
    bool isEcran;                             //!< Flag set to indicate if the point is a screen
    static const double _dSeuilDistanceCarre; //!< Square of the threshold distance between two points to
                                              //!< indicate they lie on the location

    bool operator==(TYPointParcours& p); //!< Return true if this point and p are on same location

    double normeCarree(); //!< Return x^2+y^2

    static bool Confondus(TYPointParcours* P1,
                          TYPointParcours* P2); //!< Return true if P1 and P2 are on the same location
                                                //!< (separated by less than a threshold distance)
    static double ZCross(TYPointParcours SR,
                         TYPointParcours SP); //!< Return cross product applied to SR and SP points
    static bool IntersectionDroites(TYPointParcours& P1, TYPointParcours& P2, TYPointParcours& P3,
                                    TYPointParcours& P4,
                                    TYPointParcours& P); //!< Return false if (P1P2) and (P3P4) are parallel,
                                                         //!< true if not with P the intersection
    /**
     * @brief Return true if [P1P2] intersects [P3P4]
     * if P1 or P2 coincide with the intersection point P, then returns false
     * if bP3OrP4MustNotCoincideWithP is true and P3 our P4 coincide with the intersection point P, then
     * returns false
     * @param [in] P1, P2, P3, P4 The edges of the two segements [P1P2] and [P3P4]
     * @param [in] bP3OrP4MustNotCoincideWithP Boolean indicating if the intersection result must not coincide
     * with P3 or P4
     * @param [out] P TYPointParcours Intersection point if intersection exists
     * @param [in] bP3OrP4MustNotCoincideWithP true if P3 or P4 coincide with P
     *
     * @return <code>true</code> if [P1P2] intersects [P3P4] and intersection point does not coincide with P1
     * or P2 and (bP3OrP4MustNotCoincideWithP if false or intersection point does not coincide with P3 or P4);
     *         <code>false</code> [P1P2] else.
     */
    static bool IntersectionSegments(TYPointParcours& P1, TYPointParcours& P2, TYPointParcours& P3,
                                     TYPointParcours& P4, TYPointParcours& P,
                                     bool bP3OrP4MustNotCoincideWithP = false);
    /**
     * @brief Compute the scalar product of the vector P1P2 and P3P4
     * @param TYPointParcours& P1
     * @param TYPointParcours& P2
     * @param TYPointParcours& P3
     * @param TYPointParcours& P4
     *
     * @return double scalar product of the vectors
     */
    static double Scalaire(TYPointParcours& P1, TYPointParcours& P2, TYPointParcours& P3,
                           TYPointParcours& P4);

    /**
     * @brief Compute the scalar product of the vector P1P2 and P3P4
     * @param TYPointParcours& vecteur1
     * @param TYPointParcours& vecteur2
     *
     * @return double scalar product of the vectors
     */
    static double Scalaire(TYPointParcours& vecteur1, TYPointParcours& vecteur2);

    /**
     * @brief Compute the 2 dimensional vector P1P2 in the XY plane
     * @param TYPointParcours& P1
     * @param TYPointParcours& P2
     *
     * @return TYPointParcours 2D vector
     */
    static TYPointParcours vecteur2D(TYPointParcours& P1, TYPointParcours& P2);

    /**
     * @brief Return the square of the curvilinear abscissa of point P on [SR]
     * @param TYPointParcours& P
     * @param TYPointParcours& S
     * @param TYPointParcours& R
     *
     * @return double square of the curvilinear abscissa of point P on [SR]
     */
    static double AbscisseCurviligneCarreSurSR(TYPointParcours& P, TYPointParcours& S, TYPointParcours& R);
};

#endif // TYPOINT_PARCOURS_H
