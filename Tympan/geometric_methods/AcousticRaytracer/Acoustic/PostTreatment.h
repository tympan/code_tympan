/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef POSTTREATMENT_H
#define POSTTREATMENT_H

#include "Geometry/Scene.h"
#include "Recepteur.h"

#ifdef _ALLOW_TARGETING_
    #include "Tools/TargetManager.h"
#endif

/**
 * \brief Namespace for post-treatment
 */
namespace PostTreatment
{
/// Build the edges list of a scene
bool constructEdge(Scene* scene);

#ifdef _ALLOW_TARGETING_
bool findTargetsForNMPB(Scene* scene, std::vector<Recepteur>& recepteurs, TargetManager& targetManager,
                        decimal density);
void appendDirectionToSources(TargetManager* targets, std::vector<Source>& sources);
#endif
}; // namespace PostTreatment

#endif
