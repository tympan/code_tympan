/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RECEPTEUR_H
#define RECEPTEUR_H

#include "Geometry/Shape.h"
#include "Geometry/Sphere.h"
#include "Geometry/mathlib.h"
#include "Base.h"

/**
 * \brief Receptor inherits from a Sphere Shape
 */
class Recepteur : public Sphere
{
public:
    /// Default constructor
    Recepteur(){};
    /// Constructor
    Recepteur(const vec3& pos, const decimal& r) : Sphere(pos, r)
    {
        name = "unknow receptor";
    }
    /// Copy constructor
    Recepteur(const Recepteur& other) : Sphere(other)
    {
        id = other.id;
    }
    /// Destructor
    virtual ~Recepteur() {}
    /// Return the Shape
    Shape* getShape()
    {
        return dynamic_cast<Shape*>(this);
    }

    /// Get the center of the bounding box
    vec3 getPosition()
    {
        return this->getBBox().centroid;
    }

    /// Get the Intersection between a ray and this shape
    bool intersectionRecepteur(vec3& origine, vec3& directeur, float tmax, Intersection& result);

    /// Get/Set identification
    unsigned int getId()
    {
        return id;
    }
    void setId(unsigned int _id)
    {
        id = _id;
    }

protected:
    unsigned int id; //!< Shape identification
};

#endif
