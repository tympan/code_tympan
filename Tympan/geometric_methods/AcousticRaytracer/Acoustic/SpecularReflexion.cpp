/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "SpecularReflexion.h"
#include "Geometry/Shape.h"

bool SpecularReflexion::getResponse(vec3& r, bool force)
{
    nbResponseLeft--;
    if (nbResponseLeft < 0)
    {
        return false;
    }

    r = from - shape->getNormal() * 2.0 * from.dot(shape->getNormal());
    return true;
}

double SpecularReflexion::getAngle()
{
    double angle = from.angle(shape->getNormal() * -1.0);
    return angle;
}
