/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CLEANER_SELECTOR
#define CLEANER_SELECTOR

#include "Selector.h"

/*!
 * \brief Clean DoNothing events from ray events list
 * \      --> After ray validation DoNothing events are no longer useful
 */
template <typename T> class CleanerSelector : public Selector<T>
{
public:
    /// Constructor
    CleanerSelector() : Selector<T>() {}

    virtual Selector<T>* Copy()
    {
        CleanerSelector* newSelector = new CleanerSelector();
        newSelector->setIsDeletable(this->deletable);
        return newSelector;
    }

    virtual SELECTOR_RESPOND canBeInserted(T* r, unsigned long long& replace)
    {
        std::vector<boost::shared_ptr<Event>>* events = r->getEvents();
        if (events->size() == 0)
        {
            return SELECTOR_ACCEPT;
        }

        std::vector<boost::shared_ptr<Event>>::iterator it = events->begin();
        while (it != events->end())
        {
            if ((*it)->getType() == NOTHING)
            {
                it = events->erase(it);
                continue;
            }

            it++;
        }

        return SELECTOR_REPLACE;
    }

    virtual void insert(T* r)
    {
        return;
    }
    virtual bool insertWithTest(T* r)
    {
        std::vector<boost::shared_ptr<Event>>* events = r->getEvents();
        if (events->size() == 0)
        {
            return true;
        }

        std::vector<boost::shared_ptr<Event>>::iterator it = events->begin();
        while (it != events->end())
        {
            if ((*it)->getType() == NOTHING)
            {
                it = events->erase(it);
                continue;
            }

            it++;
        }

        return true;
    }

    /**
     * \brief Return the class type of the selector
     */
    virtual const char* getSelectorName()
    {
        return typeid(this).name();
    }

protected:
};

#endif // CLEANER_SELECTOR
