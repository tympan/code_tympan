/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <sstream>
#include <fstream>

extern std::stringstream ss;

/**
 * \brief A logger class
 */
class Logger
{

public:
    /// Constructor
    Logger(){};
    /// Destructor
    ~Logger(){};

    // static stringstream& getStream(){ return ss; }
    /// Write into a file a String
    void write(const char* filename)
    {
        std::ofstream fichier(filename,
                              std::ios::out | std::ios::trunc); // declaration du flux et ouverture du fichier

        if (fichier)
        {
            fichier << ss.str();
            fichier.close();
        }
    }

    // static stringstream ss;
};

#endif
