/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "Simulation.h"
void Simulation::clean()
{
    scene.clean();

    // Nettoyage des sources et des recepteurs
    sources.clear();
    recepteurs.clear();

    compteurSource = 0;
    compteurRecepteur = 0;
}

#ifdef TEST_ACCELERATION_RECEPTORS
bool Simulation::launchSimulation()
{
    ss << "Lancement de la simulation." << std::endl;
    if (solver)
    {
        solver->clean();
    }
    if (engine)
    {
        delete engine;
    }
    // Create the engine from engineC enum
    switch (engineC)
    {
        case DEFAULT:
            engine = new DefaultEngine(&scene, &sources, solver, &receptors_landscape);
            break;
        case PARALLELDEFAULT:
            engine = new ParallelDefaultEngine(&scene, &sources, solver, &receptors_landscape);
            break;
        default:
            engine = new DefaultEngine(&scene, &sources, solver, &receptors_landscape);
            break;
    }
    return engine->process();
}
#else
bool Simulation::launchSimulation()
{
    if (solver)
    {
        solver->clean();
    }
    if (engine)
    {
        delete engine;
    }
    switch (engineC)
    {
        case DEFAULT:
            engine = new DefaultEngine(&scene, &sources, solver, &recepteurs);
            break;
        case PARALLELDEFAULT:
            engine = new ParallelDefaultEngine(&scene, &sources, solver, &recepteurs);
            break;
        default:
            engine = new DefaultEngine(&scene, &sources, solver, &recepteurs);
            break;
    }

    return engine->process();
}
#endif // TEST_ACCELERATION_RECEPTORS
