/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#include "AcousticRaytracerConfiguration.h"

AcousticRaytracerConfiguration* AcousticRaytracerConfiguration::_pInstance = 0;
AcousticRaytracerConfiguration::AcousticRaytracerConfiguration()
{
    // Ray tracing
    NbRaysPerSource = 400000;
    Discretization = 2;
    Accelerator = 3;
    MaxTreeDepth = 12;
    NbRayWithDiffraction = 0;
    MaxProfondeur = 10;
    MaxReflexion = 6;
    MaxDiffraction = 4;
    MaxLength = 5000;
    SizeReceiver = 2.;
    AngleDiffMin = 5.;
    CylindreThick = 0.5;
    MaxPathDifference = 25;

    // Sampler
    InitialAnglePhi = 0.;
    FinalAnglePhi = 360.;
    InitialAngleTheta = 0.;
    FinalAngleTheta = 360.;

    // Filters
    UseSol = false;
    UsePathDifValidation = false;
    DiffractionFilterRayAtCreation = true;
    DiffractionUseDistanceAsFilter = true;
    DiffractionUseRandomSampler = false;
    DiffractionDropDownNbRays = false;

    // Post Filters
    UsePostFilters = false;
    DebugUseCloseEventSelector = false;
    DebugUseDiffractionAngleSelector = false;
    DebugUseDiffractionPathSelector = false;
    DebugUseFermatSelector = false;
    DebugUseFaceSelector = false;

    // Miscellaneous
    KeepDebugRay = false;
}
AcousticRaytracerConfiguration* AcousticRaytracerConfiguration::get()
{
    if (!_pInstance)
    {
        _pInstance = new AcousticRaytracerConfiguration();
    }
    return _pInstance;
}
