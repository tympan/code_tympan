/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PARALLEL_DEFAULT_ENGINE_H
#define PARALLEL_DEFAULT_ENGINE_H

#include "DefaultEngine.h"

/**
 * \brief Parallel default engine class
 */
#ifdef TEST_ACCELERATION_RECEPTORS
class ParallelDefaultEngine : public Engine
{

public:
    /// Default constructor
    ParallelDefaultEngine() : Engine() {}
    /// Constructor
    ParallelDefaultEngine(Scene* _scene, std::vector<Source>* _sources, Solver* _solver, Scene* _recepteurs)
        : Engine(_scene, _sources, _solver, _recepteurs)
    {
    }
    /// Copy constructor
    ParallelDefaultEngine(const ParallelDefaultEngine& other)
    {
        scene = other.scene;
        sources = other.sources;
        solver = other.solver;
        recepteurs = other.recepteurs;
    }
    /// Destructor
    virtual ~ParallelDefaultEngine() {}

    virtual bool process();
};
#else
class ParallelDefaultEngine : public Engine
{

public:
    ParallelDefaultEngine() : Engine() {}

    ParallelDefaultEngine(Scene* _scene, std::vector<Source>* _sources, Solver* _solver,
                          std::vector<Recepteur>* _recepteurs)
        : Engine(_scene, _sources, _solver, _recepteurs)
    {
    }

    ParallelDefaultEngine(const ParallelDefaultEngine& other)
    {
        scene = other.scene;
        sources = other.sources;
        solver = other.solver;
        recepteurs = other.recepteurs;
    }

    virtual ~ParallelDefaultEngine() {}

    virtual bool process();
};

#endif // TEST_ACCELERATION_RECEPTORS
#endif
