/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BASE_H
#define BASE_H

#include <string>

/**
 * \brief Base class of Event, Material, PostFilter, Ray, Repere, Scene, Shape, Simulation, Source
 */
class Base
{

public:
    /// Default constructor
    Base()
    {
        name = "unkown element";
    }
    /// Copy constructor
    Base(const Base& other)
    {
        name = other.name;
    }
    /// Destructor
    virtual ~Base() {}
    /// Get the name of the object
    std::string getName()
    {
        return name;
    }
    /// Set the name of the object
    void setName(const std::string& _name)
    {
        name = _name;
    }

protected:
    std::string name; //!< Each instantiated object may be named
};

#endif
