/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MESH_H
#define MESH_H

#include "Shape.h"

/**
 * \brief Mesh class
 */
class Mesh : public Shape
{

public:
    /// Constructors
    Mesh(){};
    Mesh(const Mesh& other){};
    /// Destructor
    virtual ~Mesh()
    {
        clear();
    };
    /// Clear arrays
    void clear();

    /// Get/Set the vertices of the mesh
    std::vector<vec3>& getVertices()
    {
        return vertices;
    }
    void setVertices(const std::vector<vec3>& _vertices)
    {
        for (unsigned int i = 0; i < _vertices.size(); i++)
        {
            vertices.push_back(vec3(_vertices.at(i)));
        }
    }

    /// Get/Set triangles of the mesh
    std::vector<ivec3>& getTriangles()
    {
        return triangles;
    }
    void setTriangles(const std::vector<ivec3>& _triangles)
    {
        for (unsigned int i = 0; i < _triangles.size(); i++)
        {
            triangles.push_back(ivec3(_triangles.at(i)));
        }
    }
    /// Add a triangle to the mesh with the material m
    bool addTriangle(const ivec3 newTriangle, Material* m);

    virtual bool getIntersection(Ray& ray, Intersection& inter)
    {
        return false;
    }

protected:
    std::vector<vec3> vertices;   //!< Vertices of the mesh
    std::vector<ivec3> triangles; //!< Triangles of the mesh
};
#endif
