/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 *
 */

#ifndef __OGL_ELEMENT
#define __OGL_ELEMENT

/**
 * Classe de liste d'objets OGL a afficher
 */
class OGLElement
{
public:
    /**
     * Constructeur.
     */
    OGLElement()
    {
        m_DisplayPositionX = m_DisplayPositionY = 0;
        m_bVisible = false;
    };

    /**
     * Destructeur.
     */
    virtual ~OGLElement(){};

    void setDisplayPosition(double displayPositionX, double displayPositionY)
    {
        m_DisplayPositionX = displayPositionX;
        m_DisplayPositionY = displayPositionY;
    };
    void setVisibility(bool bVisible)
    {
        m_bVisible = bVisible;
    };
    bool getVisibility()
    {
        return m_bVisible;
    };
    void setIs3D(bool bIs3D)
    {
        m_bIs3D = bIs3D;
    };
    bool getIs3D()
    {
        return m_bIs3D;
    };

    virtual int render()
    {
        return 0;
    };

protected:
    double m_DisplayPositionX;
    double m_DisplayPositionY;
    bool m_bVisible;
    bool m_bIs3D;
};

#endif //__OGL_ELEMENT
