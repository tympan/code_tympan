/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 *
 */

#ifndef __OGL_GRID_ELEMENT
#define __OGL_GRID_ELEMENT

#include "OGLElement.h"
#include "Tympan/core/color.h"

#include <qstring.h>

class OGLGrid;

/**
 * Classe pour la grille OGL
 */
class OGLGridElement : public OGLElement
{
public:
    /**
     * Constructeur.
     */
    OGLGridElement();

    /**
     * Destructeur.
     */
    virtual ~OGLGridElement();

    virtual int render();

    void setColor(const OColor& oColor);

    void setGridDimX(const float gridDimX)
    {
        m_fGridDimX = gridDimX;
    };
    void setGridDimY(const float gridDimY)
    {
        m_fGridDimY = gridDimY;
    };
    void setGridStep(const float gridStep)
    {
        m_fGridStep = gridStep;
    };

    void setShowGridXY(bool bShow)
    {
        m_bShowGridXY = bShow;
    };
    void setShowGridXZ(bool bShow)
    {
        m_bShowGridXZ = bShow;
    };
    void setShowGridZY(bool bShow)
    {
        m_bShowGridZY = bShow;
    };

    bool getShowGridXY()
    {
        return m_bShowGridXY;
    };
    bool getShowGridXZ()
    {
        return m_bShowGridXZ;
    };
    bool getShowGridZY()
    {
        return m_bShowGridZY;
    };

    float getGridDimX()
    {
        return m_fGridDimX;
    };
    float getGridDimY()
    {
        return m_fGridDimY;
    };
    float getGridStep()
    {
        return m_fGridStep;
    };

protected:
    OColor m_oColor;
    float m_fGridDimX;
    float m_fGridDimY;
    float m_fGridStep;
    OGLGrid* m_pOGLGridToDisplay;
    bool m_bShowGridXY;
    bool m_bShowGridXZ;
    bool m_bShowGridZY;
};

#endif //__OGL_GRID_ELEMENT
