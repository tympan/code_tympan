/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 *
 */

#ifndef __OGL_TEXT_ELEMENT
#define __OGL_TEXT_ELEMENT

#include "OGLElement.h"
#include "Tympan/core/color.h"

#include <qstring.h>

class OGLFont;
/**
 * Classe Text OGL
 */
class OGLTextElement : public OGLElement
{
public:
    /**
     * Constructeur.
     */
    OGLTextElement();

    /**
     * Destructeur.
     */
    virtual ~OGLTextElement();

    virtual int render();

    void setTextToDisplay(const QString& qsText);

    void setColor(const OColor& oColor);

    void setFont(const QString& qsFontPath);

    QString getText()
    {
        return m_qsText;
    };

protected:
    QString m_qsText;
    QString m_qsFontPath;
    OGLFont* m_pOGLFontToDisplay;
    OColor m_oColor;
};

#endif //__OGL_TEXT_ELEMENT
