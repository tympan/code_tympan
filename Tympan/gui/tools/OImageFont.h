/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 *
 */

#ifndef __O_IMAGEGLF__
#define __O_IMAGEGLF__

#include "OImage.h"
#include <map>
#include <vector>

class OImageFont : public OImage
{
public:
    struct OGLFontChar
    {
        int x;
        int y;
        int w;
        int h;
        int advance;
    };

public:
    OImageFont();
    virtual ~OImageFont();

    /**
     * Overloading of OImage::load
     */
    virtual bool load(const std::string& filename);

    /**
     * returns a constant reference to the character
     */
    const OGLFontChar& getChar(unsigned char c) const;

    /**
     * returns the kerning between the first and second characters
     */
    const int getKerning(unsigned char first, unsigned char second) const;

private:
    std::vector<OGLFontChar> _char;
    int _kernings[256][256] = {0};
    OGLFontChar _invalid_char;
};

#endif // __O_IMAGEGLF__
