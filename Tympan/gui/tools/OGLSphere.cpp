/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 */

#include "OGLSphere.h"

#if _WIN32
    #include <windows.h>
#endif //_WIN32

#include <GL/gl.h>

#include "Tympan/core/color.h"
#include "Tympan/models/common/3d.h"

OGLSphere::OGLSphere() {}

OGLSphere::~OGLSphere() {}

void OGLSphere::drawSphere(const OPoint3D& center, float radius, const OColor& oColor) const
{
    // Antialiasing sur les points
    glEnable(GL_POINT_SMOOTH);
    // Gestion de la transparence
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    // Couleur du point
    glColor3fv(oColor);
    glPointSize(radius);

    glBegin(GL_POINTS);
    glVertex3d(center._x, center._y, center._z);
    glEnd();
}
