/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 *
 */

#ifndef __O_GLTEXTURE__
#define __O_GLTEXTURE__

#if _WIN32
    #include <windows.h>
#endif //_WIN32

#include <GL/gl.h>

#include "Tympan/core/smartptr.h"
/**
 * Interface pour la gestion des textures OpengGL.
 */
class OGLTexture : public IRefCount
{
public:
    OGLTexture();
    virtual ~OGLTexture();

    /*
     * Charge la texture a partir d'un fichier.
     * Methode virtuelle pure a implementer.
     */
    virtual bool load(const char* filename) = 0;

    /*
     * Attache la texture.
     */
    virtual void bind();

    /*
     * Liberation de la memoire.
     */
    virtual void free();

protected:
    // Generation d'une id de texture
    void genTexture();

    // Id de texture OpenGL
    GLuint id;
};

#endif // __O_GLTEXTURE__
