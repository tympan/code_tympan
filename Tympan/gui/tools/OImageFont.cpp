/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 */

#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1

#include "OImageFont.h"
#include <cassert>
#include <fstream>
OImageFont::OImageFont() : OImage() {}

OImageFont::~OImageFont() {}

bool OImageFont::load(const std::string& filename)
{

    std::string line;
    char spaces[100];
#ifdef _WIN32
    int ascii = 0, xOffset = 0, yOffset = 0, page = 0, first = 0, second = 0, kerning = 0;
#else
    int ascii, xOffset, yOffset, page, first, second, kerning;
#endif

    char* charLinePattern =
        "char id=%d%[ ]x=%d%[ ]y=%d%[ ]width=%d%[ ]height=%d%[ ]xoffset=%d%[ ]yoffset=%d%[ "
        "]xadvance=%d%[ ]page=%d%[ ]\n";

    // Read the file with the description of the chararacters and their kernings (.fnt)
    std::ifstream ifs;
    ifs.open(filename.c_str(), std::ios::in | std::ios::binary);
    if (!ifs)
    {
        return false;
    }

    // Skip the header
    std::getline(ifs, line);
    std::getline(ifs, line);
    std::getline(ifs, line);
    std::getline(ifs, line);

    // Read the first char (which must be the invalid chararacter!)
    std::getline(ifs, line);
#ifdef _WIN32
    if (sscanf_s(line.c_str(), charLinePattern, &ascii, spaces, (unsigned)_countof(spaces), &_invalid_char.x,
                 spaces, (unsigned)_countof(spaces), &_invalid_char.y, spaces, (unsigned)_countof(spaces),
                 &_invalid_char.w, spaces, (unsigned)_countof(spaces), &_invalid_char.h, spaces,
                 (unsigned)_countof(spaces), &xOffset, spaces, (unsigned)_countof(spaces), &yOffset, spaces,
                 (unsigned)_countof(spaces), &_invalid_char.advance, spaces, (unsigned)_countof(spaces),
                 &page, spaces, (unsigned)_countof(spaces)) != 18)
        return false;
#else
    if (sscanf(line.c_str(), charLinePattern, &ascii, spaces, &_invalid_char.x, spaces, &_invalid_char.y,
               spaces, &_invalid_char.w, spaces, &_invalid_char.h, spaces, &xOffset, spaces, &yOffset, spaces,
               &_invalid_char.advance, spaces, &page, spaces) != 18)
        return false;
#endif

    // Initialize the char vector with the invalid_char
    _char = std::vector<OGLFontChar>(256, _invalid_char);

    // Read the rest of the file
    while (std::getline(ifs, line))
    {

        // Match char lines
        OGLFontChar c;
#ifdef _WIN32
        if (sscanf_s(line.c_str(), charLinePattern, &ascii, spaces, (unsigned)_countof(spaces), &c.x, spaces,
                     (unsigned)_countof(spaces), &c.y, spaces, (unsigned)_countof(spaces), &c.w, spaces,
                     (unsigned)_countof(spaces), &c.h, spaces, (unsigned)_countof(spaces), &xOffset, spaces,
                     (unsigned)_countof(spaces), &yOffset, spaces, (unsigned)_countof(spaces), &c.advance,
                     spaces, (unsigned)_countof(spaces), &page, spaces, (unsigned)_countof(spaces)) == 18)
            _char[ascii] = c;

        // Match kerning lines
        if (sscanf_s(line.c_str(), "kerning first=%d%[ ]second=%d%[ ]amount=%d%[ ]", &first, spaces,
                     (unsigned)_countof(spaces), &second, spaces, (unsigned)_countof(spaces), &kerning,
                     spaces, (unsigned)_countof(spaces)))
            _kernings[first][second] = kerning;
#else
        if (sscanf(line.c_str(), charLinePattern, &ascii, spaces, &c.x, spaces, &c.y, spaces, &c.w, spaces,
                   &c.h, spaces, &xOffset, spaces, &yOffset, spaces, &c.advance, spaces, &page, spaces) == 18)
            _char[ascii] = c;

        // Match kerning lines
        if (sscanf(line.c_str(), "kerning first=%d%[ ]second=%d%[ ]amount=%d%[ ]", &first, spaces, &second,
                   spaces, &kerning, spaces))
            _kernings[first][second] = kerning;
#endif
    }
    ifs.close();

    // Read the texture (.tga)
    std::string tgaFilename = filename.substr(0, filename.find_last_of('.')) + ".tga";
    ifs.open(tgaFilename.c_str(), std::ios::in | std::ios::binary);
    if (!ifs)
    {
        return false;
    }

    // Skip the first 12 bytes of the header
    ifs.seekg(12, std::ios::beg);

    // Read the width and height of the texture in pixels (2 bytes each)
    ifs.read((char*)&_sizeX, 2);
    ifs.read((char*)&_sizeY, 2);

    // Skip the first 12 bytes of the header
    ifs.seekg(2, std::ios::cur);

    // Read the texture's data (1 byte per pixel)
    int textureSize = _sizeX * _sizeY;
    _data = new unsigned char[textureSize];
    ifs.read((char*)_data, textureSize);

    _depth = 8;

    ifs.close();
    return true;
}

const OImageFont::OGLFontChar& OImageFont::getChar(unsigned char c) const
{
    if (c >= 0 && c < 256)
        return _char.at(c);
    else
        return _invalid_char;
}

const int OImageFont::getKerning(unsigned char first, unsigned char second) const
{
    if (first >= 0 && first < 256 && second >= 0 && second < 256)
        return _kernings[first][second];
    else
        return 0;
}
