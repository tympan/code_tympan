/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 *
 */

#ifndef __O_GLLINE__
#define __O_GLLINE__

class OColor;
class OPoint3D;

#include "Tympan/core/smartptr.h"

class OGLLine
{
public:
    OGLLine();
    virtual ~OGLLine();

    /**
     * Affiche une ligne
     */
    void drawLine(const OPoint3D& point1, const OPoint3D& point2, const OColor& oColor,
                  float lineWidth) const;

private:
};

// Smart pointer sur OGLFont.
typedef SmartPtr<OGLLine> LPOGLLine;

#endif // __O_GLLINE__
