/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYModelerFrame.h
 * \brief Classe generique pour une fenetre de modeleur (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_MODELER_FRAME__
#define __TY_MODELER_FRAME__

#include <qwidget.h>
// Added by qt3to4:
#include <QCloseEvent>
#include <QBoxLayout>
#include <QShowEvent>
#include <QWheelEvent>
#include <QResizeEvent>
#include <QGridLayout>
#include <QFocusEvent>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QEvent>

#include "TYAppDefines.h"
#include "TYRenderWindowInteractor.h"
#include "TYActionManager.h"
#include "Tympan/models/business/TYElement.h"

class QGridLayout;
class QBoxLayout;
class QComboBox;
class QStatusBar;
class QToolButton;
class OGLTextElement;
class OGLScalarBarElement;
class OGLLineElement;
class OGLGridElement;
class OGLLightElement;
class OGLCamera;

/**
 * \class TYModelerFrame
 * \brief Generic class for a modeler window
 */
class TYModelerFrame : public QWidget
{
    Q_OBJECT

public:
    /**
     * The 5 views of the modeler:
     *  - Top view,
     *  - Left view,
     *  - Front view,
     *  - Perspective view,
     *  - Free camera view.
     */
    enum ModelerViewType
    {
        TopView,
        LeftView,
        FrontView,
        PerspView,
        FreeView,
        NbOfViews
    };

    /**
     * Rendering modes.
     */
    enum RenderMode
    {
        Points,
        Wireframe,
        Surface,
        Flat,
        Gouraud,
        Phong,
        NbOfRenderMode
    };

    /**
     * Different editing modes for a site.
     */
    enum ModelerMode
    {
        NoMode = -1,
        CameraMode,
        CameraZoneMode,
        DistanceMode,
        MovingMode,
        RotationMode,
        EditionMode,
        NbOfModelerMode
    };

    /**
     * Default constructor.
     */
    TYModelerFrame(QWidget* parent = 0, const char* name = 0, Qt::WindowFlags f = Qt::SubWindow);
    /**
     * Destructor.
     */
    virtual ~TYModelerFrame();

    /**
     * Returns the graphics view.
     */
    TYRenderWindowInteractor* getView()
    {
        return _pView;
    }

    /**
     * Returns the renderer associated with the view.
     */
    TYOpenGLRenderer* getRenderer()
    {
        return _pView->getRenderer();
    }

    /**
     * Returns the type of the current view.
     */
    int getCurrentView()
    {
        return _curViewType;
    }

    /**
     * Returns the active rendering mode.
     */
    int getRenderMode()
    {
        return _curRenderMode;
    }

    /**
     * Returns the element picker.
     *
     * @return The element picker.
     */
    TYElementPicker* getElementPicker()
    {
        return _pPicker;
    }
    /**
     * Returns a pointer to the pick editor associated with this modeler.
     */
    TYPickEditor* getPickEditor()
    {
        return _pPickEditor;
    }

    /**
     * Returns a pointer to the current editor of this modeler.
     */
    TYAbstractSceneEditor* getCurrentEditor()
    {
        return _pCurrentEditor;
    }

    /**
     * Returns a pointer to the camera editor.
     */
    TYCameraEditor* getCameraEditor()
    {
        return _pCameraEditor;
    }

    /**
     * Returns a pointer to the status bar.
     */
    QStatusBar* statusBar()
    {
        return _pStatusBar;
    }

    /**
     * Returns the state of magnetic grid activation.
     */
    bool getSnapGridActive()
    {
        return _snapGridActive;
    }

    /**
     * Returns the action manager (history).
     */
    TYActionManager* getActionManager()
    {
        return &_actionManager;
    }

    /**
     * Indicates whether or not to display point sources.
     */
    bool getShowSources()
    {
        return _showSources;
    }

    /**
     * Indicates the state of grid visualization.
     */
    bool getShowGrid()
    {
        return _showGrid;
    }

    /**
     * Returns the state of the special rendering mode during camera movement.
     *
     * @see _wireframeOnMovingCamera
     */
    bool getWireframeOnMovingCamera()
    {
        return _wireframeOnMovingCamera;
    }

    /**
     * Set/Get the element to edit.
     */
    void setElement(LPTYElement pElement)
    {
        _pElement = pElement;
    }
    /**
     * Set/Get the element to edit.
     */
    LPTYElement getElement()
    {
        return _pElement;
    }

    /**
     * Indicates if the edited element is part of the selection of the current
     * calculation of the current project.
     */
    bool isElementInCurrentCalcul();
    /**
     * Indicates if the edited element is part of the current project.
     * The search is done by parentage.
     */
    bool isElementInCurrentProjet();
    /**
     * Indicates whether the user agrees to erase the result (if necessary)
     * to perform the action.
     */
    bool askForResetResultat();

    /**
     * Computes the 3D scene coordinates from the
     * cursor position.
     * This method can be overridden for altitude calculation.
     */
    virtual bool computeCurPos(int x, int y, float* pos);

    /**
     * @brief Returns default Z coordinate for editors
     * This value will depend on the type of modeler and
     * of the fixed altitude (or not) of the underlying site
     *
     * @return
     */
    virtual float getDefaultZCoord();

    /**
     * @brief sets default Z coordinate for editors
     *
     * @param fDefaultZCoord
     */
    void setDefaultZCoord(float fDefaultZCoord)
    {
        _defaultZCoord = fDefaultZCoord;
    }

public slots:
    /**
     * Selects the view type.
     */
    virtual void setViewType(int view);

    /**
     * Selects the rendering mode.
     */
    virtual void setRenderModeSlot(int mode);

    /**
     * Selects the rendering mode.
     */
    virtual void setRenderMode(int mode, bool bUpdateGL);

    /**
     * Activates an editing mode.
     */
    virtual void setEditorMode(int mode);
    /**
     * Activates the camera mode.
     */
    void setEditorModeToCamera()
    {
        setEditorMode(CameraMode);
    }

    /**
     * Shows or hides the grid.
     */
    void showGrid(bool show);

    /**
     * Shows or hides normals.
     * Calls updateView().
     */
    void showNormals(bool show);

    /**
     * Shows or hides the ceiling of buildings.
     * Calls updateView().
     */
    void showPlafond(bool show);

    /**
     * Shows or hides point sources.
     */
    void showSources();

    /**
     * Displays rays
     */
    void showRays(bool show);

    /**
     * Enable or disable button allowing to show rays
     */
    void setKeepRays(bool keepRays);

    /**
     * Activates or deactivates the magnetic grid.
     */
    void setSnapGridActive(bool state);

    /**
     * Specifies the camera coordinates (in "mobile" mode).
     */
    void setCameraCoordinates();

    /**
     * Takes a snapshot of the 3D view to save
     * it in an image file.
     */
    void screenShot();

    /**
     * Updates the graphics view as well as the
     * axes and the grid.
     *
     * @param clipping To update the camera's clipping range.
     * @param axesAndGrid To update the axes and the grid.
     */
    virtual void updateView(bool clipping = true, bool axesAndGrid = true);

    /**
     * Updates the orientation of the axes based on the position
     * of the camera.
     */
    void updateAxes();

    /**
     * Updates the cursor position information.
     */
    void updateCurPosInfo();

    /**
     * Builds the grid.
     */
    void updateGrid();

    /**
     * Updates the graphical structure of the element associated with this modeler.
     *
     * @param force Force the update even if the element is not marked
     *              as modified.
     */
    void updateElementGraphic(bool force = false);

    /**
     * Launches the printing of the current window.
     */
    void print();

    /**
     * Takes a snapshot of the 3D view to copy
     * it to the clipboard.
     */
    void copy();

    /**
     * Displays the editing properties box of the edited element.
     */
    void editElement();

    /**
     * Frames the view to see all the graphic objects
     * present in the scene.
     */
    void fit();

    /**
     * Updates the parameters of the preferences.
     */
    virtual void updatePreferences();

    /**
     * Updates the grid.
     */
    void resizeGrid();

    /**
     * Overload of the close function of QWidget.
     * Proposes to save on closing.
     */
    virtual bool close();

    /**
     * Activates the special rendering mode during camera movement.
     *
     * @see _wireframeOnMovingCamera
     */
    void setWireframeOnMovingCamera(bool state)
    {
        _wireframeOnMovingCamera = state;
    }

    /**
     * Switches to special rendering mode during camera movement.
     *
     * @see _wireframeOnMovingCamera
     */
    void startMovingRenderMode();
    /**
     * Switches back to normal rendering mode after camera movement.
     *
     * @see _wireframeOnMovingCamera
     */
    void stopMovingRenderMode();

    /**
     * Updates the scale.
     */
    void updateScale();

    /**
     * Shows or hides the scale.
     */
    void showScale(bool show);

    /**
     * Opens a window asking the user to enter a double.
     */
    static double getDouble(const QString& title, const QString& txt, double min, double max, double val,
                            bool& ok, int dec = 2);

signals:
    /**
     * Indicates that the widget is about to be closed.
     */
    void aboutToClose();

    /**
     * Indicates that the view type has changed.
     */
    void viewTypeChanged(int);

    /**
     * Indicates that the editing mode has changed.
     */
    void editorModeChanged(int mode);

    /**
     * Indicates that the element has been modified.
     */
    void eltModified(LPTYElement pElt);

    /**
     * Indicates that the mouse cursor enters the widget.
     */
    void mouseEnter();

    /**
     * Indicates that the mouse cursor leaves the widget.
     */
    void mouseLeave();

    /**
     * Indicates that the frame has been maximized.
     * Note: a slot named isMaximized already exists.
     */
    void frameResized();

protected:
    virtual void keyPressEvent(QKeyEvent* pEvent);
    virtual void keyReleaseEvent(QKeyEvent* pEvent);
    virtual void mouseMoveEvent(QMouseEvent* pEvent);
    virtual void wheelEvent(QWheelEvent* pEvent);
    virtual void resizeEvent(QResizeEvent* pEvent);
    virtual void focusInEvent(QFocusEvent* pEvent);
    virtual void showEvent(QShowEvent* pEvent);
    virtual void closeEvent(QCloseEvent* pEvent);
    virtual void enterEvent(QEvent* pEvent);
    virtual void leaveEvent(QEvent* pEvent);

    /**
     * Sets the visibility of actors constituting the grids.
     */
    void setGridLinesActorsVisibility(bool showGridXY, bool showGridXZ, bool showGridZY);

    OBox getGlobalBoundingBox();

private:
    /// The layout of this frame.
    QGridLayout* _pLayout;

    /// Combo box for rendering mode.
    QComboBox* _pRenderModeBox;

    /// Button to activate the magnetic grid.
    QToolButton* _pSnapGridBtn;

    /// Indicates whether or not to activate the magnetic grid.
    bool _snapGridActive;

    /// Button for taking a screenshot.
    QToolButton* _pScreenShotBtn;

    /// Button for displaying point sources.
    QToolButton* _pShowSourcesBtn;

    /// Indicates whether or not to display point sources.
    bool _showSources;

    /// Button for displaying normals.
    QToolButton* _pShowNormalsBtn;

    /// Indicates whether or not to display or hide normals.
    bool _showNormals;

    /// Button for displaying the ceiling of buildings.
    QToolButton* _pShowPlafondBtn;

    /// Indicates whether or not to display or hide the ceiling of buildings.
    bool _showPlafond;

    /// Button for displaying rays calculated by ray tracing.
    QToolButton* _pShowRaysBtn;

    /// Indicates whether or not to display rays.
    bool _showRays;

    /// Status bar.
    QStatusBar* _pStatusBar;

    /// The current rendering mode.
    int _curRenderMode;

    /// Indicates whether to switch to wireframe rendering during camera movement.
    bool _wireframeOnMovingCamera;

    /// The last current rendering mode.
    int _lastRenderMode;

    /// To keep track of the last editing mode.
    int _lastEditorMode;

    /// For picking.
    TYElementPicker* _pPicker;

    /// Manages the camera.
    TYCameraEditor* _pCameraEditor;

    /// Tools for measuring.
    TYDistanceEditor* _pDistanceEditor;

    /// Editor for moving elements.
    TYPositionEditor* _pPositionEditor;

    /// Editor to define a zoom area for the camera.
    TYCameraZoneEditor* _pCameraZoneEditor;

    /// 2D label to display the type of view.
    OGLTextElement* _pOGLTextElement;

    /// Geometry of the Axes.
    OGLLineElement* _pOGLLineElementAxeX;
    OGLLineElement* _pOGLLineElementAxeY;
    OGLLineElement* _pOGLLineElementAxeZ;
    OGLTextElement* _pOGLTextElementLabelX;
    OGLTextElement* _pOGLTextElementLabelY;
    OGLTextElement* _pOGLTextElementLabelZ;

    /// Button to activate the grid.
    QToolButton* _pGridBtn;

    /// X and Y axes of the grid.
    OGLLineElement* _pOGLLineElementX;
    OGLLineElement* _pOGLLineElementY;

    /// Indicates whether to display the grid or not.
    bool _showGrid;

    /// For managing the history.
    TYActionManager _actionManager;

    /// The default light.
    OGLLightElement* _pLightElement;

    // Collections of lines for the grid.
    OGLGridElement* _pOGLGridElement;

    /// Graphic object for representing the scale.
    OGLScalarBarElement* _pOGLScalarBarElement;

    /// Button for displaying the scale.
    QToolButton* _pShowScale;

    /// Indicates whether to display the scale or not.
    bool _showScale;

    bool _firstTimeShown;

protected:
    /// The layout where buttons, etc., are located.
    QBoxLayout* _pCtrlLayout;

    /// Combo box for selecting the current camera.
    QComboBox* _pViewTypeBox;

    /// The graphics window.
    TYRenderWindowInteractor* _pView;

    /// The type of the current view.
    int _curViewType;

    /// Indicates if the editing mode has been processed.
    bool _editorModeAccepted;

    /// The current editor.
    TYAbstractSceneEditor* _pCurrentEditor;

    /// Manages the context menu.
    TYPickEditor* _pPickEditor;

    /// Cameras for each type of view.
    OGLCamera* _pOGLCameras[NbOfViews];

    /// Grid step.
    float _gridStep;
    /// Magnetic grid step.
    float _gridMagnStep;
    /// Grid dimension in X.
    float _gridDimX;
    /// Grid dimension in Y.
    float _gridDimY;

    /// Button to specify camera coordinates.
    QToolButton* _pSetCameraCoordinatesBtn;

    /// A pointer to the element to edit.
    LPTYElement _pElement;

    /// Default Z coordinate for editors.
    float _defaultZCoord;
};

#endif //__TY_MODELER_FRAME__
