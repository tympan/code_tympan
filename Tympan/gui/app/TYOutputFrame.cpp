/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYOutputFrame.cpp
 * \brief Frame pour les messages de retour
 *
 *
 */

#include <qtextedit.h>
#include <qlayout.h>
// Added by qt3to4:
#include <QBoxLayout>
#include <QVBoxLayout>

#include "Tympan/models/business/OLocalizator.h"
#include "Tympan/models/business/TYPreferenceManager.h"
#include "TYOutputFrame.h"

#define TR(id) OLocalizator::getString("TYOutputFrame", (id))

TYOutputFrame::TYOutputFrame(QWidget* parent, const char* name, Qt::WindowFlags f) : QWidget(parent, f)
{
    setObjectName(name);
    QBoxLayout* pLayout = new QVBoxLayout(this);

    _pTextEdit = new QTextEdit(this);
    _pTextEdit->setReadOnly(true);
    //  _pTextEdit->setFontFamily("Courier");
    //  _pTextEdit->setFontPointSize(10);

    pLayout->addWidget(_pTextEdit);
}

TYOutputFrame::~TYOutputFrame() {}

void TYOutputFrame::appendMsg(QString msg)
{
    _pTextEdit->append(msg);
}
