/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYMainWindow.h
 * \brief Fenetre principale de l'application Tympan (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_MAIN_WINDOW__
#define __TY_MAIN_WINDOW__

#include <qmainwindow.h>
#include <memory>
#include "Tympan/gui/app/TYAppDefines.h"
#include "Tympan/models/business/TYProjet.h"

class QAction;
class QToolBar;
class QMenu;
class QDockWidget;
class QToolButton;
class QButtonGroup;
class QTextBrowser;
class QCloseEvent;
class QMdiArea;
class QMdiSubWindow;

class TYSpectreManager;
class TYMaillageManager;

/**
 * \class TYMainWindow
 * \brief Main window of the Tympan application
 */
class TYMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * Default constructor.
     */
    TYMainWindow();
    /**
     * Destructor.
     */
    virtual ~TYMainWindow();

    QMdiArea* getWorkspace()
    {
        return _pWorkspace;
    }

    TYModelerFrame* getCurrentModeler()
    {
        return _pCurrentModeler;
    }

    TYProjetFrame* getProjetFrame()
    {
        return _pProjetFrame;
    }
    TYSiteFrame* getSiteFrame()
    {
        return _pSiteFrame;
    }
    TYOutputFrame* getOutputFrame()
    {
        return _pOutputFrame;
    }

    TYFaceToolbar* getFaceToolbar()
    {
        return _pToolbarFace;
    }
    TYModelerToolbar* getModelerToolbar()
    {
        return _pToolbarModeler;
    }

    void updateCurrentFileName(const QString& fileName);

    /// Returns the save request status
    bool getSaveStatus()
    {
        return _closeAndQuit;
    }
    void initSaveStatus()
    {
        _closeAndQuit = true;
    }

    virtual QMenu* createPopupMenu();

    /**
     * @brief save geometry of windows to preferences
     */
    void saveGeometryToPreferences();

public slots:
    /**
     * Refreshes the window title.
     */
    void refreshWindowTitle();
    /**
     * Loads settings.
     */
    bool loadSettings(const QString& fileName);

    /**
     * @brief Returns a regular file name or the most recent file matching the file name pattern in the Tympan
     * user directory
     *
     * @param TympanUserDir Tympan user directory
     * @param fileNameBegin Beginning of the file name to search for
     * @param fileNameEnd End of the file name to search for
     * @param fileNamePattern File name pattern with a single-digit placeholder for 'x'
     * @return A QFile
     */
    std::unique_ptr<QFile> searchSettingsFile(const QString& TympanUserDir, const QString& fileNameBegin,
                                              const QString& fileNameEnd, const QString& fileNamePattern);
    /**
     * Saves settings.
     */
    bool saveSettings(const QString& fileName);

    /**
     * Sets the current project.
     */
    void setCurProjet(LPTYProjet pProjet);
    /**
     * Sets the current site node.
     */
    void setCurSiteNode(LPTYSiteNode pSiteNode);
    /**
     * Updates the current calculation.
     */
    void updateCurCalcul();

    /**
     * Displays the dialog for creating a new element.
     */
    void createNew();
    /**
     * Creates a new project.
     */
    void createNewProjet();

    /**
     * Show the select zone page to create new geo project
     */
    void showZoneSelectPage();

    /**
     * Creates a new geographic project.
     */
    void createNewGeoProjet();

    /**
     * Creates a new site.
     */
    void createNewSite();
    /**
     * Creates a new building.
     */
    void createNewBatiment();
    /**
     * Creates a new machine.
     */
    void createNewMachine();

    /**
     * Opens an XML file.
     */
    void open();

    /**
     * Opens an XML file (used for importing components).
     */
    void open(std::vector<LPTYElement>& tabElem, const bool& bRegenerate = false);

    /**
     *
     */
    bool save(QString dirName, QString& fileName, TYElement* pElement);

    /**
     *
     */
    void formatFileName(QString& fileName)
    {
        if (!fileName.endsWith(".xml"))
        {
            fileName += ".xml";
        }
    }

    /**
     *
     */
    void updateCurrentAppFile(const QString& dirName, const QString& fileName);

    /**
     * Saves the element of the active window.
     */
    bool save();

    /**
     * Saves the project (or active element) under a different name.
     */
    bool saveAs();

    /**
     * Saves the project (or active element) under a different name without calculation results.
     */
    bool saveAsNoResult();

    /**
     * Saves the element passed as a parameter.
     */
    bool saveAs(LPTYElement pElement);

    /**
     *
     */
    TYElement* elementToSave();

    /**
     * Creates a copy of the file passed as a parameter.
     */
    bool backupFile(const QString& fileName);

    /**
     * Displays the plugin manager.
     */
    void showPluginManager();

    /**
     * Closes the current project or site.
     */
    void close();

    /**
     * Quits the application.
     */
    void quitte();

    /**
     * Displays the print dialog.
     */
    void print();

    /**
     * Undoes the last action.
     */
    void undo();
    /**
     * Redoes the last undone action.
     */
    void redo();

    /**
     * Cut-Copy-Paste.
     */
    void cut();
    /**
     * Cut-Copy-Paste.
     */
    void copy();
    /**
     * Cut-Copy-Paste.
     */
    void paste();

    /**
     * Displays the help browser.
     */
    void showHelp();

    /**
     * Displays the information window about Tympan.
     */
    void about();

    /**
     * Shows or hides the spectrum manager.
     */
    void showSpectreManager(bool show);
    /**
     * Shows or hides the mesh manager.
     */
    void showMaillageManager(bool show);
    /**
     * Shows or hides the library.
     */
    void showBiblio(bool show);
    /**
     * Displays the preferences dialog.
     */
    void showPreferenceDialog();
    /**
     * Displays the output window.
     */
    void showOutput(bool show);

    /**
     * Synchronizes the button state with the visibility of the output window.
     */
    void updateHideOrShowOutputAction(bool visibilityChanged);

    /**
     * Opens the Importer (Tympan II to Tympan 3 conversion)
     * in a new process.
     */
    void launchImporter();

    /**
     * Creates a modeler based on a type.
     */
    bool makeModeler(TYElement* pElt);

    /**
     * Instantiates a new Building Modeler window.
     */
    bool makeBatimentModeler(LPTYBatiment pBatiment = NULL);
    /**
     * Instantiates a new Machine Modeler window.
     */
    bool makeMachineModeler(LPTYMachine pMachine = NULL);
    /**
     * Instantiates a new Site Modeler window.
     */
    void makeSiteModeler(LPTYSiteNode pSite = NULL);
    /**
     * Instantiates a new Project Modeler window.
     */
    void makeProjetModeler(LPTYProjet pProjet = NULL);

    /**
     * Connects an ActionManager to this MainWindow.
     * This way, the undo and redo actions will be connected to this new
     * ActionManager, and the previous one will be disconnected.
     *
     * @param pActionManager The ActionManager to connect.
     */
    void connectActionManager(TYActionManager* pActionManager);
    /**
     * Connects the Application's ActionManager to this MainWindow.
     */
    void connectDefaultActionManager();

    /**
     * Updates the graphical views of the open modelers.
     *
     * @param clipping To update the camera's clipping range.
     * @param axesAndGrid To update the axes and grid.
     * @param displayList To update the global display list.
     */
    void updateModelers(bool clipping = true, bool axesAndGrid = true, bool displayList = true);
    /**
     * Updates the graphical structure of the element associated with the open modelers.
     *
     * @param force Forces the update even if the element is not marked
     *              as modified.
     */
    void updateModelersElementGraphic(bool force = false);

    /**
     * Updates the open modelers after acoustic calculation.
     *
     * @param result The new project after acoustic calculation.
     */
    void updateModelersAfterComputation(LPTYProjet& result);

    /**
     * Closes the modeler of an element.
     * @param pElement Object whose modeler is open.
     */
    void closeModeler(const TYElement* pElement);

    /**
     * Closes all modelers
     */
    void closeModelers();

    /**
     * Displays the widget of the current calculation.
     */
    void editCurCalcul();
    /**
     * Launches the current calculation.
     */
    void goCurCalcul();

    void reloadSlots();

    /**
     * Resets the interface to the default camera mode
     * (automatically used after creating elements)
     */
    void setDefaultCameraMode();

private slots:
    /**
     * Manages the display of windows.
     */
    void windowsMenuAboutToShow();
    /**
     * Manages the display of windows.
     */
    void windowsMenuActivated();
    /**
     * Called when a window is activated.
     */
    void subWindowActivated();

    /**
     * To preserve the current mode of SiteModeler windows.
     */
    void saveCurSiteMode(int mode);
    /**
     * To preserve the current mode of MachineModeler windows.
     */
    void saveCurMachineMode(int mode);
    /**
     * To preserve the current mode of BatimentModeler windows.
     */
    void saveCurBatimentMode(int mode);
    /**
     * To preserve the current mode of FaceModeler windows.
     */
    void saveCurFaceMode(int mode);

    /**
     * Updates the menus and buttons for undo and redo.
     */
    void updateUndoRedo(bool undoAvailable, bool redoAvailable, const QString& undoCmd,
                        const QString& redoCmd);

    /*
     * When the modeler is deleted, assign nullptr
     */
    void onDestroyed()
    {
        _pCurrentModeler = nullptr;
    };

    /*
     * Add the level curve to main site topography, when it is created from geo provider
     */
    void onCourbeNiveauCreated(LPTYCourbeNiveau courbeNiveau);

signals:
    /**
     * Signal that an undo is requested.
     */
    void onUndo();
    /**
     * Signal that a redo is requested.
     */
    void onRedo();

protected:
    virtual void closeEvent(QCloseEvent* pEvent);

    /// Tests the existence and the possibility of writing the file
    bool testFile(const QString& fileName);

    /// Produces a valid file name (or nothing!)
    QString getFileName(const QString& filter, QString& dirName, const bool& forceNewName = false);

protected:
    /// Workspace for MDI management.
    QMdiArea* _pWorkspace;

    // CLM-NT35 End

    /// Currently displayed modeler
    TYModelerFrame* _pCurrentModeler;

    /// Action to create a new element.
    QAction* _pCreateNewAction;
    /// Action to open the library manager.
    QAction* _pOpenAction;
    /// Action to close the current project or site.
    QAction* _pCloseAction;

    /// Action to save an element.
    QAction* _pSaveAction;

    /// Action to save an element with a specific name.
    QAction* _pSaveAsAction;

    /// Action to save an element with a specific name without calculation results.
    QAction* _pSaveAsNoResultAction;

    /// Action to display the plug-in manager.
    QAction* _pShowPluginManagerAction;

    /// Action to save all elements.
    QAction* _pSaveAllAction;

    /// Action to import an element from an XML file.
    QAction* _pImportXMLAction;
    /// Action to export the element from the active modeler to XML.
    QAction* _pExportXMLAction;

    /// Action to print.
    QAction* _pPrintAction;

    /// Action to perform an undo.
    QAction* _pUndoAction;
    /// Action to perform a redo.
    QAction* _pRedoAction;

    /// Action to perform a cut.
    QAction* _pCutAction;
    /// Action to perform a copy.
    QAction* _pCopyAction;
    /// Action to perform a paste.
    QAction* _pPasteAction;

    /// Shows or hides the Spectrum Manager.
    QAction* _pHideOrShowSpectreMngrAction;
    /// Shows or hides the Mesh Manager.
    QAction* _pHideOrShowMaillageMngrAction;
    /// Shows or hides the Library of elements.
    QAction* _pHideOrShowBiblioAction;
    /// Shows or hides the output messages window.
    QAction* _pHideOrShowOutputAction;

    /// Action to edit the preferences of the current calculation.
    QAction* _pEditCurCalculAction;
    /// Action to start the current calculation.
    QAction* _pGoCurCalculAction;

    /// Main toolbar.
    QToolBar* _pMainToolbar;

    /// Toolbar for window management.
    QToolBar* _pWindowToolbar;

    /// Toolbar for acoustic calculations.
    QToolBar* _pCalculAcousticToolbar;

    /// General toolbar for modelers.
    TYModelerToolbar* _pToolbarModeler;

    /// Button group for site toolbars.
    QButtonGroup* _pSiteBtnGroup;
    /// Preserves the current mode of SiteModeler windows.
    int _curSiteMode;
    /// Topography toolbar.
    TYTopoToolbar* _pToolbarTopo;
    /// Infrastructure toolbar.
    TYInfraToolbar* _pToolbarInfra;
    /// Calculation toolbar.
    TYCalculToolbar* _pToolbarCalcul;

    /// Button group for machine toolbar.
    QButtonGroup* _pMachineBtnGroup;
    /// Preserves the current mode of MachineModeler windows.
    int _curMachineMode;
    /// Machine toolbar.
    TYMachineToolbar* _pToolbarMachine;

    /// Button group for face toolbar.
    QButtonGroup* _pFaceBtnGroup;
    /// Preserves the current mode of FaceModeler windows.
    int _curFaceMode;
    /// Face toolbar.
    TYFaceToolbar* _pToolbarFace;

    /// Button group for building toolbar.
    QButtonGroup* _pBatimentBtnGroup;
    /// Preserves the current mode of BatimentModeler windows.
    int _curBatimentMode;
    /// Building toolbar.
    TYBatimentToolbar* _pToolbarBatiment;

    /// Frame for project management.
    TYProjetFrame* _pProjetFrame;
    /// Dockable window for project management.
    QDockWidget* _pProjetDockWnd;

    /// Frame for site management.
    TYSiteFrame* _pSiteFrame;
    /// Dockable window for site management.
    QDockWidget* _pSiteDockWnd;

    /// Frame for feedback messages.
    TYOutputFrame* _pOutputFrame;
    /// Dockable window for feedback messages.
    QDockWidget* _pOutputDockWnd;

    /// Spectra manager.
    // TYSpectreManager * _pSpectreManager;

    /// Mesh manager.
    // TYMaillageManager * _pMaillageManager;
    /// Mesh modeler for the mesh manager.
    // TYMaillageModelerFrame * _pMaillageModeler;//az--

    /// Window management menu.
    QMenu* _pWindowsMenu;

    /// Help browser.
    QTextBrowser* _pHelpBrowser;

    /// Indicator for canceling the exit command.
    bool _closeAndQuit;
};

#endif //__TY_MAIN_WINDOW__
