/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYMachineModelerFrame.h
 * \brief Modeler specialisee pour l'edition des machines (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_MACHINE_MODELER_FRAME__
#define __TY_MACHINE_MODELER_FRAME__

#include <qwidget.h>
#include "Tympan/models/business/infrastructure/TYMachine.h"
#include "TYModelerFrame.h"

/**
 * \class TYMachineModelerFrame
 * \brief Classe Modeler specialisee pour l'edition des machines.
 */
class TYMachineModelerFrame : public TYModelerFrame
{
    Q_OBJECT

public:
    /**
     * Constructeur par defaut.
     */
    TYMachineModelerFrame(LPTYMachine pMachine = 0, QWidget* parent = 0, const char* name = 0,
                          Qt::WindowFlags f = Qt::SubWindow);
    /**
     * Destructeur.
     */
    virtual ~TYMachineModelerFrame();

    /**
     * Set/Get de la machine a editer.
     */
    void setMachine(LPTYMachine pMachine);
    /**
     * Set/Get de la machine a editer.
     */
    LPTYMachine getMachine()
    {
        return _pMachine;
    }

    /// Les differents modes d'edition d'une Machine.
    enum MachineModelerMode
    {
        AddBoxMode = NbOfModelerMode,
        AddCylMode,
        AddSemiCylMode,
        NbOfMachineModelerMode
    };

public slots:
    virtual void setEditorMode(int mode);
    virtual void updatePreferences();
    virtual bool close();

    void calculDistribution();
    virtual void closeEvent(QCloseEvent* pEvent); // CloseEvent is overriden

protected:
    /// Un pointeur sur la machine a editer.
    LPTYMachine _pMachine;

    /// Box editor.
    TYBoxEditor* _pBoxEditor;
    /// Cylinder editor.
    TYCylinderEditor* _pCylinderEditor;
    /// Semi cylinder editor.
    TYSemiCylinderEditor* _pSemiCylinderEditor;

    /// Nombre d'instance de type TYMachineModelerFrame.
    static int _nbInstance;
};

#endif //__TY_MACHINE_MODELER_FRAME__
