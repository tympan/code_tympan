/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYInfraToolbar.h
 * \brief Barre d'outil infastructure (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_INFRA_TOOLBAR__
#define __TY_INFRA_TOOLBAR__

#include <qtoolbar.h>

class QMainWindow;
class QToolButton;
class QButtonGroup;

/**
 * \class TYInfraToolbar
 * \brief Barre d'outil infastructure.
 */
class TYInfraToolbar : public QToolBar
{
    Q_OBJECT

public:
    /**
     * Constructeur par defaut.
     */
    TYInfraToolbar(QButtonGroup* pBtnGroup, QMainWindow* parent = 0, QString title = "");
    /**
     * Destructeur.
     */
    virtual ~TYInfraToolbar();

public slots:
    /**
     * Active/Desactive chaque boutons.
     */
    void activeButtons(bool active);

protected:
    /// Bouton source ponctuelle.
    QToolButton* _toolButtonSrcPonct;
    /// Bouton etage.
    QToolButton* _toolButtonEtage;
    /// Bouton Silo
    QToolButton* _toolButtonSilos;
#if WITH_NMPB
    /// Bouton route.
    QToolButton* _toolButtonRoute;
#endif
    /// Bouton reseau de transport.
    QToolButton* _toolButtonReseau;
};

#endif //__TY_INFRA_TOOLBAR__
