/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYCustomPopupMenu.h
 * \brief Definit un popup menu, necessaire pour maitriser l'ouverture automtique apres un createPopupMenu (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_CUSTOM_POPUP_MENU__
#define __TY_CUSTOM_POPUP_MENU__

#include <qmenu.h>

/**
 * \class TYCustomPopupMenu
 * \brief Definit un popup menu, necessaire pour maitriser l'ouverture automtique apres un createPopupMenu.
 */
class TYCustomPopupMenu : public QMenu
{
    Q_OBJECT

public:
    /**
     * \fn TYCustomPopupMenu(QWidget * parent = 0 )
     * \brief Constructeur.
     */
    TYCustomPopupMenu(QWidget* parent = 0);

    /**
     * \fn TYCustomPopupMenu( const QString & title, QWidget * parent = 0 )
     * \brief Constructeur.
     */
    TYCustomPopupMenu(const QString& title, QWidget* parent = 0);

public slots:
    void onAboutToShow();
    virtual void setVisible(bool visible);
};

#endif // __TY_CUSTOM_POPUP_MENU__
