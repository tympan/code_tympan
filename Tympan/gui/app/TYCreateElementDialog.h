/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYCreateElementDialog.h
 * \brief Boite de dialogue pour la creation d'un nouvel element metier. L'element cree peut etre sauvegarde en BDD ou exporter en XML. (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_CREATE_ELEMENT_DIALOG__
#define __TY_CREATE_ELEMENT_DIALOG__

#include "TYAppDefines.h"
#include <qdialog.h>

#include "Tympan/models/business/TYElement.h"

class QListWidget;

/**
 * \class TYCreateElementDialog
 * \brief Boite de dialogue pour la creation d'un nouvel element metier. L'element cree peut etre sauvegarde en BDD ou exporter en XML.
 */
class TYCreateElementDialog : public QDialog
{
    Q_OBJECT

public:
    /**
     * \fn TYCreateElementDialog(QWidget * parent = 0, const char * name = 0, Qt::WindowFlags f = 0)
     * \brief Constructeur par defaut.
     */
    TYCreateElementDialog(QWidget* parent = 0, const char* name = 0, Qt::WindowFlags f = QFlag(0));
    /**
     * \fn virtual ~TYCreateElementDialog()
     * \brief Destructeur.
     */
    virtual ~TYCreateElementDialog();

public slots:
    /**
     * \fn void createElement(QString eltType)
     * \brief Gere la creation d'un nouvel element au sein de l'application.
     * \param eltType Le type d'element a creer.
     */
    void createElement(QString eltType);

protected slots:
    /**
     * \fn virtual void done(int r)
     * \brief Ce slot est appele lorsqu'on valide un choix de la boite de dialogue.
     */
    virtual void done(int r);

protected:
    QListWidget* _pElementChoiceListBox;
};

#endif //__TY_CREATE_ELEMENT_DIALOG__
