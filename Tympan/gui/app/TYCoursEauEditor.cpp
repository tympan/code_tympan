/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYCoursEauEditor.cpp
 * \brief Construit un CoursEau a partir des points saisis
 */

#include <qdialog.h>

#include "Tympan/models/business/OLocalizator.h"
#include "Tympan/models/business/infrastructure/TYSiteNode.h"
#include "Tympan/models/business/topography/TYCoursEau.h"
#include "Tympan/gui/app/TYModelerFrame.h"
#include "Tympan/gui/app/TYSiteModelerFrame.h"
#include "Tympan/gui/app/TYActions.h"
#include "Tympan/gui/app/TYApplication.h"
#include "Tympan/gui/app/TYMainWindow.h"
#include "TYCoursEauEditor.h"

#define TR(id) OLocalizator::getString("TYCoursEauEditor", (id))

TYCoursEauEditor::TYCoursEauEditor(TYModelerFrame* pModeler) : TYPolyLineEditor(pModeler)
{
    QObject::connect(this, &TYCoursEauEditor::endedSavingPoints, this, &TYCoursEauEditor::endCoursEau);
}

TYCoursEauEditor::~TYCoursEauEditor() {}

void TYCoursEauEditor::endCoursEau()
{
    if (!(getSavedPoints().size() > 1) || (!_pModeler->askForResetResultat()))
    {
        return;
    }

    LPTYCoursEau pCoursEau = new TYCoursEau();
    pCoursEau->setTabPoint(getSavedPoints());

    if (pCoursEau->edit(_pModeler) == QDialog::Accepted)
    {
        TYSiteNode* pSite = ((TYSiteModelerFrame*)_pModeler)->getSite();

        if (pSite->getTopographie()->addCrsEau(pCoursEau))
        {
            TYAction* pAction = new TYAddElementToTopoAction((LPTYElement&)pCoursEau, pSite->getTopographie(),
                                                             _pModeler, TR("id_action_addcrseau"));
            _pModeler->getActionManager()->addAction(pAction);

            pSite->getTopographie()->updateGraphicTree();

            updateSiteFrame();
            _pModeler->getView()->getRenderer()->updateDisplayList();

            _pModeler->updateView();
        }

        // repasse en mode camera selection
        getTYMainWnd()->setDefaultCameraMode();
    }
}
