/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYReseauTransportEditor.h
 * \brief Construit un reseau de transport a partir des points saisis (fichier header)
 *
 *
 *
 */

#ifndef __TY_RESEAU_TRANSPORT_EDITOR__
#define __TY_RESEAU_TRANSPORT_EDITOR__

#include "TYPolyLineEditor.h"
#include "Tympan/models/business/TYDefines.h"

/**
 * \class TYReseauTransportEditor
 * \brief Construit un reseau de transport a partir des points saisis
 */
class TYReseauTransportEditor : public TYPolyLineEditor
{
    Q_OBJECT

public:
    TYReseauTransportEditor(TYModelerFrame* pModeler);
    ~TYReseauTransportEditor();

public slots:
    /**
     * Construit un ReseauTransport a partir des points saisis.
     */
    void endReseauTransport();

protected:
};

#endif // __TY_RESEAU_TRANSPORT_EDITOR__
