/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYOutputFrame.h
 * \brief Frame pour les messages de retour (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_OUTPUT_FRAME__
#define __TY_OUTPUT_FRAME__

#include "TYAppDefines.h"
#include <qwidget.h>

class QTextEdit;

/**
 * \class TYOutputFrame
 * \brief Frame pour les messages de retour
 */
class TYOutputFrame : public QWidget
{
    Q_OBJECT

public:
    /**
     * Constructeur par defaut.
     */
    TYOutputFrame(QWidget* parent = 0, const char* name = 0, Qt::WindowFlags f = QFlag(0));
    /**
     * Destructeur.
     */
    virtual ~TYOutputFrame();

    /**
     * Ajoute un nouveau message dans le fenetre de sortie.
     */
    void appendMsg(QString msg);

protected:
    QTextEdit* _pTextEdit;
};

#endif //__TY_OUTPUT_FRAME__
