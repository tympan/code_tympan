/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYFaceModelerPickEditor.cpp
 * \brief gestion du picking des faces
 */

#include <math.h>
#include <qdialog.h>
#include <qpushbutton.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qmenu.h>
#include <qcursor.h>
#include <qinputdialog.h>
#include <QMessageBox>
// Added by qt3to4:
#include <QHBoxLayout>
#include <QBoxLayout>
#include <QGridLayout>
#include <QPixmap>

#include "Tympan/models/business/OLocalizator.h"
#include "Tympan/models/business/TYMaillage.h"
#include "Tympan/models/business/TYPointControl.h"
#include "Tympan/models/business/infrastructure/TYSiteNode.h"
#include "Tympan/models/business/infrastructure/TYBatiment.h"
#include "Tympan/gui/gl/TYPickHandler.h"
#include "Tympan/gui/widgets/TYWidget.h"
#include "Tympan/gui/app/TYModelerFrame.h"
#include "Tympan/gui/app/TYActions.h"
#include "Tympan/gui/app/TYApplication.h"
#include "Tympan/gui/app/TYMainWindow.h"
#include "Tympan/gui/widgets/TYFormDialog.h"
#include "Tympan/gui/widgets/TYLineEdit.h"
#include "TYFaceModelerPickEditor.h"

#define TR(id) OLocalizator::getString("TYFaceModelerPickEditor", (id))
#define IMG(id) OLocalizator::getPicture("TYPickEditor", (id))

TYFaceModelerPickEditor::TYFaceModelerPickEditor(TYModelerFrame* pModeler) : TYPickEditor(pModeler) {}

TYFaceModelerPickEditor::~TYFaceModelerPickEditor() {}

void TYFaceModelerPickEditor::showPopupMenu(std::shared_ptr<LPTYElementArray> pElts)
{
    if (!pElts)
    {
        return;
    }

    QMenu* pPopup = new QMenu(NULL);
    TYAcousticRectangleNode* pParent = NULL;
    std::map<QAction*, int> retCodes;
    std::map<QAction*, LPTYGeometryNode> posRetCodes;
    std::map<QAction*, TYAcousticRectangle*> dimAccRectRetCodes;
    std::map<QAction*, TYAcousticRectangle*> remAccRectRetCodes;
    QAction* code = NULL;
    unsigned int i = 0;

    while ((i < pElts->size()) && (i + 1 < pElts->size()))
    {
        if (dynamic_cast<TYAcousticRectangle*>(pElts->at(i)._pObj) != nullptr)
        {
            // Proprietes
            QFont font = pPopup->font();
            code = pPopup->addAction(QIcon(QPixmap(IMG("id_icon_editeelt"))),
                                     TYWidget::getDisplayName(pElts->at(i)));
            font.setBold(true);
            code->setFont(font);

            retCodes[code] = i;

            // Dimensions
            if (dynamic_cast<TYAcousticSurfaceNode*>(pElts->at(i + 1)._pObj) != nullptr)
            {
                pParent = (LPTYAcousticRectangleNode&)pElts->at(i + 1);
                code = pPopup->addAction(TR("id_popup_dimension"));
                dimAccRectRetCodes[code] = (LPTYAcousticRectangle&)pElts->at(i);
            }

            // Position via le parent (GeoNode)
            if (dynamic_cast<TYAcousticSurfaceNode*>(pElts->at(i + 1)._pObj) != nullptr)
            {
                pParent = (LPTYAcousticRectangleNode&)pElts->at(i + 1);
                code = pPopup->addAction(QIcon(IMG("id_icon_moving")), TR("id_popup_position"));
                posRetCodes[code] =
                    (TYGeometryNode*)pParent->findAcousticSurf((LPTYAcousticSurface&)pElts->at(i));
            }

            // Suppression
            code = pPopup->addAction(QIcon(QPixmap(IMG("id_icon_del"))), TR("id_popup_remove"));
            remAccRectRetCodes[code] = (LPTYAcousticRectangle&)pElts->at(i);

            pPopup->addSeparator();
        }
        else if (dynamic_cast<TYAcousticRectangleNode*>(pElts->at(i)._pObj) != nullptr)
        {
            // Face parent
            code = pPopup->addAction(QIcon(QPixmap(IMG("id_icon_editeelt"))),
                                     TYWidget::getDisplayName(pElts->at(i)));
            retCodes[code] = i;

            // On a la sous-face et son parent
            break;
        }

        // Increment
        i++;
    }

    QAction* popupRet = pPopup->exec(QCursor::pos());

    if (retCodes.find(popupRet) != retCodes.end())
    {
        pElts->at(retCodes[popupRet])->edit(_pModeler);
    }
    else if (dimAccRectRetCodes.find(popupRet) != dimAccRectRetCodes.end())
    {
        showDimensionsDialog(dimAccRectRetCodes[popupRet]->getShape(), pParent);
    }
    else if (posRetCodes.find(popupRet) != posRetCodes.end())
    {
        showPositionDialog(posRetCodes[popupRet].getRealPointer(), false);
        if (pParent)
        {
            pParent->updateGrid();
        }
    }
    else if (remAccRectRetCodes.find(popupRet) != remAccRectRetCodes.end())
    {
        if (_pModeler->askForResetResultat())
        {
            LPTYAcousticSurfaceGeoNode pGeoNode = pParent->findAcousticSurf(remAccRectRetCodes[popupRet]);

            pParent->remAcousticSurf(remAccRectRetCodes[popupRet]);
            pParent->setIsGeometryModified(true);
            pParent->updateGraphicTree();

            TYAction* pAction =
                new TYRemAccSurfToAccSurfNodeAction(pGeoNode, pParent, _pModeler, TR("id_action_remface"));
            _pModeler->getActionManager()->addAction(pAction);
        }
    }

    // Deselection
    resetPicker();

    // Update
    getTYMainWnd()->updateModelers(false, false);

    delete pPopup;
}

void TYFaceModelerPickEditor::showDimensionsDialog(TYRectangle* pRect,
                                                   TYAcousticRectangleNode* pParent /*=NULL*/)
{
    if (!pRect)
    {
        return;
    }

    TYFormDialog* pDlg = new TYFormDialog(_pModeler);
    pDlg->setWindowTitle(TR("id_popup_dimension"));

    QGridLayout* pLayout = new QGridLayout();
    pDlg->setLayout(pLayout);

    QBoxLayout* pEditLayout = new QHBoxLayout();
    pEditLayout->setContentsMargins(10, 10, 10, 10);
    pLayout->addLayout(pEditLayout, 0, 1);

    TYLineEdit* pXLineEdit = NULL;
    TYLineEdit* pYLineEdit = NULL;

    float sizeX = NAN, sizeY = NAN;
    pRect->getSize(sizeX, sizeY);

    // Size X
    QLabel* pXLabelName = new QLabel(pDlg);
    pXLabelName->setText(TR("id_x_label"));
    pEditLayout->addWidget(pXLabelName);
    pXLineEdit = new TYLineEdit(pDlg);
    pXLineEdit->setFixedWidth(60);
    pXLineEdit->setText(QString().setNum(sizeX, 'f', 2));
    pEditLayout->addWidget(pXLineEdit);

    // Size Y
    pEditLayout->addSpacing(10);
    QLabel* pYLabelName = new QLabel(pDlg);
    pYLabelName->setText(TR("id_y_label"));
    pEditLayout->addWidget(pYLabelName);
    pYLineEdit = new TYLineEdit(pDlg);
    pYLineEdit->setFixedWidth(60);
    pYLineEdit->setText(QString().setNum(sizeY, 'f', 2));
    pEditLayout->addWidget(pYLineEdit);

    QBoxLayout* pBtnLayout = new QHBoxLayout();
    pLayout->addLayout(pBtnLayout, 1, 1);

    pBtnLayout->addStretch(1);

    QPushButton* pButtonOK = new QPushButton(TR("id_ok_btn"), pDlg);
    pButtonOK->setDefault(true);
    QObject::connect(pButtonOK, &QPushButton::clicked, pDlg, &QDialog::accept);
    pBtnLayout->addWidget(pButtonOK);

    QPushButton* pButtonCancel = new QPushButton(TR("id_cancel_btn"), pDlg);
    pButtonCancel->setShortcut(Qt::Key_Escape);
    QObject::connect(pButtonCancel, &QPushButton::clicked, pDlg, &QDialog::reject);
    pBtnLayout->addWidget(pButtonCancel);

    // Affiche la boite de dialogue
    int ret = pDlg->exec();

    // Applique les modificatins si necessaire
    if (ret == QDialog::Accepted)
    {
        if (_pModeler->askForResetResultat())
        {
            float sizeX = pXLineEdit->text().toDouble();
            float sizeY = pYLineEdit->text().toDouble();

            if (sizeX <= 1E-4 || sizeY <= 1.E-4) // Eviter les surfaces nulles
            {
                QMessageBox::warning(_pModeler, "Tympan", TR("id_warning_size_not_ok"),
                                     QMessageBox::Yes); //, QMessageBox::No);
                return;
            }

            TYAction* pAction =
                new TYResizeRectAction(pRect, sizeX, sizeY, _pModeler, TR("id_action_resizeface"));
            _pModeler->getActionManager()->addAction(pAction);

            pRect->setSize(sizeX, sizeY);

            if (pParent)
            {
                pParent->updateGrid();
            }

            // Refresh
            pRect->updateGraphicTree();
        }
    }

    delete pBtnLayout;
    delete pEditLayout;
}
