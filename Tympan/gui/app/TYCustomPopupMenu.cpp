/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYCustomPopupMenu.cpp
 * \brief Definit un popup menu, necessaire pour maitriser l'ouverture automtique apres un createPopupMenu
 */

#include "Tympan/gui/app/TYApplication.h"
#include "Tympan/gui/app/TYMainWindow.h"
#include "TYCustomPopupMenu.h"

#include <QMdiArea>

TYCustomPopupMenu::TYCustomPopupMenu(QWidget* parent) : QMenu(parent)
{
    QObject::connect(this, &QMenu::aboutToShow, this, &TYCustomPopupMenu::onAboutToShow);
}

TYCustomPopupMenu::TYCustomPopupMenu(const QString& title, QWidget* parent) : QMenu(title, parent)
{
    QObject::connect(this, &QMenu::aboutToShow, this, &TYCustomPopupMenu::onAboutToShow);
}

void TYCustomPopupMenu::onAboutToShow()
{
    setVisible(true);
}

void TYCustomPopupMenu::setVisible(bool visible)
{
    QPoint point = pos();
    QPoint point2;

    if (visible)
    {
        int w = getTYMainWnd()->getWorkspace()->width();
        int h = getTYMainWnd()->getWorkspace()->height();
        point2 = getTYMainWnd()->getWorkspace()->mapFromGlobal(point);
        if ((point2.x() >= 0) && (point2.y() >= 0) && (point2.x() <= w) && (point2.y() <= h))
        {
            return;
        }
        QMenu::setVisible(visible);
    }
    else
    {
        QMenu::setVisible(visible);
    }
}
