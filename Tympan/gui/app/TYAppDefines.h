/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TYAPPDEFINES__
#define __TYAPPDEFINES__

class OSplashScreen;
class TYAbstractSceneEditor;
class TYAction;
class TYActionManager;
class TYActions;
class TYActorEditor;
class TYAddLibraryDialog;
class TYApplication;
class TYBatimentModelerFrame;
class TYBatimentToolbar;
class TYBoxEditor;
class TYCalculManager;
class TYCalculToolbar;
class TYCameraEditor;
class TYCameraZoneEditor;
class TYConsole;
class TYCourbeNiveauEditor;
class TYCoursEauEditor;
class TYCreateElementDialog;
class TYCylinderEditor;
class TYDistanceEditor;
class TYEchelleEditor;
class TYElementCheckListItem;
class TYElementListItem;
class TYElementPicker;
class TYEmpriseEditor;
class TYEtageEditor;
class TYFaceModelerFrame;
class TYFaceModelerPickEditor;
class TYFaceToolbar;
class TYGetLibraryDialog;
class TYInfraToolbar;
class TYLibraryWidget;
class TYLinearMaillageEditor;
class TYMachineModelerFrame;
class TYMachineToolbar;
class TYMainWindow;
class TYMenuItem;
class TYMessageManager;
class TYModelerFrame;
class TYModelerToolbar;
class TYOpenElementDialog;
class TYOpenGLRenderer;
class TYOrientationEditor;
class TYOutputFrame;
class TYPickEditor;
class TYPlanEauEditor;
class TYPluginDialog;
class TYPointCalculEditor;
class TYPointRefEditor;
class TYPolyLineEditor;
class TYPositionDialog;
class TYPositionEditor;
class TYPreferenceDialog;
class TYProjetFrame;
class TYRectangleEditor;
class TYRectangularMaillageEditor;
class TYRenderWindow;
class TYRenderWindowInteractor;
class TYReseauTransportEditor;
class TYRotationDialog;
#if WITH_NMPB
class TYRouteEditor;
#endif
class TYSemiCylinderEditor;
class TYSilosEditor;
class TYSiteFrame;
class TYSiteModelerFrame;
class TYSourceEditor;
class TYTerrainEditor;
class TYTopoToolbar;
class TYVerticalMaillageEditor;
class TYBoundaryNoiseMapEditor;

#endif // __TYAPPDEFINES__
