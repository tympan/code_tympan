/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYDimensionDialog.h
 * \brief boite de dialogue pour la gestion des dimensions des volumes (fichier header)
 * \author Projet_Tympan
 *
 */

#ifndef __TY_DIMENSION_DIALOG__
#define __TY_DIMENSION_DIALOG__

#include "TYAppDefines.h"
#include "Tympan/gui/widgets/TYFormDialog.h"
#include "Tympan/gui/widgets/TYLineEdit.h"

#include <qdialog.h>
#include <qlayout.h>
// Added by qt3to4:
#include <QGridLayout>
#include <QLabel>

class OPoint3D;
class QGridLayout;
class QLabel;
class QGroupBox;
class TYElementWidget;
class TYAcousticVolume;

/**
 * \class TYDimensionDialog
 * \brief boite de dialogue pour la gestion des dimensions des volumes
 */
class TYDimensionDialog : public TYFormDialog
{
    Q_OBJECT

    // Methodes
public:
    /**
     * Constructeur.
     */
    TYDimensionDialog(TYAcousticVolume* pElement, QWidget* _pParent = NULL);
    /**
     * Destructeur.
     */
    virtual ~TYDimensionDialog();

public slots:
    virtual void updateContent();
    virtual void apply();

    // Membres
protected:
    TYLineEdit* _pXLineEdit;
    TYLineEdit* _pYLineEdit;
    TYLineEdit* _pZLineEdit;
    TYLineEdit* _pDiamLineEdit;
    TYLineEdit* _pHauteurLineEdit;

    TYAcousticVolume* _pElement;
};

#endif // __TY_DIMENSION_DIALOG__
