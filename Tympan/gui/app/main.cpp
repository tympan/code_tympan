/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file main.cpp
 * \brief fichier principal de lancement de l'application Tympan en mode IHM
 */

#include <qmessagebox.h>
#include <QDir>
#include <QtWebView>

#include "Tympan/core/logging.h"
#include "Tympan/models/business/init_registry.h"
#include "TYApplication.h"

static QtMessageHandler old_handler;

static void MyQTMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& message)
{
    if (old_handler != NULL)
    {
        old_handler(type, context, message);
    }

    switch (type)
    {
        case QtInfoMsg:
            break;
        case QtDebugMsg:
            break;
        case QtWarningMsg:
            break;
        case QtCriticalMsg:
        case QtFatalMsg:
            int selected = 0;

            selected = QMessageBox::critical(NULL, "Attention !", message, "Debug", "Continue", "Quit");

            if (selected == 0)
            {
                // Debug
#if defined(WIN32)
                _CrtDbgBreak();
#endif
            }
            else if (selected == 1)
            {
                // Continue
            }
            else if (selected == 2)
            {
                // Quit
                exit(1);
            }
            break;
    }
}

static int tyMain(int argc, char** argv)
{
    bool success = false;

    old_handler = qInstallMessageHandler(MyQTMessageHandler);

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    TYApplication tyApp(argc, argv);
    QtWebView::initialize();
    if (tyApp.init())
    {
        // Lance la boucle principal
        success = tyApp.run();

        // Termine correctement l'appli
        tyApp.close();
    }
    OMessageManager::get()->debug("Counters : TYElements created %u, deleted %u and ID generated %u",
                                  TYElement::getConstructorCount(), TYElement::getDestructorCount(),
                                  TYElement::getIdGenerationCount());
    qInstallMessageHandler(old_handler);

    // return code
    int ret = 0;
    success ? ret = 0 : ret = 1;

    return ret;
}

bool setenv(const char* pVarEnvName, QString& pVarEnvValue, bool pForceUpdate)
{
    bool ret = true;
    QString currentVarEnvValue = QString::fromLocal8Bit(qgetenv(pVarEnvName));
    if (currentVarEnvValue != "" && !pForceUpdate)
    {
        OMessageManager::get()->info("Environment variable %s already set to : '%s'.\n", pVarEnvName,
                                     currentVarEnvValue.toUtf8().data());
        pVarEnvValue = currentVarEnvValue;
    }
    else
    {
        OMessageManager::get()->info("Trying to set environment variable %s to : '%s'.\n", pVarEnvName,
                                     pVarEnvValue.toUtf8().data());
        ret = qputenv(pVarEnvName, pVarEnvValue.toUtf8());
    }
    return ret;
}

bool setenv()
{
    bool ret = true;
    const QChar SEP = QDir::listSeparator();
#ifdef _DEBUG
    const char* PLUGINS = "/pluginsd";
    const char* CYTHON = "/cython_d";
#else
    const char* PLUGINS = "/plugins";
    const char* CYTHON = "/cython";
#endif

#if defined(WIN32)
    const char* PYTHON_DIR_NAME = "/Python310";
    const char* PYTHON_EXECUTABLE = "/python.exe";
    const char* PYTHON_LIBS = "/Lib";
    const char* PYTHON_SCRIPTS = "/Scripts";
#else
    const char* PYTHON_DIR_NAME = "Python3";
    const char* PYTHON_EXECUTABLE = "/bin/python";
    const char* PYTHON_LIBS = "/lib";
    const char* PYTHON_SCRIPTS = "/bin";
#endif

    const QString TYMPAN_INSTALL_DIR = QDir::toNativeSeparators(QDir::currentPath());
    OMessageManager::get()->info("TYMPAN_INSTALL_DIR is '%s'.\n", TYMPAN_INSTALL_DIR.toUtf8().data());

    ret |= qputenv("PYTHONIOENCODING", "UTF8");

    QString TYMPAN_INSTALL_PATH = QString(TYMPAN_INSTALL_DIR);
    ret |= setenv("TYMPAN_INSTALL_PATH", TYMPAN_INSTALL_PATH, false);

    QString PYTHONTYMPAN = QDir::toNativeSeparators(TYMPAN_INSTALL_DIR + QString(PYTHON_DIR_NAME));
    ret |= setenv("PYTHONTYMPAN", PYTHONTYMPAN, false);
    OMessageManager::get()->info("PYTHONTYMPAN is '%s'.\n", PYTHONTYMPAN.toUtf8().data());

    QString TYMPAN_SOLVERDIR = QDir::toNativeSeparators(TYMPAN_INSTALL_DIR + QString(PLUGINS));
    ret |= setenv("TYMPAN_SOLVERDIR", TYMPAN_SOLVERDIR, false);

    QString CGAL_BINDINGS_PATH = QDir::toNativeSeparators(TYMPAN_INSTALL_DIR + CYTHON + QString("/CGAL"));
    ret |= setenv("CGAL_BINDINGS_PATH", CGAL_BINDINGS_PATH, false);

    QString TYMPAN_PYTHON_INTERP = QDir::toNativeSeparators(PYTHONTYMPAN + QString(PYTHON_EXECUTABLE));
    ret |= setenv("TYMPAN_PYTHON_INTERP", TYMPAN_PYTHON_INTERP, false);

    QString PYTHONPATH = QDir::toNativeSeparators(TYMPAN_INSTALL_DIR + CYTHON) + SEP + PYTHONTYMPAN + SEP +
                         QDir::toNativeSeparators(PYTHONTYMPAN + "/DLLs") + SEP +
                         QDir::toNativeSeparators(PYTHONTYMPAN + PYTHON_LIBS) + SEP +
                         QDir::toNativeSeparators(PYTHONTYMPAN + PYTHON_LIBS + "/site-packages") + SEP +
                         TYMPAN_INSTALL_DIR;

    ret |= setenv("PYTHONPATH", PYTHONPATH, false);

    QString PATH = TYMPAN_INSTALL_DIR + SEP + PYTHONTYMPAN + SEP +
                   QDir::toNativeSeparators(PYTHONTYMPAN + "/DLLs") + SEP +
                   QDir::toNativeSeparators(PYTHONTYMPAN + PYTHON_LIBS) + SEP +
                   QDir::toNativeSeparators(PYTHONTYMPAN + PYTHON_SCRIPTS);
    ret |= setenv("PATH", PATH, true);

    QString LD_LIBRARY_PATH = QDir::toNativeSeparators(TYMPAN_INSTALL_DIR + PYTHON_LIBS) + SEP +
                              QDir::toNativeSeparators(TYMPAN_INSTALL_DIR + CYTHON) + SEP +
                              QDir::toNativeSeparators(TYMPAN_INSTALL_DIR + CYTHON + QString("/CGAL"));
    ret |= setenv("LD_LIBRARY_PATH", LD_LIBRARY_PATH, false);

    QString PYTHONLEGACYWINDOWSSTDIO = QString("1");
    ret |= setenv("PYTHONLEGACYWINDOWSSTDIO", PYTHONLEGACYWINDOWSSTDIO, false);

    return ret;
}

int main(int argc, char** argv)
{
    int ret = 0;
    bool ret_setenv = true;
    ret_setenv = setenv();
    if (!ret_setenv)
    {
        OMessageManager::get()->info("Unable to set environment variables, closing application.");
        return ret;
    }
    // Register TY* classes before starting the application
    tympan::init_registry();
    // Appel le main de Tympan
    ret = tyMain(argc, argv);
    return ret;
}
