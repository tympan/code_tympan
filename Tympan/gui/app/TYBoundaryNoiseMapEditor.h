/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYBoundaryNoiseMapEditor.h
 * \brief Creation of a \c TYBoundaryNoiseMap (header file)
 *
 *
 *
 *
 *
 */

#ifndef __TY_BOUNDARY_NOISE_MAP_EDITOR__
#define __TY_BOUNDARY_NOISE_MAP_EDITOR__

#include <qdialog.h>
#include <qlineedit.h>
#include <qradiobutton.h>
// Added by qt3to4:
#include <QBoxLayout>
#include <QLabel>

#include "TYPolyLineEditor.h"
#include "Tympan/models/business/TYDefines.h"
#include "Tympan/gui/widgets/TYDoubleSpinBox.h"

class TabPointsWidget;
/**
 * \class TYBoundaryNoiseMapEditor
 * \brief Creation of a \c TYBoundaryNoiseMap
 */
class TYBoundaryNoiseMapEditor : public TYPolyLineEditor
{
    Q_OBJECT

public:
    TYBoundaryNoiseMapEditor(TYSiteModelerFrame* pModeler);
    ~TYBoundaryNoiseMapEditor();

public slots:
    /**
     * \brief Build a BoundaryNoiseMap from an array of points.
     */
    void endBoundaryNoiseMap();

    /**
     * \brief Update the minimum value of the density thanks to the new thickness value.
     * \param thickness The value of thickness to be tested
     */
    void updateMinimumDensity(double thickness);

    /**
     * \brief Return true if the polyline is valid and can be built. It also tells if the polyline should be opened.
     * \param forceOpened If the user will have the choice between a closed/opened polyline.
     * \return True if the polyline can be created.
     */
    bool checkValidity(bool& forceOpened);
    /**
     * \brief Create the properties dialog that will pop up once the user finishes the polyline creation.
     */
    void createPropertiesDlg(bool forceOpened);

    /**
     * \brief When the user confirms the boundary noise map creation after the properties dialog pop up.
     */
    void dialogConfirmed(double height, double thickness, bool closed, double distance, bool forceOpened);

    virtual void slotMousePressed(int x, int y, Qt::MouseButton button, Qt::KeyboardModifiers state);

protected:
    /// Density spin box.
    TYDoubleSpinBox* _pDistanceSpinBox;

    /// Thickness spin box.
    TYDoubleSpinBox* _pThicknessSpinBox;

    /// Geometry description
    TabPointsWidget* _tabPtsW;
};

#endif // __TY_BOUNDARY_NOISE_MAP_EDITOR__
