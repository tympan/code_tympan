/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file Tympan/gui/app/os.cpp
 * \brief Utilitaires pour les interactions entre l'interface graphique et le système
 */

#include <QCoreApplication>
#include <qmessagebox.h>

#include "Tympan/models/business/subprocess_util.h"
#include "Tympan/gui/app/TYApplication.h"
#include "Tympan/core/logging.h"
#include "os.h"

bool python_gui(QStringList args)
{
    // Disable GUI
    getTYMainWnd()->setEnabled(false);
    TYApplication::setOverrideCursor(QCursor(Qt::WaitCursor));

    std::string error_msg;
    bool computation_ok = python(args, error_msg);
    if (!computation_ok)
    {
        OMessageManager& logger = *OMessageManager::get();
        logger.error("Echec du calcul acoustique: %s", error_msg.c_str());
        QMessageBox msgBox;
        msgBox.setText("Echec du calcul acoustique. Veuillez consulter le journal d'erreurs.\n");
        msgBox.exec();
    }
    // Enable GUI
    TYApplication::restoreOverrideCursor();
    getTYMainWnd()->setEnabled(true);
    return computation_ok;
}
