/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYMachineToolbar.h
 * \brief Barre d'outils Machine (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_MACHINE_TOOLBAR__
#define __TY_MACHINE_TOOLBAR__

#include <qtoolbar.h>

class QMainWindow;
class QToolButton;
class QButtonGroup;

/**
 * \class TYMachineToolbar
 * \brief Barre d'outils Machine
 */
class TYMachineToolbar : public QToolBar
{
    Q_OBJECT

public:
    /**
     * Constructeur par defaut.
     */
    TYMachineToolbar(QButtonGroup* pBtnGroup, QMainWindow* parent = 0, QString title = "");
    /**
     * Destructeur.
     */
    virtual ~TYMachineToolbar();

public slots:
    /**
     * Active/Desactive chaque boutons.
     */
    void activeButtons(bool active);

protected:
    /// Bouton box.
    QToolButton* _toolButtonBox;
    /// Bouton cylindre.
    QToolButton* _toolButtonCyl;
    /// Bouton demi cylindre.
    QToolButton* _toolButtonSemiCyl;
};

#endif //__TY_MACHINE_TOOLBAR__
