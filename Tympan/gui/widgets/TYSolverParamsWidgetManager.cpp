/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYSolverParamsWidgetManager.cpp
 * \brief Objet permettant de gerer les différents widgets servant a parametriser le solveur
 * \author Projet_Tympan
 *
 */

#include <QCoreApplication>
#include <QDir>
#include <QJsonParseError>
#include <QJsonDocument>

#include "Tympan/core/config.h"
#include "Tympan/core/exceptions.h"
#include "Tympan/models/business/OLocalizator.h"

#include "TYSolverParamsWidgetManager.h"
#include <iostream>
#define TR(id) OLocalizator::getString("TYSolverParamsWidgetManager", (id))

TYSolverParamsWidgetManager::TYSolverParamsWidgetManager()
{
    QString test(QCoreApplication::applicationDirPath() + "/" + SOLVER_PARAMS_JSON);
    readDataModel(test);
}

TYSolverParamsWidgetManager::TYSolverParamsWidgetManager(QString dataModelPath)
{
    readDataModel(dataModelPath);
}

void TYSolverParamsWidgetManager::readDataModel(QString dataModelPath)
{

    // Lecture du modele de donnees des parametres du solveur
    dataModelPath = QDir::toNativeSeparators(dataModelPath);

    QFile file(dataModelPath);
    bool opened = file.open(QIODevice::ReadOnly | QIODevice::Text);

    if (!opened)
        throw tympan::invalid_data(TR("id_invalid_datamodel_filepath").arg(dataModelPath).toStdString())
            << tympan_source_loc;

    QJsonParseError JsonParseError;
    QJsonObject dataModel = QJsonDocument::fromJson(file.readAll(), &JsonParseError).object();
    file.close();

    // Map<nom_parametre, data_model>
    for (QString sectionName : dataModel.keys())
    {
        QJsonObject section = dataModel[sectionName].toObject();
        for (QString paramName : section.keys())
        {
            _dataModelMap.insert(
                paramName, TYSolverParamsDataModel::fromJsonObject(paramName, section[paramName].toObject()));
        }
    }
}

TYSolverParamsDataModel* TYSolverParamsWidgetManager::getDataModel(QString paramName)
{
    if (!_dataModelMap.contains(paramName))
        throw tympan::invalid_data((paramName).toStdString()) << tympan_source_loc;
    return _dataModelMap[paramName];
}

TYSolverParamsWidget* TYSolverParamsWidgetManager::getWidget(QString paramName)
{
    if (!_solverParamsWidgets.contains(paramName))
        throw tympan::invalid_data((paramName).toStdString()) << tympan_source_loc;
    return _solverParamsWidgets[paramName];
}

TYSolverParamsWidget* TYSolverParamsWidgetManager::makeWidget(QString paramName)
{
    TYSolverParamsDataModel* dataModel = this->getDataModel(paramName);
    return makeWidget(dataModel);
}

TYSolverParamsWidget* TYSolverParamsWidgetManager::makeWidget(TYSolverParamsDataModel* dataModel)
{

    // creation d'un widget en fonction du modele de donnees
    TYSolverParamsWidget* widget = nullptr;
    if (dataModel->isLabeledValues())
    {
        widget = new TYSolverParamsRadioButtonsWidget(dataModel);
    }
    else if (dataModel->isBool())
    {
        widget = new TYSolverParamsCheckBoxWidget(dataModel);
    }
    else
    {
        widget = new TYSolverParamsInputValueWidget(dataModel);
    }

    // enregister le widget
    registerWidget(widget);

    return widget;
}

void TYSolverParamsWidgetManager::registerWidget(TYSolverParamsWidget* widget)
{

    // enregistrement du widget et connection de la valeur de son parametre au gestionnaire de widgets
    _solverParamsWidgets.insert(widget->dataModel->paramName, widget);
    QObject::connect(widget, &TYSolverParamsWidget::valueChanged, this,
                     &TYSolverParamsWidgetManager::widgetEdited);
}

void TYSolverParamsWidgetManager::updateWidgets(QString newParamValues)
{

    _solverParams = newParamValues;

    // separations de la chaine de characteres en lignes
    QStringList lines = _solverParams.split(QRegExp("[\r\n]"), Qt::SkipEmptyParts);
    for (QString line : lines)
    {

        // recuperation du nom et de la valeur du parametre
        QStringList splitted_line = line.split(QRegExp("\\s?=\\s?"));
        if (splitted_line.size() == 2)
        {
            QString paramName = splitted_line[0];
            QString value = splitted_line[1];
            if (_solverParamsWidgets.contains(paramName))
            {

                // on remplace des True/False par des nombres
                if (value == "True")
                    value = "1";
                if (value == "False")
                    value = "0";

                // mise a jours de la valeur du widget
                getWidget(paramName)->setValue(value);
            }
        }
    }
}

void TYSolverParamsWidgetManager::widgetEdited(QString newValue)
{
    const TYSolverParamsWidget* editedWidget = (TYSolverParamsWidget*)QObject::sender();
    QString paramName = editedWidget->dataModel->paramName;
    newValue = newValue.replace(',', '.');
    // on remplace les nombres utilises pour representer les booleens par True/False
    if (editedWidget->dataModel->isBool())
    {
        newValue = (newValue == "1" ? "True" : "False");
    }
    const QRegExp paramRegex{QRegExp::escape(paramName) + "\\s?=\\s?([[0-9]*\\.?[0-9]*|(True|False))"};
    _solverParams = _solverParams.replace(paramRegex, paramName + "=" + newValue);
}

void TYSolverParamsWidgetManager::forceSyncParams()
{
    // on force l'emission du signal valueChanged pour chaque widget enregistre
    // pour s'assurer que 'IHM est bien à jours
    for (QString paramName : _solverParamsWidgets.keys())
    {
        TYSolverParamsWidget* widget = _solverParamsWidgets[paramName];
        emit widget->valueChanged(widget->value());
    }
}
