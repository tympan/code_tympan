/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYSolverParamsWidget.cpp
 * \brief Widgets permettant de controler les parametres du solveur
 * \author Projet_Tympan
 *
 */

#include <QRadioButton>
#include <QHBoxLayout>
#include <QSpacerItem>
#include <QSizePolicy>
#include <QIntValidator>
#include <QDoubleValidator>
#include <QFontMetrics>

#include "Tympan/models/business/OLocalizator.h"
#include "TYSolverParamsWidget.h"

#define TR(id) OLocalizator::getString("TYSolverParamsWidget", (id))

// using namespace std;

TYSolverParamsDataModel::TYSolverParamsDataModel(QString paramName, QString type, QStringList valuesLabel,
                                                 QString defaultValue)
{

    this->paramName = paramName;
    this->type = type;
    this->valueLabels = valuesLabel;
    this->defaultValue = defaultValue;
}

TYSolverParamsDataModel* TYSolverParamsDataModel::fromJsonObject(QString paramName, QJsonObject dataModelJson)
{
    QString defaultValue = dataModelJson["default"].toVariant().toString();
    if (dataModelJson["type"] == "bool")
        if (defaultValue == "true")
            defaultValue = "1";
        else if (defaultValue == "false")
            defaultValue = "0";

    return new TYSolverParamsDataModel(paramName, dataModelJson["type"].toString(),
                                       dataModelJson["labels"].toVariant().toStringList(), defaultValue);
}

TYSolverParamsWidget::TYSolverParamsWidget(TYSolverParamsDataModel* dataModel)
{
    this->dataModel = dataModel;
    this->setValue(dataModel->defaultValue);
    this->setToolTip(TR(dataModel->paramName + "_help"));
}

void TYSolverParamsWidget::setValue(QString value)
{
    if (m_value != value)
    {
        m_value = value;
        emit valueChanged(value);
    }
}

void TYSolverParamsWidget::setValue(int value)
{
    setValue(QString::number(value));
}

void TYSolverParamsWidget::setValue(bool value)
{
    setValue(int(value));
}

TYSolverParamsInputValueWidget::TYSolverParamsInputValueWidget(TYSolverParamsDataModel* dataModel)
    : TYSolverParamsWidget(dataModel)
{
    this->setLayout(new QHBoxLayout(this));
    this->layout()->setContentsMargins(-1, 0, -1, 0);

    label = new QLabel(this);
    label->setText(TR(dataModel->paramName));
    // definition de la longueur du label en nombre de characteres 'M'
    label->setMinimumWidth(QFontMetrics(label->font()).horizontalAdvance("M") * 15);
    label->setMaximumWidth(QFontMetrics(label->font()).horizontalAdvance("M") * 15);
    this->layout()->addWidget(label);

    lineEdit = new QLineEdit(this);
    // definition de la longueur du lineEdit en nombre de characteres 'M'
    lineEdit->setMinimumWidth(QFontMetrics(lineEdit->font()).horizontalAdvance("M") * 10);
    lineEdit->setMaximumWidth(QFontMetrics(lineEdit->font()).horizontalAdvance("M") * 10);

    // validation des entrees en fonction du type de donnees
    if (dataModel->isInt())
    {
        lineEdit->setValidator(new QIntValidator());
    }
    else
    {
        // nombres decimals avec separateur '.' ou ','
        lineEdit->setValidator(new QRegExpValidator(QRegExp("[0-9]*[\\.,][0-9]*")));
    }

    this->layout()->addWidget(lineEdit);
    this->layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum));

    // connection du widget a la valeur du parametre
    auto setValueStr = static_cast<void (TYSolverParamsWidget::*)(QString)>(&TYSolverParamsWidget::setValue);
    QObject::connect(lineEdit, &QLineEdit::textChanged, this, setValueStr);
    QObject::connect(this, &TYSolverParamsWidget::valueChanged, lineEdit, &QLineEdit::setText);
}

TYSolverParamsRadioButtonsWidget::TYSolverParamsRadioButtonsWidget(TYSolverParamsDataModel* dataModel)
    : TYSolverParamsWidget(dataModel)
{

    // on met un buttonGroup dans une groupBox
    groupBox = new QGroupBox(this);
    groupBox->setLayout(new QHBoxLayout(groupBox));
    groupBox->setTitle(TR(dataModel->paramName));
    buttonGroup = new QButtonGroup(groupBox);

    // ajout d'un bouton radio pour chaque valeur possible
    int id = 0;
    for (QString label : dataModel->valueLabels)
    {
        QRadioButton* radioButton = new QRadioButton(groupBox);
        radioButton->setText(TR(dataModel->paramName + "_" + label));
        buttonGroup->addButton(radioButton, id);
        id++;
        groupBox->layout()->addWidget(radioButton);
    }
    groupBox->layout()->addItem(new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum));

    // on place la groupBox dans le layout du widget principal
    this->setLayout(new QHBoxLayout(this));
    this->layout()->addWidget(groupBox);

    // connection du widget a la valeur du parametre
    auto buttonClickedInt = static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::idClicked);
    auto setValueInt = static_cast<void (TYSolverParamsWidget::*)(int)>(&TYSolverParamsWidget::setValue);
    QObject::connect(buttonGroup, buttonClickedInt, this, setValueInt);

    auto toggle_button = [this](QString value) { buttonGroup->button(value.toInt())->setChecked(true); };
    QObject::connect(this, &TYSolverParamsWidget::valueChanged, buttonGroup, toggle_button);
}

TYSolverParamsCheckBoxWidget::TYSolverParamsCheckBoxWidget(TYSolverParamsDataModel* dataModel)
    : TYSolverParamsWidget(dataModel)
{
    this->setLayout(new QHBoxLayout(this));
    this->layout()->setContentsMargins(-1, 0, -1, 0);

    checkBox = new QCheckBox(this);
    checkBox->setText(TR(dataModel->paramName));
    this->layout()->addWidget(checkBox);
    checkBox->setChecked(false);

    // connection du widget a la valeur du parametre
    auto setValueBool = static_cast<void (TYSolverParamsWidget::*)(bool)>(&TYSolverParamsWidget::setValue);
    QObject::connect(checkBox, &QCheckBox::toggled, this, setValueBool);

    auto checkTheBox = [this](QString value) { checkBox->setChecked(value == "1"); };
    QObject::connect(this, &TYSolverParamsWidget::valueChanged, checkBox, checkTheBox);
}
