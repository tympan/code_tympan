/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYBoundaryNoiseMapWidget.cpp
 * \brief GUI tool for a TYBoundaryNoiseMap
 */

#include <limits>

// Added by qt3to4:
#include <QGridLayout>
#include <QLabel>

#include "Tympan/models/business/OLocalizator.h"
#include "Tympan/models/business/TYBoundaryNoiseMap.h"
#include "Tympan/models/business/TYProjet.h"
#include "Tympan/gui/widgets/TYMaillageWidget.h"
#include "TYBoundaryNoiseMapWidget.h"

#define TR(id) OLocalizator::getString("TYBoundaryNoiseMapWidget", (id))

#undef min
#undef max

TYBoundaryNoiseMapWidget::TYBoundaryNoiseMapWidget(TYBoundaryNoiseMap* pElement, QWidget* _pParent /*=NULL*/)
    : TYWidget(pElement, _pParent)
{

    _maillageW = new TYMaillageWidget(pElement, this);

    resize(300, 200);
    setWindowTitle(TR("id_caption"));
    QGridLayout* pBoundaryNoiseMapLayout = new QGridLayout();
    setLayout(pBoundaryNoiseMapLayout);

    pBoundaryNoiseMapLayout->addWidget(_maillageW, 0, 0);

    QGroupBox* pGroupBox = new QGroupBox(this);
    QGridLayout* pGroupBoxLayout = new QGridLayout();
    pGroupBox->setLayout(pGroupBoxLayout);

    // Thickness
    _pThicknessSpinBox = new TYDoubleSpinBox(pGroupBox);
    _pThicknessSpinBox->setRange(0.1, std::numeric_limits<double>::max());
    _pThicknessSpinBox->setCorrectionMode(QAbstractSpinBox::CorrectToNearestValue);
    pGroupBoxLayout->addWidget(new QLabel(TR("id_thickness"), pGroupBox), 0, 0);
    pGroupBoxLayout->addWidget(_pThicknessSpinBox, 0, 1);

    // Closed or open polyline
    _pClosedCheckBox = new QCheckBox(pGroupBox);
    if (!pElement->getCanBeClosed())
    {
        _pClosedCheckBox->setDisabled(true);
    }
    pGroupBoxLayout->addWidget(new QLabel(TR("id_closed"), pGroupBox), 1, 0);
    pGroupBoxLayout->addWidget(_pClosedCheckBox, 1, 1);

    // Density
    _pDistanceSpinBox = new TYDoubleSpinBox(pGroupBox);
    _pDistanceSpinBox->setSingleStep(0.1);
    _pDistanceSpinBox->setCorrectionMode(QAbstractSpinBox::CorrectToNearestValue);
    pGroupBoxLayout->addWidget(new QLabel(TR("id_distance"), pGroupBox), 2, 0);
    pGroupBoxLayout->addWidget(_pDistanceSpinBox, 2, 1);
    pGroupBoxLayout->addWidget(new QLabel(TR("id_distance_unit"), pGroupBox), 2, 2);

    // The minimum value of thickness and density will be set automatically, because
    // the signal valueChanged() will be triggered by the setValue() functions in updateContent()
    // (that's why we don't use valueEdited() that only works with user input)
    void (QDoubleSpinBox::*qDoubleSpinBox_valueChanged)(double) = &QDoubleSpinBox::valueChanged;
    QObject::connect(_pThicknessSpinBox, qDoubleSpinBox_valueChanged, this,
                     &TYBoundaryNoiseMapWidget::updateMinimumDensity);

    pBoundaryNoiseMapLayout->addWidget(pGroupBox, 1, 0);

    updateContent();
}

TYBoundaryNoiseMapWidget::~TYBoundaryNoiseMapWidget() {}

void TYBoundaryNoiseMapWidget::updateContent()
{
    _maillageW->updateContent();

    _pThicknessSpinBox->setValue(getElement()->getThickness());
    _pClosedCheckBox->setChecked(getElement()->isClosed());
    _pDistanceSpinBox->setValue(1.0f / (getElement()->getDensity()));
}

void TYBoundaryNoiseMapWidget::apply()
{
    _maillageW->apply();

    double thickness = _pThicknessSpinBox->value();
    bool closed = _pClosedCheckBox->isChecked();
    double distance = _pDistanceSpinBox->value();

    LPTYProjet pProj = dynamic_cast<TYProjet*>(getElement()->getParent());
    // If something changed
    if (getElement()->getThickness() != thickness || getElement()->isClosed() != closed ||
        getElement()->getDistance() != distance)
    {
        getElement()->make(getElement()->getTabPoint(), thickness, closed, 1.0f / distance);
        dynamic_cast<TYProjet*>(getElement()->getParent())->updateCalculsWithMaillage(getElement());

        // La densite a changee, il faut mettre a jour l'altimetrie
        if (pProj && pProj->getSite()->getAltimetry()->containsData())
        {
            pProj->updateAltiRecepteurs();
        }
    }
    if (pProj)
    {
        pProj->updateGraphic();
    }

    emit modified();
}

void TYBoundaryNoiseMapWidget::updateMinimumDensity(double thickness)
{
    _pDistanceSpinBox->setRange(0.1, 1.0f / TYBoundaryNoiseMap::computeMinimumDensity(thickness));
}
