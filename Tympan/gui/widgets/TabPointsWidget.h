/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TABPOINTSWIDGET_H__
#define __TABPOINTSWIDGET_H__

#include <qtablewidget.h>
#include <QStyledItemDelegate>
#include "Tympan/models/business/TYDefines.h"

// itemdelegate.h
class ItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit ItemDelegate(QObject* parent = nullptr) : QStyledItemDelegate(parent) {}

    bool eventFilter(QObject* object, QEvent* event) override;
};

class TabPointsWidget : public QTableWidget
{
    Q_OBJECT

public:
    TabPointsWidget(TYTabPoint& listPoints, QWidget* parent = nullptr);

    virtual void update();
    virtual void apply();

public slots:
    void tabValueChanged(int, int);
    virtual void contextMenuEvent(QContextMenuEvent* e);

private:
    void setAttributes(int row);
    void initRow(int row);

private:
    TYTabPoint& _listPoints;
};

#endif //__TABPOINTSWIDGET_H__
