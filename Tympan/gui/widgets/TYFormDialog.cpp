/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYFormDialog.cpp
 * \brief Parent class of Tympan Qt dialogs of type form (body file)
 * \author Projet_Tympan
 *
 */

#include <QLineEdit>

#include "Tympan/gui/widgets/TYLineEdit.h"

#include "TYFormDialog.h"

TYFormDialog::TYFormDialog(QWidget* parent, Qt::WindowFlags f) : QDialog(parent, f) {}

bool TYFormDialog::validate()
{
    bool isValidated = true;
    QPushButton* pDefaultButton = nullptr;
    QObjectList objectsList = children();

    // Search default button
    for (int i = 0; i < objectsList.size(); i++)
    {
        pDefaultButton = dynamic_cast<QPushButton*>(objectsList[i]);
        if (pDefaultButton != nullptr && pDefaultButton->isDefault())
        {
            break;
        }
    }

    // If there is a default button then validate
    if (pDefaultButton != nullptr)
    {
        isValidated = validateChildren(this);
        pDefaultButton->setEnabled(isValidated);
    }
    else
    {
        isValidated = false;
    }
    return isValidated;
}

bool TYFormDialog::validateChildren(QObject* object)
{
    bool ret = true;

    QLineEdit* pQLineEdit = nullptr;
    pQLineEdit = dynamic_cast<QLineEdit*>(object);
    // If it is a QLineEdit, then validate it
    if (pQLineEdit != nullptr)
    {
        bool valid = (pQLineEdit->hasAcceptableInput() || !pQLineEdit->isEnabled());
        if (!valid)
        {
            pQLineEdit->setStyleSheet("QLineEdit { border: 2px solid red; color: red;}");
        }
        ret = ret && valid;
    }
    else
    // Else validate its children
    {
        QObjectList objectsList = object->children();
        for (int i = 0; i < objectsList.size(); i++)
        {
            ret = ret & validateChildren(objectsList[i]);
        }
    }
    return ret;
}
