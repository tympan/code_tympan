/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYLineEditInt.h
 * \brief outil IHM pour une entrée utilisateur de type int (fichier header)
 * \author Projet_Tympan
 *
 */

#ifndef __TY_LINE_EDIT_INT__
#define __TY_LINE_EDIT_INT__

#include "TYLineEdit.h"
#include <QIntValidator>

class TYLineEditInt : public TYLineEdit
{
    Q_OBJECT

public:
    TYLineEditInt(QWidget* parent = 0);
    ~TYLineEditInt();

    void init() override;

    /**
     * overload the QLineEdit method
     * If the key pressed is a coma, discard it
     */
    void keyPressEvent(QKeyEvent* evt) override;
};

#endif
