/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYToolButton.cpp
 * \brief outil IHM pour la gestion de la couleur des ToolButton
 *
 *
 */

#include <qcolordialog.h>
#include <QPainter>
#include <QStyle>
#include "TYToolButton.h"

// using namespace Qt;

TYToolButton::TYToolButton(QWidget* parent, const char* name) : QToolButton(parent)
{
    setAutoFillBackground(true);
    setObjectName(name);
    setToolButtonStyle(Qt::ToolButtonTextOnly);
    _color = Qt::white;
    QObject::connect(this, &TYToolButton::clicked, this, &TYToolButton::selectColor);
}

TYToolButton::~TYToolButton() {}

void TYToolButton::selectColor()
{
    QColor c = QColorDialog::getColor(_color);

    if (c.isValid())
    {
        _color = c;
        updateColor();

        emit colorAccepted(_color);
    }
    else
    {
        emit colorRejected();
    }
}

void TYToolButton::updateColor()
{
    setStyleSheet(QString("background-color: %1;").arg(_color.name()));
}
