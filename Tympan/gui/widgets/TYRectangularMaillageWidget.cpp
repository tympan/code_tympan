/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYRectangularMaillageWidget.cpp
 * \brief outil IHM pour un maillage rectangulaire
 */

// Added by qt3to4:
#include <QGridLayout>
#include <QLabel>

#include "Tympan/models/business/OLocalizator.h"
#include "Tympan/models/business/TYRectangularMaillage.h"
#include "Tympan/models/business/TYCalcul.h"
#include "Tympan/models/business/TYProjet.h"
#include "Tympan/gui/widgets/TYMaillageWidget.h"
#include "Tympan/gui/widgets/TYLineEdit.h"
#include "TYRectangularMaillageWidget.h"

#define TR(id) OLocalizator::getString("TYRectangularMaillageWidget", (id))

TYRectangularMaillageWidget::TYRectangularMaillageWidget(TYRectangularMaillage* pElement,
                                                         QWidget* _pParent /*=NULL*/)
    : TYWidget(pElement, _pParent)
{

    _maillageW = new TYMaillageWidget(pElement, this);

    resize(300, 200);
    setWindowTitle(TR("id_caption"));
    QGridLayout* pRectangularMaillageLayout = new QGridLayout();
    setLayout(pRectangularMaillageLayout);

    pRectangularMaillageLayout->addWidget(_maillageW, 0, 0);

    QGroupBox* pGroupBox = new QGroupBox(this);
    QGridLayout* pGroupBoxLayout = new QGridLayout();
    pGroupBox->setLayout(pGroupBoxLayout);

    QLabel* pLabelDistanceX = new QLabel(TR("id_distance_x"), pGroupBox);
    _lineEditDistanceX = new TYLineEdit(pGroupBox);
    QObject::connect(_lineEditDistanceX, &QLineEdit::textChanged, this,
                     &TYRectangularMaillageWidget::updateNbPoints);
    QLabel* pLabelDistanceUniteX = new QLabel(TR("id_distance_unit"), pGroupBox);
    pGroupBoxLayout->addWidget(pLabelDistanceX, 0, 0);
    pGroupBoxLayout->addWidget(_lineEditDistanceX, 0, 1);
    pGroupBoxLayout->addWidget(pLabelDistanceUniteX, 0, 2);

    QLabel* pLabelDensiteY = new QLabel(TR("id_distance_y"), pGroupBox);
    _lineEditDistanceY = new TYLineEdit(pGroupBox);
    QObject::connect(_lineEditDistanceY, &QLineEdit::textChanged, this,
                     &TYRectangularMaillageWidget::updateNbPoints);
    QLabel* pLabelDensiteUniteY = new QLabel(TR("id_distance_unit"), pGroupBox);
    pGroupBoxLayout->addWidget(pLabelDensiteY, 1, 0);
    pGroupBoxLayout->addWidget(_lineEditDistanceY, 1, 1);
    pGroupBoxLayout->addWidget(pLabelDensiteUniteY, 1, 2);

    pRectangularMaillageLayout->addWidget(pGroupBox, 1, 0);

    QGroupBox* pGroupBoxRectangle = new QGroupBox(this);
    pGroupBoxRectangle->setTitle(TR("id_rectangle"));
    QGridLayout* pGroupBoxRectangleLayout = new QGridLayout();
    pGroupBoxRectangle->setLayout(pGroupBoxRectangleLayout);

    _lineEditNomRectangle = new QLineEdit(pGroupBoxRectangle);
    _lineEditNomRectangle->setEnabled(false);
    pGroupBoxRectangleLayout->addWidget(_lineEditNomRectangle, 0, 0);

    QLabel* pSurfaceLabel = new QLabel(TR("id_surface"), pGroupBoxRectangle);
    pGroupBoxRectangleLayout->addWidget(pSurfaceLabel, 0, 1);
    _lineEditSurface = new TYLineEdit(pGroupBoxRectangle);
    _lineEditSurface->setReadOnly(true);
    pGroupBoxRectangleLayout->addWidget(_lineEditSurface, 0, 2);

    QPushButton* pPushButtonRectangle = new QPushButton(TR("id_proprietes_button"), pGroupBoxRectangle);
    pGroupBoxRectangleLayout->addWidget(pPushButtonRectangle, 0, 3);

    pRectangularMaillageLayout->addWidget(pGroupBoxRectangle, 2, 0);

    updateContent();

    connect(pPushButtonRectangle, &QPushButton::clicked, this, &TYRectangularMaillageWidget::editRectangle);
}

TYRectangularMaillageWidget::~TYRectangularMaillageWidget() {}

void TYRectangularMaillageWidget::updateContent()
{
    _maillageW->updateContent();

    if (getElement()->getRectangle())
    {
        _lineEditNomRectangle->setText(getElement()->getRectangle()->getName());
        _lineEditSurface->setText(QString().setNum(getElement()->getRectangle()->surface(), 'f', 2));
    }

    _lineEditDistanceX->setText(QString().setNum(getElement()->getDistanceX(), 'f', 2));
    _lineEditDistanceY->setText(QString().setNum(getElement()->getDistanceY(), 'f', 2));
}

void TYRectangularMaillageWidget::updateNbPoints()
{
    double surface = _lineEditSurface->text().toDouble();
    double densiteX = 1.0f / (_lineEditDistanceX->text().toDouble());
    double densiteY = 1.0f / (_lineEditDistanceY->text().toDouble());

    _maillageW->updateNbPoints((int)(surface * densiteX * densiteY));
}

void TYRectangularMaillageWidget::apply()
{
    _maillageW->apply();

    // Si les densites sont differentes
    double distanceX = _lineEditDistanceX->text().toDouble();
    double distanceY = _lineEditDistanceY->text().toDouble();
    LPTYProjet pProj = dynamic_cast<TYProjet*>(getElement()->getParent());

    if (((getElement()->getDistanceX() != distanceX) || (getElement()->getDistanceY() != distanceY)) &&
        getElement()->getRectangle())
    {
        // Rebuild the noise map
        getElement()->make(getElement()->getRectangle(), 1.0f / distanceX, 1.0f / distanceY);

        // Update computation for new number of spectrums
        dynamic_cast<TYProjet*>(getElement()->getParent())->updateCalculsWithMaillage(getElement());

        // La densite a changee, il faut mettre a jour l'altimetrie
        if (pProj && pProj->getSite()->getAltimetry()->containsData())
        {
            pProj->updateAltiRecepteurs();
        }
    }
    if (pProj)
    {
        pProj->updateGraphic();
    }

    emit modified();
}

void TYRectangularMaillageWidget::editRectangle()
{
    int ret = getElement()->getRectangle()->edit(this);

    if ((ret == QDialog::Accepted) && (getElement()->getRectangle()))
    {
        _lineEditNomRectangle->setText(getElement()->getRectangle()->getName());
    }
}
