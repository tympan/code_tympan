/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYCalculWidget.cpp
 * \brief outil IHM pour un calcul
 */

#include <qtablewidget.h>
#include <qtabwidget.h>
#include <qradiobutton.h>
#include <qbuttongroup.h>
#include <qmenu.h>
#include <qstringlist.h>
#include <qcombobox.h>
// Added by qt3to4:
#include <QLabel>
#include <QGridLayout>
#include <QFrame>
#include <QDateEdit>
#include <QTextEdit>
#include <QTreeWidgetItem>
#include <QString>

#include "Tympan/core/config.h"
#include "Tympan/models/business/subprocess_util.h"
#include "Tympan/models/business/OLocalizator.h"
#include "Tympan/models/business/TYProjet.h"
#include "Tympan/models/business/TYCalcul.h"
#include "Tympan/models/business/TYProjet.h"
#include "Tympan/models/business/TYLinearMaillage.h"
#include "Tympan/models/business/TYRectangularMaillage.h"
#include "Tympan/models/business/TYPluginManager.h"
#include "Tympan/gui/app/TYMessageManager.h"
#include "Tympan/gui/widgets/TYEtatsWidget.h"
#include "Tympan/gui/widgets/TYSolverParamsWidgetManager.h"
#include "TYCalculWidget.h"

#define TR(id) OLocalizator::getString("TYCalculWidget", (id))

TYCalculWidget::TYCalculWidget(TYCalcul* pElement, QWidget* _pParent /*=NULL*/) : TYWidget(pElement, _pParent)
{
    QString num;
    short iln = 0; // Line number for setting elements up (ease the process when adding a new element)

    resize(300, 690);
    setWindowTitle(TR("id_caption"));

    _calculLayout = new QGridLayout();
    setLayout(_calculLayout);

    // Computation current elements
    _elmW = new TYElementWidget(pElement, this);
    _calculLayout->addWidget(_elmW, iln++, 0);

    // LOCK COMPUTATION
    _buttonGroupState = new QButtonGroup(this);
    _buttonGroupState->setExclusive(true);
    _pRadioButtonActif = new QRadioButton(TR("id_actif"));
    _buttonGroupState->addButton(_pRadioButtonActif, 0);
    _pRadioButtonLocked = new QRadioButton(TR("id_locked"));
    _buttonGroupState->addButton(_pRadioButtonLocked, 1);

    QGridLayout* groupBoxStateLayout = new QGridLayout();
    groupBoxStateLayout->addWidget(_pRadioButtonActif, 0, 0);
    groupBoxStateLayout->addWidget(_pRadioButtonLocked, 0, 1);

    QGroupBox* groupBoxState = new QGroupBox();
    groupBoxState->setTitle(TR("id_etat_calcul"));
    groupBoxState->setLayout(groupBoxStateLayout);

    _calculLayout->addWidget(groupBoxState, iln++, 0);

    // CHOOSE SOLVER METHOD
    QGroupBox* groupBoxCalcMethod = new QGroupBox(this);
    groupBoxCalcMethod->setTitle(TR(""));
    QGridLayout* groupBoxCalcMethodLayout = new QGridLayout();
    groupBoxCalcMethod->setLayout(groupBoxCalcMethodLayout);

    QLabel* labelCalcMethod = new QLabel(groupBoxCalcMethod);
    labelCalcMethod->setText(TR("id_calc_method_label"));
    groupBoxCalcMethodLayout->addWidget(labelCalcMethod, 0, 0);

    // 20070913 - suite au plassage aux plugins, changement du combo en une ligne de texte
    _comboSolver = new QComboBox(groupBoxCalcMethod);
    _comboSolver->setEditable(false);

    groupBoxCalcMethodLayout->addWidget(_comboSolver, 0, 1);

    _calculLayout->addWidget(groupBoxCalcMethod, iln++, 0);

    // COMPUTATION PARAMETERS
    _tabWidget = new QTabWidget(this);

    // Read data model of solver parameters
    QString dataModelPath(QCoreApplication::applicationDirPath() + "/" + SOLVER_PARAMS_JSON);
    try
    {
        solverParamsWidgetManager = new TYSolverParamsWidgetManager(dataModelPath);

        bool bDefaultSolverSelected{false};

        // 9613
        if (getElement()->getSolverId() == OGenID("{B45873B6-550C-11ED-BDC3-0242AC120002}"))
        {
            bDefaultSolverSelected = false;
        }
        // DefaultSolver
        else
        {
            bDefaultSolverSelected = true;
        }
        // Parameters tab
        _initSolverParamsTabs();

        // Meteo tab
        _initMeteoParamsTabs();

        _enableDisableDefaultSolverWidgets(bDefaultSolverSelected);
    }
    catch (const tympan::invalid_data& exc)
    {
        OMessageManager::get()->error(exc.what());
    }

    // Ponctual receptors tab
    _tableauPointControle = new QTableWidget();
    _tableauPointControle->setColumnCount(5);
    _tableauPointControle->setHorizontalHeaderItem(0, new QTableWidgetItem(TR("id_nom_pc")));
    _tableauPointControle->setHorizontalHeaderItem(1, new QTableWidgetItem(TR("id_pos_x")));
    _tableauPointControle->setHorizontalHeaderItem(2, new QTableWidgetItem(TR("id_pos_y")));
    _tableauPointControle->setHorizontalHeaderItem(3, new QTableWidgetItem(TR("id_pos_h")));
    _tableauPointControle->setHorizontalHeaderItem(4, new QTableWidgetItem(TR("id_actif")));

    _tabWidget->insertTab(_tabWidget->count() + 1, _tableauPointControle, TR("id_opt_pc"));

    // Surfacic receptors tab
    _tableauMaillages = new QTableWidget();
    _tableauMaillages->setColumnCount(2);
    _tableauMaillages->setHorizontalHeaderItem(0, new QTableWidgetItem(TR("id_nom_pc")));
    _tableauMaillages->setHorizontalHeaderItem(1, new QTableWidgetItem(TR("id_actif")));

    _tabWidget->insertTab(_tabWidget->count() + 1, _tableauMaillages, TR("id_opt_maillage"));

    _calculLayout->addWidget(_tabWidget, iln++, 0);

    // General informations tab

    _groupBox = new QGroupBox(_tabWidget);

    _groupBox->setTitle(TR(""));
    QGridLayout* groupBoxLayout = new QGridLayout();
    _groupBox->setLayout(groupBoxLayout);

    _lineEditAuteur = new QLineEdit(_groupBox);
    groupBoxLayout->addWidget(_lineEditAuteur, 1, 1);

    _labelAuteur = new QLabel(_groupBox);
    _labelAuteur->setText(TR("id_auteur_label"));
    groupBoxLayout->addWidget(_labelAuteur, 1, 0);

    QDate date;

    _editDateCreation = new QDateEdit(_groupBox);
    if (getElement()->getDateCreation() == "2001-10-01")
    {
        getElement()->setDateCreation(date.currentDate().toString(Qt::ISODate));
    }
    _editDateCreation->setDate(date.fromString(getElement()->getDateCreation(), Qt::ISODate));
    groupBoxLayout->addWidget(_editDateCreation, 2, 1);

    _labelDateCreation = new QLabel(_groupBox);
    _labelDateCreation->setText(TR("id_datecreation_label"));
    groupBoxLayout->addWidget(_labelDateCreation, 2, 0);

    _labelDateModif = new QLabel(_groupBox);
    _labelDateModif->setText(TR("id_datemodif_label"));
    groupBoxLayout->addWidget(_labelDateModif, 3, 0);

    _editDateModif = new QDateEdit(_groupBox);
    if (getElement()->getDateModif() == "2001-10-01")
    {
        getElement()->setDateModif(date.currentDate().toString(Qt::ISODate));
    }
    _editDateModif->setDate(date.fromString(getElement()->getDateModif(), Qt::ISODate));
    groupBoxLayout->addWidget(_editDateModif, 3, 1);

    _lineEditComment = new QTextEdit(_groupBox);
    groupBoxLayout->addWidget(_lineEditComment, 5, 0, 1, 2);

    _labelComment = new QLabel(_groupBox);
    _labelComment->setText(TR("id_comment_label"));
    groupBoxLayout->addWidget(_labelComment, 4, 0);

    _calculLayout->addWidget(_groupBox, iln++, 0);

    _tabWidget->insertTab(_tabWidget->count() + 1, _groupBox, TR("id_info_calc"));

    //  Button allowing the access to states
    QGroupBox* pGroupBoxTableEtats = new QGroupBox(this);
    pGroupBoxTableEtats->setTitle(TR("id_map_box"));
    QGridLayout* groupBoxTableEtatsLayout = new QGridLayout();
    pGroupBoxTableEtats->setLayout(groupBoxTableEtatsLayout);

    QLineEdit* pLineEditNomTableEtats = new QLineEdit(pGroupBoxTableEtats);
    pLineEditNomTableEtats->setEnabled(false);
    groupBoxTableEtatsLayout->addWidget(pLineEditNomTableEtats, 0, 0);

    QPushButton* pPushButtonTableEtats = new QPushButton(pGroupBoxTableEtats);
    pPushButtonTableEtats->setText(TR("id_proprietes"));
    groupBoxTableEtatsLayout->addWidget(pPushButtonTableEtats, 0, 1);

    _calculLayout->addWidget(pGroupBoxTableEtats, iln++, 0);

    _etatsWidget = new TYEtatsWidget(getElement(), this);

    // Results
    _groupBoxResultat = new QGroupBox(this);
    _groupBoxResultat->setTitle(TR("id_resultat"));
    QGridLayout* groupBoxResultatLayout = new QGridLayout();
    _groupBoxResultat->setLayout(groupBoxResultatLayout);

    // Workaround issue #138
    // _labelStoreGlobalMatrix = new QLabel(_groupBoxResultat);
    // _labelStoreGlobalMatrix->setText(TR("id_storeglobalmatrix_label"));
    // groupBoxResultatLayout->addWidget(_labelStoreGlobalMatrix, 0, 0);
    //
    // _checkBoxStoreGlobalMatrix = new QCheckBox(_groupBoxResultat);
    // _checkBoxStoreGlobalMatrix->setText(TR(""));
    // groupBoxResultatLayout->addWidget(_checkBoxStoreGlobalMatrix, 0, 1);

    _pushButtonResultat = new QPushButton(_groupBoxResultat);
    _pushButtonResultat->setText(TR("id_proprietes"));
    groupBoxResultatLayout->addWidget(_pushButtonResultat, 0, 2);

    _calculLayout->addWidget(_groupBoxResultat, iln++, 0);

    updateContent();

    connect(pPushButtonTableEtats, &QPushButton::clicked, _etatsWidget, &TYEtatsWidget::show);
    connect(_pushButtonResultat, &QPushButton::clicked, this, &TYCalculWidget::editResultat);

    connect(_comboSolver, &QComboBox::currentTextChanged, this, &TYCalculWidget::changeSolverMethod);
}

TYCalculWidget::~TYCalculWidget()
{
    delete _etatsWidget;
}

void TYCalculWidget::updateContent()
{
    QString num;
    QDate date;

    _elmW->setEnabled(true);
    _groupBox->setEnabled(true);

    _elmW->updateContent(); // Affichage du nom du calcul

    // General infos
    _editDateModif->setDate(date.currentDate());
    _editDateCreation->setDate(date.fromString(getElement()->getDateCreation(), Qt::ISODate));
    _lineEditAuteur->setText(getElement()->getAuteur());
    _lineEditComment->setPlainText(getElement()->getComment());

    // Mise a jour de l'etat du calcul
    updateCalculState();

    // Choix du type de calcul
    updateComboSolver();

    // Workaround issue #138
    // Keep partial results (or not)
    // _checkBoxStoreGlobalMatrix->setChecked(getElement()->getStatusPartialResult());

    // Remplissage du tableau des points de controleet maillages
    _tableauPointControle->setEnabled(true);
    _tableauMaillages->setEnabled(true);

    TYProjet* pProjet = getElement()->getProjet();
    if (pProjet != NULL)
    {
        updateControlPointsTab(pProjet);
        updateNoiseMapsTab(pProjet);
    }

    // initialisation des valeurs des paramètres
    solverParamsWidgetManager->updateWidgets(getElement()->solverParams);

    // on force la synchronisation des paramètres et des widgets
    // pour finaliser l'initialisation de l'IHM
    solverParamsWidgetManager->forceSyncParams();
}

void TYCalculWidget::updateControlPointsTab(TYProjet* pProjet)
{
    TYTabLPPointControl& tabPoints = pProjet->getPointsControl();
    unsigned int nbPoints = static_cast<uint32>(tabPoints.size());
    _tableauPointControle->setRowCount(nbPoints);

    QString msg;
    unsigned int row = 0;
    for (row = 0; row < nbPoints; row++)
    {
        _tableauPointControle->setItem(row, 0, new QTableWidgetItem(tabPoints[row]->getName()));

        msg = QString(TR("id_cell_posx")).arg(tabPoints[row]->_x, 7, 'f', 1);
        _tableauPointControle->setItem(row, 1, new QTableWidgetItem(msg));

        msg = QString(TR("id_cell_posy")).arg(tabPoints[row]->_y, 7, 'f', 1);
        _tableauPointControle->setItem(row, 2, new QTableWidgetItem(msg));

        msg = QString(TR("id_cell_posh")).arg(tabPoints[row]->getHauteur(), 7, 'f', 1);
        _tableauPointControle->setItem(row, 3, new QTableWidgetItem(msg));

        QTableWidgetItem* pCheckItemActif = new QTableWidgetItem("");

        if (getElement()->getPtCtrlStatus(tabPoints[row]->getID()))
        {
            pCheckItemActif->setCheckState(Qt::Checked);
        }
        else
        {
            pCheckItemActif->setCheckState(Qt::Unchecked);
        }

        _tableauPointControle->setItem(row, 4, pCheckItemActif);

        _tableauPointControle->setRowHeight(row, 30);
    }
}

void TYCalculWidget::updateNoiseMapsTab(TYProjet* pProjet)
{
    int nbPoints = static_cast<uint32>(pProjet->getMaillages().size());
    _tableauMaillages->setRowCount(nbPoints);

    LPTYMaillage pMaillage = NULL;

    for (int row = 0; row < nbPoints; row++)
    {
        pMaillage = pProjet->getMaillage(row);
        _tableauMaillages->setItem(row, 0, new QTableWidgetItem(pMaillage->getName()));

        QTableWidgetItem* pCheckItemActif = new QTableWidgetItem("");
        if (pMaillage->etat(getElement()) == true)
        {
            pCheckItemActif->setCheckState(Qt::Checked);
        }
        else
        {
            pCheckItemActif->setCheckState(Qt::Unchecked);
        }
        _tableauMaillages->setItem(row, 1, pCheckItemActif);

        _tableauMaillages->setRowHeight(row, 30);
    }
}

void TYCalculWidget::updateComboSolver()
{
    OGenID currentId = getElement()->getSolverId(); // Id du solveur courant

    LPTYPluginManager pPlug = TYPluginManager::get();
    TYPluginManager::TYPluginList& plugList = pPlug->getPluginList();
    TYPluginManager::TYPluginList::iterator iter;

    QString solverName;
    unsigned short i = 0, currentSolverIndex = 0;
    OGenID id;
    for (iter = plugList.begin(), i = 0; iter != plugList.end(); iter++, i++)
    {
        solverName = (*iter)->getPlugin()->getName();
        _comboSolver->insertItem(i, solverName);

        if ((*iter)->getPlugin()->getUUID() == currentId)
        {
            currentSolverIndex = i;
        }
    }

    // On affiche le regime courant
    _comboSolver->setCurrentIndex(currentSolverIndex);
}

void TYCalculWidget::updateCalculState()
{
    if (getElement()->getState() == TYCalcul::Actif)
    {
        _pRadioButtonActif->setChecked(true);
        _pRadioButtonLocked->setChecked(false);
    }
    else
    {
        _pRadioButtonActif->setChecked(false);
        _pRadioButtonLocked->setChecked(true);
    }
}

void TYCalculWidget::_enableDisableDefaultSolverWidgets(bool pIsDefaultSolverSelected)
{
    solverParamsWidgetManager->getWidget("UseRealGround")->setEnabled(pIsDefaultSolverSelected);
    solverParamsWidgetManager->getWidget("UseScreen")->setEnabled(pIsDefaultSolverSelected);
    solverParamsWidgetManager->getWidget("UseReflection")->setEnabled(pIsDefaultSolverSelected);
    solverParamsWidgetManager->getWidget("PropaConditions")->setEnabled(pIsDefaultSolverSelected);
    solverParamsWidgetManager->getWidget("ModSummation")->setEnabled(pIsDefaultSolverSelected);
    solverParamsWidgetManager->getWidget("UseLateralDiffraction")->setEnabled(pIsDefaultSolverSelected);
    solverParamsWidgetManager->getWidget("AngleFavorable")->setEnabled(pIsDefaultSolverSelected);
    solverParamsWidgetManager->getWidget("DSWindDirection")->setEnabled(pIsDefaultSolverSelected);
}

void TYCalculWidget::_initSolverParamsTabs()
{

    _paramsTabWidget = new QWidget();
    QGridLayout* tabLayout = new QGridLayout();
    _paramsTabWidget->setLayout(tabLayout);
    try
    {
        // agencement des widgets
        // 1ere colonne
        tabLayout->addWidget(solverParamsWidgetManager->makeWidget("UseRealGround"), 1, 0, 1, 2);
        tabLayout->addWidget(solverParamsWidgetManager->makeWidget("UseScreen"), 2, 0, 1, 1);
        tabLayout->addWidget(solverParamsWidgetManager->makeWidget("UseReflection"), 3, 0, 1, 1);
        tabLayout->addWidget(solverParamsWidgetManager->makeWidget("PropaConditions"), 4, 0, 1, 2);
        tabLayout->addWidget(solverParamsWidgetManager->makeWidget("ModSummation"), 5, 0, 1, 2);
        tabLayout->addWidget(solverParamsWidgetManager->makeWidget("MinSRDistance"), 6, 0, 1, 2);

        // 2eme colonne
        tabLayout->addWidget(solverParamsWidgetManager->makeWidget("UseLateralDiffraction"), 2, 2, 1, 1);
        tabLayout->addWidget(solverParamsWidgetManager->makeWidget("H1parameter"), 4, 2, 1, 1);
        tabLayout->addWidget(solverParamsWidgetManager->makeWidget("KeepRays"), 5, 2, 1, 1);

        // on recupere les widgets qui necessitent une interactivite particuliere
        QCheckBox* useReflectionCheckBox =
            ((TYSolverParamsCheckBoxWidget*)solverParamsWidgetManager->getWidget("UseReflection"))->checkBox;
        QCheckBox* useLateralDiffCheckBox =
            ((TYSolverParamsCheckBoxWidget*)solverParamsWidgetManager->getWidget("UseLateralDiffraction"))
                ->checkBox;
        QCheckBox* useScreenCheckBox =
            ((TYSolverParamsCheckBoxWidget*)solverParamsWidgetManager->getWidget("UseScreen"))->checkBox;

        // activation/desactivation des reflexions et des diffractions lateral lorsque la prise en compte des
        // ecrans change
        connect(useScreenCheckBox, &QCheckBox::toggled, useReflectionCheckBox, &QCheckBox::setEnabled);
        connect(useScreenCheckBox, &QCheckBox::toggled, useLateralDiffCheckBox, &QCheckBox::setEnabled);

        // desactivation des reflexions a la desactivation des ecrans
        auto uncheck_reflection_if_not_use_screen = [useScreenCheckBox, useReflectionCheckBox](bool clicked)
        {
            if (!useScreenCheckBox->isChecked())
                useReflectionCheckBox->setChecked(false);
        };
        connect(useScreenCheckBox, &QCheckBox::toggled, useReflectionCheckBox,
                uncheck_reflection_if_not_use_screen);

        // activation des diffractions laterales lors a l'activation des reflexions
        auto check_lateral_diffractions = [useLateralDiffCheckBox](bool clicked)
        { useLateralDiffCheckBox->setChecked(true); };
        connect(useReflectionCheckBox, &QCheckBox::clicked, useLateralDiffCheckBox,
                check_lateral_diffractions);

        solverParamsWidgetManager->getWidget("H1parameter")->setDisabled(true);
    }
    catch (const tympan::invalid_data& exc)
    {
        std::ostringstream msg;
        msg << TR("id_solver_param_tab_err").toStdString() << " " << TR("id_opt_calc").toStdString() << ": "
            << exc.what();
        OMessageManager::get()->error(msg.str().c_str());
    }

    _paramsTabWidget->layout()->setAlignment(Qt::AlignTop);
    _tabWidget->insertTab(0, _paramsTabWidget, TR("id_opt_calc"));
}

void TYCalculWidget::_initMeteoParamsTabs()
{
    _meteoTabWidget = new QWidget();
    QGridLayout* tabLayout = new QGridLayout();
    _meteoTabWidget->setLayout(tabLayout);

    QGroupBox* atmosGroupBox = new QGroupBox(TR("id_atmos_box"), _meteoTabWidget);
    QGridLayout* atmosGroupBoxLayout = new QGridLayout(atmosGroupBox);
    atmosGroupBox->setLayout(atmosGroupBoxLayout);
    atmosGroupBoxLayout->setColumnStretch(2, 1);

    QGroupBox* windGroupBox = new QGroupBox(TR("id_wind_box"), _meteoTabWidget);
    QGridLayout* windGroupBoxLayout = new QGridLayout(windGroupBox);
    windGroupBox->setLayout(windGroupBoxLayout);
    windGroupBoxLayout->setColumnStretch(2, 1);

    try
    {
        atmosGroupBoxLayout->addWidget(solverParamsWidgetManager->makeWidget("AtmosPressure"), 0, 0);
        atmosGroupBoxLayout->addWidget(new QLabel(TR("id_unite_pression")), 0, 1, Qt::AlignLeft);

        atmosGroupBoxLayout->addWidget(solverParamsWidgetManager->makeWidget("AtmosTemperature"), 1, 0);
        atmosGroupBoxLayout->addWidget(new QLabel(TR("id_unite_temp")), 1, 1, Qt::AlignLeft);

        atmosGroupBoxLayout->addWidget(solverParamsWidgetManager->makeWidget("AtmosHygrometry"), 2, 0);
        atmosGroupBoxLayout->addWidget(new QLabel(TR("id_unite_hygro")), 2, 1, Qt::AlignLeft);

        windGroupBoxLayout->addWidget(solverParamsWidgetManager->makeWidget("AngleFavorable"), 0, 0);

        windGroupBoxLayout->addWidget(solverParamsWidgetManager->makeWidget("DSWindDirection"), 1, 0);

        tabLayout->addWidget(atmosGroupBox, 0, 0);
        tabLayout->addWidget(windGroupBox, 1, 0);
    }
    catch (const tympan::invalid_data& exc)
    {
        std::ostringstream msg;
        msg << TR("id_solver_param_tab_err").toStdString() << " " << TR("id_opt_meteo").toStdString() << ": "
            << exc.what();
        OMessageManager::get()->error(msg.str().c_str());
    }

    // activation des paramètres de vent uniquement si propaConditions == 2 (mixed)
    auto enable_wind_params = [windGroupBox](QString value) { windGroupBox->setEnabled(value == "2"); };
    connect(solverParamsWidgetManager->getWidget("PropaConditions"), &TYSolverParamsWidget::valueChanged,
            windGroupBox, enable_wind_params);

    _meteoTabWidget->layout()->setAlignment(Qt::AlignTop);
    _tabWidget->insertTab(1, _meteoTabWidget, TR("id_opt_meteo"));
}

void TYCalculWidget::apply()
{
    _elmW->apply();

    getElement()->setComment(_lineEditComment->toPlainText());
    getElement()->setAuteur(_lineEditAuteur->text());

    // CHOIX DU SOLVEUR
    LPTYPluginManager pPlug = TYPluginManager::get();
    TYPluginManager::TYPluginList& plugList = pPlug->getPluginList();
    TYPluginManager::TYPluginList::iterator iter;

    unsigned short i = 0;
    unsigned short currentIndex = _comboSolver->currentIndex();
    OGenID id;
    for (i = 0, iter = plugList.begin(); i <= currentIndex; i++, iter++)
    {
        id = (*iter)->getPlugin()->getUUID();
    }

    getElement()->setSolverId(id);

    // ==================

    if (_pRadioButtonActif->isChecked())
    {
        getElement()->setState(TYCalcul::Actif);
    }
    else
    {
        getElement()->setState(TYCalcul::Locked);
        if (getElement()->getResultat())
        {
            getElement()->getResultat()->purge();
        }
    }

    getElement()->setDateModif(_editDateModif->date().currentDate().toString(Qt::ISODate));
    getElement()->setDateCreation(_editDateCreation->date().toString(Qt::ISODate));

    // Mise a jour des points de controles
    TYTabLPPointControl& tabPoints = getElement()->getProjet()->getPointsControl();
    int row = 0;
    bool need_to_rebuild_result(false);
    for (row = 0; row < _tableauPointControle->rowCount(); row++)
    {
        QTableWidgetItem* pCheck = (QTableWidgetItem*)_tableauPointControle->item(row, 4);
        if (pCheck->checkState() == Qt::Checked)
        {
            tabPoints[row]->setEtat(getElement()->getID(), true);
            need_to_rebuild_result |= getElement()->addPtCtrlToResult(tabPoints[row]);
        }
        else
        {
            tabPoints[row]->setEtat(getElement()->getID(), false);
            need_to_rebuild_result |= getElement()->remPtCtrlFromResult(tabPoints[row]);
        }
    }

    if (need_to_rebuild_result)
    {
        getElement()->getResultat()->buildMatrix();
    }

    // Mise a jour des maillages
    for (row = 0; row < _tableauMaillages->rowCount(); row++)
    {
        QTableWidgetItem* pCheck = (QTableWidgetItem*)_tableauMaillages->item(row, 1);
        if (pCheck->checkState() == Qt::Checked)
        {
            getElement()->addMaillage(getElement()->getProjet()->getMaillage(row));
        }
        else
        {
            getElement()->remMaillage(getElement()->getProjet()->getMaillage(row));
        }
    }

    getElement()->solverParams = solverParamsWidgetManager->getSolverParams();

    // Mise a jour du statut de sauvegarde des resultats partiels
    // Workaround issue #138
    // bool bEtat1 = _checkBoxStoreGlobalMatrix->isChecked();
    // getElement()->setStatusPartialResult(bEtat1) ; //(_checkBoxStoreGlobalMatrix->isChecked());

    emit modified();
}

void TYCalculWidget::changeSolverMethod(const QString& pSolverName)
{
    bool bIsDefaultSolverSelected{false};
    if (pSolverName == "9613Solver")
    {
        bIsDefaultSolverSelected = false;
    }
    else
    {
        bIsDefaultSolverSelected = true;
    }
    _enableDisableDefaultSolverWidgets(bIsDefaultSolverSelected);
}

void TYCalculWidget::editResultat()
{
    getElement()->getResultat()->edit(this);
}

void TYCalculWidget::contextMenuEvent(QContextMenuEvent* e)
{
    QWidget* currentTab = _tabWidget->currentWidget();
    unsigned short checkCol = 4; // Numero de la colonne a checker
    if (currentTab == _tableauPointControle)
    {
        checkCol = 4;
        QPoint point = _tableauPointControle->mapFrom(this, e->pos());
        if ((point.x() >= 0) && (point.y() >= 0) && (point.x() <= _tableauPointControle->width()) &&
            (point.y() <= _tableauPointControle->height()))
        {
            bool bActif = true;
            QMenu* pMenu = new QMenu(this);

            QAction* selectAll = pMenu->addAction(TR("id_popup_select_all"));
            QAction* unSelectAll = pMenu->addAction(TR("id_popup_unselect_all"));

            QAction* ret = pMenu->exec(_tableauPointControle->mapToGlobal(point));
            if (ret != NULL)
            {
                if (ret == selectAll)
                {
                    bActif = true;
                }
                else if (ret == unSelectAll)
                {
                    bActif = false;
                }
                // Update de la table active
                for (int row = 0; row < _tableauPointControle->rowCount(); row++)
                {
                    QTableWidgetItem* pCheck = (QTableWidgetItem*)_tableauPointControle->item(row, checkCol);
                    if (bActif)
                    {
                        pCheck->setCheckState(Qt::Checked);
                    }
                    else
                    {
                        pCheck->setCheckState(Qt::Unchecked);
                    }
                }
            }
        }
    }
    else if (currentTab == _tableauMaillages)
    {
        checkCol = 1;
        QPoint point = _tableauMaillages->mapFrom(this, e->pos());
        if ((point.x() >= 0) && (point.y() >= 0) && (point.x() <= _tableauMaillages->width()) &&
            (point.y() <= _tableauMaillages->height()))
        {
            bool bActif = true;
            QMenu* pMenu = new QMenu(this);

            QAction* selectAll = pMenu->addAction(TR("id_popup_select_all"));
            QAction* unSelectAll = pMenu->addAction(TR("id_popup_unselect_all"));

            QAction* ret = pMenu->exec(_tableauMaillages->mapToGlobal(point));
            if (ret != NULL)
            {
                if (ret == selectAll)
                {
                    bActif = true;
                }
                else if (ret == unSelectAll)
                {
                    bActif = false;
                }
                // Update de la table active
                for (int row = 0; row < _tableauMaillages->rowCount(); row++)
                {
                    QTableWidgetItem* pCheck = (QTableWidgetItem*)_tableauMaillages->item(row, checkCol);
                    if (bActif)
                    {
                        pCheck->setCheckState(Qt::Checked);
                    }
                    else
                    {
                        pCheck->setCheckState(Qt::Unchecked);
                    }
                }
            }
        }
    }
}
