/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYLineEdit.h
 * \brief outil IHM pour une entrée utilisateur (fichier header)
 * \author Projet_Tympan
 *
 */

#include <QKeyEvent>

#include "Tympan/gui/widgets/TYFormDialog.h"
#include "TYLineEdit.h"

TYLineEdit::TYLineEdit(const QString& contents, QWidget* parent) : QLineEdit(contents, parent)
{
    init();
}

TYLineEdit::TYLineEdit(QWidget* parent) : QLineEdit(parent)
{
    init();
}

TYLineEdit::TYLineEdit(const QString& contents, bool isZeroOK, bool canBeNegative, QWidget* parent)
{
    _isZeroOK = isZeroOK;
    _canBeNegative = canBeNegative;
    init();
}

void TYLineEdit::init()
{
    QDoubleValidator* validator = new QDoubleValidator();
    QLocale* locale = new QLocale(QLocale::C);
    locale->setNumberOptions(QLocale::RejectGroupSeparator);
    validator->setLocale(*locale);

    validator->setNotation(QDoubleValidator::StandardNotation);

    if (!_canBeNegative)
    {
        if (_isZeroOK)
        {
            validator->setBottom(0.0);
        }
        else
        {
            validator->setBottom(0.0001);
        }
    }
    setValidator(validator);
    connect();
}

void TYLineEdit::connect()
{
    QObject::connect(this, &QLineEdit::textChanged, this, &TYLineEdit::adjustTextColor);
}

void TYLineEdit::keyPressEvent(QKeyEvent* evt)
{

    if (evt->key() == Qt::Key_Comma)
    {
        evt = new QKeyEvent(QEvent::KeyPress, Qt::Key_Period, Qt::NoModifier, 0, 0, 0, ".");
    }

    QLineEdit::keyPressEvent(evt);
}

void TYLineEdit::adjustTextColor()
{
    if (!this->hasAcceptableInput())
    {
        this->setStyleSheet("QLineEdit { border: 2px solid red; color: red;}");
    }
    else
    {
        this->setStyleSheet("QLineEdit { color: black;}");
    }

    // Validate if parent or grand parent (case of Tabs) is TYFormDialog
    TYFormDialog* formDialog = nullptr;
    QObject* parent = this->parent();
    formDialog = dynamic_cast<TYFormDialog*>(parent);
    while (formDialog == nullptr && parent != nullptr)
    {
        parent = parent->parent();
        formDialog = dynamic_cast<TYFormDialog*>(parent);
    }

    if (formDialog != nullptr)
    {
        formDialog->validate();
    }
}
