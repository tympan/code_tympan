/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/*
 *
 *
 *
 */
#ifdef _MSC_VER
    #pragma once
#endif

#ifndef __TY_INCLUDES__QT__
    #define __TY_INCLUDES__QT__

    #include <qapplication.h>
    #include <qwidget.h>
    #include <qlabel.h>
    #include <qlineedit.h>
    #include <qlayout.h>
    #include <qcheckbox.h>
    #include <qgroupbox.h>
    #include <qpushbutton.h>
    #include <qcombobox.h>
    #include <qtextedit.h>
    #include <qdatetime.h>
    #include <qdatetimeedit.h>
    #include <qdialog.h>

#endif // __TY_INCLUDES__QT__
