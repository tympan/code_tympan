/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYAcousticRectangleNodeWidget.cpp
 * \brief outil IHM pour un ensemble de rectangles acoustiques
 */

// Added by qt3to4:
#include <QGridLayout>

#include "Tympan/models/business/OLocalizator.h"
#include "Tympan/models/business/geoacoustic/TYAcousticRectangleNode.h"
#include "Tympan/gui/widgets/TYAcousticSurfaceNodeWidget.h"
#include "TYAcousticRectangleNodeWidget.h"

#define TR(id) OLocalizator::getString("TYAcousticRectangleNodeWidget", (id))

TYAcousticRectangleNodeWidget::TYAcousticRectangleNodeWidget(TYAcousticRectangleNode* pElement,
                                                             QWidget* _pParent /*=NULL*/)
    : TYWidget(pElement, _pParent)
{
    _elmW = new TYAcousticSurfaceNodeWidget(pElement, this);

    resize(300, 680);
    setWindowTitle(TR("id_caption"));
    _acousticRectangleNodeLayout = new QGridLayout();
    setLayout(_acousticRectangleNodeLayout);

    _acousticRectangleNodeLayout->addWidget(_elmW, 0, 0);
}

TYAcousticRectangleNodeWidget::~TYAcousticRectangleNodeWidget() {}

void TYAcousticRectangleNodeWidget::updateContent()
{
    _elmW->updateContent();
}

void TYAcousticRectangleNodeWidget::apply()
{
    _elmW->apply();

    emit modified();
}
