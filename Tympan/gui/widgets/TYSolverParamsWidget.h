/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYSolverParamsWidget.h
 * \brief Widgets permettant de controler les parametres du solveur
 * \author Projet_Tympan
 */

#ifndef TYSOLVERPARAMSWIDGET_H
#define TYSOLVERPARAMSWIDGET_H

#include <QMap>
#include <QString>
#include <QJsonObject>
#include <QWidget>
#include <QVariant>
#include <QGroupBox>
#include <QButtonGroup>
#include <QCheckBox>
#include <QLabel>
#include <QLineEdit>

/**
 * \class TYSolverParamsDataModel
 * \brief Objet contenant les informations concernant les parametres du solveur
 **/
class TYSolverParamsDataModel
{

public:
    TYSolverParamsDataModel(QString paramName, QString type, QStringList valuesLabel = QStringList(),
                            QString defaultValue = "0");
    static TYSolverParamsDataModel* fromJsonObject(QString paramName, QJsonObject dataModelJson);
    virtual ~TYSolverParamsDataModel(){};

    bool isLabeledValues()
    {
        return valueLabels.size() > 0;
    }
    bool isBool()
    {
        return type == "bool";
    }
    bool isInt()
    {
        return type == "int";
    }
    bool isNumber()
    {
        return type == "int" || type == "double" || type == "float";
    }

    QString paramName;
    QString type;
    QString defaultValue;
    QStringList valueLabels;
};

/**
 * \class TYSolverParamsWidget
 * \brief Objet de base dont doivent heriter les widgets utilises pour controler les parametres du solveur
 **/
class TYSolverParamsWidget : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QString value READ value WRITE setValue NOTIFY valueChanged)

public:
    TYSolverParamsWidget(TYSolverParamsDataModel* dataModel);
    virtual ~TYSolverParamsWidget(){};

    QString value() const
    {
        return m_value;
    }

    TYSolverParamsDataModel* dataModel;

private:
    QString m_value;

public slots:
    void setValue(QString value);
    void setValue(int value);
    void setValue(bool value);

signals:
    void valueChanged(QString);
};

/**
 * \class TYSolverParamsWidget
 * \brief Objet prevu pour les parametres numeriques
 **/
class TYSolverParamsInputValueWidget : public TYSolverParamsWidget
{
    Q_OBJECT
public:
    TYSolverParamsInputValueWidget(TYSolverParamsDataModel* dataModel);

    QLabel* label;
    QLineEdit* lineEdit;
};

/**
 * \class TYSolverParamsWidget
 * \brief Objet prevu pour les parametres categoriels
 **/
class TYSolverParamsRadioButtonsWidget : public TYSolverParamsWidget
{
    Q_OBJECT

public:
    TYSolverParamsRadioButtonsWidget(TYSolverParamsDataModel* dataModel);

    QGroupBox* groupBox;
    QButtonGroup* buttonGroup;
};

/**
 * \class TYSolverParamsWidget
 * \brief Objet prevu pour les parametres booleens
 **/
class TYSolverParamsCheckBoxWidget : public TYSolverParamsWidget
{
    Q_OBJECT
public:
    TYSolverParamsCheckBoxWidget(TYSolverParamsDataModel* dataModel);

    QCheckBox* checkBox;
};

#endif // TYSOLVERPARAMSWIDGET_H
