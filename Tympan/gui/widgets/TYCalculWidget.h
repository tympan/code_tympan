/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *
 * \file TYCalculWidget.h
 * \brief outil IHM pour un calcul (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_CALCUL_WIDGET__
#define __TY_CALCUL_WIDGET__

#include "TYWidget.h"

#include <vector>
// Added by qt3to4:
#include <QGridLayout>
#include <QLabel>
#include "TYSolverParamsWidgetManager.h"

using std::vector;
typedef vector<TYElement*> tabPtrElement;

class TYProjet;
class TYCalcul;
class TYEtatsWidget;
class QLineEdit;
class QGridLayout;
class QLabel;
class QTreeWidget;
class QGroupBox;
class QPushButton;
class QCheckBox;
class QTreeWidgetItem;
class QPoint;
class QTextEdit;
class QDateEdit;
class TYElementWidget;
class QTableWidget;
class QTabWidget;
class QRadioButton;
class QButtonGroup;
class QComboBox;

/**
 * \class TYCalculWidget
 * \brief classe de l'objet IHM pour un calcul
 */
class TYCalculWidget : public TYWidget
{
    Q_OBJECT

    TY_DECL_METIER_WIDGET(TYCalcul)

    // Methodes
public:
    /**
     * Constructeur.
     */
    TYCalculWidget(TYCalcul* pElement, QWidget* _pParent = NULL);

    /**
     * Destructeur.
     */
    virtual ~TYCalculWidget();

public slots:
    virtual void updateContent();
    virtual void apply();
    void changeSolverMethod(const QString& pSolverName);

    /**
     * Edite la widget du resultat.
     */
    void editResultat();

    /**
     * Affiche un menu contextuel.
     */
    virtual void contextMenuEvent(QContextMenuEvent* e);

private:
    void updateControlPointsTab(TYProjet* pProjet);
    void updateNoiseMapsTab(TYProjet* pProjet);
    void updateComboSolver();
    void updateCalculState();
    void _enableDisableDefaultSolverWidgets(bool pIsDefaultSolverSelected);
    void _initSolverParamsTabs();
    void _initMeteoParamsTabs();

    // Membres
protected:
    TYSolverParamsWidgetManager* solverParamsWidgetManager;
    QLineEdit* _lineEditAuteur;
    QDateEdit* _editDateCreation;
    QDateEdit* _editDateModif;
    QTextEdit* _lineEditComment;
    QLabel* _labelAuteur;
    QLabel* _labelDateCreation;
    QLabel* _labelDateModif;
    QLabel* _labelComment;

    // Affichage du type de calcul
    QComboBox* _comboSolver;

    QGroupBox* _groupBox;
    QGroupBox* _groupBoxFlag;
    QGroupBox* _groupBoxMaillage;
    QGroupBox* _groupBoxResultat;
    QWidget* _paramsTabWidget;
    QWidget* _meteoTabWidget;

    // Presentation des resultats
    QLineEdit* _lineEditNomResultat;
    QPushButton* _pushButtonResultat;

    // Workaround issue #138
    // QLabel* _labelStoreGlobalMatrix;
    // QCheckBox* _checkBoxStoreGlobalMatrix;

    QGridLayout* _calculLayout;

    TYElementWidget* _elmW;

    QWidget* _maillagesWidget;
    TYEtatsWidget* _etatsWidget;

    QButtonGroup* _buttonGroupState;

    QRadioButton* _pRadioButtonActif;
    QRadioButton* _pRadioButtonLocked;

    /// Gestion des differentes options sous forme d'onglet
    QTabWidget* _tabWidget;

    /// Onglet des points de controle
    QTableWidget* _tableauPointControle;

    /// Onglet des points de controle
    QTableWidget* _tableauMaillages;
};

#endif // __TY_CALCUL_WIDGET__
