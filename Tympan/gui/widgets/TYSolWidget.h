/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYSolWidget.h
 * \brief outil IHM pour un sol (fichier header)
 * \author Projet_Tympan
 *
 */

#ifndef __TY_SOL_WIDGET__
#define __TY_SOL_WIDGET__

#include "TYWidget.h"
#include <QGridLayout>
#include <QLabel>

class TYSol;
class TYLineEdit;
class QGridLayout;
class QLabel;
class QCheckBox;
class QGroupBox;
class QPushButton;
class TYElementWidget;

/**
 * \class TYSolWidget
 * \brief classe de l'objet IHM pour un sol
 */
class TYSolWidget : public TYWidget
{
    Q_OBJECT

    TY_DECL_METIER_WIDGET(TYSol)

    // Methodes
public:
    /**
     * Constructeur.
     */
    TYSolWidget(TYSol* pElement, QWidget* _pParent = NULL);
    /**
     * Destructeur.
     */
    virtual ~TYSolWidget();

public slots:
    virtual void updateContent();
    virtual void apply();
    void editResistivite();

    // Membres
protected:
    QGroupBox* _groupBoxDefaultSolver;
    QGroupBox* _groupBox9613Solver;
    TYLineEdit* _lineEditEpaisseur;
    TYLineEdit* _lineEditResistivite;
    TYLineEdit* _lineEditEcartType;
    TYLineEdit* _lineEditLongueur;
    TYLineEdit* _lineEditFacteurG;
    QLabel* _labelEpaisseur;
    QLabel* _labelResistivite;
    QLabel* _labelEcartType;
    QLabel* _labelLongueur;
    QLabel* _labelFacteurG;

    QPushButton* _pushButtonResistivite;

protected:
    QGridLayout* _solLayout;
    QGridLayout* _groupBoxDefaultSolverLayout;
    QGridLayout* _groupBox9613SolverLayout;

    TYElementWidget* _elmW;
};

#endif // __TY_SOL_WIDGET__
