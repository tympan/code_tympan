/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYDoubleSpinBox.cpp
 * \brief SpinBox converting comas to points
 * \author Projet_Tympan
 *
 */

#include <QKeyEvent>

#include "TYDoubleSpinBox.h"

TYDoubleSpinBox::TYDoubleSpinBox(QWidget* parent) : QDoubleSpinBox(parent)
{
    init();
}

void TYDoubleSpinBox::init()
{
    QLocale locale = QLocale(QLocale::C);
    setLocale(locale);
}

void TYDoubleSpinBox::keyPressEvent(QKeyEvent* evt)
{

    if (evt->key() == Qt::Key_Comma)
    {
        evt = new QKeyEvent(QEvent::KeyPress, Qt::Key_Period, Qt::NoModifier, 0, 0, 0, ".");
    }

    QDoubleSpinBox::keyPressEvent(evt);
}
