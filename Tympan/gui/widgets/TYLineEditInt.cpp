/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYLineEdit.h
 * \brief outil IHM pour une entrée utilisateur de type int (fichier body)
 * \author Projet_Tympan
 *
 */

#include <QKeyEvent>

#include "TYLineEditInt.h"

TYLineEditInt::TYLineEditInt(QWidget* parent) : TYLineEdit(parent)
{
    init();
}

TYLineEditInt::~TYLineEditInt() {}

void TYLineEditInt::init()
{
    QIntValidator* validator = new QIntValidator();
    QLocale* locale = new QLocale(QLocale::C);
    locale->setNumberOptions(QLocale::RejectGroupSeparator);
    validator->setLocale(*locale);

    setValidator(validator);
    connect();
}

void TYLineEditInt::keyPressEvent(QKeyEvent* evt)
{

    if (evt->key() == Qt::Key_Comma)
    {
        return;
    }

    QLineEdit::keyPressEvent(evt);
}
