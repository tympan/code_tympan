/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYLinearMaillageWidget.cpp
 * \brief Outil IHM pour un maillage lineaire
 */

// Added by qt3to4:
#include <QGridLayout>
#include <QLabel>

#include "Tympan/models/business/OLocalizator.h"
#include "Tympan/models/business/TYLinearMaillage.h"
#include "Tympan/models/business/TYProjet.h"
#include "Tympan/gui/widgets/TYMaillageWidget.h"
#include "Tympan/gui/widgets/TYLineEdit.h"
#include "TYLinearMaillageWidget.h"

#define TR(id) OLocalizator::getString("TYLinearMaillageWidget", (id))

TYLinearMaillageWidget::TYLinearMaillageWidget(TYLinearMaillage* pElement, QWidget* _pParent /*=NULL*/)
    : TYWidget(pElement, _pParent)
{

    _maillageW = new TYMaillageWidget(pElement, this);

    resize(300, 200);
    setWindowTitle(TR("id_caption"));
    QGridLayout* pLinearMaillageLayout = new QGridLayout();
    setLayout(pLinearMaillageLayout);

    pLinearMaillageLayout->addWidget(_maillageW, 0, 0);

    QGroupBox* pGroupBox = new QGroupBox(this);
    QGridLayout* pGroupBoxLayout = new QGridLayout();
    pGroupBox->setLayout(pGroupBoxLayout);

    QLabel* pLabelDistance = new QLabel(TR("id_distance"), pGroupBox);
    _lineEditDistance = new TYLineEdit(pGroupBox);
    QObject::connect(_lineEditDistance, &QLineEdit::textChanged, this,
                     &TYLinearMaillageWidget::updateNbPoints);
    QLabel* pLabelDistanceUnite = new QLabel(TR("id_distance_unit"), pGroupBox);
    pGroupBoxLayout->addWidget(pLabelDistance, 0, 0);
    pGroupBoxLayout->addWidget(_lineEditDistance, 0, 1);
    pGroupBoxLayout->addWidget(pLabelDistanceUnite, 0, 2);

    pLinearMaillageLayout->addWidget(pGroupBox, 1, 0);

    QGroupBox* pGroupBoxSegment = new QGroupBox(this);
    pGroupBoxSegment->setTitle(TR("id_segment"));
    QGridLayout* pGroupBoxSegmentLayout = new QGridLayout();
    pGroupBoxSegment->setLayout(pGroupBoxSegmentLayout);

    _lineEditNomSegment = new QLineEdit(pGroupBoxSegment);
    _lineEditNomSegment->setEnabled(false);
    pGroupBoxSegmentLayout->addWidget(_lineEditNomSegment, 0, 0);

    QLabel* pLongueurLabel = new QLabel(TR("id_longueur"), pGroupBoxSegment);
    pGroupBoxSegmentLayout->addWidget(pLongueurLabel, 0, 1);
    _lineEditLongueur = new TYLineEdit(pGroupBoxSegment);
    _lineEditLongueur->setReadOnly(true);
    pGroupBoxSegmentLayout->addWidget(_lineEditLongueur, 0, 2);

    QPushButton* pPushButtonSegment = new QPushButton(TR("id_proprietes_button"), pGroupBoxSegment);
    pGroupBoxSegmentLayout->addWidget(pPushButtonSegment, 0, 3);

    pLinearMaillageLayout->addWidget(pGroupBoxSegment, 2, 0);

    updateContent();

    connect(pPushButtonSegment, &QPushButton::clicked, this, &TYLinearMaillageWidget::editSegment);
}

TYLinearMaillageWidget::~TYLinearMaillageWidget() {}

void TYLinearMaillageWidget::updateContent()
{
    _maillageW->updateContent();

    if (getElement()->getSegment())
    {
        _lineEditNomSegment->setText(getElement()->getSegment()->getName());
        _lineEditLongueur->setText(QString().setNum(getElement()->getSegment()->longueur(), 'f', 2));
    }

    _lineEditDistance->setText(QString().setNum(1.0f / (getElement()->getDensite()), 'f', 2));
}

void TYLinearMaillageWidget::updateNbPoints()
{
    double longueur = _lineEditLongueur->text().toDouble();
    double densite = 1.0f / (_lineEditDistance->text().toDouble());

    _maillageW->updateNbPoints(longueur * densite);
}

void TYLinearMaillageWidget::apply()
{
    _maillageW->apply();
    LPTYProjet pProj = dynamic_cast<TYProjet*>(getElement()->getParent());

    // Si la densite est differente
    if ((getElement()->getDistance() != _lineEditDistance->text().toDouble()) && getElement()->getSegment())
    {
        getElement()->make(getElement()->getSegment(), 1.0f / (_lineEditDistance->text().toDouble()));
        dynamic_cast<TYProjet*>(getElement()->getParent())->updateCalculsWithMaillage(getElement());

        // La densite a changee, il faut mettre a jour l'altimetrie
        if (pProj && pProj->getSite()->getAltimetry()->containsData())
        {
            pProj->updateAltiRecepteurs();
        }
    }
    if (pProj)
    {
        pProj->updateGraphic();
    }

    emit modified();
}

void TYLinearMaillageWidget::editSegment()
{
    int ret = getElement()->getSegment()->edit(this);

    if (ret == QDialog::Accepted)
    {
        _lineEditNomSegment->setText(getElement()->getSegment()->getName());
    }
}
