/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYSourceWidget.cpp
 * \brief outil IHM pour une source
 *
 *
 */

#include "Tympan/models/business/OLocalizator.h"
#include "Tympan/models/business/acoustic/TYSource.h"
#include <qmenu.h>
#include <qbuttongroup.h>

#include "Tympan/models/business/acoustic/TYSpectre.h"
#include "TYSourceWidget.h"

#define TR(id) OLocalizator::getString("TYSourceWidget", (id))
#define IMG(id) OLocalizator::getPicture("TYSourceWidget", (id))

TYSourceWidget::TYSourceWidget(TYSource* pElement, QWidget* _pParent /*=NULL*/) : TYWidget(pElement, _pParent)
{
    _elmW = new TYElementWidget(pElement, this);
    //  _colorW = new TYColorInterfaceWidget(pElement, this);

    // resize( 300, 220 );
    setWindowTitle(TR("id_caption"));
    _sourceLayout = new QGridLayout();

    _sourceLayout->addWidget(_elmW, 0, 0);
    //  _sourceLayout->addWidget( _colorW, 1, 0);

    _groupBox = new QGroupBox(this);
    _groupBox->setTitle(TR("id_tabspectre_box"));
    _groupBoxLayout = new QGridLayout();
    _groupBox->setLayout(_groupBoxLayout);

    _pushButtonShowSpectre = new QPushButton(_groupBox);
    _pushButtonShowSpectre->setText(TR("id_button_spectre"));

    _groupBoxLayout->addWidget(_pushButtonShowSpectre, 0, 0);

    _sourceLayout->addWidget(_groupBox, 2, 0);

    updateContent();

    connect(_pushButtonShowSpectre, &QPushButton::clicked, this, &TYSourceWidget::showSpectre);
    _sourceLayout->setContentsMargins(0, 0, 0, 0);

    setLayout(_sourceLayout);
}

TYSourceWidget::~TYSourceWidget() {}

void TYSourceWidget::updateContent()
{
    _elmW->updateContent();
    //  _colorW->updateContent();
}

void TYSourceWidget::apply()
{
    _elmW->apply();
    //  _colorW->apply();

    emit modified();
}

void TYSourceWidget::showSpectre()
{
    TYSpectre* spectre = getElement()->getCurrentSpectre();
    spectre->edit(this);
}
