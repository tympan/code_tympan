/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYSolverParamsWidgetManager.h
 * \brief Objet permettant de gerer les differents widgets servant a parametriser le solveur
 * \author Projet_Tympan
 *
 */

#ifndef TYSOLVERPARAMSWIDGETMANAGER_H
#define TYSOLVERPARAMSWIDGETMANAGER_H

#include <QString>
#include <QMap>

#include "TYSolverParamsWidget.h"

class TYSolverParamsWidgetManager : public QObject
{
    Q_OBJECT

public:
    TYSolverParamsWidgetManager();
    TYSolverParamsWidgetManager(QString dataModelPath);

    void readDataModel(QString dataModelPath);
    TYSolverParamsDataModel* getDataModel(QString paramName);
    TYSolverParamsWidget* getWidget(QString paramName);
    TYSolverParamsWidget* makeWidget(QString paramName);
    TYSolverParamsWidget* makeWidget(TYSolverParamsDataModel* dataModel);
    void registerWidget(TYSolverParamsWidget* widget);
    void updateWidgets(QString newParamValues);
    QString getSolverParams()
    {
        return _solverParams;
    }
    void setSolverParams(QString newSolverParams)
    {
        _solverParams = newSolverParams;
    }
    void forceSyncParams();

private:
    QMap<QString, TYSolverParamsDataModel*> _dataModelMap;
    QMap<QString, TYSolverParamsWidget*> _solverParamsWidgets;
    QString _solverParams;

public slots:
    void widgetEdited(QString newValue);
};

#endif // TYSOLVERPARAMSWIDGETMANAGER_H
