/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYAcousticRectangleNodeWidget.h
 * \brief outil IHM pour un ensemble de rectangles acoustiques (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_ACOUSTICRECTANGLENODE_WIDGET__
#define __TY_ACOUSTICRECTANGLENODE_WIDGET__

#include "TYWidget.h"
#include <qlayout.h>
// Added by qt3to4:
#include <QGridLayout>
#include <QLabel>

class TYAcousticRectangleNode;
class TYAcousticSurfaceNodeWidget;
class QLineEdit;
class QGridLayout;
class QLabel;
class QGroupBox;

/**
 * \class TYAcousticRectangleNodeWidget
 * \brief Classe de l'objet IHM pour un ensemble de rectangles acoustiques
 */
class TYAcousticRectangleNodeWidget : public TYWidget
{
    Q_OBJECT

    TY_DECL_METIER_WIDGET(TYAcousticRectangleNode)

    // Methodes
public:
    /**
     * Constructeur.
     */
    TYAcousticRectangleNodeWidget(TYAcousticRectangleNode* pElement, QWidget* _pParent = NULL);
    /**
     * Destructeur.
     */
    virtual ~TYAcousticRectangleNodeWidget();

    /**
     * Permet de modifier les parametres du layout.
     *
     * @param margin La valeur de la marge.
     * @param spacing La valeur de l'espacement.
     */
    void setLayoutSpacing(int margin, int spacing)
    {
        _acousticRectangleNodeLayout->setContentsMargins(margin, margin, margin, margin);
        _acousticRectangleNodeLayout->setSpacing(spacing);
    }

public slots:
    virtual void updateContent();
    virtual void apply();

    // Membres
protected:
    TYAcousticSurfaceNodeWidget* _elmW;
    QGridLayout* _acousticRectangleNodeLayout;
};

#endif // __TY_ACOUSTICRECTANGLENODE_WIDGET__
