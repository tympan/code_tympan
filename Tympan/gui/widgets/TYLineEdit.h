/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYLineEdit.h
 * \brief outil IHM pour une entrée utilisateur (fichier header)
 * \author Projet_Tympan
 *
 */

#ifndef __TY_LINE_EDIT__
#define __TY_LINE_EDIT__

#include <QLineEdit>
#include <QRegExpValidator>
#include <QPushButton>

class TYLineEdit;

class TYLineEdit : public QLineEdit
{
    Q_OBJECT

public:
    /**
     * Constructor
     */
    TYLineEdit(const QString& contents, bool isZeroOK = true, bool canBeNegative = true,
               QWidget* parent = nullptr);
    /**
     * Constructor
     */
    TYLineEdit(const QString& contents, QWidget* parent = nullptr);
    /**
     * Constructor
     */
    TYLineEdit(QWidget* parent = nullptr);

    /**
     * Initialize the widget
     */
    virtual void init();

    /**
     * overload the QLineEdit method
     * If the key pressed is a coma, replace it with a point
     */
    virtual void keyPressEvent(QKeyEvent* evt);

    /**
     * Connect the signal'textChanged' with the slot 'adjustTextColor'
     */
    void connect();

public slots:
    /**
     * If the text is not a double with at most 2 decimal numbers, it is colored red
     */
    void adjustTextColor();

private:
    bool _isZeroOK = true;
    bool _canBeNegative = true;
};

#endif