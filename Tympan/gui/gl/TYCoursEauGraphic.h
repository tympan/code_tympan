/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYCoursEauGraphic.h
 * \brief Representation graphique d'un cours d'eau (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_COURS_EAU_GRAPHIC__
#define __TY_COURS_EAU_GRAPHIC__

#if _MSC_VER > 1000
    #pragma once
#endif // _MSC_VER > 1000

#include "TYAcousticLineGraphic.h"
class TYCoursEau;

/**
 * \class TYCoursEauGraphic
 * \brief classe graphique pour un cours d'eau
 */
class TYCoursEauGraphic : public TYAcousticLineGraphic
{
    TY_DECL_METIER_GRAPHIC(TYCoursEau)

    // Methodes
public:
    TYCoursEauGraphic(TYCoursEau* pElement);
    virtual ~TYCoursEauGraphic() {}
};

#endif // __TY_COURS_EAU_GRAPHIC__
