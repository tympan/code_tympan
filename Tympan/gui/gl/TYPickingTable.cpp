/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYPickingTable.cpp
 * \brief Gestion de la table de correspondance indice/element pour le picking
 *
 *
 */

#include "TYPickingTable.h"

TYTabPtrElement TYPickingTable::_elements;
int TYPickingTable::_index;

TYPickingTable::TYPickingTable()
{
    _index = -1;
}

TYPickingTable::~TYPickingTable()
{
    _elements.clear();
}

void TYPickingTable::addElement(TYElement* pElt)
{
    _elements.push_back(pElt);
    _index++;
}

TYElement* TYPickingTable::getElement(int index)
{
    TYElement* pElt = NULL;

    if (index < (int)_elements.size())
    {
        pElt = _elements[index];
    }

    return pElt;
}

void TYPickingTable::purgeElements()
{
    _elements.clear();
    _index = -1;
}
