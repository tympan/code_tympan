/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYAcousticRectangleNodeGraphic.cpp
 * \brief Representation graphique d'un ensemble de rectangles acoustiques
 *
 *
 */

#include "Tympan/models/business/geoacoustic/TYAcousticRectangleNode.h"

#include "TYAcousticRectangleNodeGraphic.h"

TYAcousticRectangleNodeGraphic::TYAcousticRectangleNodeGraphic(TYAcousticRectangleNode* pElement)
    : TYAcousticSurfaceNodeGraphic(pElement)
{
}

void TYAcousticRectangleNodeGraphic::update(bool force /*=false*/)
{
    if (getModified() || force)
    {
        getElement()->updateGrid();
    }

    TYAcousticSurfaceNodeGraphic::update(force);
}

void TYAcousticRectangleNodeGraphic::computeBoundingBox()
{
    OBox reset;
    _boundingBox = reset;

    TYAcousticSurfaceNodeGraphic::computeBoundingBox();
}

void TYAcousticRectangleNodeGraphic::display(TYElement* pModelerElement /*= nullptr*/,
                                             GLenum mode /*= GL_RENDER*/)
{
    TYAcousticSurfaceNodeGraphic::display(pModelerElement, mode);
}
