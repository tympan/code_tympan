/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYAcousticPolygonGraphic.cpp
 * \brief Representation graphique d'un polygone accoustique
 */

#include "Tympan/models/business/geoacoustic/TYAcousticPolygon.h"
#include "Tympan/gui/gl/TYPickingTable.h"
#include "TYAcousticPolygonGraphic.h"

TYAcousticPolygonGraphic::TYAcousticPolygonGraphic(TYAcousticPolygon* pElement) : TYElementGraphic(pElement)
{
}

void TYAcousticPolygonGraphic::update(bool force /*=false*/)
{
    TYElementGraphic::update(force);
}

void TYAcousticPolygonGraphic::getChilds(TYListPtrTYElementGraphic& childs, bool recursif /*=true*/)
{
    TYElementGraphic* pTYElementGraphic = nullptr;
    pTYElementGraphic = getElement()->getSrcSurf()->getGraphicObject();
    childs.push_back(pTYElementGraphic);
    if (recursif)
    {
        pTYElementGraphic->getChilds(childs, recursif);
    }

    pTYElementGraphic = getElement()->getPolygon()->getGraphicObject();
    childs.push_back(pTYElementGraphic);
    if (recursif)
    {
        pTYElementGraphic->getChilds(childs, recursif);
    }
}

void TYAcousticPolygonGraphic::computeBoundingBox()
{
    OBox reset;
    _boundingBox = reset;

    getElement()->getSrcSurf()->getGraphicObject()->computeBoundingBox();
    getElement()->getPolygon()->getGraphicObject()->computeBoundingBox();

    _boundingBox.Enlarge(getElement()->getSrcSurf()->getGraphicObject()->GetBox());
    _boundingBox.Enlarge(getElement()->getPolygon()->getGraphicObject()->GetBox());
}

void TYAcousticPolygonGraphic::display(TYElement* pModelerElement /*= nullptr*/, GLenum mode /*= GL_RENDER*/)
{
    OColor color = getElement()->getColor();
    color.a = 1.0;
    glColor4fv(color);
    getElement()->getSrcSurf()->getGraphicObject()->display(pModelerElement, mode);
    getElement()->getPolygon()->getGraphicObject()->highlight(_highlight);

    if (mode == GL_SELECT)
    {
        TYPickingTable::addElement(getElement());
        glPushName((GLuint)(TYPickingTable::getIndex()));
    }

    getElement()->getPolygon()->getGraphicObject()->display(pModelerElement, mode);

    if (mode == GL_SELECT)
    {
        glPopName();
    }
}
