/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYPolyLineGraphic.h
 * \brief Representation graphique d'une polyligne (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_POLY_LINE_GRAPHIC__
#define __TY_POLY_LINE_GRAPHIC__

#if _MSC_VER > 1000
    #pragma once
#endif // _MSC_VER > 1000

#include "Tympan/models/business/TYDefines.h"
#include "TYElementGraphic.h"
#include "Tympan/models/business/geometry/TYPoint.h"

/**
 * \class TYPolyLineGraphic
 * \brief classe graphique pour representer une polyligne a partir d'un tableau de points.
 */
class TYPolyLineGraphic : public TYElementGraphic
{
    TY_DECL_METIER_GRAPHIC(TYElement)

    // Methodes
public:
    /// Default line width in screen pixels
    /// (cf OpenGL doc for more details of meaning when coupled with anti-aliasing)
    static const unsigned int default_width_px = 2;

    /**
     * Constructor
     */
    TYPolyLineGraphic(bool closed = false);

    /**
     * Destructor
     */
    virtual ~TYPolyLineGraphic();

    /**
     * Updates the 3D object associated to this object
     * Note : When this method is overloaded, the parent implementation
     * (TYElementGraphic::update()) must be called at the end
     *
     * @param force overstep the state of the flag 'modified' and apply the update.
     */
    virtual void update(bool force = false);

    /**
     * Fonction d'affichage
     *
     * @param pModelerElement element du modeleur a afficher
     * @param mode mode d'affichage
     *
     */
    virtual void display(TYElement* pModelerElement = nullptr, GLenum mode = GL_RENDER);

    /**
     * Build the bounding box, with the min and max of each coordinate
     */
    virtual void computeBoundingBox();

    void setTabPoint(const TYTabPoint& tabPts)
    {
        _tabPts = tabPts;
        computeBoundingBox();
    }
    TYTabPoint& getTabPoint()
    {
        return _tabPts;
    }
    const TYTabPoint& getTabPoint() const
    {
        return _tabPts;
    }

    // Membres
protected:
    TYTabPoint _tabPts;
    bool _closed;

    GLfloat width; ///< Line width - cf OpenGL doc for exact meaning
};

/// Smart Pointer sur TYPolyLineGraphic.
typedef SmartPtr<TYPolyLineGraphic> LPTYPolyLineGraphic;

#endif // __TY_POLY_LINE_GRAPHIC__
