/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYAcousticSurfaceNodeGraphic.cpp
 * \brief Représentation graphique d'un ensemble de surfaces acoustiques
 */

#include "Tympan/models/business/geoacoustic/TYAcousticSurfaceNode.h"
#include "Tympan/gui/gl/TYPickingTable.h"
#include "TYAcousticSurfaceNodeGraphic.h"

TYAcousticSurfaceNodeGraphic::TYAcousticSurfaceNodeGraphic(TYAcousticSurfaceNode* pElement)
    : TYElementGraphic(pElement)
{
}

void TYAcousticSurfaceNodeGraphic::update(bool force /*=false*/) {}

void TYAcousticSurfaceNodeGraphic::getChilds(TYListPtrTYElementGraphic& childs, bool recursif /*=true*/)
{
    TYTabAcousticSurfaceGeoNode* pTab = &getElement()->getTabAcousticSurf();
    LPTYElementGraphic pTYElementGraphic = nullptr;
    for (int i = 0; i < pTab->size(); i++)
    {
        TYAcousticSurfaceGeoNode* pAccSurfGeoNode = pTab->operator[](i);
        pTYElementGraphic = pAccSurfGeoNode->getGraphicObject();
        childs.push_back(pTYElementGraphic.getRealPointer());
        if (recursif)
        {
            pTYElementGraphic->getChilds(childs, recursif);
        }
    }
}

void TYAcousticSurfaceNodeGraphic::computeBoundingBox()
{
    OBox reset;
    _boundingBox = reset;

    TYTabAcousticSurfaceGeoNode* pTab = &getElement()->getTabAcousticSurf();
    for (int i = 0; i < pTab->size(); i++)
    {
        TYAcousticSurfaceGeoNode* pAccSurfGeoNode = pTab->operator[](i);
        pAccSurfGeoNode->getGraphicObject()->computeBoundingBox();

        // CLM-NT33 : Correction calcul BB des machines
        //_boundingBox.Enlarge(pAccSurfGeoNode->getGraphicObject()->GetBox());
        OBox bb = pAccSurfGeoNode->getGraphicObject()->GetBox();
        bb.Translate(pAccSurfGeoNode->getORepere3D()._origin);
        _boundingBox.Enlarge(bb);
    }
}

void TYAcousticSurfaceNodeGraphic::display(TYElement* pModelerElement /*= nullptr*/,
                                           GLenum mode /*= GL_RENDER*/)
{
    // CLM-NT35: En overlay, affiche seulement le nom de l'élément localisé
    if (mode == GL_COMPILE)
    {
        drawName(pModelerElement);
    }
    else
    {
        if (_highlight)
        {
            if (_bFirstDisp)
            {
                computeBoundingBox();
                _bFirstDisp = false;
            }
            drawLineBoundingBox();
        }

        TYTabAcousticSurfaceGeoNode* pTab = &getElement()->getTabAcousticSurf();
        for (int i = 0; i < pTab->size(); i++)
        {
            // Get face child
            TYAcousticSurfaceGeoNode* pAccSurfGeoNode = pTab->operator[](i);

            if (mode == GL_SELECT)
            {
                TYPickingTable::addElement(getElement());
                glPushName((GLuint)(TYPickingTable::getIndex()));
            }

            // pAccSurfGeoNode->getElement()->getGraphicObject()->highlight(_highlight); //sm++ repercution du
            // highlight sur les elements composes.
            pAccSurfGeoNode->getGraphicObject()->display(pModelerElement, mode);

            if (mode == GL_SELECT)
            {
                glPopName();
            }
        }
    }
}
