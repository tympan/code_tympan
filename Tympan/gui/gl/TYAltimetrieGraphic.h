/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYAltimetrieGraphic.h
 * \brief Representation graphique de l'altimetrie (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_ALTIMETRIE_GRAPHIC__
#define __TY_ALTIMETRIE_GRAPHIC__

#if _MSC_VER > 1000
    #pragma once
#endif // _MSC_VER > 1000

#include "Tympan/gui/tools/OGLTexture2D.h"
#include "TYElementGraphic.h"
#include "TYPolygonGraphic.h"
#include <QImage>

class TYAltimetrie;

/**
 * \class TYAltimetrieGraphic
 * \brief classe graphique pour une altimetrie
 */
class TYAltimetrieGraphic : public TYElementGraphic
{
    TY_DECL_METIER_GRAPHIC(TYAltimetrie)

    // Methodes
public:
    /**
     * Constructor
     */
    TYAltimetrieGraphic(TYAltimetrie* pElement);

    /**
     * Destructor
     */
    virtual ~TYAltimetrieGraphic();

    /**
     * Updates the 3D object associated to this object
     * Note : When this method is overloaded, the parent implementation
     * (TYElementGraphic::update()) must be called at the end
     *
     * @param force overstep the state of the flag 'modified' and apply the update.
     */
    virtual void update(bool force = false);

    /**
     * Effectue le trace openGL de l'objet graphique
     *
     * @param pModelerElement element du modeleur a afficher
     * @param mode Gere les modes rendu et select pour le picking.
     */
    virtual void display(TYElement* pModelerElement = nullptr, GLenum mode = GL_RENDER);

    /**
     * Store each child of the box in childs. If recursif is true, also store the children
     * of each child
     */
    virtual void getChilds(TYListPtrTYElementGraphic& childs, bool recursif = true);

    /**
     * Build the bounding box, with the min and max of each coordinate
     */
    virtual void computeBoundingBox();

    /**
     * Set a background image
     *
     * @param QString sTopoFileName : name of the file containing the image
     * @param int semiX : half of the size of the image on the X axis
     * @param int semiY : half of the size of the image on the Y axis
     * @param TYPoint ptPosition : center of the image
     * @param OVector3D bgImageOrientation : orientation of the image
     */
    void setBackgroundImage(QString sTopoFileName, int semiX, int semiY, TYPoint ptPosition,
                            OVector3D bgImageOrientation);

    /**
     * Unset the background image
     */
    void unsetBackgroundImage();

protected:
    void bindTexture();
    OLookupTable _oColorMap;
    LPOGLTexture2D _pTex;
    bool _isBgImage;
    int _semiX;
    int _semiY;
    OVector3D _bgImageOrientation;
    TYPoint _imagePosition;
    GLuint _theTexture;
    double _angle;
    QString _sOldTopoFileName;
    QString _sTopoFileName;
    unsigned char* _imgDataPtr;
    int _imgXSize;
    int _imgYSize;
    int _imgBytesPerPixel;
};

#endif // __TY_ALTIMETRIE_GRAPHIC__
