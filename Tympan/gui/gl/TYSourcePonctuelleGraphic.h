/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYSourcePonctuelleGraphic.h
 * \brief Representation graphique d'une source ponctuelle (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_SOURCEPONCTUELLE_GRAPHIC__
#define __TY_SOURCEPONCTUELLE_GRAPHIC__

#if _MSC_VER > 1000
    #pragma once
#endif // _MSC_VER > 1000

class TYSourcePonctuelle;

#include "TYElementGraphic.h"
#include "Tympan/models/business/geometry/TYPoint.h"

/**
 * \class  TYSourcePonctuelleGraphic
 * \brief classe graphique pour une source ponctuelle
 */
class TYSourcePonctuelleGraphic : public TYElementGraphic
{
    TY_DECL_METIER_GRAPHIC(TYSourcePonctuelle)

    // Methodes
public:
    TYSourcePonctuelleGraphic(TYSourcePonctuelle* pElement);
    virtual ~TYSourcePonctuelleGraphic();

    /**
     * \fn virtual void update(bool)
     * \brief Mise a jour
     */
    virtual void update(bool force = false);

    /**
     * Fonction d'affichage
     *
     * @param pModelerElement element du modeleur a afficher
     * @param mode mode d'affichage
     *
     */
    virtual void display(TYElement* pModelerElement = nullptr, GLenum mode = GL_RENDER);

    /**
     * \fn  virtual void computeBoundingBox()
     * \brief calcul de la boite englobante
     * */
    virtual void computeBoundingBox();

    // Membres
protected:
    /// Taille de la source.
    float _size;

private:
    void drawCube(TYPoint* pPt) const;
    void drawPyramid(TYPoint* pPt) const;
    void drawSphere(TYPoint* pPt) const;
    void drawStar(TYPoint* pPt) const;
    void setSize();
};

#endif // __TY_SOURCEPONCTUELLE_GRAPHIC__
