/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYCalculGraphic.cpp
 * \brief Representation graphique d'un calcul
 */

#include "Tympan/models/business/TYProjet.h"
#include "Tympan/models/business/TYCalcul.h"
#include "Tympan/gui/gl/TYRayGraphic.h"
#include "TYCalculGraphic.h"

TYCalculGraphic::TYCalculGraphic(TYCalcul* pElement) : TYElementGraphic(pElement) {}

void TYCalculGraphic::update(bool force /*=false*/)
{
    TYElementGraphic::update(force);
}

void TYCalculGraphic::getChilds(TYListPtrTYElementGraphic& childs, bool recursif /*=true*/) {}

void TYCalculGraphic::display(TYElement* pModelerElement /*= nullptr*/, GLenum mode /*= GL_RENDER*/)
{
    if (TYRayGraphic::_gVisible)
    {
        TYTabRay& tabRays = getElement()->getTabRays();
        for (size_t i = 0; i < tabRays.size(); i++)
        {
            tabRays.at(i)->getGraphicObject()->display(pModelerElement, mode);
        }
    }
}
