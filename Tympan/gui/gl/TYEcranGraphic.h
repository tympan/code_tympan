/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYEcranGraphic.h
 * \brief Representation graphique d'un ecran (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_ECRAN_GRAPHIC__
#define __TY_ECRAN_GRAPHIC__

#if _MSC_VER > 1000
    #pragma once
#endif // _MSC_VER > 1000

#include "TYAcousticFaceSetGraphic.h"
class TYEcran;

/**
 * \class TYEcranGraphic
 * \brief classe graphique pour un ecran
 */
class TYEcranGraphic : public TYAcousticFaceSetGraphic
{
    TY_DECL_METIER_GRAPHIC(TYEcran)

    // Methodes
public:
    TYEcranGraphic(TYEcran* pElement);
    virtual ~TYEcranGraphic();

    /**
     * Effectue le trace openGL de l'objet graphique
     *
     * @param pModelerElement element du modeleur a afficher
     * @param mode Gere les modes rendu et select pour le picking.
     */
    virtual void display(TYElement* pModelerElement = nullptr, GLenum mode = GL_RENDER);
};

#endif // __TY_ECRAN_GRAPHIC__
