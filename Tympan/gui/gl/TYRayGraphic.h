/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef __TY_RAY_GRAPHIC__
#define __TY_RAY_GRAPHIC__

#if _MSC_VER > 1000
    #pragma once
#endif // _MSC_VER > 1000

#include "Tympan/models/business/geometry/TYPoint.h"
#include "TYElementGraphic.h"

class TYRay;
class acoustic_event;

/**
 * Classe graphique pour representer un rayon
 * utilise par le calcul.
 */
class TYRayGraphic : public TYElementGraphic
{
    TY_DECL_METIER_GRAPHIC(TYRay)

    // Methodes
public:
    /**
     * Constructor
     */
    TYRayGraphic(TYRay* pElement);

    /**
     * Destructor
     */
    virtual ~TYRayGraphic();

    /**
     * Updates the 3D object associated to this object
     * Note : When this method is overloaded, the parent implementation
     * (TYElementGraphic::update()) must be called at the end
     *
     * @param force overstep the state of the flag 'modified' and apply the update.
     */
    virtual void update(bool force = false);

    /**
     * Fonction d'affichage
     *
     * @param pModelerElement element du modeleur a afficher
     * @param mode mode d'affichage
     *
     */
    virtual void display(TYElement* pModelerElement = nullptr, GLenum mode = GL_RENDER);

    /**
     * Build the bounding box, with the min and max of each coordinate
     */
    virtual void computeBoundingBox();

    // Membres
public:
    /// Indique si toutes les instances sont visibles.
    static bool _gVisible;

protected:
    void getRayEventColor(acoustic_event& e);
    float _r, _g, _b; // current color of ray

    int _lastRayEventType;    /*!< Type du dernier evenement du rayon affiche */
    TYPoint _lastRayEventPos; /*!< Position du dernier evenement du rayon affiche */
    int _repeatRayEventCount; /*!< Compteur de repetition d'un meme type d'evenement */
};

/// Smart Pointer sur TYRayGraphic.
typedef SmartPtr<TYRayGraphic> LPTYRayGraphic;

#endif // __TY_RAY_GRAPHIC__
