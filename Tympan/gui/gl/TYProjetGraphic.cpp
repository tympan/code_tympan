/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYProjetGraphic.cpp
 * \brief Representation graphique d'un projet
 *
 *
 */

#include "Tympan/models/business/TYProjet.h"
#include "TYProjetGraphic.h"

TYProjetGraphic::TYProjetGraphic(TYProjet* pElement) : TYElementGraphic(pElement) {}

TYProjetGraphic::~TYProjetGraphic() {}

void TYProjetGraphic::update(bool force /*=false*/)
{
    TYElementGraphic::update(force);
}

void TYProjetGraphic::getChilds(TYListPtrTYElementGraphic& childs, bool recursif /*=true*/)
{
    TYElementGraphic* pTYElementGraphic = getElement()->getSite()->getGraphicObject().getRealPointer();
    childs.push_back(pTYElementGraphic);
    if (recursif)
    {
        pTYElementGraphic->getChilds(childs, recursif);
    }
    if (getElement()->getCurrentCalcul())
    {
        pTYElementGraphic = getElement()->getCurrentCalcul()->getGraphicObject().getRealPointer();
        childs.push_back(pTYElementGraphic);
        if (recursif)
        {
            pTYElementGraphic->getChilds(childs, recursif);
        }
    }

    // Points de controle
    unsigned int i = 0;
    for (i = 0; i < getElement()->getPointsControl().size(); i++)
    {
        pTYElementGraphic = getElement()->getPointControl(i)->getGraphicObject().getRealPointer();
        childs.push_back(pTYElementGraphic);
        if (recursif)
        {
            pTYElementGraphic->getChilds(childs, recursif);
        }
    }

    // Maillage
    TYTabMaillageGeoNode* pTabMaillage = &getElement()->getMaillages();
    LPTYMaillageGeoNode pMaillageGeoNode;
    for (i = 0; i < pTabMaillage->size(); i++)
    {
        pMaillageGeoNode = pTabMaillage->operator[](i);
        pTYElementGraphic = pMaillageGeoNode->getGraphicObject().getRealPointer();
        childs.push_back(pTYElementGraphic);
        if (recursif)
        {
            pTYElementGraphic->getChilds(childs, recursif);
        }
    }
}

void TYProjetGraphic::display(TYElement* pModelerElement /*= nullptr*/, GLenum mode /*= GL_RENDER*/)
{
    TYElementGraphic::display(pModelerElement, mode);

    // Site
    getElement()->getSite()->getGraphicObject()->display(pModelerElement, mode);

    // Draw project objects in root site node 3D referential
    float fSiteNodeAltitude = (float)getElement()->getSite()->getAltiEmprise();
    glTranslatef(0.0f, 0.0f, -fSiteNodeAltitude);

    // Points de controle
    unsigned int i = 0;
    for (i = 0; i < getElement()->getPointsControl().size(); i++)
    {
        getElement()->getPointControl(i)->getGraphicObject()->display(pModelerElement, mode);
    }

    // Maillage
    TYTabMaillageGeoNode* pTabMaillage = &getElement()->getMaillages();
    TYMaillageGeoNode* pMaillageGeoNode = NULL;
    for (i = 0; i < pTabMaillage->size(); i++)
    {
        pMaillageGeoNode = pTabMaillage->operator[](i);
        pMaillageGeoNode->getGraphicObject()->display(pModelerElement, mode);
    }

    // Calcul courant
    if (getElement()->getCurrentCalcul())
    {
        getElement()->getCurrentCalcul()->getGraphicObject()->display(pModelerElement, mode);
    }

    glTranslatef(0.0f, 0.0f, +fSiteNodeAltitude);

    activateSignal();
}

#if TY_USE_IHM
void TYProjetGraphic::connectUpdateSignal(QObject* pReceiver, const char* member)
{
    //  _pUpdateSignal->connect(pReceiver, member);
}
#endif // TY_USE_IHM

#if TY_USE_IHM
void TYProjetGraphic::disconnectUpdateSignal(QObject* pReceiver, const char* member)
{
    //  _pUpdateSignal->disconnect(pReceiver, member);
}
#endif // TY_USE_IHM
