/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYAcousticLineGraphic.cpp
 * \brief Representation graphique d'une ligne acoustique
 */

#include <boost/foreach.hpp>

#include "Tympan/models/business/geoacoustic/TYAcousticLine.h"
#include "Tympan/gui/gl/TYPickingTable.h"
#include "TYAcousticLineGraphic.h"

TYAcousticLineGraphic::TYAcousticLineGraphic(TYAcousticLine* pElement) : TYElementGraphic(pElement)
{
    _pPolyLineGraphic = new TYPolyLineGraphic(false);
    _pPolyLineGraphic->setElement(pElement);
}

void TYAcousticLineGraphic::update(bool force /*=false*/)
{
    TYElementGraphic::update(force);
}

void TYAcousticLineGraphic::getChilds(TYListPtrTYElementGraphic& childs, bool recursif /*=true*/)
{
    TYElementGraphic* pTYElementGraphic = _pPolyLineGraphic;
    childs.push_back(pTYElementGraphic);
    if (recursif)
    {
        pTYElementGraphic->getChilds(childs, recursif);
    }
}

void TYAcousticLineGraphic::computeBoundingBox()
{
    OBox reset;
    _boundingBox = reset;

    _pPolyLineGraphic->computeBoundingBox();
    _boundingBox.Enlarge(_pPolyLineGraphic->GetBox());
}

void TYAcousticLineGraphic::display(TYElement* pModelerElement /*= nullptr*/, GLenum mode /*= GL_RENDER*/)
{
    if (!getElement()->isInCurrentCalcul())
    {
        return;
    }

    _pPolyLineGraphic->highlight(_highlight);

    TYTabPoint& tabpts = _pPolyLineGraphic->getTabPoint();
    tabpts.clear();
    tabpts.reserve(getElement()->getTabPoint().size()); //->getSrcLineic()->getNbSrcs());

    BOOST_FOREACH (TYPoint pt, getElement()->getTabPoint()) //->getSrcLineic()->getSrcs())
    {
        tabpts.push_back(pt);
    }

    _pPolyLineGraphic->setTabPoint(tabpts);

    if (mode == GL_SELECT)
    {
        TYPickingTable::addElement(getElement());
        glPushName((GLuint)(TYPickingTable::getIndex()));
    }

    glColor4fv(getElement()->getColor());
    _pPolyLineGraphic->display(pModelerElement, mode);

    if (mode == GL_SELECT)
    {
        glPopName();
    }
}
