/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYPlanEauGraphic.cpp
 * \brief Representation graphique d'un plan d'eau
 *
 *
 */

#include "TYPlanEauGraphic.h"

#include "Tympan/models/business/topography/TYPlanEau.h"

#include "Tympan/models/business/TYPreferenceManager.h"
#include "Tympan/gui/gl/TYPickingTable.h"

TYPlanEauGraphic::TYPlanEauGraphic(TYPlanEau* pElement) : TYElementGraphic(pElement)
{
    _pPolygon = new TYPolygon();
}

void TYPlanEauGraphic::update(bool force /*=false*/)
{
    TYElementGraphic::update(force);
}

void TYPlanEauGraphic::getChilds(TYListPtrTYElementGraphic& childs, bool recursif /*=true*/)
{
    TYElementGraphic* pTYElementGraphic = _pPolygon->getGraphicObject();
    childs.push_back(pTYElementGraphic);
    if (recursif)
    {
        pTYElementGraphic->getChilds(childs, recursif);
    }
}

void TYPlanEauGraphic::computeBoundingBox()
{
    OBox reset;
    _boundingBox = reset;
    _pPolygon->getGraphicObject()->computeBoundingBox();
    _boundingBox = _pPolygon->getGraphicObject()->GetBox();
}

void TYPlanEauGraphic::display(TYElement* pModelerElement /*= nullptr*/, GLenum mode /*= GL_RENDER*/)
{
    if (!_visible)
    {
        return;
    }

    TYElementGraphic::display(pModelerElement, mode);

    TYTabPoint tabpoints = getElement()->getListPoints();

    // Ajout d'un offset pour limiter le flickering avec l'altimetrie
    for (unsigned int i = 0; i < tabpoints.size(); i++)
    {
        tabpoints[i]._z += 0.5; // Anciennement 0.05
    }

    _pPolygon->setPoints(tabpoints);

    if (mode == GL_SELECT)
    {
        TYPickingTable::addElement(getElement());
        glPushName((GLuint)(TYPickingTable::getIndex()));
    }

    glColor4fv(getElement()->getColor());

    _pPolygon->getGraphicObject()->highlight(_highlight);
    _pPolygon->getGraphicObject()->display(pModelerElement, mode);
    if ((_highlight) && (mode == GL_COMPILE))
    {
        computeBoundingBox();
        drawName(pModelerElement);
    }

    if (mode == GL_SELECT)
    {
        glPopName();
    }
}
