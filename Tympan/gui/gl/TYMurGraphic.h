/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYMurGraphic.h
 * \brief Representation graphique d'un mur (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_MUR_GRAPHIC__
#define __TY_MUR_GRAPHIC__

#if _MSC_VER > 1000
    #pragma once
#endif // _MSC_VER > 1000

#include "TYAcousticRectangleNodeGraphic.h"
class TYMur;

/**
 * \class TYMurGraphic
 * \brief classe graphique pour un mur
 */
class TYMurGraphic : public TYAcousticRectangleNodeGraphic
{
    TY_DECL_METIER_GRAPHIC(TYMur)

    // Methodes
public:
    TYMurGraphic(TYMur* pElement);
    virtual ~TYMurGraphic() {}
};

#endif // __TY_MUR_GRAPHIC__
