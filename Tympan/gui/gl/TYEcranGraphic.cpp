/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYEcranGraphic.cpp
 * \brief Representation graphique d'un ecran
 *
 *
 */

#include "Tympan/models/business/infrastructure/TYEcran.h"

#include "TYEcranGraphic.h"

TYEcranGraphic::TYEcranGraphic(TYEcran* pElement) : TYAcousticFaceSetGraphic(pElement) {}

TYEcranGraphic::~TYEcranGraphic() {}

void TYEcranGraphic::display(TYElement* pModelerElement /*= nullptr*/, GLenum mode /*= GL_RENDER*/)
{
    glColor4fv(getElement()->getColor());
    TYAcousticFaceSetGraphic::display(pModelerElement, mode);
}
