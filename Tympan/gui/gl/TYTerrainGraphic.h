/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYTerrainGraphic.h
 * \brief Representation graphique d'un terrain (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_TERRAIN_GRAPHIC__
#define __TY_TERRAIN_GRAPHIC__

#if _MSC_VER > 1000
    #pragma once
#endif // _MSC_VER > 1000

#include "Tympan/gui/tools/OGLTexture2D.h"
#include "Tympan/models/business/geometry/TYPolygon.h"
#include "TYPolyLineGraphic.h"

class TYTerrain;

/**
 * \class TYTerrainGraphic
 * \brief classe graphique pour un terrain
 */
class TYTerrainGraphic : public TYElementGraphic
{
    TY_DECL_METIER_GRAPHIC(TYTerrain)

    // Methodes
public:
    /**
     * Constructor
     */
    TYTerrainGraphic(TYTerrain* pElement);

    /**
     * Destructor
     */
    virtual ~TYTerrainGraphic() {}

    /**
     * Updates the 3D object associated to this object
     * Note : When this method is overloaded, the parent implementation
     * (TYElementGraphic::update()) must be called at the end
     *
     * @param force overstep the state of the flag 'modified' and apply the update.
     */
    virtual void update(bool force = false);

    /**
     * Fonction d'affichage
     *
     * @param pModelerElement element du modeleur a afficher
     * @param mode mode d'affichage
     *
     */
    virtual void display(TYElement* pModelerElement = nullptr, GLenum mode = GL_RENDER);

    /**
     * getter pour enfant
     *
     * @param childs liste des resultats
     * @param recursif mode recursif (vrai apr defaut)
     *
     */
    virtual void getChilds(TYListPtrTYElementGraphic& childs, bool recursif = true);

    /**
     * Build the bounding box, with the min and max of each coordinate
     */
    virtual void computeBoundingBox();

protected:
    void bindTexture();

    LPTYPolygon _pPolygon;
    LPTYPolyLineGraphic _pPolyLineGraphic;

    LPOGLTexture2D _pTex;
};

#endif // __TY_TERRAIN_GRAPHIC__
