/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYAcousticVolumeNodeGraphic.cpp
 * \brief Representation graphique d'un ensemble de volumes acoustiques
 *
 *
 */

#include "Tympan/models/business/geoacoustic/TYAcousticVolumeNode.h"

#include "TYAcousticVolumeNodeGraphic.h"

TYAcousticVolumeNodeGraphic::TYAcousticVolumeNodeGraphic(TYAcousticVolumeNode* pElement)
    : TYElementGraphic(pElement)
{
}

void TYAcousticVolumeNodeGraphic::update(bool force /*=false*/)
{
    TYElementGraphic::update(force);
}

void TYAcousticVolumeNodeGraphic::getChilds(TYListPtrTYElementGraphic& childs, bool recursif /*=true*/)
{
    TYTabAcousticVolumeGeoNode& pTab = getElement()->getTabAcousticVol();
    LPTYElementGraphic pTYElementGraphic;
    TYAcousticVolumeGeoNode* pAccVolGeoNode = NULL;

    for (unsigned int i = 0; i < pTab.size(); i++)
    {
        pAccVolGeoNode = pTab[i];
        pTYElementGraphic = pAccVolGeoNode->getGraphicObject();
        childs.push_back(pTYElementGraphic.getRealPointer());
        if (recursif)
        {
            pTYElementGraphic->getChilds(childs, recursif);
        }
    }
}

void TYAcousticVolumeNodeGraphic::display(TYElement* pModelerElement /*= nullptr*/,
                                          GLenum mode /*= GL_RENDER*/)
{
    if (!getElement()->isInCurrentCalcul())
    {
        return;
    }

    // CLM-NT35: En overlay, affiche seulement le nom de l'élément localisé
    if (mode == GL_COMPILE)
    {
        drawName(pModelerElement);
    }
    else
    {
        if (_highlight)
        {
            if (_bFirstDisp)
            {
                computeBoundingBox();
                _bFirstDisp = false;
            }
            drawLineBoundingBox();

            // Calcul du volume englobant pour le fit:
            _globalBoundingBox.Enlarge(_boundingBox);
        }

        TYTabAcousticVolumeGeoNode* pTab = &getElement()->getTabAcousticVol();
        TYAcousticVolumeGeoNode* pAccVolGeoNode = NULL;

        for (unsigned int i = 0; i < pTab->size(); i++)
        {
            // Get face child
            pAccVolGeoNode = pTab->operator[](i);
            pAccVolGeoNode->getGraphicObject()->display(pModelerElement, mode);
        }
    }
}

void TYAcousticVolumeNodeGraphic::computeBoundingBox()
{
    OBox reset;
    _boundingBox = reset;

    TYTabAcousticVolumeGeoNode* pTab = &getElement()->getTabAcousticVol();
    TYAcousticVolumeGeoNode* pAccVolGeoNode = NULL;

    for (unsigned int i = 0; i < pTab->size(); i++)
    {
        // Get face child
        pAccVolGeoNode = pTab->operator[](i);
        pAccVolGeoNode->getGraphicObject()->computeBoundingBox();

        // CLM-NT33 : Correction calcul BB des machines
        //_boundingBox.Enlarge(pAccVolGeoNode->getGraphicObject()->GetBox());
        OBox bb = pAccVolGeoNode->getGraphicObject()->GetBox();
        OPoint3D origin = pAccVolGeoNode->getORepere3D()._origin;
        bb.Translate(origin);
        _boundingBox.Enlarge(bb);
        // CLM-NT33
    }

    /*
    TYTabPoint sommets = getElement()->sommets();

    size_t nbPts = sommets.size();
    TYPoint pt;
    for (size_t i = 0; i < nbPts; i++)
    {
        pt = sommets[i];
        _boundingBox.Enlarge((float)(pt._x), (float)(pt._y), (float)(pt._z));
    }*/
}
