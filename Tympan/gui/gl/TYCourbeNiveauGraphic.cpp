/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYCourbeNiveauGraphic.cpp
 * \brief Representation graphique d'une courbe de niveau
 */

#include "Tympan/models/business/topography/TYCourbeNiveau.h"
#include "Tympan/gui/gl/TYPickingTable.h"
#include "TYCourbeNiveauGraphic.h"

TYCourbeNiveauGraphic::TYCourbeNiveauGraphic(TYCourbeNiveau* pElement) : TYElementGraphic(pElement)
{
    _pPolyLineGraphic = new TYPolyLineGraphic(false);
    _pPolyLineGraphic->setElement(pElement);
}

void TYCourbeNiveauGraphic::update(bool force /*=false*/)
{
    TYElementGraphic::update(force);
}

void TYCourbeNiveauGraphic::getChilds(TYListPtrTYElementGraphic& childs, bool recursif /*=true*/)
{
    TYElementGraphic* pTYElementGraphic = _pPolyLineGraphic;
    childs.push_back(pTYElementGraphic);
    if (recursif)
    {
        pTYElementGraphic->getChilds(childs, recursif);
    }
}

void TYCourbeNiveauGraphic::computeBoundingBox()
{
    OBox reset;
    _boundingBox = reset;
    _pPolyLineGraphic->computeBoundingBox();
    _boundingBox.Enlarge(_pPolyLineGraphic->GetBox());
}

void TYCourbeNiveauGraphic::display(TYElement* pModelerElement /*= nullptr*/, GLenum mode /*= GL_RENDER*/)
{
    if (!_visible)
    {
        return;
    }

    TYElementGraphic::display(pModelerElement, mode);
    TYTabPoint tabpoints = getElement()->getListPoints();

    // Ajout d'un offset pour voir la courbe de niveau sur l'image site
    for (unsigned int i = 0; i < tabpoints.size(); i++)
    {
        tabpoints[i]._z += 0.5; // Anciennement 0.05
    }

    _pPolyLineGraphic->setModified(true);
    _pPolyLineGraphic->setTabPoint(tabpoints);

    if (mode == GL_SELECT)
    {
        TYPickingTable::addElement(getElement());
        glPushName((GLuint)(TYPickingTable::getIndex()));
    }
    GLenum errorCode = glGetError();
    if (errorCode != GL_NO_ERROR)
    {
        const unsigned char* sError = gluErrorString(errorCode);
        printf("Error OpenGL %d : %s\n", errorCode, sError);
    }
    glColor4fv(getElement()->getColor());
    errorCode = glGetError();
    if (errorCode != GL_NO_ERROR)
    {
        const unsigned char* sError = gluErrorString(errorCode);
        printf("Erreur OpenGL %d : %s\n", errorCode, sError);
    }
    float color[4];                       // az++ test
    glGetFloatv(GL_CURRENT_COLOR, color); // az++ test
    errorCode = glGetError();
    if (errorCode != GL_NO_ERROR)
    {
        const unsigned char* sError = gluErrorString(errorCode);
        printf("Erreur OpenGL %d : %s\n", errorCode, sError);
    }
    _pPolyLineGraphic->highlight(getHighlightState()); // az++
    _pPolyLineGraphic->display(pModelerElement, mode);

    if (mode == GL_SELECT)
    {
        glPopName();
    }
}
