/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYAcousticSemiCircleGraphic.h
 * \brief Représentation graphique d'1/2 cercle acoustique (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_ACOUSTIC_SEMI_CIRCLE_GRAPHIC__
#define __TY_ACOUSTIC_SEMI_CIRCLE_GRAPHIC__

#if _MSC_VER > 1000
    #pragma once
#endif // _MSC_VER > 1000

#include "TYElementGraphic.h"
class TYAcousticSemiCircle;

/**
 * \class TYAcousticSemiCircleGraphic
 * \brief classe graphique pour 1/2 cercle acoustique
 */
class TYAcousticSemiCircleGraphic : public TYElementGraphic
{
    TY_DECL_METIER_GRAPHIC(TYAcousticSemiCircle)

    // Methodes
public:
    /**
     * Constructor
     */
    TYAcousticSemiCircleGraphic(TYAcousticSemiCircle* pElement);

    /**
     * Destructor
     */
    virtual ~TYAcousticSemiCircleGraphic();

    /**
     * Updates the 3D object associated to this object
     * Note : When this method is overloaded, the parent implementation
     * (TYElementGraphic::update()) must be called at the end
     *
     * @param force overstep the state of the flag 'modified' and apply the update.
     */
    virtual void update(bool force = false);

    /**
     * Effectue le trace openGL de l'objet graphique
     *
     * @param pModelerElement element du modeleur a afficher
     * @param mode Gere les modes rendu et select pour le picking.
     */
    virtual void display(TYElement* pModelerElement = nullptr, GLenum mode = GL_RENDER);

    /**
     * Build the bounding box, with the min and max of each coordinate
     */
    virtual void computeBoundingBox();
};

#endif // __TY_ACOUSTIC_SEMI_CIRCLE_GRAPHIC__
