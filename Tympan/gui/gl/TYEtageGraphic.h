/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYEtageGraphic.h
 * \brief Representation graphique d'un etage (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_ETAGE_GRAPHIC__
#define __TY_ETAGE_GRAPHIC__

#if _MSC_VER > 1000
    #pragma once
#endif // _MSC_VER > 1000

#include "TYElementGraphic.h"
class TYEtage;

/**
 * \class TYEtageGraphic
 * \brief classe graphique pour un etage
 */
class TYEtageGraphic : public TYElementGraphic
{
    TY_DECL_METIER_GRAPHIC(TYEtage)
    TYElementGraphic* _pPolyLineGraphic; // az++
    // Methodes
public:
    /**
     * Constructor
     */
    TYEtageGraphic(TYEtage* pElement);

    /**
     * Destructor
     */
    virtual ~TYEtageGraphic();

    /**
     * Updates the 3D object associated to this object
     * Note : When this method is overloaded, the parent implementation
     * (TYElementGraphic::update()) must be called at the end
     *
     * @param force overstep the state of the flag 'modified' and apply the update.
     */
    virtual void update(bool force = false);

    /**
     * Fonction d'affichage
     *
     * @param pModelerElement element du modeleur a afficher
     * @param mode mode d'affichage
     *
     */
    virtual void display(TYElement* pModelerElement = nullptr, GLenum mode = GL_RENDER);

    /**
     * Store each child of the box in childs. If recursif is true, also store the children
     * of each child
     */
    virtual void getChilds(TYListPtrTYElementGraphic& childs, bool recursif = true);

    /**
     * Build the bounding box, with the min and max of each coordinate
     */
    virtual void computeBoundingBox();
};

#endif // __TY_ETAGE_GRAPHIC__
