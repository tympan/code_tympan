/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * \file TYAcousticFaceSetGraphic.cpp
 * \brief Representation graphique d'un ensemble de faces acoustiques
 */

#include "Tympan/models/business/geometry/TYFaceSet.h"
#include "Tympan/models/business/geoacoustic/TYAcousticFaceSet.h"
#include "Tympan/gui/gl/TYPickingTable.h"
#include "TYAcousticFaceSetGraphic.h"

TYAcousticFaceSetGraphic::TYAcousticFaceSetGraphic(TYAcousticFaceSet* pElement) : TYElementGraphic(pElement)
{
}

void TYAcousticFaceSetGraphic::update(bool force /*=false*/)
{
    TYFaceSet* pFaces = getElement()->getFaceSet();
    //  LPTYFaceSetGraphic* pFacesGraphic= pFaces->getGraphicObject();
    //  pFacesGraphic->update();
    pFaces->getGraphicObject()->update();

    TYElementGraphic::update(force);
}

void TYAcousticFaceSetGraphic::display(TYElement* pModelerElement /*= nullptr*/, GLenum mode /*= GL_RENDER*/)
{
    TYElementGraphic::display(pModelerElement, mode);

    glColor4fv(getElement()->getColor());

    if (mode == GL_SELECT)
    {
        TYPickingTable::addElement(getElement());
        glPushName((GLuint)(TYPickingTable::getIndex()));
    }

    TYFaceSet* pFaces = getElement()->getFaceSet();
    pFaces->getGraphicObject()->display(pModelerElement, mode);

    if (mode == GL_SELECT)
    {
        glPopName();
    }
}

void TYAcousticFaceSetGraphic::computeBoundingBox()
{
    OBox reset;
    _boundingBox = reset;

    TYTabPoint sommets = getElement()->sommets();

    size_t nbPts = sommets.size();
    TYPoint pt;
    for (size_t i = 0; i < nbPts; i++)
    {
        pt = sommets[i];
        _boundingBox.Enlarge((float)(pt._x), (float)(pt._y), (float)(pt._z));
    }
}
