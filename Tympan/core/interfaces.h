/*
 * Copyright (C) <2012-2024> <EDF-DTG> <FRANCE>
 * This file is part of Code_TYMPAN (R).
 * Code_TYMPAN (R) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * Code_TYMPAN (R) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with Code_TYMPAN (R). If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TY_INTERFACES
#define TY_INTERFACES

#include <boost/shared_ptr.hpp>

namespace tympan
{
class AcousticProblemModel;
class AcousticResultModel;
class SolverConfiguration;
typedef boost::shared_ptr<SolverConfiguration> LPSolverConfiguration;
} // namespace tympan

/**
 * \brief Interface class for solvers
 */
class SolverInterface
{
public:
    SolverInterface() {}
    virtual ~SolverInterface() {}

    virtual bool solve(const tympan::AcousticProblemModel& aproblem, tympan::AcousticResultModel& aresult,
                       tympan::LPSolverConfiguration configuration) = 0;

    virtual void purge() {}
};

/**
 * \brief Interface class for acoustic models
 */
class AcousticModelInterface
{
public:
    AcousticModelInterface() {}
    virtual ~AcousticModelInterface() {}
};

#endif // TY_INTERFACES
